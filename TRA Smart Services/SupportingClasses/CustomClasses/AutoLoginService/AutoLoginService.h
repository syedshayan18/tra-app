//
//  AutoLoginService.h
//  TRA Smart Services
//
//  Created by Admin on 22.07.15.
//

@interface AutoLoginService : NSObject

@property (strong, nonatomic) void (^autoLoginServiceResalt)();
@property (strong, nonatomic) void (^autoLoginServicePause)();

- (void)performAutoLoginIfPossible;
- (void)performAutoLoginWithPassword:(NSString *)userPassword;

@end
