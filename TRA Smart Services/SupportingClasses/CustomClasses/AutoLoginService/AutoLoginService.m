//
//  AutoLoginService.m
//  TRA Smart Services
//
//  Created by Admin on 22.07.15.
//

#import "AutoLoginService.h"
#import "KeychainStorage.h"

@interface AutoLoginService()

@property (strong, nonatomic) KeychainStorage *keychain;

@end

@implementation AutoLoginService

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        _keychain = [[KeychainStorage alloc] init];
    }
    return self;
}


#pragma mark - Public

- (void)performAutoLoginIfPossible
{
    if ([self isAutoLoginPossible]) {
        NSDictionary *userCredentials = [self.keychain credentialsForLoginedUser];
        if (!userCredentials) {
            return;
        }

        NSInteger questionType = 0;
        NSString *questionAnswer = @"";
        
        Question *question = [userCredentials valueForKey:KeychainStorageKeyQuestion];
        if (question) {
            questionType = question.secretQuestionValue;
            questionAnswer = question.secretQuestionAnswer;
        }
        
        [[NetworkManager sharedManager] traSSLoginUsername:[userCredentials valueForKey:KeychainStorageKeyLogin] password:[userCredentials valueForKey:KeychainStorageKeyPassword] accountType:[[userCredentials valueForKey:KeychainStorageKeyAccountType] integerValue] secretQuesionType:questionType secretQuestionAnswer:questionAnswer requestResult:^(id response, NSError *error) {
            if (self.autoLoginServiceResalt) {
                self.autoLoginServiceResalt();
            }
        }];
    }
}

- (void)performAutoLoginWithPassword:(NSString *)userPassword
{
    if ([self isAutoLoginPossible]) {
        NSString *userName = [KeychainStorage userName];
        
        NSDictionary *userCredentials = [self.keychain credentialsForLoginedUser];

        if (!userCredentials) {
            return;
        }
        
        NSInteger accountType = [[userCredentials valueForKey:KeychainStorageKeyAccountType] integerValue];

        NSInteger questionType = 0;
        NSString *questionAnswer = @"";
        
        Question *question = [userCredentials valueForKey:KeychainStorageKeyQuestion];
        if (question) {
            questionType = question.secretQuestionValue;
            questionAnswer = question.secretQuestionAnswer;
        }

        [[NetworkManager sharedManager] traSSLoginUsername:userName password:userPassword accountType:accountType secretQuesionType:questionType secretQuestionAnswer:questionAnswer requestResult:^(id response, NSError *error) {
            if (error) {
                if (self.autoLoginServicePause) {
                    self.autoLoginServicePause();
                }
                [AppHelper alertViewWithMessage:[response isKindOfClass:[NSString class]] ? response : error.localizedDescription delegate:self];
            } else if (self.autoLoginServiceResalt) {
                self.autoLoginServiceResalt();
            }
        }];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.autoLoginServiceResalt) {
        self.autoLoginServiceResalt();
    }
}

#pragma mark - Private

- (BOOL)isAutoLoginPossible
{
    BOOL possible = NO;
    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:UserNameKey];
    if (userName.length) {
        possible = YES;
    }
    return possible;
}

@end
