//
//  UITableView+BottomRefreshControl.h
//  Showroom
//
//  Created by  on 14.01.14.
//  Copyright (c) 2014 . All rights reserved.
//

@interface UIScrollView (BottomRefreshControl)

@property (nonatomic) UIRefreshControl *bottomRefreshControl;

@end


@interface UIRefreshControl (BottomRefreshControl)

@property (nonatomic) CGFloat triggerVerticalOffset;

@end
