//
//  RequredIndicatorTextView.m
//  TRA Smart Services
//
//  Created by Admin on 26.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "RequredIndicatorTextView.h"

@interface RequredIndicatorTextView()

@property (strong, nonatomic) UILabel *requredIndicatorLabel;

@end

@implementation RequredIndicatorTextView

#pragma mark - LifeCycle

//- (instancetype)initWithCoder:(NSCoder *)aDecoder
//{
//    self = [super initWithCoder:aDecoder];
//    if (self) {
//        _requredIndicatorColor = [UIColor redColor];
//    }
//    return self;
//}
//
//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        _requredIndicatorColor = [UIColor redColor];
//    }
//    return self;
//}

- (void)didMoveToSuperview
{
    [super didMoveToSuperview];
    
    [self addrequredIndicatolLabel];
}

#pragma mark - Accessor

- (void)setRequredIndicatorEnable:(BOOL)requredIndicatorEnable
{
    _requredIndicatorEnable = requredIndicatorEnable;
    
    self.requredIndicatorLabel.hidden = !requredIndicatorEnable;
}

- (void)setRequredIndicatorColor:(UIColor *)requredIndicatorColor
{
    _requredIndicatorColor = requredIndicatorColor;
    
    self.requredIndicatorLabel.textColor = requredIndicatorColor;
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment
{
    [super setTextAlignment:textAlignment];
    
    self.requredIndicatorLabel.textAlignment = textAlignment == NSTextAlignmentRight ? NSTextAlignmentLeft : NSTextAlignmentRight;
}

#pragma mark - Private

- (void)addrequredIndicatolLabel
{
    self.requredIndicatorLabel = [UILabel new];
    self.requredIndicatorLabel.text = @"*";
    self.requredIndicatorLabel.textAlignment = self.textAlignment == NSTextAlignmentRight ? NSTextAlignmentLeft : NSTextAlignmentRight;
    self.requredIndicatorLabel.textColor = self.requredIndicatorColor;
    self.requredIndicatorLabel.hidden = !self.requredIndicatorEnable;
    self.requredIndicatorLabel.backgroundColor = [UIColor clearColor];
    
    self.requredIndicatorLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.superview insertSubview:self.requredIndicatorLabel belowSubview:self];
    [self addConstraintsRequredIndicatorLabelForView];
}

- (void)addConstraintsRequredIndicatorLabelForView
{
    [self.requredIndicatorLabel addConstraint:[NSLayoutConstraint constraintWithItem:self.requredIndicatorLabel attribute:NSLayoutAttributeHeight
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                                                          multiplier:1.0 constant:21.0]
                                                ];
    [self.superview addConstraints:@[[NSLayoutConstraint constraintWithItem:self.requredIndicatorLabel attribute:NSLayoutAttributeTop
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self attribute:NSLayoutAttributeTop
                                                                 multiplier:1.0 constant:0],
                                     [NSLayoutConstraint constraintWithItem:self.requredIndicatorLabel attribute:NSLayoutAttributeLeft
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self attribute:NSLayoutAttributeLeft
                                                                 multiplier:1.0 constant:0],
                                     [NSLayoutConstraint constraintWithItem:self.requredIndicatorLabel attribute:NSLayoutAttributeRight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self attribute:NSLayoutAttributeRight
                                                                 multiplier:1.0 constant:0]
                                     ]];
}

@end
