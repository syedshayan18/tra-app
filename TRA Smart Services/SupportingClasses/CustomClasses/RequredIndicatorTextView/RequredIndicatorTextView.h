//
//  RequredIndicatorTextView.h
//  TRA Smart Services
//
//  Created by Admin on 26.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "ContextMenuTextView.h"

@interface RequredIndicatorTextView : ContextMenuTextView

@property (assign, nonatomic) BOOL requredIndicatorEnable;
@property (strong, nonatomic) UIColor *requredIndicatorColor;

@end
