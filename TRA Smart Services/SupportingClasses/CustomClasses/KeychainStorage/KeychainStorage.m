//
//  KeychainStorage.m
//  TRA Smart Services
//
//  Created by Admin on 21.07.15.
//

#import "KeychainStorage.h"
#import <Security/Security.h>

#import "SSKeychain.h"

@implementation KeychainStorage

#pragma mark - Public

- (void)storePassword:(NSString *)password forUser:(NSString *)userName
{
    if (!password.length || !userName.length) {
        return;
    }
    [self saveUserLogin:userName andPassword:password];
    [self storeUserName:userName];
}

- (NSDictionary *)credentialsForLoginedUser
{
    NSMutableDictionary *credentials = [[NSMutableDictionary alloc] init];
    NSString *userLogin = [self nameForLoginedUser];
    if (userLogin.length) {
        [credentials setObject:userLogin forKey:KeychainStorageKeyLogin];
        if ([self getPasswordForUserName:userLogin].length) {
            [credentials setObject:[self getPasswordForUserName:userLogin] forKey:KeychainStorageKeyPassword];
        } else {
            return nil;
        }
    }
    
    if ([[[NSUserDefaults standardUserDefaults] dictionaryRepresentation].allKeys containsObject:SecretQuestion]) {
        Question *storedQuestion = [self loadCustomObjectWithKey:SecretQuestion];
        if (storedQuestion) {
            [credentials setObject:storedQuestion forKey:KeychainStorageKeyQuestion];
        }
    }
    
    if ([[[NSUserDefaults standardUserDefaults] dictionaryRepresentation].allKeys containsObject:KeychainStorageKeyAccountType]) {
        Question *storedQuestion = [self loadCustomObjectWithKey:KeychainStorageKeyAccountType];
        if (storedQuestion) {
            [credentials setObject:storedQuestion forKey:KeychainStorageKeyAccountType];
        }
    }
    
    return credentials;
}

- (void)removeStoredCredentials
{
    [self deleteStoredCredentials];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UserNameKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)userName
{
    NSString *name = @"";
    if ([[NSUserDefaults standardUserDefaults] objectForKey:UserNameKey]) {
        name = [[NSUserDefaults standardUserDefaults] objectForKey:UserNameKey];
    }
    return name;
}

#pragma mark - Private

#pragma mark - UserDefaults

- (void)storeUserName:(NSString *)userName
{
    [[NSUserDefaults standardUserDefaults] setValue:userName forKey:UserNameKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)nameForLoginedUser
{
    NSString *name = [[NSUserDefaults standardUserDefaults] valueForKey:UserNameKey];
    return name;
}

- (void)saveCustomObject:(id)object key:(NSString *)key
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
}

- (id)loadCustomObjectWithKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSObject *object = nil;
    if ([[defaults dictionaryRepresentation].allKeys containsObject:key]) {
        NSData *encodedObject = [defaults objectForKey:key];
        object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    }
    return object;
}

#pragma mark - Keychain

- (void)saveUserLogin:(NSString *)login andPassword:(NSString *)password
{
    NSParameterAssert(login);
    NSParameterAssert(password);
    
    [self deleteStoredCredentials];
    
    BOOL success = [SSKeychain setPassword:password forService:@"TRA" account:login];
}

- (NSString *)getPasswordForUserName:(NSString *)userName
{
    return [SSKeychain passwordForService:@"TRA" account:userName];
}

- (void)deleteStoredCredentials
{
    
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    NSString *userLogin = [self nameForLoginedUser];

    if (!userLogin.length) {
        return;
    }
    [SSKeychain deletePasswordForService:@"TRA" account:userLogin];
}

@end
