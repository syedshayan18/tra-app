//
//  ContextMenuTextView.m
//  TRA Smart Services
//
//  Created by Admin on 25.11.15.
//

#import "ContextMenuTextView.h"

@implementation ContextMenuTextView

#pragma mark - LifeCycle

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self prepareMenuController];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self prepareMenuController];
    }
    return self;
}

#pragma mark - Override

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    BOOL returnValue = NO;
    if (action == @selector(customCut:)) {
        returnValue = [super canPerformAction:@selector(cut:) withSender:sender];
    } else if (action == @selector(customCopy:)) {
        returnValue = [super canPerformAction:@selector(copy:) withSender:sender];
    } else if (action == @selector(customPaste:)) {
        returnValue = [super canPerformAction:@selector(paste:) withSender:sender];
    } else if (action == @selector(customSelect:)) {
        returnValue = [super canPerformAction:@selector(select:) withSender:sender];
    } else if (action == @selector(customSelectAll:)) {
        returnValue = [super canPerformAction:@selector(selectAll:) withSender:sender];
    }
    return returnValue;
}

#pragma mark - Accessor

- (void)setTextAlignment:(NSTextAlignment)textAlignment
{
    [super setTextAlignment:textAlignment];
    
    [self prepareMenuController];
}

#pragma mark - Private

- (void)prepareMenuController
{
    UIMenuItem *customCopy = [[UIMenuItem alloc] initWithTitle:dynamicLocalizedString(@"contextMenu.Copy") action:@selector(customCopy:)];
    UIMenuItem *customPaste = [[UIMenuItem alloc] initWithTitle:dynamicLocalizedString(@"contextMenu.Paste") action:@selector(customPaste:)];
    UIMenuItem *customCut = [[UIMenuItem alloc] initWithTitle:dynamicLocalizedString(@"contextMenu.Cut") action:@selector(customCut:)];
    UIMenuItem *customSelect = [[UIMenuItem alloc] initWithTitle:dynamicLocalizedString(@"contextMenu.Select") action:@selector(customSelect:)];
    UIMenuItem *customSelectAll = [[UIMenuItem alloc] initWithTitle:dynamicLocalizedString(@"contextMenu.SelectAll") action:@selector(customSelectAll:)];
    
    NSArray *itemsMenu = @[customSelect, customSelectAll, customCut, customCopy, customPaste];
    [[UIMenuController sharedMenuController] setMenuItems:itemsMenu];
}

- (void)customCopy:(id)sender
{
    [self copy:sender];
}

- (void)customPaste:(id)sender
{
    [self paste:sender];
}

- (void)customCut:(id)sender
{
    [self cut:sender];
}

- (void)customSelect:(id)sender
{
    [self select:sender];
}

- (void)customSelectAll:(id)sender
{
    [self selectAll:sender];
}

@end
