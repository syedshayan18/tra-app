//
//  CarrierInfo.h
//  TRA Smart Services
//
//  Created by Misha2g on 16.06.16.
//  Copyright © 2016 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarrierInfo : NSObject

@property (strong, nonatomic) NSString *carrierName;
@property (strong, nonatomic) NSString *radioTechnologyName;
@property (assign, nonatomic) NSInteger signalStrength;
@property (assign, nonatomic) NSInteger barsSignalStrength;

@property (copy) void (^operatorInformationDidUpdated)(void);

- (void)startUpdating;
- (void)stopUpdating;

+ (instancetype)carrierInfoWithUpdateInfoBlock:(void (^) (void))infoUpdated;

@end
