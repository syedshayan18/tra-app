//
//  CarrierInfo.m
//  TRA Smart Services
//
//  Created by Misha2g on 16.06.16.
//  Copyright © 2016 . All rights reserved.
//

#import "CarrierInfo.h"
@import CoreTelephony;


@protocol BarSignalStrengthItem <NSObject>


@end



@interface CarrierInfo ()

@property (strong, nonatomic) CTTelephonyNetworkInfo *networkInfo;
@property (strong, nonatomic) NSTimer *timer;

@property (weak, nonatomic) UIView *signalStrengthItemView;

@property (assign, nonatomic) int signalStrengthRaw;
@property (assign, nonatomic) int signalStrengthBars;

@end

@implementation CarrierInfo


+ (instancetype)carrierInfoWithUpdateInfoBlock:(void (^) (void))infoUpdated {
    CarrierInfo *info = [[self alloc] init];
    info.operatorInformationDidUpdated = infoUpdated;
    return info;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _networkInfo = [[CTTelephonyNetworkInfo alloc] init];
        _timer = [NSTimer timerWithTimeInterval:0.1 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
        self.carrierName = self.networkInfo.subscriberCellularProvider.carrierName;
        self.radioTechnologyName = [self radioTechnologyHRName:self.networkInfo.currentRadioAccessTechnology];
        self.signalStrength = self.signalStrengthRaw;
        self.barsSignalStrength = self.signalStrengthBars;
    }
    return self;
}


- (int)signalStrengthRaw {
    return [[self.signalStrengthItemView valueForKey:@"signalStrengthRaw"] intValue];
}

- (int)signalStrengthBars {
    return [[self.signalStrengthItemView valueForKey:@"signalStrengthBars"] intValue];
}


- (void)timerTick:(NSTimer *)timer {
    int i = 0;
    i += ![self.carrierName isEqualToString:self.networkInfo.subscriberCellularProvider.carrierName];
    i += ![self.radioTechnologyName isEqualToString:[self radioTechnologyHRName:self.networkInfo.currentRadioAccessTechnology]];
    i += !(self.signalStrength == self.signalStrengthRaw);
    if (i && self.operatorInformationDidUpdated) {
        self.operatorInformationDidUpdated();
    }
    
    self.carrierName = self.networkInfo.subscriberCellularProvider.carrierName;
    self.radioTechnologyName = [self radioTechnologyHRName:self.networkInfo.currentRadioAccessTechnology];
    self.signalStrength = self.signalStrengthRaw;
    self.barsSignalStrength = self.signalStrengthBars;
}



- (NSString *)radioTechnologyHRName:(NSString *)enumValue {
    NSDictionary *d = @{@"CTRadioAccessTechnologyGPRS" : @"GPRS",
                        @"CTRadioAccessTechnologyEdge" : @"Edge",
                        @"CTRadioAccessTechnologyWCDMA" : @"CDMA",
                        @"CTRadioAccessTechnologyHSDPA" : @"3G",
                        @"CTRadioAccessTechnologyHSUPA" : @"3G",
                        @"CTRadioAccessTechnologyCDMA1x" : @"CDMA1x",
                        @"CTRadioAccessTechnologyCDMAEVDORev0" : @"CDMAEVDORev0",
                        @"CTRadioAccessTechnologyCDMAEVDORevA" : @"CDMAEVDORevA",
                        @"CTRadioAccessTechnologyCDMAEVDORevB" : @"CDMAEVDORevB",
                        @"CTRadioAccessTechnologyeHRPD" : @"3G",
                        @"CTRadioAccessTechnologyLTE" : @"4G"};
    return d[enumValue];
}


- (UIView *)signalStrengthItemView {
    if (_signalStrengthItemView) {
        return _signalStrengthItemView;
    }
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *subviews = [[[app valueForKey:@"statusBar"] valueForKey:@"foregroundView"] subviews];
    UIView *dataNetworkItemView = nil;
    for (id subview in subviews) {
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarSignalStrengthItemView") class]]) {
            dataNetworkItemView = subview;
            break;
        }
    }
    _signalStrengthItemView = dataNetworkItemView;
    return _signalStrengthItemView;
}

- (void)startUpdating {
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    
}

- (void)stopUpdating {
    [self.timer invalidate];
}

@end
