//
//  BottomBorderTextView.m
//  TRA Smart Services
//
//  Created by Admin on 9/25/15.
//

#import "BottomBorderTextView.h"

@interface BottomBorderTextView ()

@property (strong, nonatomic) UIView *bottomBorder;

@end

@implementation BottomBorderTextView

#pragma mark - LifeCycle

- (void)didMoveToSuperview
{
    [super didMoveToSuperview];
    
    [self addBottomBorder];
}

#pragma mark - Accessor

- (void)setBottomBorderColor:(UIColor *)bottomBorderColor
{
    _bottomBorderColor = bottomBorderColor;
    _bottomBorder.backgroundColor = bottomBorderColor;
}

#pragma mark - Private

- (void)addBottomBorder
{
    if (!_bottomBorder) {
        _bottomBorder = [[UIView alloc] init];
    }
    _bottomBorder.translatesAutoresizingMaskIntoConstraints = NO;
    _bottomBorder.backgroundColor = [UIColor clearColor];
    [self.superview addSubview:_bottomBorder];
    
    [self addConstraintsForView];
}

- (void)addConstraintsForView
{
    [_bottomBorder addConstraint:[NSLayoutConstraint constraintWithItem:_bottomBorder attribute:NSLayoutAttributeHeight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                                               multiplier:1.0 constant:1.0]
                                   ];
    [self.superview addConstraints:@[[NSLayoutConstraint constraintWithItem:_bottomBorder attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0 constant:0],
                           [NSLayoutConstraint constraintWithItem:_bottomBorder attribute:NSLayoutAttributeLeft
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self attribute:NSLayoutAttributeLeft
                                                       multiplier:1.0 constant:0],
                           [NSLayoutConstraint constraintWithItem:_bottomBorder attribute:NSLayoutAttributeRight
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self attribute:NSLayoutAttributeRight
                                                       multiplier:1.0 constant:0]
                           ]];
}

@end
