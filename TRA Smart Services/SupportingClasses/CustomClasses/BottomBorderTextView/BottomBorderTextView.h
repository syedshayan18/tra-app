//
//  BottomBorderTextView.h
//  TRA Smart Services
//
//  Created by Admin on 9/25/15.
//

#import "RequredIndicatorTextView.h"

@interface BottomBorderTextView : RequredIndicatorTextView

@property (strong, nonatomic) UIColor *bottomBorderColor;

@end
