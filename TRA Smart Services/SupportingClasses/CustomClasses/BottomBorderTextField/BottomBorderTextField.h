//
//  BottomBorderTextField.h
//  TRA Smart Services
//
//  Created by Admin on 9/24/15.
//

#import "RequredIndicatorTextField.h"

@interface BottomBorderTextField : RequredIndicatorTextField

@property (strong, nonatomic) UIColor *bottomBorderColor;

@end
