//
//  RequredIndicatorTextField.h
//  TRA Smart Services
//
//  Created by Admin on 25.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "ContextMenuTextField.h"

@interface RequredIndicatorTextField : ContextMenuTextField

@property (assign, nonatomic) BOOL requredIndicatorEnable;
@property (strong, nonatomic) UIColor *requredIndicatorColor;

@end
