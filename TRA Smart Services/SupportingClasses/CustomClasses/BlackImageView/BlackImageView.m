//
//  BlackImageView.m
//  TRA Smart Services
//
//  Created by Admin on 22.10.15.
//

#import "BlackImageView.h"

@implementation BlackImageView

#pragma mark - CustomAccessors

- (void)setImage:(UIImage *)image
{
    if ([DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite ) {
        image = [[BlackWhiteConverter sharedManager] convertedBlackAndWhiteImage:image];
    }
    [super setImage:image];
}

@end
