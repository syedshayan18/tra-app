//
//  ReverseSecure.h
//  TRA Smart Services
//
//  Created by Admin on 07.10.15.
//

#import <Foundation/Foundation.h>

@interface ReverseSecure : NSObject

+ (BOOL)isJailBroken;

@end
