//
//  CustomCollectionViewFlowLayout.m
//  CollectionViewLayout
//
//  Created by Roman Vizenko on 03.02.16.
//  Copyright © 2016 Roman Vizenko. All rights reserved.
//

#import "CircleCellCollectionViewFlowLayout.h"

@implementation CircleCellCollectionViewFlowLayout

- (void)prepareLayout
{
    [super prepareLayout];
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray* attributesToReturn = [super layoutAttributesForElementsInRect:rect];
    
    CGFloat contentOffsetY = self.collectionView.contentOffset.y;
    CGRect collectionFrame = self.collectionView.frame;
    CGPoint center = CGPointMake(collectionFrame.size.width / 2. + 78., collectionFrame.size.height / 2. + contentOffsetY);
    CGFloat radius = collectionFrame.size.width / 2.;
    CGFloat coefficient = collectionFrame.size.height / (M_PI * radius + (collectionFrame.size.width - center.x));
    
    for (UICollectionViewLayoutAttributes *attributes in attributesToReturn) {
        NSInteger indexPathRow = attributes.indexPath.row;

        CGFloat heightCellBottomOffset = (attributes.frame.size.height ) * (CGFloat)indexPathRow;
        
        if (attributesToReturn.count < 4) {
            CGFloat alpha = -3.;
            if (attributesToReturn.count == 3) {
                alpha = (contentOffsetY - heightCellBottomOffset) / (radius * coefficient) - M_PI_2;
                alpha = (attributesToReturn.count == 2) ? alpha - M_PI / 3 : alpha - M_PI * 0.2;
            } else if (attributesToReturn.count == 2) {
                alpha = indexPathRow ? -2.5 : -4.1;
            }
            CGFloat x = floorf(radius * cosf(alpha) + center.x);
            CGFloat y = floorf(radius * cosf(M_PI_2 - alpha) + center.y);
            attributes.center = CGPointMake(x, y);        

        } else if (heightCellBottomOffset <= contentOffsetY) {
            CGFloat x = center.x + (contentOffsetY - heightCellBottomOffset) / coefficient;
            CGFloat y = center.y - radius;
            attributes.center = CGPointMake(x, y);
        } else if (heightCellBottomOffset >= contentOffsetY + M_PI * radius * coefficient) {
            CGFloat x = center.x - M_PI * radius - (contentOffsetY - heightCellBottomOffset) / coefficient;
            CGFloat y = center.y + radius;
            attributes.center = CGPointMake(x, y);
        } else {
            CGFloat alpha = attributesToReturn.count ? (contentOffsetY - heightCellBottomOffset) / (radius * coefficient) - M_PI_2 : 0 ;
            CGFloat x = floorf(radius * cosf(alpha) + center.x);
            CGFloat y = floorf(radius * cosf(M_PI_2 - alpha) + center.y);
            attributes.center = CGPointMake(x, y);
        }
        CGFloat offset = self.collectionView.frame.size.width - attributes.frame.origin.x - attributes.frame.size.width;
        if (offset < 0) {
            CGFloat  alphaValue = exp2f( - offset * offset / 4000);
            if (alphaValue < 0.01) {
                alphaValue = 0.f;
            }
            attributes.transform3D = CATransform3DMakeScale(alphaValue, alphaValue, alphaValue);
        } else {
            attributes.transform3D = CATransform3DIdentity;
        }
    }
    return attributesToReturn;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes* currentItemAttributes = [super layoutAttributesForItemAtIndexPath:indexPath];
    return currentItemAttributes;
}

@end
