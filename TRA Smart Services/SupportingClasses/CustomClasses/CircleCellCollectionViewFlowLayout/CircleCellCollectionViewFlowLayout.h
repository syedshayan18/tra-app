//
//  CustomCollectionViewFlowLayout.h
//  CollectionViewLayout
//
//  Created by Roman Vizenko on 03.02.16.
//  Copyright © 2016 Roman Vizenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleCellCollectionViewFlowLayout : UICollectionViewFlowLayout

@end
