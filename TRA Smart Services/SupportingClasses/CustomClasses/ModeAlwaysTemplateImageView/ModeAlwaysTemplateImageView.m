//
//  ModeAlwaysTemplateImageView.m
//  TRA Smart Services
//
//  Created by Admin on 28.04.16.
//  Copyright © 2016 . All rights reserved.
//

#import "ModeAlwaysTemplateImageView.h"

@implementation ModeAlwaysTemplateImageView

#pragma mark - Accessor

- (void)setImage:(UIImage *)image
{
    UIImage *modeAlwaysTemplateImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    [super setImage:modeAlwaysTemplateImage];
}

@end
