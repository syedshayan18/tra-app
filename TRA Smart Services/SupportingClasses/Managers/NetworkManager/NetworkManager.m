//
//  BaseNetworkManager.m
//  TRA Smart Services
//
//  Created by Admin on 21.07.15.
//

#import "NetworkManager.h"
#import <CoreTelephony/CoreTelephonyDefines.h>
#import "Announcement.h"
#import "DynamicServiceModel.h"
#import "ContactItemModel.h"
#import "TransactionModel.h"

static NSString *const ResponseDictionaryErrorKey = @"error";
static NSString *const ResponseDictionarySuccessKey = @"success";

@interface NetworkManager()

@end

@implementation NetworkManager

#pragma mark - Public

+ (instancetype)sharedManager
{
    static NetworkManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[NetworkManager alloc] initWithBaseURL:[NSURL URLWithString:traSSNoCRMServiceBaseURL]];
        
    });
    return sharedManager;
}

#pragma mark - Public

- (void)cancelAllOperations
{
    [self.manager.operationQueue cancelAllOperations];
}

- (BOOL)hasActiveRequest
{
    return self.manager.operationQueue.operationCount;
}

#pragma mark - NoCRMServices

- (void)traSSNoCRMServiceGetDomainData:(NSString *)domainURL requestResult:(ResponseBlock)domainOwnerResponse
{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", traSSNOCRMServiceGETDomainData, domainURL];
    [self performGET:requestURL withParameters:nil response:domainOwnerResponse];
}

- (void)traSSNoCRMServiceGetDomainAvaliability:(NSString *)domainURL requestResult:(ResponseBlock)domainAvaliabilityResponse
{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", traSSNOCRMServiceGETDomainAvaliability, domainURL];
    [self performGET:requestURL withParameters:nil response:domainAvaliabilityResponse];
}

- (void)traSSNoCRMServicePerformSearchByIMEI:(NSString *)mobileIMEI requestResult:(ResponseBlock)mobileIMEISearchResponse
{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", traSSNOCRMServiceGETSearchMobileIMEI, mobileIMEI];
    [self performGET:requestURL withParameters:nil response:mobileIMEISearchResponse];
}

- (void)traSSNoCRMServicePerformSearchByMobileBrand:(NSString *)mobileBrand requestResult:(ResponseBlock)mobileBrandSearchResponse
{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", traSSNOCRMServiceGETSearchMobileBrand, mobileBrand];
    [self performGET:requestURL withParameters:nil response:mobileBrandSearchResponse];
}

- (void)traSSNoCRMServicePOSTFeedback:(NSString *)feedback forSerivce:(NSString *)serviceName withRating:(NSUInteger)rating requestResult:(ResponseBlock)serviceFeedbackResponse
{
    NSDictionary *parameters = @{
                                 @"serviceName": serviceName,
                                 @"rate": @(rating),
                                 @"feedback": feedback.length ? feedback : @""
                                 };
    [self performPOST:traSSNOCRMServicePOSTFeedBack withParameters:parameters response:serviceFeedbackResponse];
}

- (void)traSSNoCRMServicePOSTSMSSpamReport:(NSString *)spammerPhoneNumber notes:(NSString *)note requestResult:(ResponseBlock)SMSSpamReportResponse
{
    NSDictionary *parameters = @{
                                 @"phone": spammerPhoneNumber,
                                 @"description": note
                                 };
    [self performPOST:traSSNOCRMServicePOSTSMSSPamReport withParameters:parameters response:SMSSpamReportResponse];
}

- (void)traSSNoCRMServicePOSTSMSBlock:(NSString *)spammerPhoneNumber phoneProvider:(NSString *)provider providerType:(NSString *)providerType notes:(NSString *)note requestResult:(ResponseBlock)SMSBlockResponse
{
    NSDictionary *parameters = @{
                                 @"phone": spammerPhoneNumber,
                                 @"phoneProvider": provider,
                                 @"providerType": providerType,
                                 @"description": note
                                 };
    [self performPOST:traSSNOCRMServicePOSTSMSBlock withParameters:parameters response:SMSBlockResponse];
}

- (void)traSSNoCRMServicePOSTHelpSalim:(NSString *)urlAddress notes:(NSString *)comment requestResult:(ResponseBlock)helpSalimReportResponse
{
    NSDictionary *parameters = @{
                                 @"url" : urlAddress,
                                 @"description" : comment
                                 };
    [self performPOST:traSSNOCRMServicePOSTHelpSalim withParameters:parameters response:helpSalimReportResponse];
}

- (void)traSSNoCRMServicePOSTComplianService:(ComplianType)complaintType parameters:(NSDictionary *)parameters requestResult:(ResponseBlock)responseBlok
{
    NSString *path;
    switch (complaintType) {
        case ComplianTypeCustomProvider:{
            path = traSSNOCRMServicePOSTCompliantServiceProvider;
            break;
        }
        case ComplianTypeEnquires: {
            path = traSSNOCRMServicePOSTCompliantAboutEnquires;
            break;
        }
        case ComplianTypeTRAService: {
            path = traSSNOCRMServicePOSTCompliantAboutTRAService;
            break;
        }
    }
    [self performPOST:path withParameters:parameters response:responseBlok];
}

- (void)traSSNoCRMServicePOSTSendSuggestion:(NSDictionary *)suggestionModelDisctinary requestResult:(ResponseBlock)suggestionResponse
{
    NSMutableDictionary *dictionaryMutable = [suggestionModelDisctinary mutableCopy];
    [dictionaryMutable removeObjectForKey:@"hasAttachment"];
    [self performPOST:traSSNOCRMServicePOSTSendSuggestin withParameters:dictionaryMutable response:suggestionResponse];
}

- (void)traSSNoCRMServicePOSTPoorCoverageAtLatitude:(CGFloat)latitude longtitude:(CGFloat)longtitude address:(NSString *)address signalPower:(NSUInteger)signalLevel requestResult:(ResponseBlock)poorCoverageResponse
{
    NSMutableDictionary *parameters = [ @{
                                          @"signalLevel" : @(signalLevel)
                                          } mutableCopy];
    
    if (latitude && longtitude) {
        [parameters setValue: @{
                                @"latitude" : @(latitude),
                                @"longitude" : @(longtitude)
                                }
                      forKey:@"location"];
    }
    
    if (address.length) {
        [parameters setValue:address forKey:@"address"];
    }
    [self performPOST:traSSNOCRMServicePOSTPoorCoverage withParameters:parameters response:poorCoverageResponse];
}

- (void)traSSNoCRMServicePostInnovationTitle:(NSString *)title message:(NSString *)message type:(NSNumber *)type responseBlock:(ResponseBlock)innovationResponse{
    NSDictionary *parameters = @{
                                 @"title" : title,
                                 @"message" : message,
                                 @"type" : type
                                 };
    [self performPOST:traSSNOCRMServicePostInnovation withParameters:parameters response:innovationResponse];
}

- (void)traSSNoCRMServiceGetAnnoucement:(NSInteger)limit offset:(NSInteger)offset responseBlock:(ResponseBlock)announcementResponse
{
    NSString *lang = [DynamicUIService service].language == LanguageTypeArabic ? @"AR" : @"EN";
    NSString *requestURL = [NSString stringWithFormat:@"%@?offset=%i&limit=%i&lang=%@", traSSNOCRMServiceGetAnnouncement, (int)offset, (int)limit, lang];
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        id value = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([value isKindOfClass:[NSDictionary class]]) {
            if ([[(NSDictionary *)value valueForKey:@"announcements"] isKindOfClass:[NSArray class]]) {
                NSArray *responseAnnouncement = [[NSArray alloc] initWithArray:[(NSDictionary *)value valueForKey:@"announcements"]];
                NSMutableArray *resultAnnouncement = [[NSMutableArray alloc] init];
                
                for (int i = 0; i < responseAnnouncement.count; i++) {
                    if ([responseAnnouncement[i] isKindOfClass:[NSDictionary class]]) {
                        Announcement *annoucement = [[Announcement alloc] initWithDictionary:responseAnnouncement[i]];
                        [resultAnnouncement addObject:annoucement];
                    }
                }
                announcementResponse(resultAnnouncement, nil);
            }
        } else {
            announcementResponse(nil, [self responseError]);
        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        PerformFailureRecognition(operation, error, announcementResponse);
    }];
}

- (void)traSSNoCRMServiceSearchAnnoucement:(NSInteger)limit offset:(NSInteger)offset searchText:(NSString *)searchText responseBlock:(SearchResponseBlock)getAnnoucementResponse
{
    NSString *lang = [DynamicUIService service].language == LanguageTypeArabic ? @"AR" : @"EN";
    NSString *requestURL = [NSString stringWithFormat:@"%@?offset=%i&limit=%i&lang=%@&search=%@", traSSNOCRMServiceGetAnnouncement, (int)offset, (int)limit, lang, searchText];
    NSString *stringCleanPath = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self.manager GET:stringCleanPath parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSString *request = [operation.request.URL absoluteString];
        id value = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([value isKindOfClass:[NSDictionary class]]) {
            if ([[(NSDictionary *)value valueForKey:@"announcements"] isKindOfClass:[NSArray class]]) {
                NSArray *responseAnnouncement = [[NSArray alloc] initWithArray:[(NSDictionary *)value valueForKey:@"announcements"]];
                NSMutableArray *resultAnnouncement = [[NSMutableArray alloc] init];
                
                for (int i = 0; i < responseAnnouncement.count; i++) {
                    if ([responseAnnouncement[i] isKindOfClass:[NSDictionary class]]) {
                        Announcement *annoucement = [[Announcement alloc] initWithDictionary:responseAnnouncement[i]];
                        [resultAnnouncement addObject:annoucement];
                    }
                }
                getAnnoucementResponse(resultAnnouncement, nil, [[request componentsSeparatedByString:@"="] lastObject]);
            }
        } else {
            getAnnoucementResponse(nil, [self responseError], [[request componentsSeparatedByString:@"="] lastObject]);
        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        NSString *responseString = dynamicLocalizedString(@"api.message.serverError");
        if (operation.responseObject) {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseObject options:kNilOptions error:&error];
            id responsedObject = [response valueForKey:ResponseDictionaryErrorKey];
            if ([responsedObject isKindOfClass:[NSArray class]]){
                responseString = [(NSArray *)responsedObject firstObject];
            } else if ([responsedObject isKindOfClass:[NSString class]]){
                responseString = responsedObject;
            }
        }
        getAnnoucementResponse(responseString, error, nil);
    }];
}

- (void)traSSNoCRMServiceGetContacts:(ResponseBlock)result
{
    NSString *lang = [DynamicUIService service].language == LanguageTypeArabic ? @"AR" : @"EN";
    NSString *requestURL = [NSString stringWithFormat:@"%@?lang=%@", traSSNOCRMServiceGetContacts, lang];
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        id value = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([value isKindOfClass:[NSArray class]]) {
            NSArray *response = [[NSArray alloc] initWithArray:(NSArray *)value];
            NSMutableArray *resultDynamicServiceModelArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < response.count; i++) {
                if ([response[i] isKindOfClass:[NSDictionary class]]) {
                    ContactItemModel *contactItemModel = [[ContactItemModel alloc] initWithDictionary:response[i]];
                    [resultDynamicServiceModelArray addObject:contactItemModel];
                }
            }
            result(resultDynamicServiceModelArray, nil);
            
        } else {
            result(nil, [self responseError]);
        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        PerformFailureRecognition(operation, error, result);
    }];
}

#pragma mark - CRMdynamicService

- (void)traSSCRMServiceGetServicesList:(ResponseBlock)dynamicServices
{
    NSString *requestURL = [NSString stringWithFormat:@"%@", traSSCRMServiceGetServiceList];
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        id value = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([value isKindOfClass:[NSArray class]]) {
            NSArray *response = [[NSArray alloc] initWithArray:(NSArray *)value];
            NSMutableArray *resultDynamicServiceModelArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < response.count; i++) {
                if ([response[i] isKindOfClass:[NSDictionary class]]) {
                    DynamicServiceModel *dynamicServiceModel = [[DynamicServiceModel alloc] initWithDictionary:response[i]];
                    [resultDynamicServiceModelArray addObject:dynamicServiceModel];
                }
            }
            dynamicServices(resultDynamicServiceModelArray, nil);
            
        } else {
            dynamicServices(nil, [self responseError]);
        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        PerformFailureRecognition(operation, error, dynamicServices);
    }];
}

- (void)traSSCRMServiceGetServiceDetails:(NSString *)serviceID responseBlock:(ResponseBlock)dynamicService
{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", traSSCRMServiceGetServiceDetails, serviceID];
    
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        id value = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([value isKindOfClass:[NSDictionary class]]) {
            DynamicServiceModel *dynamicServiceModel= [[DynamicServiceModel alloc] initWithDictionary:(NSDictionary *)value];
            dynamicService(dynamicServiceModel, nil);
        } else {
            dynamicService(nil, [self responseError]);
        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        PerformFailureRecognition(operation, error, dynamicService);
    }];
}

- (void)traSSCRMServiceGetTransactionByID:(NSString *)transactionID type:(NSInteger)serviceType responseBlock:(ResponseBlock)dynamicService
{
    NSString *patch = [NSString stringWithFormat:@"%@/%@?type=%d", traSSNOCRMServiceGetTransactions, transactionID, (int)serviceType];
    
    [self.manager GET:patch parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        id value = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([value isKindOfClass:[NSDictionary class]]) {
            DynamicServiceModel *dynamicServiceModel= [[DynamicServiceModel alloc] initWithDictionary:(NSDictionary *)value];
            dynamicService(dynamicServiceModel, nil);
        } else {
            dynamicService(nil, [self responseError]);
        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        PerformFailureRecognition(operation, error, dynamicService);
    }];
}

- (void)traSSCRMDynamicServicePOSTServiceReportWithServiceID:(NSString *)serviceID parameters:(NSDictionary *)parameters postResponse:(ResponseBlock)postResponseBlock
{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", traSSCRMServicePOSTService, serviceID];
    NSString *stringCleanPath = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self performPOST:stringCleanPath withParameters:parameters response:postResponseBlock];
}

- (void)traSSCRMDynamicServicePutEditTransactions:(NSString *)transactionID parametrs:(NSDictionary *)parametersDictionary responseBlock:(ResponseBlock)editTransactionResponse
{
    NSString *requestURL = [NSString stringWithFormat:@"%@/%@", traSSNOCRMServiceGetTransactions, transactionID];
    [self performPUT:requestURL withParameters:parametersDictionary response:editTransactionResponse];
}

- (void)traSSCRMDynamicServicePOSTImage:(UIImage *)image imagePOSTResult:(ResponseBlock)imagePostResponse
{
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *base64PhotoString = [imageData base64EncodedStringWithOptions:kNilOptions];
    base64PhotoString = [ImagePrefixBase64String stringByAppendingString:base64PhotoString];
    NSDictionary *param = @{
                            @"attachment" : base64PhotoString
                            };
    [self performPOST:traSSCRMServicePOSTServiceAttachment withParameters:param response:imagePostResponse];
}

- (void)traSSCRMDynamicServiceGetPDF:(ResponseBlock)dynamicService {
    [self performGET:traSSCRMServiceGetPDF withParameters:nil response:^(id response, NSError *error) {
        if (error == nil) {
            DynamicServiceModel *dynamicServiceModel = [[DynamicServiceModel alloc] initWithDictionary:(NSDictionary *)response];
            dynamicService(dynamicServiceModel, nil);
        } else {
            dynamicService(nil, [self responseError]);
        }
    }];
}

#pragma mark - Favourite

- (void)traSSNoCRMSErviceGetFavoritesServices:(ResponseBlock)favoriteServices
{
    [self performGET:traSSNOCRMServiceGETFavoritesServices withParameters:nil response:favoriteServices];
}

- (void)traSSNoCRMServicePostAddServicesToFavorites:(NSArray *)serviceNames responce:(ResponseBlock)result
{
    NSDictionary *parameters;
    if (serviceNames) {
        parameters = @{@"serviceNames" : serviceNames};
    } else {
        return;
    }
    [self performPOST:traSSNOCRMServicePOSTAddServicesToFavorites withParameters:parameters response:result];
}

- (void)traSSNoCRMServiceDeleteServicesFromFavorites:(NSArray *)serviceNames responce:(ResponseBlock)result
{
    NSDictionary *parameters;
    if (serviceNames) {
        parameters = @{@"serviceNames" : serviceNames};
    } else {
        return;
    }
    [self performDELETE:traSSNOCRMServiceDELETEServicesFromFavorites withParameters:parameters response:result];
}

- (void)traSSNoCRMServiceGetAllServicesEnable:(ResponseBlock)result
{
    [self performGET:traSSNOCRMServiceGETAllServicesEnable withParameters:nil response:result];
}

- (void)traSSNoCRMServiceGetServiceAboutInfo:(NSString *)serviceName languageCode:(NSString *)languageCode responseBlock:(ResponseBlock)aboutServiceInfoResponse
{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@&lang=%@", traSSNOCRMServiceGETAboutServiceInfo, serviceName, languageCode];
    [self performGET:requestURL withParameters:nil response:aboutServiceInfoResponse];
}

- (void)traSSNoCRMServiceGetDynamicServiceAboutInfo:(NSString *)serviceID responseBlock:(ResponseBlock)aboutServiceInfoResponse
{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", traSSNOCRMServiceGETAboutDynamicServiceInfo, serviceID];
    [self performGET:requestURL withParameters:nil response:aboutServiceInfoResponse];
}

- (void)traSSNoCRMServiceGetGetTransactions:(NSInteger)page count:(NSInteger)count orderAsc:(BOOL)orderArc responseBlock:(ResponseBlock)getTransactionResponse
{
    NSString *requestURL = [NSString stringWithFormat:@"%@", traSSNOCRMServiceGetTransactions];
    
    if (page && count) {
        NSString *pagesNumber = [NSString stringWithFormat:@"%i", (int)page];
        NSString *countElements = [NSString stringWithFormat:@"%i", (int)count];
        requestURL = [NSString stringWithFormat:@"%@?page=%@&cout=%@", traSSNOCRMServiceGetTransactions, pagesNumber, countElements]; //temp asc not work
    }
    
    [self.manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        id value = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([value isKindOfClass:[NSArray class]]) {
            NSMutableArray *responseTransactions = [[NSMutableArray alloc] init];
            
            for (NSDictionary *transActionDict in value) {
                if ([transActionDict isKindOfClass:[NSDictionary class]]) {
                    TransactionModel *transactionModel = [[TransactionModel alloc] initWithDictionary:transActionDict];
                    [responseTransactions addObject:transactionModel];
                }
            }
            getTransactionResponse(responseTransactions, nil);
        } else {
            getTransactionResponse(nil, [self responseError]);

        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        PerformFailureRecognition(operation, error, getTransactionResponse);
    }];
}

- (void)traSSNoCRMServiceSearchTransactions:(NSInteger)page count:(NSInteger)count orderAsc:(BOOL)orderArc searchText:(NSString *)searchText responseBlock:(SearchResponseBlock)getTransactionResponse
{
    NSString *pagesNumber = [NSString stringWithFormat:@"%i", (int)page];
    NSString *countElements = [NSString stringWithFormat:@"%i", (int)count];
    NSString *requestURL = [NSString stringWithFormat:@"%@%@&cout=%@&orderAsc=%@&search=%@", traSSNOCRMServiceSearchTransactions, pagesNumber, countElements, orderArc ? @"1" : @"0", searchText];
    NSString *stringCleanPath = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    [self.manager GET:stringCleanPath parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSString *request = [operation.request.URL absoluteString];
        
        id value = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([value isKindOfClass:[NSArray class]]) {
            NSMutableArray *responseTransactions = [[NSMutableArray alloc] init];
            for (NSDictionary *transActionDict in value) {
                TransactionModel *transactionModel = [[TransactionModel alloc] initWithDictionary:transActionDict];
                [responseTransactions addObject:transactionModel];
            }
            getTransactionResponse(responseTransactions, nil, [[request componentsSeparatedByString:@"="] lastObject]);
        } else {
            getTransactionResponse(nil, [self responseError], [[request componentsSeparatedByString:@"="] lastObject]);
        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        NSString *responseString = dynamicLocalizedString(@"api.message.serverError");
        if (operation.responseObject) {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseObject options:kNilOptions error:&error];
            id responsedObject = [response valueForKey:ResponseDictionaryErrorKey];
            if ([responsedObject isKindOfClass:[NSArray class]]){
                responseString = [(NSArray *)responsedObject firstObject];
            } else if ([responsedObject isKindOfClass:[NSString class]]){
                responseString = responsedObject;
            }
        }
        getTransactionResponse(responseString, error, nil);
    }];
}

#pragma mark - UserInteraction

- (void)traSSRegisterUsername:(NSString *)emailAddress firstNameEN:(NSString *)firstNameEN lastNameEN:(NSString *)lastNameEN firstNameAR:(NSString *)firstNameAR lastNameAR:(NSString *)lastNameAR emiratesID:(NSString *)emiratesID mobilePhone:(NSString *)mobile requestResult:(ResponseBlock)registerResponse
{
    NSMutableDictionary *parameters = [@{
                                         @"email" : emailAddress,
                                         @"first_EN" : firstNameEN,
                                         @"last_EN" : lastNameEN,
                                         @"first_AR" : firstNameAR,
                                         @"last_AR" : lastNameAR,
                                         @"emiratesId" : emiratesID,
                                         @"mobile" : mobile,
                                         } mutableCopy];
    [self performPOST:traSSRegister withParameters:parameters response:registerResponse];
}

- (void)traSSLoginUsername:(NSString *)username password:(NSString *)password accountType:(NSInteger)accountType secretQuesionType:(NSInteger)type secretQuestionAnswer:(NSString *)answer requestResult:(ResponseBlock)loginResponse
{
    NSMutableDictionary *parameters = [@{
                                         @"userType" : @(accountType),
                                         @"login" : username,
                                         @"pass" : password
                                         } mutableCopy];
    
    if (answer.length) {
        [parameters setObject:@(type) forKey:@"secretQuestionType"];
        [parameters setObject:answer  forKey:@"secretQuestionAnswer"];
    }
    
    __weak typeof(self) weakSelf = self;
    [self.manager POST:traSSLogin parameters:parameters success:^(AFHTTPRequestOperation * __nonnull operation, id  __nonnull responseObject) {
        PerformSuccessRecognition(operation, responseObject, loginResponse);
        weakSelf.isUserLoggined = YES;
    } failure:^(AFHTTPRequestOperation * __nonnull operation, NSError * __nonnull error) {
        if (operation.responseData) {
            id value = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
            if ([value objectForKey:@"secretQuestions"]) {
                loginResponse(value, nil);
            } else {
                PerformFailureRecognition(operation, error, loginResponse);
            }
        } else {
            PerformFailureRecognition(operation, error, loginResponse);
        }
    }];
}

- (void)traSSGetUserProfileResult:(ResponseBlock)profileResponse
{
    [self performGET:traSSProfile withParameters:nil response:profileResponse];
}

- (void)traSSUpdateUserProfile:(UserModel *)userProfile requestResult:(ResponseBlock)updateProfileResponse
{
    NSMutableDictionary *parameters = [@{
                                         @"first_EN" : userProfile.firstNameEN ? userProfile.firstNameEN : @"",
                                         @"last_EN" : userProfile.lastNameEN ? userProfile.lastNameEN : @"",
                                         @"first_AR" : userProfile.firstNameAR ? userProfile.firstNameAR : @"",
                                         @"last_AR" : userProfile.lastNameAR ? userProfile.lastNameAR : @"",
                                         @"emiratesId" : userProfile.emirateId ? userProfile.emirateId : @"",
                                         @"email" : userProfile.email ? userProfile.email : @"",
                                         @"mobile" : userProfile.contactNumber ? userProfile.contactNumber : @"",
                                         @"image" : userProfile.avatarImageBase64.length ? userProfile.avatarImageBase64 : @"",
                                         } mutableCopy];
    if (userProfile.secretQuestionAnswer.length) {
        [parameters setObject:@(YES) forKey:@"enhancedSecurity"];
        [parameters setObject:userProfile.secretQuestionAnswer forKey:@"secretQuestionAnswer"];
        [parameters setObject:@([userProfile.secretQuestionType integerValue]) forKey:@"secretQuestionType"];
    }
    
    [self performPUT:traSSProfile withParameters:parameters response:updateProfileResponse];
}

- (void)traSSChangePassword:(NSString *)newPassword confirmPassword:(NSString *)confirmPassword requestResult:(ResponseBlock)changePasswordResponse
{
    NSDictionary *parameters = @{
                                 @"pass" : newPassword,
                                 @"confirmPass" : confirmPassword
                                 };
    [self performPUT:traSSChangePassword withParameters:parameters response:changePasswordResponse];
}

- (void)traSSForgotPasswordForEmail:(NSString *)email requestResult:(ResponseBlock)forgotPasswordResponse
{
    [self performPOST:traSSForgotPassword withParameters:@{@"email" : email} response:forgotPasswordResponse];
}

- (void)traSSForgotChangePasswordUID:(NSString *)uid verifityOTP:(NSString *)verifityOTP newPassword:(NSString *)newPassword requestResult:(ResponseBlock)forgotChangePasswordResponse
{
    NSDictionary *parametrs = @{
                                @"uid" : uid,
                                @"otp" : verifityOTP,
                                @"pass" : newPassword,
                                @"confirmPass" : newPassword
                                };
    [self performPOST:traSSForgotVerifityResetPassword withParameters:parametrs response:forgotChangePasswordResponse];
}

- (void)traSSLogout:(ResponseBlock)logoutResponse
{
    __weak typeof(self) weakSelf = self;
    [self.manager POST:traSSLogOut parameters:nil success:^(AFHTTPRequestOperation * __nonnull operation, id  __nonnull responseObject) {
        PerformSuccessRecognition(operation, responseObject, logoutResponse);
        weakSelf.isUserLoggined = NO;
    } failure:^(AFHTTPRequestOperation * __nonnull operation, NSError * __nonnull error) {
        PerformFailureRecognition(operation, error, logoutResponse);
    }];
}

- (void)traSSGetSecretQuestions:(ResponseBlock)secretQuestions
{
    [self performGET:traSSSecretQuestions withParameters:nil response:secretQuestions];
}

#pragma mark - Image

- (void)traSSGetImageWithPath:(NSString *)imagePath withCompletition:(DownloadingComplete)completitionHandler
{
    if ([[imagePath substringToIndex:1] isEqualToString:@"/"]) {
        imagePath = [imagePath substringFromIndex:1];
    }
    [self.manager GET:imagePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
        UIImage *image = [UIImage imageWithData:responseObject];
        if (image) {
            completitionHandler(YES, image);
        } else {
            completitionHandler(NO, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completitionHandler(NO, nil);
    }];
}

#pragma mark - LifeCycle

- (instancetype)initWithBaseURL:(NSURL *)baseURL
{
    self = [super init];
    if (self) {
        [self prepareNetworkManagerWithURL:baseURL];
    }
    return self;
}

#pragma mark - Reachability

- (void)startMonitoringNetwork
{
    __weak typeof(self) weakSelf = self;
    [self.manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        weakSelf.networkStatus = status;
    }];
    [self.manager.reachabilityManager startMonitoring];
}

- (void)stopMonitoringNetwork
{
    [self.manager.reachabilityManager stopMonitoring];
}

#pragma mark - Network Rest Methods

- (void)performPOST:(NSString *)path withParameters:(NSDictionary *)parameters response:(ResponseBlock)completionHandler
{
    [self.manager POST:path parameters:parameters success:^(AFHTTPRequestOperation * __nonnull operation, id  __nonnull responseObject) {
        PerformSuccessRecognition(operation, responseObject, completionHandler);
    } failure:^(AFHTTPRequestOperation * __nonnull operation, NSError * __nonnull error) {
        PerformFailureRecognition(operation, error, completionHandler);
    }];
}

- (void)performPUT:(NSString *)path withParameters:(NSDictionary *)parameters response:(ResponseBlock)completionHandler
{
    [self.manager PUT:path parameters:parameters success:^(AFHTTPRequestOperation * __nonnull operation, id  __nonnull responseObject) {
        PerformSuccessRecognition(operation, responseObject, completionHandler);
    } failure:^(AFHTTPRequestOperation * __nonnull operation, NSError * __nonnull error) {
        PerformFailureRecognition(operation, error, completionHandler);
    }];
}

- (void)performGET:(NSString *)path withParameters:(NSDictionary *)parameters response:(ResponseBlock)completionHandler
{
    NSString *stringCleanPath = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self.manager GET:stringCleanPath parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
        PerformSuccessRecognition(operation, responseObject, completionHandler);
    } failure:^(AFHTTPRequestOperation * __nonnull operation, NSError * __nonnull error) {
        PerformFailureRecognition(operation, error, completionHandler);
    }];
}

- (void)performDELETE:(NSString *)path withParameters:(NSDictionary *)parameters response:(ResponseBlock)completionHandler
{
    [self.manager DELETE:path parameters:parameters success:^(AFHTTPRequestOperation * __nonnull operation, id  __nonnull responseObject) {
        PerformSuccessRecognition(operation, responseObject, completionHandler);
    } failure:^(AFHTTPRequestOperation * __nonnull operation, NSError * __nonnull error) {
        PerformFailureRecognition(operation, error, completionHandler);
    }];
}

#pragma mark - Private

- (NSError *)responseError
{
    NSError *error = [[NSError alloc] initWithDomain:[AppHelper appName] code:999 userInfo:@{NSLocalizedDescriptionKey : dynamicLocalizedString(@"api.message.serverError")}];
    return error;
}

void(^PerformFailureRecognition)(AFHTTPRequestOperation * __nonnull operation, NSError * __nonnull error, ResponseBlock handler) = ^(AFHTTPRequestOperation * __nonnull operation, NSError * __nonnull error, ResponseBlock handler) {
//#if DEBUG
//    [AppHelper alertViewWithMessage:error.debugDescription];
//#endif
    NSString *responseString = dynamicLocalizedString(@"api.message.serverError");
    if (operation.responseObject) {
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseObject options:kNilOptions error:&error];
        id responsedObject = [response valueForKey:ResponseDictionaryErrorKey];
        if ([operation.response statusCode] == 426) {
            if ([[response valueForKey:@"services"] isKindOfClass:[NSDictionary class]]) {
                handler([response valueForKey:@"services"], error);
                return;
            }
        } else if ([responsedObject isKindOfClass:[NSArray class]]) {
            responseString = [(NSArray *)responsedObject firstObject];
        } else if ([responsedObject isKindOfClass:[NSString class]]) {
            responseString = [NetworkManager localizedDescriptionForResponse:responsedObject];
        }
    }
    handler(responseString, error);
};

void(^PerformSuccessRecognition)(AFHTTPRequestOperation * __nonnull operation, id  __nonnull responseObject, ResponseBlock handler) = ^(AFHTTPRequestOperation * __nonnull operation, id  __nonnull responseObject, ResponseBlock handler) {
    if (responseObject) {
        id value = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        NSLog(@"response:%@",value);
        NSString *info = dynamicLocalizedString(@"api.message.noDataFound");
        if ([value isKindOfClass:[NSDictionary class]]) {
            NSDictionary *responseDictionary = value;
            if ([responseDictionary valueForKey:ResponseDictionarySuccessKey]) {
                info = [responseDictionary valueForKey:ResponseDictionarySuccessKey];
            } else if ([responseDictionary valueForKey:@"availableStatus"]) {
                info = [responseDictionary valueForKey:@"availableStatus"];
            } else if ([responseDictionary valueForKey:@"urlData"]) {
                info = [responseDictionary valueForKey:@"urlData"];
            } else {
                handler(responseDictionary, nil);
                return;
            }
            handler(info, nil);
        } else if ([value isKindOfClass:[NSArray class]]) {
            handler(value, nil);
        }
    } else {
        handler(dynamicLocalizedString(@"api.message.noDataFound"), nil);
    }
};

+ (NSString *)localizedDescriptionForResponse:(NSString *)responseString
{
    NSDictionary *responseValue = [NetworkManager responseValues];
    
    NSParameterAssert(responseValue.allKeys.count);
    NSParameterAssert(responseString);
    
    NSString *localizedDescription = [responseValue valueForKey:responseString] ? [responseValue valueForKey:responseString] : dynamicLocalizedString(@"api.message.serverError");
    
    return localizedDescription;
}

+ (NSDictionary *)responseValues
{
    return @{
             @"Account Locked" : dynamicLocalizedString(@"api.message.error.description.accountLocked"),
             @"Invalid Credentials" : dynamicLocalizedString(@"api.message.error.description.invalidCredentials"),
             @"Invalid Data" : dynamicLocalizedString(@"api.message.error.description.invalidData"),
             @"Connection Error.Try Again" : dynamicLocalizedString(@"api.message.error.description.connectionErrorTryAgain"),
             @"Password Not as per policy" : dynamicLocalizedString(@"api.message.error.description.passwordNotAsPerPolicy"),
             @"Expired" : dynamicLocalizedString(@"api.message.error.description.expired"),
             @"Wrong OTP" : dynamicLocalizedString(@"api.message.error.description.wrongOTP"),
             @"Connection Error" : dynamicLocalizedString(@"api.message.error.description.connectionError"),
             @"System error" : dynamicLocalizedString(@"api.message.error.description.systemError")
             };
}

#pragma mark - Preparation

- (void)prepareNetworkManagerWithURL:(NSURL *)baseURL
{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:TESTBaseURLPathKey]) {
        NSString *path = [[NSUserDefaults standardUserDefaults] valueForKey:TESTBaseURLPathKey];
        if (path.length) {
            baseURL = [NSURL URLWithString:path];
        }
    }

    self.manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //shayan networking code
    [self.manager.requestSerializer setTimeoutInterval:30];
    [self.manager.requestSerializer setValue:@"testAppKey" forHTTPHeaderField:@"appkey"];
    [self.manager.requestSerializer setValue:RestApiVersion forHTTPHeaderField:@"rest-api-version"];
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    self.manager.securityPolicy = securityPolicy;

}

#pragma mark - Util

- (void)setBaseURL:(NSString *)baseURL
{
    [[NSUserDefaults standardUserDefaults] setValue:baseURL forKey:TESTBaseURLPathKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self prepareNetworkManagerWithURL:[NSURL URLWithString:baseURL]];
}

@end
