//
//  BaseNetworkManager.h
//  TRA Smart Services
//
//  Created by Admin on 21.07.15.
//

#import "AFNetworkReachabilityManager.h"
#import "TRALoaderViewController.h"
#import "UserModel.h"
#import "NetworkEndPoints.h"
#import "AFNetworking.h"

@class TransactionModel;

static NSString *const ImagePrefixBase64String = @"data:image/jpeg;base64,";

static NSString *const PostfixImage2x = @"@2x";
static NSString *const PostfixImage3x = @"@3x";

static NSString *const ServiceTypeGetDomainDataStringName = @"domain check";
static NSString *const ServiceTypeGetDomainAvaliabilityStringName = @"domain check";
static NSString *const ServiceTypeSearchMobileIMEIStringName = @"verification";
static NSString *const ServiceTypeSearchMobileBrandStringName = @"mobile brand";
static NSString *const ServiceTypeFeedbackStringName = @"Rating service";
static NSString *const ServiceTypeSMSSpamReportStringName = @"sms report";
static NSString *const ServiceTypeHelpSalimStringName = @"web report";
static NSString *const ServiceTypeVerificationStringName = @"domain check";
static NSString *const ServiceTypeCoverageStringName = @"coverage";
static NSString *const ServiceTypeInternetSpeedTestStringName = @"";
static NSString *const ServiceTypeCompliantAboutServiceProviderStringName = @"complain about service provider";
static NSString *const ServiceTypeSuggestionStringName = @"suggestion";
static NSString *const ServiceTypeCompliantAboutServiceProviderEnquiresStringName = @"enquiries";
static NSString *const ServiceTypeCompliantAboutServiceProviderTRAStringName = @"complain about tra";

static NSString *const ServiceTypeGetDomainCheckWhoisStringName = @"Domain Check Whois";
static NSString *const ServiceTypeGetDomainCheckAvaliabilityStringName = @"Domain Check Availability";

static NSString *const TESTBaseURLPathKey = @"TESTBaseURLPathKey";

typedef NS_ENUM(NSUInteger, ServiceType) {
    ServiceTypeGetDomainData = 0,
    ServiceTypeGetDomainAvaliability = 1,
    ServiceTypeSearchMobileIMEI = 2,
    ServiceTypeSearchMobileBrand = 3,
    ServiceTypeFeedback = 4,
    ServiceTypeSMSSpamReport = 5,
    ServiceTypeHelpSalim = 6,
    ServiceTypeVerification = 7,
    ServiceTypeCoverage = 8,
    ServiceTypeInternetSpeedTest = 9,
    ServiceTypeCompliantAboutServiceProvider = 10,
    ServiceTypeSuggestion = 11,
    ServiceTypeCompliantAboutServiceProviderEnquires = 12,
    ServiceTypeCompliantAboutServiceProviderTRA = 13
};

typedef NS_ENUM(NSUInteger, ComplianType) {
    ComplianTypeCustomProvider = 0,
    ComplianTypeTRAService,
    ComplianTypeEnquires
};

typedef NS_ENUM(NSUInteger, TransactionModelType) {
    TransactionModelTypeCRMEnquiries = 1,
    TransactionModelTypeCRMSuggestion = 2,
    TransactionModelTypeCRMComplainAboutTRA = 3,
    TransactionModelTypeCRMCompliantAboutServiceProvider = 4,
    TransactionModelTypeCoverage = 101,
    TransactionModelTypeCRMSMSSpam = 102,
    TransactionModelTypeWebReport = 103
};

typedef void(^ResponseBlock)(id response, NSError *error);
typedef void(^SearchResponseBlock)(id response, NSError *error, NSString *requestURI);
typedef void(^DownloadingComplete)(BOOL success, UIImage *response);

@interface NetworkManager : NSObject <NSStreamDelegate>

@property (assign, nonatomic) AFNetworkReachabilityStatus networkStatus;
@property (assign, nonatomic) __block BOOL isUserLoggined;
@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;

+ (instancetype)sharedManager;

- (void)cancelAllOperations;
- (BOOL)hasActiveRequest;

#pragma mark - Reachability

- (void)startMonitoringNetwork;
- (void)stopMonitoringNetwork;

#pragma mark - NoCRMServices

- (void)traSSNoCRMServiceGetDomainData:(NSString *)domainURL requestResult:(ResponseBlock)domainOwnerResponse;
- (void)traSSNoCRMServiceGetDomainAvaliability:(NSString *)domainURL requestResult:(ResponseBlock)domainAvaliabilityResponse;
- (void)traSSNoCRMServicePerformSearchByIMEI:(NSString *)mobileIMEI requestResult:(ResponseBlock)mobileIMEISearchResponse;
- (void)traSSNoCRMServicePerformSearchByMobileBrand:(NSString *)mobileBrand requestResult:(ResponseBlock)mobileBrandSearchResponse;
- (void)traSSNoCRMServicePOSTFeedback:(NSString *)feedback forSerivce:(NSString *)serviceName withRating:(NSUInteger)rating requestResult:(ResponseBlock)mobileBrandSearchResponse;
- (void)traSSNoCRMServicePOSTSMSSpamReport:(NSString *)spammerPhoneNumber notes:(NSString *)note requestResult:(ResponseBlock)SMSSpamReportResponse;
- (void)traSSNoCRMServicePOSTSMSBlock:(NSString *)spammerPhoneNumber phoneProvider:(NSString *)provider providerType:(NSString *)providerType notes:(NSString *)note requestResult:(ResponseBlock)SMSSpamReportResponse;
- (void)traSSNoCRMServicePOSTHelpSalim:(NSString *)urlAddress notes:(NSString *)comment requestResult:(ResponseBlock)helpSalimReportResponse;
- (void)traSSNoCRMServicePOSTPoorCoverageAtLatitude:(CGFloat)latitude longtitude:(CGFloat)longtitude address:(NSString *)address signalPower:(NSUInteger)signalLevel requestResult:(ResponseBlock)poorCoverageResponse;
- (void)traSSNoCRMServicePOSTComplianService:(ComplianType)complaintType parameters:(NSDictionary *)parameters requestResult:(ResponseBlock)responseBlok;
- (void)traSSNoCRMServicePOSTSendSuggestion:(NSDictionary *)suggestionModelDisctinary requestResult:(ResponseBlock)suggestionResponse;
- (void)traSSNoCRMSErviceGetFavoritesServices:(ResponseBlock)favoriteServices;
- (void)traSSNoCRMServicePostAddServicesToFavorites:(NSArray *)serviceNames responce:(ResponseBlock)result;
- (void)traSSNoCRMServiceDeleteServicesFromFavorites:(NSArray *)serviceNames responce:(ResponseBlock)result;
- (void)traSSNoCRMServiceGetAllServicesEnable:(ResponseBlock)result;
- (void)traSSNoCRMServiceGetServiceAboutInfo:(NSString *)serviceName languageCode:(NSString *)languageCode responseBlock:(ResponseBlock)aboutServiceInfoResponse;
- (void)traSSNoCRMServiceGetDynamicServiceAboutInfo:(NSString *)serviceID responseBlock:(ResponseBlock)aboutServiceInfoResponse;
- (void)traSSNoCRMServicePostInnovationTitle:(NSString *)title message:(NSString *)message type:(NSNumber *)type responseBlock:(ResponseBlock)innovationResponse;
- (void)traSSNoCRMServiceGetGetTransactions:(NSInteger)page count:(NSInteger)count orderAsc:(BOOL)orderArc responseBlock:(ResponseBlock)getTransactionResponse;
- (void)traSSNoCRMServiceSearchTransactions:(NSInteger)page count:(NSInteger)count orderAsc:(BOOL)orderArc searchText:(NSString *)searchText responseBlock:(SearchResponseBlock)getTransactionResponse;
- (void)traSSNoCRMServiceGetAnnoucement:(NSInteger)limit offset:(NSInteger)offset responseBlock:(ResponseBlock)announcementResponse;
- (void)traSSNoCRMServiceSearchAnnoucement:(NSInteger)limit offset:(NSInteger)offset searchText:(NSString *)searchText responseBlock:(SearchResponseBlock)getAnnoucementResponse;
- (void)traSSNoCRMServiceGetContacts:(ResponseBlock)result;

#pragma mark - CRMdynamicService

- (void)traSSCRMServiceGetServicesList:(ResponseBlock)dynamicServices;
- (void)traSSCRMServiceGetServiceDetails:(NSString *)serviceID responseBlock:(ResponseBlock)dynamicService;
- (void)traSSCRMServiceGetTransactionByID:(NSString *)transactionID type:(NSInteger)type responseBlock:(ResponseBlock)responseBlock;
- (void)traSSCRMDynamicServicePOSTImage:(UIImage *)image imagePOSTResult:(ResponseBlock)imagePostResponse;
- (void)traSSCRMDynamicServiceGetPDF:(ResponseBlock)dynamicService;
- (void)traSSCRMDynamicServicePOSTServiceReportWithServiceID:(NSString *)serviceID parameters:(NSDictionary *)parameters postResponse:(ResponseBlock)postResponseBlock;
- (void)traSSCRMDynamicServicePutEditTransactions:(NSString *)transactionID parametrs:(NSDictionary *)parametersDictionary responseBlock:(ResponseBlock)editTransactionResponse;

#pragma mark - User

- (void)traSSRegisterUsername:(NSString *)emailAddress firstNameEN:(NSString *)firstNameEN lastNameEN:(NSString *)lastNameEN firstNameAR:(NSString *)firstNameAR lastNameAR:(NSString *)lastNameAR emiratesID:(NSString *)emiratesID mobilePhone:(NSString *)mobile requestResult:(ResponseBlock)registerResponse;
- (void)traSSLoginUsername:(NSString *)username password:(NSString *)password accountType:(NSInteger)accountType secretQuesionType:(NSInteger)type secretQuestionAnswer:(NSString *)answer requestResult:(ResponseBlock)loginResponse;
- (void)traSSGetUserProfileResult:(ResponseBlock)profileResponse;
- (void)traSSUpdateUserProfile:(UserModel *)userProfile requestResult:(ResponseBlock)updateProfileResponse;
- (void)traSSChangePassword:(NSString *)newPassword confirmPassword:(NSString *)confirmPassword requestResult:(ResponseBlock)changePasswordResponse;
- (void)traSSForgotPasswordForEmail:(NSString *)email requestResult:(ResponseBlock)forgotPasswordResponse;
- (void)traSSLogout:(ResponseBlock)logoutResponse;
- (void)traSSGetSecretQuestions:(ResponseBlock)secretQuestions;
- (void)traSSForgotChangePasswordUID:(NSString *)uid verifityOTP:(NSString *)verifityOTP newPassword:(NSString *)newPassword requestResult:(ResponseBlock)forgotChangePasswordResponse;

#pragma mark - Image

- (void)traSSGetImageWithPath:(NSString *)imagePath withCompletition:(DownloadingComplete)completitionHandler;

#pragma mark - Util

- (void)setBaseURL:(NSString *)baseURL;
+ (NSString *)localizedDescriptionForResponse:(id)response;

@end
