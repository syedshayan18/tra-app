//
//  NetworkEndPoints.h
//  TRA Smart Services
//
//  Created by Admin on 21.07.15.
//

#pragma mark - NoCRMServices

//#if DEBUG
//   static NSString *const traSSNoCRMServiceBaseURL = @"http://185.54.19.249/v4/";
//#else
    static NSString *const traSSNoCRMServiceBaseURL = @"https://mobsrv.tra.gov.ae/v3/";
//#endif

static NSString *const RestApiVersion = @"v3";

static NSString *const traSSNOCRMServiceGETDomainData = @"checkWhois?checkUrl=";
static NSString *const traSSNOCRMServiceGETDomainAvaliability = @"checkWhoisAvailable?checkUrl=";
static NSString *const traSSNOCRMServiceGETSearchMobileIMEI = @"searchMobile?imei=";
static NSString *const traSSNOCRMServiceGETSearchMobileBrand = @"searchMobileBrand?brand=";
static NSString *const traSSNOCRMServicePOSTFeedBack = @"feedback";
static NSString *const traSSNOCRMServicePOSTSMSSPamReport = @"crm/complainSmsSpam";
static NSString *const traSSNOCRMServicePOSTSMSBlock = @"complainSmsBlock";
static NSString *const traSSNOCRMServicePOSTHelpSalim = @"sendHelpSalim";
static NSString *const traSSNOCRMServicePOSTCompliantServiceProvider = @"crm/complainServiceProvider";
static NSString *const traSSNOCRMServicePOSTCompliantAboutTRAService = @"crm/complainTRAService";
static NSString *const traSSNOCRMServicePOSTCompliantAboutEnquires = @"crm/complainEnquiries";
static NSString *const traSSNOCRMServicePOSTSendSuggestin = @"crm/sendSuggestion";
static NSString *const traSSNOCRMServicePOSTPoorCoverage = @"sendPoorCoverage";
static NSString *const traSSNOCRMServiceGETFavoritesServices = @"user/favorites";
static NSString *const traSSNOCRMServicePOSTAddServicesToFavorites = @"user/favorites";
static NSString *const traSSNOCRMServiceDELETEServicesFromFavorites = @"user/favorites";
static NSString *const traSSNOCRMServiceGETAllServicesEnable = @"service/";
static NSString *const traSSNOCRMServiceGETAboutServiceInfo = @"service/about?name=";
static NSString *const traSSNOCRMServiceGETAboutDynamicServiceInfo = @"service/about?id=";
static NSString *const traSSNOCRMServiceGetTransactions = @"crm/transactions";
static NSString *const traSSNOCRMServiceSearchTransactions = @"crm/transactions?page=";
static NSString *const traSSNOCRMServicePostInnovation = @"innovation";
static NSString *const traSSNOCRMServiceGetAnnouncement = @"announcement";
static NSString *const traSSNOCRMServiceGetContacts = @"contact";

#pragma mark - CRMService

static NSString *const traSSCRMServiceGetServiceList = @"service";
static NSString *const traSSCRMServiceGetPDF = @"setting";
static NSString *const traSSCRMServiceGetServiceDetails = @"service/";
static NSString *const traSSCRMServicePOSTService = @"service/";
static NSString *const traSSCRMServicePOSTServiceAttachment = @"attachment";

static NSString *const traSSRegister = @"crm/register";
static NSString *const traSSLogin = @"crm/signIn";
static NSString *const traSSLogOut = @"crm/signOut";
static NSString *const traSSProfile = @"crm/profile";
static NSString *const traSSChangePassword = @"crm/changePass";
static NSString *const traSSForgotPassword = @"crm/forgotPass";
static NSString *const traSSForgotVerifityResetPassword = @"crm/verifyResetPassword";
static NSString *const traSSSecretQuestions = @"crm/secretQuestions";

