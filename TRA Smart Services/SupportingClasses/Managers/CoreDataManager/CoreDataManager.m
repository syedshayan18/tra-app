//
//  CoreDataManager.m
//  TRA Smart Services
//
//  Created by Admin on 19.09.15.
//

#import "CoreDataManager.h"
#import "TRAStaticService.h"
#import "ServiceModel.h"
#import "DynamicServiceModel.h"
#import "TRADynamicService.h"

static NSString *const FetchRequestAllService = @"AllService";
static NSString *const FetchRequestFavouriteService = @"FavouriteService";
static NSString *const FetchRequestAllDynamicService = @"AllDynamicService";
static NSString *const FetchRequestFavoriteDynamicService = @"FavouriteDynamicService";

static NSString *const CoreDataEntityNameTRAStaticService = @"TRAStaticService";
static NSString *const CoreDataEntityNameTRADynamicService = @"TRADynamicService";

@implementation CoreDataManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

#pragma mark - Public

+ (instancetype)sharedManager
{
    static CoreDataManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[CoreDataManager alloc] init];
    });
    return sharedManager;
}

#pragma mark - Public

- (NSArray *)allDBSortedArray
{
    NSMutableArray *dynamicServiceAll = [[[CoreDataManager sharedManager] fetchDynamicServiceList] mutableCopy];
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(TRADynamicService *evaluatedObject, NSDictionary *bindings) {
        return !evaluatedObject.relationshipChild.count;
    }];
    [dynamicServiceAll filterUsingPredicate:predicate];
    
    NSMutableArray *staticServiceAll = [[[CoreDataManager sharedManager] fetchServiceList] mutableCopy];
    [staticServiceAll addObjectsFromArray:dynamicServiceAll];
    
    NSArray * descriptors = [NSArray arrayWithObject:[[NSSortDescriptor alloc]initWithKey:@"serviceOrder" ascending:YES]];
    
    return [staticServiceAll sortedArrayUsingDescriptors:descriptors];
}

- (NSArray *)favoriteDBSortedArray
{
    NSMutableArray *dynamicServiceFavorite = [[[CoreDataManager sharedManager] fetchFavoriteDynamicServiceList] mutableCopy];
    NSMutableArray *staticServiceFavorite = [[[CoreDataManager sharedManager] fetchFavouriteServiceList] mutableCopy];
    [staticServiceFavorite addObjectsFromArray:dynamicServiceFavorite];
    
    NSArray * descriptors = [NSArray arrayWithObject:[[NSSortDescriptor alloc]initWithKey:@"serviceFavoriteOrder" ascending:YES]];

    return [staticServiceFavorite sortedArrayUsingDescriptors:descriptors];
}

- (NSArray *)dynamicServiceModelArray
{
    NSArray *traDynamicServicesArray = [[[CoreDataManager sharedManager] fetchDynamicServiceList] mutableCopy];
    NSArray * descriptors = [NSArray arrayWithObject:[[NSSortDescriptor alloc]initWithKey:@"serviceOrder" ascending:YES]];
    traDynamicServicesArray = [traDynamicServicesArray sortedArrayUsingDescriptors:descriptors];
    
    NSMutableArray *dynamicServiceModelArray = [[NSMutableArray alloc] init];
    for (TRADynamicService *dynamicService in traDynamicServicesArray) {
        if (!dynamicService.relationshipFahter) {
            DynamicServiceModel *dynamicServiceModel = [[DynamicServiceModel alloc] initWithTRADynamicServise:dynamicService];

            if (dynamicService.relationshipChild.count) {
                dynamicServiceModel.dynamicServiceItems = [self itemsDynamicServiceModel:dynamicService];
            }
            [dynamicServiceModelArray addObject:dynamicServiceModel];

        }
    }
    return dynamicServiceModelArray;
}

- (NSArray <DynamicServiceModel *> *)itemsDynamicServiceModel:(TRADynamicService *)fahterDynamicService
{
    NSMutableArray *itemsDynamicServiceModel = [[NSMutableArray alloc] init];
    for (TRADynamicService *traDynamicService in fahterDynamicService.relationshipChild) {
        DynamicServiceModel *dynamicServiceModel = [[DynamicServiceModel alloc] initWithTRADynamicServise:traDynamicService];
        if (traDynamicService.relationshipChild.count) {
            dynamicServiceModel.dynamicServiceItems = [self itemsDynamicServiceModel:traDynamicService];
        }
        [itemsDynamicServiceModel addObject:dynamicServiceModel];
    }
    return itemsDynamicServiceModel;
}

- (NSArray *)fetchDynamicServiceList
{
    NSFetchRequest *fetchRequest = [self.managedObjectModel fetchRequestTemplateForName:FetchRequestAllDynamicService];
    NSError *error;
    NSArray *result = [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
#if DEBUG
    if (error) {
        DLog(@"Cant fetch data from DB: %@\n%@",error.localizedDescription, error.userInfo);
    }
#endif
    return result;
}

- (NSArray *)fetchFavoriteDynamicServiceList
{
    NSFetchRequest *fetchRequest = [self.managedObjectModel fetchRequestTemplateForName:FetchRequestFavoriteDynamicService];
    NSError *error;
    NSArray *result = [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
#if DEBUG
    if (error) {
        DLog(@"Cant fetch data from DB: %@\n%@",error.localizedDescription, error.userInfo);
    }
#endif
    return result;
}

- (NSArray *)fetchServiceList
{
    NSFetchRequest *fetchRequest = [self.managedObjectModel fetchRequestTemplateForName:FetchRequestAllService];
    NSError *error;
    NSArray *result = [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
#if DEBUG
    if (error) {
        DLog(@"Cant fetch data from DB: %@\n%@",error.localizedDescription, error.userInfo);
    }
#endif
    return result;
}

- (NSArray *)fetchFavouriteServiceList
{
    NSFetchRequest *fetchRequest = [self.managedObjectModel fetchRequestTemplateForName:FetchRequestFavouriteService];
    NSError *error;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
#if DEBUG
    if (error) {
        DLog(@"Cant fetch data from DB: %@\n%@",error.localizedDescription, error.userInfo);
    }
#endif
    return result;
}

- (void)saveContext
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

- (void)prepareCoreDataWintResponse:(NSDictionary *)response
{
    NSMutableArray *servicesDictionaryList = [[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SpeedAccessServices" ofType:@"plist"]];
    [servicesDictionaryList addObjectsFromArray:[[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"OtherServices" ofType:@"plist"]]];
    
    NSMutableArray *servicesModelList = [[NSMutableArray alloc] init];
    for (NSDictionary *dictionary in servicesDictionaryList) {
        ServiceModel *serviceModel = [[ServiceModel alloc] initWithDictionary:dictionary];
        [servicesModelList addObject:serviceModel];
    }
    
    if ([[response objectForKey:@"static"] isKindOfClass:[NSArray class]]) {
        NSArray *servicesNameEnable = [response objectForKey:@"static"];
        
        for (NSInteger i = 0; i < servicesNameEnable.count; i++) {
            if ([servicesNameEnable[i] isKindOfClass:[NSString class]]) {
                for (ServiceModel *serviceModel in servicesModelList) {
                    if ([serviceModel.serviceName isEqualToString:servicesNameEnable[i]]) {
                        serviceModel.serviceEnable = YES;
                    }
                }
            }
        }
    }
    [[CoreDataManager sharedManager] saveServicesListToDB:servicesModelList];
    
    NSMutableArray *servicesDynamicModelList = [[NSMutableArray alloc] init];

    if ([[response objectForKey:@"dynamic"] isKindOfClass:[NSArray class]]) {
        NSArray *servicesList = [response objectForKey:@"dynamic"];
        
        for (NSInteger i = 0; i < servicesList.count; i++) {
            if ([servicesList[i] isKindOfClass:[NSDictionary class]]) {
                DynamicServiceModel *dynamicServiceModel = [[DynamicServiceModel alloc] initWithDictionary:servicesList[i]];
                [servicesDynamicModelList addObject:dynamicServiceModel];
            }
        }
    }
    [[CoreDataManager sharedManager] saveDynamicServicesListToDB:servicesDynamicModelList];
    [self saveContext];
}

#pragma mark - Private

- (void)saveServicesListToDB:(NSArray *)servicesArray
{
    NSArray <TRAStaticService *> *favoriteServices = [self fetchFavouriteServiceList];
    NSArray <TRAStaticService *> *allServices = [self fetchServiceList];
    
    for (TRAStaticService *service in allServices) {
        [self.managedObjectContext deleteObject:service];
    }
    
    NSArray *data = [self fetchServiceList];
    
    if (!data.count) {
        
        for (ServiceModel *serviceModel in servicesArray) {
            if (![serviceModel.serviceName isEqualToString:@"speedAccess.service.name.-1"]) { //temp while not all data avaliable
                NSEntityDescription *traServiceEntity = [NSEntityDescription entityForName:CoreDataEntityNameTRAStaticService inManagedObjectContext:self.managedObjectContext];
                TRAStaticService *service = [[TRAStaticService alloc] initWithEntity:traServiceEntity insertIntoManagedObjectContext:self.managedObjectContext];
                
                service.serviceInternalID = @(serviceModel.serviceID);
                service.serviceOrder = @(serviceModel.serviceOrder);
                service.serviceName = serviceModel.serviceName;
                service.serviceEnable = @(serviceModel.serviceEnable);
                service.serviceType = @(serviceModel.serviceType);
                if (serviceModel.serviceDisplayLogo.length) {
                    service.serviceIconName = serviceModel.serviceDisplayLogo;
                }
                service.serviceDescription = @"No decription provided";
                for (TRAStaticService *traService in favoriteServices) {
                    if ([traService.serviceName isEqualToString:service.serviceName]) {
                        service.serviceIsFavorite = @(YES);
                        service.serviceFavoriteOrder = [traService.serviceFavoriteOrder copy];
                    }
                }
            }
        }
    }
}

- (void)saveDynamicServicesListToDB:(NSArray *)dynamicServicesArray
{
    NSArray <TRADynamicService *> *favoriteDynamicService = [self fetchFavoriteDynamicServiceList];
    NSArray <TRADynamicService *> *allService = [self fetchDynamicServiceList];
    for (TRADynamicService *service in allService) {
        [self.managedObjectContext deleteObject:service];
    }
    [self saveDynamicServicesListToDB:dynamicServicesArray relationshipFather:nil favoriteDynamicServiceArray:favoriteDynamicService];
}

- (void)saveDynamicServicesListToDB:(NSArray *)dynamicServicesArray relationshipFather:(TRADynamicService *)fatherDynamicService favoriteDynamicServiceArray:(NSArray <TRADynamicService *> *)favoriteDynamicService
{
    for (NSInteger i = 0; i < dynamicServicesArray.count; i++) {
        DynamicServiceModel *dynamicServiceModel = dynamicServicesArray[i];
        
        NSEntityDescription *traDynamicServiceEntity = [NSEntityDescription entityForName:CoreDataEntityNameTRADynamicService inManagedObjectContext:self.managedObjectContext];
        TRADynamicService *dynamicService = [[TRADynamicService alloc] initWithEntity:traDynamicServiceEntity insertIntoManagedObjectContext:self.managedObjectContext];
        dynamicService.serviceID = dynamicServiceModel.dynamicServiceID;
        dynamicService.serviceIcon = dynamicServiceModel.dynamicServiceIcon;
        dynamicService.serviceNeedAuth = @(dynamicServiceModel.dynamicServiceNeedAuth) ;
        dynamicService.serviceNameAR = [dynamicServiceModel.dynamicServiceName valueForKey:@"AR"];
        dynamicService.serviceNameEN = [dynamicServiceModel.dynamicServiceName valueForKey:@"EN"];
        dynamicService.serviceOrder = @(1000 + i);
        dynamicService.serviceInquiriesCategory = dynamicServiceModel.dynamicServiceInquiriesCategory;
        for (TRADynamicService *pastDynamicService in favoriteDynamicService) {
            if ([pastDynamicService.serviceID isEqualToString:dynamicService.serviceID]) {
                dynamicService.serviceIsFavorite = @(YES);
                dynamicService.serviceFavoriteOrder = [pastDynamicService.serviceFavoriteOrder copy];
            }
        }
        if (fatherDynamicService) {
            [fatherDynamicService addRelationshipChildObject:dynamicService];
            dynamicService.relationshipFahter = fatherDynamicService;
        }
        if (dynamicServiceModel.dynamicServiceItems.count) {
            [self saveDynamicServicesListToDB:dynamicServiceModel.dynamicServiceItems relationshipFather:dynamicService favoriteDynamicServiceArray:favoriteDynamicService];
        }
    }
}

#pragma mark - CoreDataStack

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"TRAService" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TRAService.sqlite"];
    
    NSDictionary *lightMigrationoptions = @{
                                            NSMigratePersistentStoresAutomaticallyOption: @(YES),
                                            NSInferMappingModelAutomaticallyOption: @(YES)
                                            };
    
    [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:lightMigrationoptions error:nil];
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext\

{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

@end
