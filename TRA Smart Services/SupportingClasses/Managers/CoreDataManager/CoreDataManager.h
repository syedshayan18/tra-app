//
//  CoreDataManager.h
//  TRA Smart Services
//
//  Created by Admin on 19.09.15.
//

#import <CoreData/CoreData.h>

@interface CoreDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (instancetype)sharedManager;

- (void)saveContext;

- (NSArray *)fetchDynamicServiceList;
- (NSArray *)fetchServiceList;
- (NSArray *)fetchFavouriteServiceList;
- (NSArray *)fetchFavoriteDynamicServiceList;

- (NSArray *)allDBSortedArray;
- (NSArray *)favoriteDBSortedArray;

- (void)prepareCoreDataWintResponse:(NSDictionary *)response;
- (NSArray *)dynamicServiceModelArray;

@end
