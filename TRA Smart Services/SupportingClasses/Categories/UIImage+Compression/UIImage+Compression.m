//
//  UIImage+Compression.m
//  TRA Smart Services
//
//  Created by Admin on 26.04.16.
//  Copyright © 2016 . All rights reserved.
//

#import "UIImage+Compression.h"

static CGFloat const OneMByte = 1000000.;

@implementation UIImage (Compression)

+ (UIImage *)compressImage:(UIImage *)sourceImage compressLessMByte:(CGFloat)compressData
{
    NSData *imageData = [[NSData alloc] init];
    for (float compression = 1.0; compression >= 0.0; compression -= .1) {
        imageData = UIImageJPEGRepresentation(sourceImage, compression);
        NSInteger imageLength = imageData.length;
        if (imageLength < compressData * OneMByte) {
            break;
        }
    }
    return [UIImage imageWithData:imageData];
}

+ (NSData *)dataCompressImage:(UIImage *)sourceImage compressLessMByte:(CGFloat)compressData
{
    NSData *imageData = [[NSData alloc] init];
    for (float compression = 1.0; compression >= 0.0; compression -= .1) {
        imageData = UIImageJPEGRepresentation(sourceImage, compression);
        NSInteger imageLength = imageData.length;
        if (imageLength < compressData * OneMByte) {
            break;
        }
    }
    return imageData;
}

@end
