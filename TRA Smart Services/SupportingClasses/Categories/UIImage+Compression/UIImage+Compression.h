//
//  UIImage+Compression.h
//  TRA Smart Services
//
//  Created by Admin on 26.04.16.
//  Copyright © 2016 . All rights reserved.
//

@interface UIImage (Compression)

+ (UIImage *)compressImage:(UIImage *)sourceImage compressLessMByte:(CGFloat)compressData;
+ (NSData *)dataCompressImage:(UIImage *)sourceImage compressLessMByte:(CGFloat)compressData;

@end
