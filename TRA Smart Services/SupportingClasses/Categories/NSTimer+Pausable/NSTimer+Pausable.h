//
//  NSTimer+Pausable.h
//  TRA Smart Services
//
//  Created by Admin on 29.05.16.
//  Copyright © 2016 . All rights reserved.
//

@interface NSTimer (Pausable)

-(void)pause;
-(void)resume;

@end
