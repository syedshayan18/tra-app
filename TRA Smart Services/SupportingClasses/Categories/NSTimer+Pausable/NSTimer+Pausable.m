//
//  NSTimer+Pausable.m
//  TRA Smart Services
//
//  Created by Admin on 29.05.16.
//  Copyright © 2016 . All rights reserved.
//

#import "NSTimer+Pausable.h"

@implementation NSTimer (Pausable)

-(void)pause
{
    [self setFireDate:[NSDate dateWithTimeIntervalSinceNow:[[NSDate distantFuture] timeIntervalSinceNow]]];
}

-(void)resume
{
    [self setFireDate:[NSDate date]];
}

@end
