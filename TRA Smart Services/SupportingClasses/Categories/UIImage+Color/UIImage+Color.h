//
//  UIImage+Color.h
//  TRA Smart Services
//
//  Created by Admin on 11.12.15.
//  Copyright © 2015 . All rights reserved.
//

@interface UIImage (Color)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
