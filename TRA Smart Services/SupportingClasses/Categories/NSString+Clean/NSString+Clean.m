//
//  NSString+Clean.m
//  TRA Smart Services
//
//  Created by Admin on 26.10.15.
//

#import "NSString+Clean.h"

@implementation NSString (Clean)

#pragma mark - Private

- (NSString *)cleanedHtmlString
{
    NSString *cleanedString = self;
    //2 step due to not replacing some of characters in attributes string
    cleanedString = [cleanedString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    cleanedString = [cleanedString stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    cleanedString = [cleanedString stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    cleanedString = [cleanedString stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    cleanedString = [cleanedString stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    cleanedString = [cleanedString stringByReplacingOccurrencesOfString:@"&rsquo;" withString:@"`"];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData:[cleanedString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]
                                            options:@{
                                                      NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                                                      NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)
                                                      }
                                            documentAttributes:nil error:nil];
    cleanedString = attributedString.string;
    
    return cleanedString;
}

@end