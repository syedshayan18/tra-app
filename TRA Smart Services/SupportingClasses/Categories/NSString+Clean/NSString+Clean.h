//
//  NSString+Clean.h
//  TRA Smart Services
//
//  Created by Admin on 26.10.15.
//

@interface NSString (Clean)

- (NSString *)cleanedHtmlString;

@end
