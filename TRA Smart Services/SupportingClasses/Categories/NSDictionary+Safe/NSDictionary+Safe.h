//
//  NSDictionary+Safe.h
//  PTSquared
//
//  Created by Admin on 13.05.15.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Safe)

- (NSDictionary *)removeNullValues;

@end
