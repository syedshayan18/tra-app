//
//  TableViewCell+Base.m
//  TRA Smart Services
//
//  Created by Admin on 18.04.16.
//  Copyright © 2016 . All rights reserved.
//

#import "TableViewCell+Base.h"

@implementation UITableViewCell (Base)

+ (NSString *)identifier
{
    return NSStringFromClass(self.class);
}

+ (UINib *)nibName
{
    return [UINib nibWithNibName:self.identifier bundle:nil];
}

+ (void)registerWithTableView:(UITableView *)tableView
{
    [tableView registerNib:[self class].nibName forCellReuseIdentifier:[self class].identifier];
}

@end
