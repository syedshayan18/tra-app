//
//  TableViewCell+Base.h
//  TRA Smart Services
//
//  Created by Admin on 18.04.16.
//  Copyright © 2016 . All rights reserved.
//

@interface UITableViewCell (Base)

+ (NSString *)identifier;
+ (UINib *)nibName;
+ (void)registerWithTableView:(UITableView *)tableView;

@end
