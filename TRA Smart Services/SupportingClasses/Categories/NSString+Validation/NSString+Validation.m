//
//  NSString+Validation.m
//  TRA Smart Services
//
//  Created by Admin on 28.08.15.
//

#import "NSString+Validation.h"

@implementation NSString (Validation)

- (BOOL)isValidEmailUseHardFilter:(BOOL)filter
{
    BOOL stricterFilter = filter;
    NSString *stricterFilterString = @"[+A-Z0-9a-z\\._%+-]+@{1}([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isValidPhoneNumber
{
    NSString *allowedSymbols = @"[+,0-9]{4,}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidNumber
{
    NSString *allowedSymbols = @"^[0-9]*$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidName
{
    NSString *allowedSymbols = @"[a-zA-z]+([ '-][a-zA-Z]+)*$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidNameArabicAndEnglish
{
    NSString *allowedSymbols = @"[a-zA-z\\p{Arabic}\\-\\ ]*$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidNameArabic
{
    NSString *allowedSymbols = @"[\\p{Arabic}\\-\\ ]*$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidString
{
    NSString *allowedSymbols = @"^[a-zA-Z]*$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:[[self stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"\n" withString:@""]];
}

- (BOOL)isValidURL
{
    NSString *allowedSymbols = @"((http|ftp|https):\\/\\/)?[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidURLDamainAE
{
    NSString *allowedSymbols = @"((http|ftp|https):\\/\\/)?[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"(SELF MATCHES %@) AND (SELF like[c] %@)", allowedSymbols, @"*.ae"];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidUserName
{
    NSString *allowedSymbols = @"[.+A-Za-z0-9-]+";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidIDEmirates
{
    NSString *allowedSymbols = @"[0-9]{3}[-]{1}[0-9]{4}[-]{1}[0-9]{7}[-]{1}[0-9]{1}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidStateCode
{
    NSString *allowedSymbols = @"[0-9]{1}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidRating
{
    NSString *allowedSymbols = @"[0-5]{1}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidPasswordFormat
{
    NSString *stricterFilterString = @"^.*(?=.{8,32})(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).*$";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    
    return [passwordTest evaluateWithObject:self];
}

- (BOOL)isValidIMEI
{
    NSString *allowedSymbols = @"[0-9]{25}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", allowedSymbols];
    return [test evaluateWithObject:self];
}

@end
