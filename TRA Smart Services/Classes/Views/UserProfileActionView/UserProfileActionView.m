//
//  UserProfileActionView.m
//  TRA Smart Services
//
//  Created by Admin on 09.09.15.
//

#import "UserProfileActionView.h"

@interface UserProfileActionView()

@property (weak, nonatomic) IBOutlet UILabel *cancelLabel;
@property (weak, nonatomic) IBOutlet UILabel *resetLabel;
@property (weak, nonatomic) IBOutlet UILabel *saveChangesLabel;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@end

@implementation UserProfileActionView

#pragma mark - LifeCycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder nibName:NSStringFromClass([self class])];
    [self prepareFillEffectButton];
    return self;
}

#pragma mark - Public

- (void)updateColorUI
{
    if ([DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite ) {
        self.cancelLabel.textColor = [UIColor blackColor];
        self.resetLabel.textColor = [UIColor blackColor];
        self.saveChangesLabel.textColor = [UIColor blackColor];
    } else {
        self.cancelLabel.textColor = [UIColor redColor];
        self.resetLabel.textColor = [UIColor defaultBlueColor];
        self.saveChangesLabel.textColor = [UIColor defaultGreenColor];
    }
    [self.cancelButton setImage:[self prepareColorImage:[UIImage imageNamed:@"btn_canc"]] forState:UIControlStateNormal];
    [self.resetButton setImage:[self prepareColorImage:[UIImage imageNamed:@"btn_reset"]] forState:UIControlStateNormal];
    [self.saveButton setImage:[self prepareColorImage:[UIImage imageNamed:@"btn_chang"]] forState:UIControlStateNormal];
}

- (void)localizeUI
{
    self.cancelLabel.text = dynamicLocalizedString(@"userProfileActionView.cancel");
    self.resetLabel.text = dynamicLocalizedString(@"userProfileActionView.reset");
    self.saveChangesLabel.text = dynamicLocalizedString(@"userProfileActionView.save");
}

- (void)setRTLStyle
{
    [self transformViewElements:TRANFORM_3D_SCALE];
}

- (void)setLTRStyle
{
    [self transformViewElements:CATransform3DIdentity];
}

#pragma mark - Action

- (IBAction)cancelButtonTapped:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(buttonCancelDidTapped)]){
        [self.delegate buttonCancelDidTapped];
    }
}

- (IBAction)resetButtonTapped:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(buttonResetDidTapped)]){
        [self.delegate buttonResetDidTapped];
    }
}

- (IBAction)saveButtonTapped:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(buttonSaveDidTapped)]){
        [self.delegate buttonSaveDidTapped];
    }
}

#pragma mark - Private

- (void)transformViewElements:(CATransform3D)transfrom
{
    self.layer.transform = transfrom;
    self.saveChangesLabel.layer.transform = transfrom;
    self.resetLabel.layer.transform = transfrom;
    self.cancelLabel.layer.transform = transfrom;
    self.saveButton.layer.transform = transfrom;
}

- (UIImage *)prepareColorImage:(UIImage *)image
{
    return [DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite ? [[BlackWhiteConverter sharedManager] convertedBlackAndWhiteImage:image] : image;
}

- (void)prepareFillEffectButton
{
    [self.saveButton setImage:[self prepareColorImage:[UIImage imageNamed:@"ic_save_act"]] forState:UIControlStateHighlighted];
    [self.cancelButton setImage:[self prepareColorImage:[UIImage imageNamed:@"ic_cancel_act"]] forState:UIControlStateHighlighted];
    [self.resetButton setImage:[self prepareColorImage:[UIImage imageNamed:@"ic_reset_act"]] forState:UIControlStateHighlighted];
}

@end
