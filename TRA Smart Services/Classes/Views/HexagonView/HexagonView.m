//
//  HexagonView.m
//  testPentagonCells
//
//  Created by Admin on 30.07.15.
//

#import "HexagonView.h"

static CGFloat ShadowOffset = 1.f;
static CGFloat ShadowRadius = 1.f;


@interface HexagonView()

@property (strong, nonatomic) CAShapeLayer *polygonLayer;
@property (strong, nonatomic) CAGradientLayer *gradientLayer;

@property (assign, nonatomic) BOOL needToDrawFradient;
@property (strong, nonatomic) NSMutableArray *colorsForGradient;

@end

@implementation HexagonView

#pragma mark - LifeCycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self prepareColors];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self preparePolygonLayer];
    if (self.colorsForGradient.count) {
        [self setGradientWithTopColors:self.colorsForGradient];
    }
}

#pragma mark - Public

- (void)removeAllDrawings
{
    [self.gradientLayer removeFromSuperlayer];
    [self.polygonLayer removeFromSuperlayer];
    [self.colorsForGradient removeAllObjects];
}

- (void)setGradientWithTopColors:(NSArray *)colors
{
    if (!self.colorsForGradient.count) {
        self.colorsForGradient = [colors mutableCopy];
    }
    
    [self.gradientLayer removeFromSuperlayer];
    
    self.gradientLayer = [CAGradientLayer layer];
    self.gradientLayer.colors = self.colorsForGradient;
    self.gradientLayer.startPoint = CGPointMake(0.5, 0.2);
    self.gradientLayer.endPoint = CGPointMake(0.5, 1);
    self.gradientLayer.frame = self.bounds;
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.fillColor = [UIColor blackColor].CGColor;
    maskLayer.frame = self.bounds;
    maskLayer.path = [self hexagonPathWithOffset:ShadowRadius].CGPath;
    
    self.gradientLayer.mask = maskLayer;
    [self.layer addSublayer:self.gradientLayer];
    
    [self prepareShadowWithLayer:self.polygonLayer];
}

#pragma mark - Private

- (void)prepareShadowWithLayer:(CAShapeLayer *)layer
{
    [layer setShadowPath:[self hexagonPathWithOffset:ShadowRadius + ShadowOffset].CGPath];
    [layer setShadowOffset:CGSizeMake(ShadowOffset, ShadowOffset)];
    [layer setShadowRadius:ShadowRadius];
    [layer setShadowOpacity:0.3f];
}

- (void)preparePolygonLayer
{
    [self.polygonLayer removeFromSuperlayer];
    
    self.polygonLayer = [CAShapeLayer layer];
    self.polygonLayer.path = [self hexagonPathWithOffset:ShadowRadius + ShadowOffset].CGPath;
    self.polygonLayer.strokeColor = self.viewStrokeColor.CGColor;
    self.polygonLayer.fillColor = self.viewFillColor.CGColor;
    [self prepareShadowWithLayer:self.polygonLayer];

    self.layer.masksToBounds = YES;
    [self.layer addSublayer:self.polygonLayer];
}

- (UIBezierPath *)hexagonPathWithOffset:(CGFloat)offset
{
    UIBezierPath *hexagonPath = [UIBezierPath bezierPath];
    
    CGRect hexagonRect = CGRectMake(self.bounds.origin.x + offset, self.bounds.origin.y + offset, self.bounds.size.width - 2 * offset, self.bounds.size.height - 2 * offset);
    
    [hexagonPath moveToPoint:CGPointMake(CGRectGetMidX(hexagonRect), CGRectGetMinY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMaxX(hexagonRect), CGRectGetHeight(hexagonRect) * 0.25 + CGRectGetMinY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMaxX(hexagonRect), CGRectGetHeight(hexagonRect) * 0.75 + CGRectGetMinY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMidX(hexagonRect), CGRectGetMaxY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMinX(hexagonRect), CGRectGetHeight(hexagonRect) * 0.75 + CGRectGetMinY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMinX(hexagonRect), CGRectGetHeight(hexagonRect) * 0.25 + CGRectGetMinY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMidX(hexagonRect), CGRectGetMinY(hexagonRect))];

    return hexagonPath;
}

- (void)prepareColors
{
    if (!self.viewFillColor) {
        self.viewFillColor = [UIColor clearColor];
    }
    if (!self.viewStrokeColor) {
        self.viewStrokeColor = [UIColor clearColor];
    }
}

@end
