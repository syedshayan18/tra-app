//
//  TRARefreshContentView.m
//  TRA Smart Services
//
//  Created by Admin  on 26.10.15.
//  Copyright © 2015 . All rights reserved.
//

#import "TRARefreshContentView.h"

static CGFloat const CoefficientSize = 0.65;

@interface TRARefreshContentView()

@property (strong, nonatomic) CAShapeLayer *hexLayer;
@property (strong, nonatomic) CAShapeLayer *tailLayer;
@property (strong, nonatomic) CAShapeLayer *backgroundLayer;
@property (strong, nonatomic) CAShapeLayer *logoLayer;

@property (assign, nonatomic) BOOL isAnimationStarted;

@end

@implementation TRARefreshContentView

#pragma mark - Public

- (void)startAnimations
{
    self.isAnimationStarted = YES;
    CABasicAnimation *pathAnimation = [self strokeEndAnimation];
    CABasicAnimation *pathAnimation2 = [self strokeStartAnimation];
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[pathAnimation, pathAnimation2];
    group.repeatCount = MAXFLOAT;
    group.duration = 2;
    
    self.backgroundLayer = [self layerWithColor: self.bacgroungLaeyerColor ? self.bacgroungLaeyerColor : [UIColor lightGrayBorderColor] inRect:self.hexLayer.bounds progress:1.];
    self.backgroundLayer.position = self.hexLayer.position;
    [self.layer insertSublayer:self.backgroundLayer below:self.hexLayer];
    
    self.tailLayer = [self layerWithColor:[UIColor clearColor] inRect:self.hexLayer.bounds progress:1.];
    self.tailLayer.position = self.hexLayer.position;
    [self.layer insertSublayer:self.tailLayer above:self.hexLayer];
    
    [self.hexLayer addAnimation:group forKey:nil];
}

- (void)stopAnimation
{
    [self.hexLayer removeFromSuperlayer];
    [self.tailLayer removeFromSuperlayer];
    [self.backgroundLayer removeFromSuperlayer];
    [self.logoLayer removeFromSuperlayer];

    self.isAnimationStarted = NO;
}

- (void)setOpacityValue:(CGFloat)value
{
    if (!self.isAnimationStarted) {
        [self prepareLayersWithProgress:value];
    }
}

#pragma mark - Private

- (CAShapeLayer *)layerWithColor:(UIColor *)color inRect:(CGRect)rect progress:(CGFloat)value
{
    CAShapeLayer *maskWhiteLayer = [CAShapeLayer layer];
    maskWhiteLayer.frame = rect;
    maskWhiteLayer.strokeColor = color.CGColor;
    maskWhiteLayer.fillColor = [UIColor clearColor].CGColor;
    UIBezierPath *path = [self hexagonPathForRect:rect];
    maskWhiteLayer.path = path.CGPath;
    maskWhiteLayer.lineWidth = 2.f;
    [maskWhiteLayer setStrokeEnd:value];
    return maskWhiteLayer;
}

- (UIBezierPath *)hexagonPathForRect:(CGRect)hexagonRect
{
    UIBezierPath *hexagonPath = [UIBezierPath bezierPath];
    [hexagonPath moveToPoint:CGPointMake(CGRectGetMidX(hexagonRect), 0)];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMaxX(hexagonRect), CGRectGetMaxY(hexagonRect) * 0.25)];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMaxX(hexagonRect), CGRectGetMaxY(hexagonRect) * 0.75)];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMidX(hexagonRect), CGRectGetMaxY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(0, CGRectGetMaxY(hexagonRect) * 0.75)];
    [hexagonPath addLineToPoint:CGPointMake(0, CGRectGetMaxY(hexagonRect) * 0.25)];
    [hexagonPath closePath];
    hexagonPath.lineJoinStyle = kCGLineJoinRound;
    return hexagonPath;
}

- (CABasicAnimation *)strokeStartAnimation
{
    CABasicAnimation *strokeStartAnimation = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
    strokeStartAnimation.beginTime = 0.2;
    strokeStartAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    strokeStartAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    strokeStartAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    return strokeStartAnimation;
}

- (CABasicAnimation *)strokeEndAnimation
{
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    return pathAnimation;
}

- (void)prepareLayersWithProgress:(CGFloat)value
{
    [self.hexLayer removeFromSuperlayer];
    [self.tailLayer removeFromSuperlayer];
    [self.backgroundLayer removeFromSuperlayer];
    [self.logoLayer removeFromSuperlayer];
    
    CGRect loaderRect = CGRectMake(0, 0, self.frame.size.height * 0.7 * CoefficientSize, self.frame.size.height * 0.8 * CoefficientSize);
    CGPoint center = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    CGRect hexagonRect = loaderRect;
    
    UIColor *laeyerColor = self.animationLaeyerColor ? self.animationLaeyerColor : [[DynamicUIService service] currentApplicationColor];
    
    self.hexLayer = [self layerWithColor:[laeyerColor colorWithAlphaComponent:value] inRect:hexagonRect progress:value];
    self.hexLayer.position = center;
    
    self.logoLayer = [self layerWithColor:[UIColor clearColor] inRect:self.hexLayer.bounds progress:1.];
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(self.logoLayer.bounds.origin.x, self.logoLayer.bounds.origin.y, self.logoLayer.bounds.size.width * 0.7, self.logoLayer.bounds.size.height * 0.7);
    imageLayer.backgroundColor = [UIColor clearColor].CGColor;
    UIImage *content = [[UIImage imageNamed:@"ic_downld_logo"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    imageLayer.contents = (__bridge id __nullable)(content).CGImage;
    imageLayer.contentsGravity = kCAGravityResizeAspect;
    imageLayer.opacity = value;
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.frame = imageLayer.frame;
    layer.backgroundColor = laeyerColor.CGColor;
    layer.mask = imageLayer;
    layer.position = self.logoLayer.position;
    
    [self.logoLayer addSublayer:layer];
    self.logoLayer.position = self.logoLayer.position;
    [self.hexLayer addSublayer:self.logoLayer];
    
    [self.layer addSublayer:self.hexLayer];
}

- (void)addConstraintsForView:(UIView *)view
{
    [self addConstraints:@[[NSLayoutConstraint constraintWithItem:view
                                                        attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0
                                                         constant:0],
                           [NSLayoutConstraint constraintWithItem:view
                                                        attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                         constant:0],
                           [NSLayoutConstraint constraintWithItem:view
                                                        attribute:NSLayoutAttributeLeft
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self attribute:NSLayoutAttributeLeft
                                                       multiplier:1.0
                                                         constant:0],
                           [NSLayoutConstraint constraintWithItem:view
                                                        attribute:NSLayoutAttributeRight
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self attribute:NSLayoutAttributeRight
                                                       multiplier:1.0
                                                         constant:0]
                           ]];
}

@end