//
//  TRARefreshContentView.h
//  TRA Smart Services
//
//  Created by Admin on 26.10.15.
//  Copyright © 2015 . All rights reserved.
//

@interface TRARefreshContentView : UIView

@property (strong, nonatomic) UIColor *bacgroungLaeyerColor;
@property (strong, nonatomic) UIColor *animationLaeyerColor;

- (void)setOpacityValue:(CGFloat)value;
- (void)startAnimations;
- (void)stopAnimation;

@end
