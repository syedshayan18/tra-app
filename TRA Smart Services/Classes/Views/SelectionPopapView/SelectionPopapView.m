//
//  SelectionPopapView.m
//  MenuPopapView
//
//  Created by Roman Vizenko on 15.03.16.
//  Copyright © 2016 Roman Vizenko. All rights reserved.
//

#import "SelectionPopapView.h"
#import "SelectionPopapTableViewCell.h"
#import "SelectionPopapCustomTableViewCell.h"

static CGFloat const OpacityView = 0.4;
static CGFloat const TableViewOffset = 15.;
static CGFloat const HeightTableViewCell = 40.;

static NSString *const SelectionPopapTableViewCellIdentifier = @"SelectionPopapTableViewCell";
static NSString *const SelectionPopapCustomTableViewCellIdentifier = @"SelectionPopapCustomTableViewCell";

@interface SelectionPopapView()

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *shadowView;
@property (assign, nonatomic) CGFloat positionYStartAnimation;

@end

@implementation SelectionPopapView

#pragma mark - LifeCycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        [self prepareBase];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self prepareBase];
    }
    return self;
}

#pragma mark - Puplic

+ (SelectionPopapView *)addSelectionPopapMenuToViewController:(UIViewController *)viewController dataSource:(NSArray *)array positionYStartAnimation:(CGFloat)positionY
{
    SelectionPopapView *selectionPopapView = [[SelectionPopapView alloc] initWithFrame:viewController.view.frame];
    selectionPopapView.dataSource = array;
    selectionPopapView.positionYStartAnimation = positionY;
    
    [viewController.view addSubview:selectionPopapView];
    selectionPopapView.delegate = (id<SelectionPopapViewDelegate>)viewController;
    
    [selectionPopapView addViewsToSuperView];
    
    selectionPopapView.translatesAutoresizingMaskIntoConstraints = NO;
    [selectionPopapView addConstraintsForView:selectionPopapView superView:selectionPopapView.superview];
    
    [selectionPopapView animationVizibleTableViewHidden:NO];
    [selectionPopapView.layer addAnimation:[selectionPopapView fadeAnimFromValue:0. to:1.] forKey:nil];
    selectionPopapView.alpha = 1.;
    return selectionPopapView;
}

- (void)removeFromSuperviewAnimation:(BOOL)animation
{
    if (animation) {
        [self animationRemoveTableView];
    } else {
        [self removeFromSuperview];
    }
}

#pragma mark - Touch Methods

 - (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSSet *allTouches = [event allTouches];
    UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
    UIView *view = (UIView *)touch.view;
    if (![view isKindOfClass:[UITableView class]]) {
        [self animationRemoveTableView];
    }
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HeightTableViewCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row) {
        cell = [tableView dequeueReusableCellWithIdentifier:SelectionPopapTableViewCellIdentifier forIndexPath:indexPath];
        cell.textLabel.text = dynamicLocalizedString(self.dataSource[indexPath.row]);
        cell.textLabel.textColor = [DynamicUIService service].currentApplicationColor;
        cell.textLabel.textAlignment = [DynamicUIService service].language == LanguageTypeArabic ? NSTextAlignmentRight : NSTextAlignmentLeft;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:SelectionPopapCustomTableViewCellIdentifier forIndexPath:indexPath];
        ((SelectionPopapCustomTableViewCell *)cell).dynamicElementLabel.text = dynamicLocalizedString(self.dataSource[indexPath.row]);
        
        if ([DynamicUIService service].language == LanguageTypeArabic) {
            cell.contentView.layer.transform = CATransform3DMakeScale(-1, 1, 1);
            ((SelectionPopapCustomTableViewCell *)cell).dynamicElementLabel.layer.transform = CATransform3DMakeScale(-1, 1, 1);
            ((SelectionPopapCustomTableViewCell *)cell).dynamicElementLabel.textAlignment = NSTextAlignmentRight;
        } else {
            cell.contentView.layer.transform = CATransform3DIdentity;
            ((SelectionPopapCustomTableViewCell *)cell).dynamicElementLabel.layer.transform = CATransform3DIdentity;
            ((SelectionPopapCustomTableViewCell *)cell).dynamicElementLabel.textAlignment = NSTextAlignmentLeft;
        }
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(SelectionPopapView:selectedIndex:)]) {
        [self.delegate SelectionPopapView:self selectedIndex:indexPath.row];
    }
    [self animationRemoveTableView];
}

#pragma mark - UIAnimatinDelegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.layer animationForKey:@"removePopap"]) {
        [self removeFromSuperview];
    }
}

#pragma mark - Private

- (void)prepareBase
{
    self.backgroundColor = [[DynamicUIService service].currentApplicationColor colorWithAlphaComponent:OpacityView];
    self.alpha = 0.0;
}

- (void)prepareTableView
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 10, 10) style:UITableViewStylePlain];
    [self.tableView registerNib:[UINib nibWithNibName:@"SelectionPopapTableViewCell" bundle:nil] forCellReuseIdentifier:SelectionPopapTableViewCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"SelectionPopapCustomTableViewCell" bundle:nil] forCellReuseIdentifier:SelectionPopapCustomTableViewCellIdentifier];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithRed:246./255. green:246./255. blue:246./255. alpha:1.];
    self.tableView.layer.cornerRadius = 5.;
    [self.tableView setSeparatorColor:[DynamicUIService service].currentApplicationColor];
}

- (void)addViewsToSuperView
{
    [self prepareTableView];
    [self prepareShadowView];
    
    CGFloat heightTableView = self.dataSource.count * HeightTableViewCell;
    self.tableView.scrollEnabled = NO;
    if (heightTableView > 0.75 * self.frame.size.height) {
        heightTableView = 0.75 * self.frame.size.height;
        self.tableView.scrollEnabled = YES;
    }
    
    [self prepareShadowView];
    self.shadowView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.shadowView];
    [self addConstraintsForView:self.shadowView height:heightTableView];
    
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.shadowView addSubview:self.tableView];
    [self addConstraintsForView:self.tableView superView:self.shadowView];
    
    [self layoutIfNeeded];
}

- (void)prepareShadowView
{
    self.shadowView = [[UIView alloc] init];
    self.shadowView.backgroundColor = [UIColor whiteColor];
    self.shadowView.layer.cornerRadius = 5.;
    self.shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.shadowView.layer.shadowOpacity = 0.4;
    self.shadowView.layer.shadowRadius = 2.5;
    self.shadowView.layer.shadowOffset = CGSizeMake(1.5, 1.5);
    self.shadowView.layer.masksToBounds = NO;
}

#pragma mark - Animation

- (CABasicAnimation *)fadeAnimFromValue:(CGFloat)fromValue to:(CGFloat)toValue
{
    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeAnimation.fromValue = @(fromValue);
    fadeAnimation.toValue = @(toValue);
    fadeAnimation.duration = 0.3f;
    fadeAnimation.removedOnCompletion = NO;
    fadeAnimation.timingFunction = [CAMediaTimingFunction functionWithName:fromValue ? kCAMediaTimingFunctionEaseOut : kCAMediaTimingFunctionEaseIn];
    fadeAnimation.delegate = self;
    
    return fadeAnimation;
}

- (void)animationVizibleTableViewHidden:(BOOL)hidden
{
    CGFloat scale = 40. / self.tableView.frame.size.height;
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale.y"];
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.repeatCount = 0;
    animation.values = hidden ? @[@1., @(scale)] : @[@(scale) , @1.05, @1.];

    CAKeyframeAnimation *animationPosition = [CAKeyframeAnimation animationWithKeyPath:@"position.y"];
    animationPosition.fillMode = kCAFillModeForwards;
    animationPosition.removedOnCompletion = NO;
    animationPosition.repeatCount = 0;
    animationPosition.values = hidden ? @[ @(self.center.y), @(self.positionYStartAnimation)] : @[@(self.positionYStartAnimation), @(self.center.y ), @(self.center.y)];

    CAAnimationGroup *group = [CAAnimationGroup animation];
    [group setAnimations:@[animation, animationPosition]];
    group.duration = hidden ? 0.3 : 0.4;
    group.timingFunction = [CAMediaTimingFunction functionWithName: hidden ? kCAMediaTimingFunctionEaseOut : kCAMediaTimingFunctionEaseInEaseOut];
    [self.shadowView.layer addAnimation:group forKey:nil];
}

- (void)animationRemoveTableView
{
    [self animationVizibleTableViewHidden:YES];
    
    [self.layer addAnimation: [self fadeAnimFromValue:1. to:0.] forKey:@"removePopap"];
    self.layer.opacity = 0.;
}

#pragma mark - addConstraints

- (void)addConstraintsForView:(UIView *)subView superView:(UIView *)superView
{
    [superView addConstraints:@[[NSLayoutConstraint constraintWithItem:subView
                                                        attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:superView attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0
                                                         constant:0],
                           [NSLayoutConstraint constraintWithItem:subView
                                                        attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:superView attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                         constant:0],
                           [NSLayoutConstraint constraintWithItem:subView
                                                        attribute:NSLayoutAttributeLeft
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:superView attribute:NSLayoutAttributeLeft
                                                       multiplier:1.0
                                                         constant:0],
                           [NSLayoutConstraint constraintWithItem:subView
                                                        attribute:NSLayoutAttributeRight
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:superView attribute:NSLayoutAttributeRight
                                                       multiplier:1.0
                                                         constant:0]
                           ]];
}

- (void)addConstraintsForView:(UIView *)view height:(CGFloat)heightTableViewConstraint
{
    [view addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0 constant:heightTableViewConstraint]
     ];
    [self addConstraints:@[[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeCenterY
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self attribute:NSLayoutAttributeCenterY
                                                                 multiplier:1.0 constant:0],
                                     [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeLeft
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self attribute:NSLayoutAttributeLeft
                                                                 multiplier:1.0 constant:TableViewOffset],
                                     [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeRight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self attribute:NSLayoutAttributeRight
                                                                 multiplier:1.0 constant:-TableViewOffset]
                                     ]];
}

@end
