//
//  SelectionPopapView.h
//  MenuPopapView
//
//  Created by Roman Vizenko on 15.03.16.
//  Copyright © 2016 Roman Vizenko. All rights reserved.
//

@class SelectionPopapView;
@protocol SelectionPopapViewDelegate <NSObject>

@optional

- (void)SelectionPopapView:(SelectionPopapView *)selectionPopapView  selectedIndex:(NSInteger)index;

@end

@interface SelectionPopapView : UIView <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSArray *dataSource;

@property (weak, nonatomic) id<SelectionPopapViewDelegate> delegate;

+ (SelectionPopapView *)addSelectionPopapMenuToViewController:(UIViewController *)viewController dataSource:(NSArray *)array positionYStartAnimation:(CGFloat)positionY;
- (void)removeFromSuperviewAnimation:(BOOL)animation;

@end
