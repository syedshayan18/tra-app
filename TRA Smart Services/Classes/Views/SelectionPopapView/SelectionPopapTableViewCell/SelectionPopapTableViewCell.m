//
//  SelectionPopapTableViewCell.m
//  MenuPopapView
//
//  Created by Roman Vizenko on 15.03.16.
//  Copyright © 2016 Roman Vizenko. All rights reserved.
//

#import "SelectionPopapTableViewCell.h"

@implementation SelectionPopapTableViewCell

- (void)prepareForReuse
{
    self.textLabel.text = @"";
}

@end
