//
//  SelectionPopapCustomTableViewCell.m
//  MenuPopapView
//
//  Created by Roman Vizenko on 15.03.16.
//  Copyright © 2016 Roman Vizenko. All rights reserved.
//

#import "SelectionPopapCustomTableViewCell.h"

@implementation SelectionPopapCustomTableViewCell

- (void)prepareForReuse
{
    self.dynamicElementLabel.text = @"";
}

@end
