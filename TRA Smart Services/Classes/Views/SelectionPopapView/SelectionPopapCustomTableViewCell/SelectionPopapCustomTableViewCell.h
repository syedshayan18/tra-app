//
//  SelectionPopapCustomTableViewCell.h
//  MenuPopapView
//
//  Created by Roman Vizenko on 15.03.16.
//  Copyright © 2016 Roman Vizenko. All rights reserved.
//

@interface SelectionPopapCustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dynamicElementLabel;

@end
