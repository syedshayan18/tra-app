//
//  RatingView.m
//  TRA Smart Services
//
//  Created by Admin on 9/2/15.
//

#import "RatingView.h"
#import "PlaceholderTextView.h"

static CGFloat const HeightForToolbars = 44.f;

@interface RatingView()

@property (weak, nonatomic) IBOutlet PlaceholderTextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIButton *badButton;
@property (weak, nonatomic) IBOutlet UIButton *neutralButton;
@property (weak, nonatomic) IBOutlet UIButton *happyButton;

@property (assign, nonatomic) NSInteger ratingSmile;

@end

@implementation RatingView

#pragma mark - LifeCycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder nibName:NSStringFromClass([self class])];
    
    [self prepareUI];
    [self prepareLocalizeUI];
    [self prepareNotification];
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSTextAlignment textAligment = [DynamicUIService service].language == LanguageTypeArabic ? NSTextAlignmentRight : NSTextAlignmentLeft;
    self.descriptionTextView.textAlignment = textAligment;
}

- (void)dealloc
{
    [self removeNotifications];
}

#pragma mark - Public

- (void)makeActivKeyboard
{
    [self.descriptionTextView becomeFirstResponder];
}

#pragma mark - Accessor

- (void)setKeyboardKeyDone:(BOOL)keyboardKeyDone
{
    _keyboardKeyDone = keyboardKeyDone;
    if (keyboardKeyDone) {
        [self configureKeyboardButtonDone];
    }
}

#pragma mark - Action

- (IBAction)setRating:(id)sender
{
    self.ratingSmile = [sender tag];
    //btn_bad
    [self.badButton setImage:[UIImage imageNamed:@"sad_inactive"] forState:UIControlStateNormal];
    //btn_neutr
    [self.neutralButton setImage:[UIImage imageNamed:@"normal_inactive"] forState:UIControlStateNormal];
    //btn_good
    [self.happyButton setImage:[UIImage imageNamed:@"happy_inactive"] forState:UIControlStateNormal];
    
    switch ([sender tag]) {
        case 1:{
           // btn_bad_act
            [self.badButton setImage:[UIImage imageNamed:@"sad_active"] forState:UIControlStateNormal];
            break;
        }
        case 2:{
            //btn_neut_act
            [self.neutralButton setImage:[UIImage imageNamed:@"normal_active"] forState:UIControlStateNormal];
            break;
        }
        case 3:{
            //btn_good_act
            [self.happyButton setImage:[UIImage imageNamed:@"happy_active"] forState:UIControlStateNormal];
            break;
        }
    }
}

- (IBAction)tappedSendButton:(id)sender
{
    if (self.ratingSmile) {
        if (_delegate && [_delegate respondsToSelector:@selector(ratingChanged:description:)]) {
            [_delegate ratingChanged:self.ratingSmile description:self.descriptionTextView.text];
        }
    } else {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.ratingView.NoSelectedSmile")];
    }
}

- (void)doneButtonTapped
{
    [self.descriptionTextView resignFirstResponder];
}

#pragma mark - Private

- (void)prepareLocalizeUI
{
    self.descriptionTextView.placeholder = dynamicLocalizedString(@"ratingView.descriptionTextView.placeholder");
    self.chooseRating.text = dynamicLocalizedString(@"ratingView.chooseRating.text");
    [self.sendButton setTitle:dynamicLocalizedString(@"ratingView.sendButton.title") forState:UIControlStateNormal];
}

- (void)prepareUI
{
    self.descriptionTextView.bottomBorderColor = [UIColor whiteColor];
    self.sendButton.tintColor = [[DynamicUIService service] currentApplicationColor];
    self.descriptionTextView.placeholderColor = [UIColor colorWithWhite:1 alpha:0.6];
}

#pragma mark - Keyboard

- (void)prepareNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    if (_delegate && [_delegate respondsToSelector:@selector(ratingkeyboardWillHide)]) {
        [_delegate ratingkeyboardWillHide];
    }
}

- (void)keyboardWillAppear:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboardRect.size.height;
    
    if (_delegate && [_delegate respondsToSelector:@selector(ratingKeyboardWillAppear:)]) {
        [_delegate ratingKeyboardWillAppear:keyboardHeight];
    }
}

- (void)configureKeyboardButtonDone
{
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, HeightForToolbars)];
    toolBar.backgroundColor = [UIColor clearColor];
    toolBar.tintColor = [[DynamicUIService service] currentApplicationColor];
    
    UIBarButtonItem *barItemCustomDone = [[UIBarButtonItem alloc] initWithTitle:dynamicLocalizedString(@"uiElement.keyboardButtom.title") style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonTapped)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolBar setItems:@[flexibleSpace, barItemCustomDone]];
    
    [self.descriptionTextView setInputAccessoryView:toolBar];
}

@end
