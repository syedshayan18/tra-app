//
//  InquiriesSelectView.h
//  TRA Smart Services
//
//  Created by Roman Vizenko on 02.02.16.
//  Copyright © 2016 Thinkmobiles. All rights reserved.
//

@protocol InquiriesSelectViewDelegate <NSObject>

@optional

- (void)inquiriesSelectedName:(NSString *)name;

@end

@interface InquiriesSelectView : BaseXibView

@property (weak, nonatomic) IBOutlet UIView *polygonView;
@property (weak, nonatomic) IBOutlet UILabel *inquiriesSelectNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *inquiriesSelectIconImageView;

@property (strong, nonatomic) NSString *inquiriesSelectName;

@property (weak, nonatomic) id <InquiriesSelectViewDelegate> delegate;

@end
