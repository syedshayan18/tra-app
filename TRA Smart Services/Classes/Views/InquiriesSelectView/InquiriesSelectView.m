//
//  InquiriesSelectView.m
//  TRA Smart Services
//
//  Created by Roman Vizenko on 02.02.16.
//  Copyright © 2016 Thinkmobiles. All rights reserved.
//

#import "InquiriesSelectView.h"

@implementation InquiriesSelectView

#pragma mark - LifeCycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        [self addGestureRecognizer];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self addGestureRecognizer];
    }
    return self;
}

#pragma mark - Method Delegate

- (void)tapGesture:(UITapGestureRecognizer *)gesture
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(inquiriesSelectedName:)]) {
        [self.delegate inquiriesSelectedName:self.inquiriesSelectName];
    }
}

#pragma mark - Private

- (void)addGestureRecognizer
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self addGestureRecognizer:tapGesture];
}

@end
