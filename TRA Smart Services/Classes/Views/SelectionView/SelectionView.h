//
//  SelectionView.h
//  TRA Smart Services
//
//  Created by Admin on 16.03.16.
//  Copyright © 2016 . All rights reserved.
//

@class SelectionView;

@protocol SelectionViewDelegate <NSObject>

@optional

- (void)tappedSelectionView:(SelectionView *)selectionView;

@end

@interface SelectionView : BaseXibView

@property (weak, nonatomic) IBOutlet UILabel *selectLabel;

@property (weak, nonatomic) id <SelectionViewDelegate> delegate;

@property (assign, nonatomic) NSInteger selectedIndex;
@property (strong, nonatomic) NSArray *dataSource;

- (void)updateUIColor;
- (void)updateLocalozed;

- (void)requiredIndicatorHidden:(BOOL)hidden;

@end
