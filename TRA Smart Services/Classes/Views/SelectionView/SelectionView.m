//
//  SelectionView.m
//  TRA Smart Services
//
//  Created by Admin on 16.03.16.
//  Copyright © 2016 . All rights reserved.
//

#import "SelectionView.h"

@interface SelectionView()

@property (weak, nonatomic) IBOutlet UIImageView *selectImage;
@property (weak, nonatomic) IBOutlet UILabel *requiredIndicatorLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

@implementation SelectionView

#pragma mark - LifeCycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder nibName:NSStringFromClass([self class])];
    if (self) {
        [self prepareUI];
    }
    return self;
}

#pragma mark - Public

- (void)requiredIndicatorHidden:(BOOL)hidden
{
    self.requiredIndicatorLabel.hidden = hidden;
}

- (void)updateUIColor
{
    self.requiredIndicatorLabel.textColor = ([DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite) ? [UIColor blackColor] : [UIColor redColor];
    UIColor *color = [DynamicUIService service].currentApplicationColor;
    self.separatorView.backgroundColor = color;
    self.selectLabel.textColor = self.selectedIndex ? color : [UIColor lightGrayColor];
    self.selectImage.tintColor = color;
}

- (void)updateLocalozed
{
    if ([DynamicUIService service].language == LanguageTypeArabic) {
        self.layer.transform = CATransform3DMakeScale(-1, 1, 1);
        self.selectLabel.layer.transform = CATransform3DMakeScale(-1, 1, 1);
        self.selectLabel.textAlignment = NSTextAlignmentRight;
    } else {
        self.layer.transform = CATransform3DIdentity;
        self.selectLabel.layer.transform = CATransform3DIdentity;
        self.selectLabel.textAlignment = NSTextAlignmentLeft;
    }
}

#pragma mark - IBAction

- (void)tappedView:(UITapGestureRecognizer *)gesture
{
    if ([self.delegate respondsToSelector:@selector(tappedSelectionView:)]) {
        [self.delegate tappedSelectionView:self];
    }
}

#pragma mark - Accessor

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    _selectedIndex = (selectedIndex >= 0 && selectedIndex < self.dataSource.count) ? selectedIndex : 0;
        
    self.selectLabel.text = dynamicLocalizedString(self.dataSource[_selectedIndex]);
    [self updateUIColor];
}

- (void)setDataSource:(NSArray *)dataSource
{
    _dataSource = dataSource;
    
    self.selectedIndex = 0;
}

#pragma mark - Private

- (void)prepareUI
{
    [self updateUIColor];
    [self updateLocalozed];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedView:)];
    [self addGestureRecognizer:tapGesture];
}

@end
