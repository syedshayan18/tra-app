//
//  DynamicHeaderView.m
//  TRA Smart Services
//
//  Created by Admin on 16.11.15.
//  Copyright © 2015 . All rights reserved.
//

#import "DynamicHeaderView.h"

@interface DynamicHeaderView()

@property (weak, nonatomic) IBOutlet UIImageView *indicatorOpenImageView;
@property (strong, nonatomic) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation DynamicHeaderView

#pragma mark - LifeCycle

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame nibName:NSStringFromClass([self class])];
    if (self) {
        [self prepareInitialParameters];
    }
    return self;
}

#pragma mark - IBAction

- (void)dynamicHeaderViewTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(tappedDynamicHeaderView:)]) {
        [self.delegate tappedDynamicHeaderView:self];
    }
    self.selectedOpenHeader = !self.selectedOpenHeader;
    [self animationSelectHeader:self.selectedOpenHeader];
}

#pragma mark - Public

- (void)setSelectedOpenHeader:(BOOL)selectedOpenHeader
{
    _selectedOpenHeader = selectedOpenHeader;
    self.indicatorOpenImageView.transform = selectedOpenHeader ? CGAffineTransformMakeRotation(M_PI) : CGAffineTransformIdentity;
}

#pragma mark - Private

- (void)prepareInitialParameters
{
    self.indicatorOpenImageView.image = [UIImage imageNamed:@"ic_drop"];
    self.backgroundColor = [DynamicUIService service].currentApplicationColor;
    [self prepareTapGestureRecognizer];
}

- (void)prepareTapGestureRecognizer
{
    if (!self.tapGestureRecognizer) {
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dynamicHeaderViewTapped:)];
        [self addGestureRecognizer: self.tapGestureRecognizer];
    }
}

- (void)animationSelectHeader:(BOOL)open
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.duration = 0.3;
    animation.fromValue = @(open ? 0.0 : - M_PI);
    animation.toValue = @(open ? - M_PI : 0.0);
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    [self.indicatorOpenImageView.layer addAnimation:animation forKey:@"180rotation"];
}

@end
