//
//  DynamicHeaderView.h
//  TRA Smart Services
//
//  Created by Admin on 16.11.15.
//  Copyright © 2015 . All rights reserved.
//

@class DynamicHeaderView;

@protocol DynamicHeaderViewDelegate <NSObject>

@optional
- (void)tappedDynamicHeaderView:(DynamicHeaderView *)dynamicHeaderView;

@end

#import "BaseXibView.h"

@interface DynamicHeaderView : BaseXibView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (assign, nonatomic) BOOL selectedOpenHeader;

@property (weak, nonatomic) id <DynamicHeaderViewDelegate> delegate;

@end
