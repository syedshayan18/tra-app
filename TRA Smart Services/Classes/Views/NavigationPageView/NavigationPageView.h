//
//  NavigationPageView.h
//  TRA Smart Services
//
//  Created by Admin on 19.11.15.
//  Copyright © 2015 . All rights reserved.
//

@protocol NavigationPageViewDelegate <NSObject>

@optional
- (void)selectedNavigationPage:(NSInteger)selectPage;

@end

#import "BaseXibView.h"

@interface NavigationPageView : BaseXibView

@property (assign, nonatomic) NSInteger elementsCount;
@property (assign, nonatomic) NSInteger elementSelected;

@property (weak, nonatomic) id <NavigationPageViewDelegate> delegate;

- (void)updateUIColor;
- (void)updateUILocalize;

@end
