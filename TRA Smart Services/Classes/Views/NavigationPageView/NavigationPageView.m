//
//  NavigationPageView.m
//  TRA Smart Services
//
//  Created by Admin on 19.11.15.
//  Copyright © 2015 . All rights reserved.
//

#import "NavigationPageView.h"

@interface NavigationPageView()

@property (weak, nonatomic) IBOutlet UIButton *prevButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *beginButton;
@property (weak, nonatomic) IBOutlet UIButton *endButton;
@property (weak, nonatomic) IBOutlet UILabel *activePageLabel;

@end

@implementation NavigationPageView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder nibName:NSStringFromClass([self class])];
    [self updateUIColor];
    [self updateUILocalize];
    self.elementSelected = 1;
    return self;
}

#pragma mark - Public

- (void)updateUIColor
{
    self.prevButton.backgroundColor = [UIColor grayBorderTextFieldTextColor];
    self.prevButton.layer.cornerRadius = 2.5;
    self.nextButton.backgroundColor = [DynamicUIService service].currentApplicationColor;
    self.nextButton.layer.cornerRadius = 2.5;
    
    self.beginButton.tintColor = [DynamicUIService service].currentApplicationColor;
    self.beginButton.layer.borderWidth = 1.;
    self.beginButton.layer.cornerRadius = 2.5;
    self.beginButton.layer.borderColor = [DynamicUIService service].currentApplicationColor.CGColor;
    self.endButton.tintColor = [DynamicUIService service].currentApplicationColor;
    self.endButton.layer.borderWidth = 1.;
    self.endButton.layer.cornerRadius = 2.5;
    self.endButton.layer.borderColor = [DynamicUIService service].currentApplicationColor.CGColor;
    
    self.activePageLabel.backgroundColor = [DynamicUIService service].currentApplicationColor;
    self.activePageLabel.layer.cornerRadius = 2.5;
    self.activePageLabel.layer.borderColor = [DynamicUIService service].currentApplicationColor.CGColor;
    self.activePageLabel.layer.masksToBounds = YES;
}

- (void)updateUILocalize
{
    [self.nextButton setTitle:dynamicLocalizedString(@"navigationPageView.nextButton.title") forState:UIControlStateNormal];
    [self.prevButton setTitle:dynamicLocalizedString(@"navigationPageView.prevButton.title") forState:UIControlStateNormal];
}

#pragma mark - Custom accessors

- (void)setElementSelected:(NSInteger)elementSelected
{
    _elementSelected = elementSelected;
    self.activePageLabel.text = [NSString stringWithFormat:@"%ld", (long)elementSelected];
}

- (void)setElementsCount:(NSInteger)elementsCount
{
    _elementsCount = elementsCount;
    [self.endButton setTitle:[NSString stringWithFormat:@"%ld", (long)elementsCount] forState:UIControlStateNormal];
}

#pragma mark - IBAction

- (IBAction)tapPrevButton:(id)sender
{
    if (self.elementSelected != 1) {
        self.elementSelected--;
        [self selectNavigationPage];
    }
}
- (IBAction)tapNextButton:(id)sender
{
    if (self.elementSelected != self.elementsCount) {
        self.elementSelected++;
        [self selectNavigationPage];
    }
}

- (IBAction)tapBeginButton:(id)sender
{
    if (self.elementSelected != 1) {
        self.elementSelected = 1;
        [self selectNavigationPage];
    }
}

- (IBAction)tapEndButton:(id)sender
{
    if (self.elementSelected != self.elementsCount) {
        self.elementSelected = self.elementsCount;
        [self selectNavigationPage];
    }
}

#pragma mark - Method Delegate

- (void)selectNavigationPage
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectedNavigationPage:)]) {
        [self.delegate selectedNavigationPage:self.elementSelected];
    }
}

@end
