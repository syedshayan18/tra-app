//
//  ContactItemModel.h
//  TRA Smart Services
//
//  Created by Admin on 04.12.15.
//  Copyright © 2015 . All rights reserved.
//

@interface ContactItemModel : NSObject

@property (strong, nonatomic) NSString *contactItemId;
@property (strong, nonatomic) NSString *contactItemTitle;
@property (strong, nonatomic) NSString *contactItemPhone;
@property (strong, nonatomic) NSString *contactItemFax;
@property (strong, nonatomic) NSString *contactItemEmail;
@property (strong, nonatomic) NSString *contactItemCustomerWorkingHours;
@property (strong, nonatomic) NSString *contactItemWorkingHours;
@property (strong, nonatomic) NSString *contactItemPoBox;
@property (strong, nonatomic) NSString *contactItemLocationLatitude;
@property (strong, nonatomic) NSString *contactItemLocationLongitude;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
