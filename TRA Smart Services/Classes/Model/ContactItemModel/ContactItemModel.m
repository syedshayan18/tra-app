//
//  ContactItemModel.m
//  TRA Smart Services
//
//  Created by Admin on 04.12.15.
//  Copyright © 2015 . All rights reserved.
//

#import "ContactItemModel.h"
#import "NSDictionary+Safe.h"

@implementation ContactItemModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    
    dictionary = [dictionary removeNullValues];
    
    _contactItemId = [dictionary valueForKey:@"_id"];
    _contactItemTitle = [dictionary valueForKey:@"title"];
    _contactItemPhone = [dictionary valueForKey:@"phone"];
    _contactItemFax = [dictionary valueForKey:@"fax"];
    _contactItemEmail = [dictionary valueForKey:@"email"];
    _contactItemCustomerWorkingHours = [dictionary valueForKey:@"customerWorkingHours"];
    _contactItemWorkingHours = [dictionary valueForKey:@"workingHours"];
    _contactItemPoBox = [dictionary valueForKey:@"poBox"];
 
    NSDictionary *location = [dictionary valueForKey:@"location"];
    location = [location removeNullValues];
    _contactItemLocationLatitude = [location valueForKey:@"latitude"];
    _contactItemLocationLongitude = [location valueForKey:@"longitude"];
    
    return self;
}

@end
