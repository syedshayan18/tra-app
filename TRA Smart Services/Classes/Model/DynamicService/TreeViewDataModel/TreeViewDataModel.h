//
//  TreeViewDataModel.h
//  TRA Smart Services
//
//  Created by Admin on 10.11.15.
//  Copyright © 2015 . All rights reserved.
//

#import "BaseDynamicModel.h"

@interface TreeViewDataModel : BaseDynamicModel

@property (strong, nonatomic) NSString *treeViewTitle;
@property (strong, nonatomic) NSString *treeViewValue;
@property (strong, nonatomic) NSArray *treeViewArray;

@property (assign, nonatomic) BOOL treeViewOpen;

@property (assign, nonatomic) NSInteger treeViewRang;

- (instancetype)initWihtDictionary:(NSDictionary *)dictionary;
- (instancetype)initWihtString:(NSString *)title acvive:(BOOL)open array:(NSArray *)array;

+ (NSInteger)countOpeningTreeView:(TreeViewDataModel *)treeView;
- (NSMutableArray *)arrayListOpenTreeView:(TreeViewDataModel *)treeView;

@end
