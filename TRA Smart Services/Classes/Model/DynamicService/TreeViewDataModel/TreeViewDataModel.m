//
//  TreeViewDataModel.m
//  TRA Smart Services
//
//  Created by Admin on 10.11.15.
//  Copyright © 2015 . All rights reserved.
//

#import "TreeViewDataModel.h"
#import "NSDictionary+Safe.h"

@implementation TreeViewDataModel

- (instancetype)initWihtDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    
    dictionary = [dictionary removeNullValues];
    
    _treeViewTitle = [self localizableString:dictionary] ;
    _treeViewOpen = NO;
    _treeViewArray = [self setupTreeViewDataItems:[dictionary valueForKey:@"items"]];
    _treeViewValue = [dictionary valueForKey:@"value"];
    
    _treeViewRang = 0;
    [self updteRang:self];
    return self;
}

- (instancetype)initWihtString:(NSString *)title acvive:(BOOL)active array:(NSArray *)array
{
    self = [self init];
    
    _treeViewTitle = title;
    _treeViewOpen = active;
    _treeViewArray = array;
    
    _treeViewRang = 0;
    [self updteRang:self];
    return self;
}

#pragma mark - Private

+ (NSInteger)countOpeningTreeView:(TreeViewDataModel *)treeView
{
    NSInteger count = 0;
    for (int i = 0; i < treeView.treeViewArray.count; i++) {
        count++;
        TreeViewDataModel *numberTreeView = treeView.treeViewArray[i];
        if (numberTreeView.treeViewOpen && numberTreeView.treeViewArray.count) {
            count += [self countOpeningTreeView:numberTreeView];
        }
    }
    return count;
}

- (NSMutableArray *)arrayListOpenTreeView:(TreeViewDataModel *)treeView
{
    NSMutableArray *listArray = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < treeView.treeViewArray.count; i++) {
        TreeViewDataModel *numberTreeView = treeView.treeViewArray[i];
        
        [listArray addObject:numberTreeView];

        if (numberTreeView.treeViewOpen && numberTreeView.treeViewArray.count) {
            [listArray addObjectsFromArray:[self arrayListOpenTreeView:numberTreeView]];
        }
    }
    return listArray;
}

#pragma mark - Private

- (void)updteRang:(TreeViewDataModel *)treeViewDataModel
{
    for (int i = 0; i < treeViewDataModel.treeViewArray.count; i++) {
        
        TreeViewDataModel *numberTreeViewDataModel = treeViewDataModel.treeViewArray[i];
        numberTreeViewDataModel.treeViewRang++;
        if ( numberTreeViewDataModel.treeViewArray.count) {
            [self updteRang:numberTreeViewDataModel];
        }
    }
}

- (NSArray *)setupTreeViewDataItems:(NSArray *)array
{
    NSMutableArray *treeViewDataArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dictionry in array) {
        TreeViewDataModel *treeViewDataModel = [[TreeViewDataModel alloc] initWihtDictionary:dictionry];
        [treeViewDataArray addObject:treeViewDataModel];
    }
    return treeViewDataArray;
}

@end
