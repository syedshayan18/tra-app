//
//  BaseDynamicModel.m
//  TRA Smart Services
//
//  Created by Admin on 03.11.15.
//  Copyright © 2015 . All rights reserved.
//

#import "BaseDynamicModel.h"

static NSString *const ARLanguageKey = @"AR";
static NSString *const ENLanguageKey = @"EN";

@implementation BaseDynamicModel

#pragma mark - Public

- (NSString *)localizableString:(NSDictionary *)dictionary
{
    return [dictionary valueForKey:[DynamicUIService service].language == LanguageTypeArabic ? ARLanguageKey : ENLanguageKey];
}

@end
