//
//  BaseDynamicModel.h
//  TRA Smart Services
//
//  Created by Admin on 03.11.15.
//  Copyright © 2015 . All rights reserved.
//

@interface BaseDynamicModel : NSObject

- (NSString *)localizableString:(NSDictionary *)dictionary;

@end
