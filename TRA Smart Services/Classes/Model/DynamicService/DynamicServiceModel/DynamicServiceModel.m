//
//  DynamicServiceModel.m
//  TRA Smart Services
//
//  Created by Admin on 28.10.15.
//  Copyright © 2015 . All rights reserved.
//

#import "DynamicServiceModel.h"
#import "DynamicPage.h"
#import "NSDictionary+Safe.h"
#import "TRADynamicService.h"

@implementation DynamicServiceModel

#pragma mark - LifeCycle

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    
    self = [self init];
    if (self) {
        
        dictionary = [dictionary removeNullValues];
        
        _dynamicServiceID = [dictionary valueForKey:@"_id"];
        _dynamicServiceIcon = [dictionary valueForKey:@"icon"];
        _dynamicServiceNeedAuth = [[dictionary valueForKey:@"needAuth"] boolValue];
        _dynamicServiceRequestButtonTitle = [dictionary valueForKey:@"buttonTitle"];
        _dynamicServicePages = [self setupDynamicServicePages:[dictionary valueForKey:@"pages"]];
        _dynamicServiceItems = [self setupDynamicServiceItems:[dictionary valueForKey:@"items"]];
        [self prepareNameService:[dictionary valueForKey:@"profile"]];
        _dynamicServiceInfo = [dictionary valueForKey:@"profile"];
        _dynamicServiceInquiriesCategory = [dictionary valueForKey:@"inquiriesCategory"];
        _dynamicServiceARPDF = [[dictionary valueForKey:@"pdfLink"] valueForKey:@"AR"];
        _dynamicServiceENPDF = [[dictionary valueForKey:@"pdfLink"] valueForKey:@"EN"];
        _urlAR = dictionary[@"url_AR"];
        _urlEN = dictionary[@"url_EN"];
        
        int needAuth = [dictionary[@"needAuth"] intValue];
        _dynamicServiceNeedAuth = needAuth != 0;
    }

    return self;
}

- (instancetype)initWithTRADynamicServise:(TRADynamicService *)traDynamicService
{
    self = [self init];
    
    _dynamicServiceID = traDynamicService.serviceID;
    _dynamicServiceIcon = traDynamicService.serviceIcon;
    _dynamicServiceNeedAuth = [traDynamicService.serviceNeedAuth boolValue];
    _dynamicServiceName = @{ @"AR" : traDynamicService.serviceNameAR, @"EN" : traDynamicService.serviceNameEN};
    
    return self;
}

#pragma mark - Private

- (void)prepareNameService:(NSDictionary *)profile
{
    profile = [profile removeNullValues];
    if ([profile valueForKey:@"Name"]) {
        NSDictionary *serviceName = [profile valueForKey:@"Name"];
        _dynamicServiceName = [serviceName removeNullValues];
    }
}

- (NSArray *)setupDynamicServicePages:(NSArray *)dataPages
{
    NSMutableArray *dynamicServicePages = [[NSMutableArray alloc] init];
    for (NSDictionary *page in dataPages) {
        DynamicPage *dynamicPage = [[DynamicPage alloc] initWithDictionary:page];
        [dynamicServicePages addObject:dynamicPage];
    }
    return dynamicServicePages;
}

- (NSArray *)setupDynamicServiceItems:(NSArray *)items
{
    NSMutableArray *dynamicServiceItems = [[NSMutableArray alloc] init];
    for (id item in items) {
        if ([item isKindOfClass:[NSDictionary class]]) {
            DynamicServiceModel *dynamicServiceModel = [[DynamicServiceModel alloc] initWithDictionary:item];
            [dynamicServiceItems addObject:dynamicServiceModel];
        }
    }
    return dynamicServiceItems;
}

@end
