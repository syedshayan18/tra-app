//
//  DynamicServiceModel.h
//  TRA Smart Services
//
//  Created by Admin on 28.10.15.
//  Copyright © 2015 . All rights reserved.
//

#import "BaseDynamicModel.h"

@class TRADynamicService;

@interface DynamicServiceModel : BaseDynamicModel

@property (strong, nonatomic) NSString *dynamicServiceID;
@property (strong, nonatomic) NSString *dynamicServiceIcon;
@property (assign, nonatomic) BOOL dynamicServiceNeedAuth;
@property (strong, nonatomic) NSDictionary *dynamicServiceName;
@property (strong, nonatomic) NSArray *dynamicServiceItems;

@property (strong, nonatomic) NSDictionary *dynamicServiceRequestButtonTitle;
@property (strong, nonatomic) NSArray *dynamicServicePages;
@property (strong, nonatomic) NSDictionary *dynamicServiceInfo;

@property (strong, nonatomic) NSString *dynamicServiceInquiriesCategory;
@property (strong, nonatomic) NSString *dynamicServiceARPDF;
@property (strong, nonatomic) NSString *dynamicServiceENPDF;
@property (strong, nonatomic) NSString *urlAR;
@property (strong, nonatomic) NSString *urlEN;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
- (instancetype)initWithTRADynamicServise:(TRADynamicService *)traDynamicService;

@end
