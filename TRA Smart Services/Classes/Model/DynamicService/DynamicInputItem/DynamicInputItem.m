//
//  DynamicInputItem.m
//  TRA Smart Services
//
//  Created by Admin on 29.10.15.
//  Copyright © 2015 . All rights reserved.
//

#import "DynamicInputItem.h"
#import "NSDictionary+Safe.h"

@implementation DynamicInputItem

#pragma mark - Public

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    
    dictionary = [dictionary removeNullValues];
    
    _dynamicInputItemID = [dictionary valueForKey:@"_id"];
    _dynamicInputItemDataSource = [self prepareDataSource:[dictionary objectForKey:@"dataSource"]];
    _dynamicInputItemDisplayName = [dictionary valueForKey:@"displayName"];
    _dynamicInputItemInputType = [self dynamicInputType:[dictionary valueForKey:@"inputType"]];
    _dynamicInputItemValidationType = [self validationTypeFrom:[dictionary valueForKey:@"validateAs"]];
    _dynamicInputItemName = [dictionary valueForKey:@"name"];
    _dynamicInputItemOrder = [[dictionary valueForKey:@"order"] integerValue];
    _dynamicInputItemPlaceholdel = [dictionary valueForKey:@"placeHolder"];
    _dynamicInputItemRequred = [[dictionary valueForKey:@"required"] boolValue];
    _dynamicInputItemAdditional = [dictionary valueForKey:@"additional"];
    _dynamicInputItemDataContent = [dictionary valueForKey:@"dataContent"];
    
    _dynamicInputItemValue = [dictionary valueForKey:@"value"];
    _dynamicInputItemHidden = [[dictionary valueForKey:@"isHidden"] boolValue];
    _displayHidden = _dynamicInputItemHidden;
    
    _dynamicSecondaryItems = [self prepareSecondaryItems];
    
    [self prepareTransactionValue];
    
    return self;
}

- (NSArray *)pickerDataSource
{
    NSMutableArray *pickerDataSource = [[NSMutableArray alloc] init];
    [pickerDataSource addObject:[self localizableString:_dynamicInputItemPlaceholdel]];
    for (NSDictionary *dictionaryPickerData in self.dynamicInputItemDataSource) {
        NSString *stringPickerData = [self localizableString:dictionaryPickerData];
        [pickerDataSource addObject:stringPickerData];
    }
    return pickerDataSource;
}

#pragma mark - Private

- (void)prepareTransactionValue
{
    switch (_dynamicInputItemInputType) {
        case InputItemTypeSwitch:
            self.clientSelectPicker = 2;
        case InputItemTypeSelectionView: {
            
            NSString *jsonInputItemValueString;
            if ([_dynamicInputItemValue isKindOfClass:[NSNumber class]]){
                jsonInputItemValueString = [(NSNumber *)_dynamicInputItemValue stringValue];
            } else if ([_dynamicInputItemValue isKindOfClass:[NSString class]]) {
                jsonInputItemValueString = (NSString *)_dynamicInputItemValue;
            }
            
            for (NSInteger i = 0; i < _dynamicInputItemDataSource.count; i++) {
                NSDictionary *dictionary = _dynamicInputItemDataSource[i];
                NSString *jsonValueString = (NSString *)[dictionary valueForKey:@"value"];
                
                if ([jsonInputItemValueString isEqualToString:jsonValueString]) {
                    self.clientSelectPicker = i + 1;
                    break;
                }
            }
            break;
        }
        case InputItemTypeSelectFile: {
            if ([_dynamicInputItemValue isKindOfClass:[NSString class]]) {
                NSString *imadeId = (NSString *)_dynamicInputItemValue;
                self.clientUploadedImageId = imadeId.length ? imadeId : nil;
            } else if (_dynamicInputItemValue) {
                self.clientUploadedImageId = @"image";
            }
            break;
        }
            
        case InputItemTypeLabel:
        case InputItemTypeTextView:
        case InputItemTypeTextField: {
            self.clientString = [_dynamicInputItemValue isKindOfClass:[NSString class]] ? _dynamicInputItemValue : @"";
            break;
        }
        case InputItemTypeDatePicker:
        case InputItemTypeTable:
        case InputItemTypePicker:
        case InputItemTypeCRMtype:
        case InputItemTypeTreeView: {
            break;
        }
    }
}

- (NSArray *)prepareDataSource:(NSArray *)responceDataSource
{
    NSMutableArray *dataSource = [[NSMutableArray alloc] init];
    for (NSDictionary *dataDictionary in responceDataSource) {
        NSDictionary *dictionary = [dataDictionary removeNullValues];
        [dataSource addObject:dictionary];
    }
    return dataSource;
}

- (NSSet *)prepareSecondaryItems
{
    NSMutableSet *secondaryItems = [[NSMutableSet alloc] init];
    for (NSDictionary *dataItem in _dynamicInputItemDataSource) {
        [secondaryItems addObjectsFromArray:[dataItem valueForKey:@"show"]];
        [secondaryItems addObjectsFromArray:[dataItem valueForKey:@"hide"]];
    }
    return secondaryItems;
}

- (InputItemType)dynamicInputType:(NSString *)inputType
{
    InputItemType dynamicInputType = InputItemTypeLabel;
    
    if ([inputType isEqualToString:@"string"]) {
        dynamicInputType = InputItemTypeTextField;
    } else if ([inputType isEqualToString:@"picker"]) {
        dynamicInputType = InputItemTypeSelectionView;
    } else if ([inputType isEqualToString:@"date"]) {
        dynamicInputType = InputItemTypeDatePicker;
    } else if ([inputType isEqualToString:@"file"]) {
        dynamicInputType = InputItemTypeSelectFile;
    } else if ([inputType isEqualToString:@"text"]) {
        dynamicInputType = InputItemTypeTextView;
    } else if ([inputType isEqualToString:@"table"]) {
        dynamicInputType = InputItemTypeTable;
    } else if ([inputType isEqualToString:@"tree"]) {
        dynamicInputType = InputItemTypeTreeView;
    } else if ([inputType isEqualToString:@"checkbox"]) {
        dynamicInputType = InputItemTypeSwitch;
    } else if ([inputType isEqualToString:@"CRMtype"]) {
        dynamicInputType = InputItemTypeCRMtype;
    }
    return dynamicInputType;
}

- (ValidationType)validationTypeFrom:(NSString *)inputType
{
    ValidationType validationType = ValidationTypeNone;
    if ([inputType isEqualToString:@"string"]) {
        validationType = ValidationTypeString;
    } else if ([inputType isEqualToString:@"number"]){
        validationType = ValidationTypeNumber;
    } else if ([inputType isEqualToString:@"url"]){
        validationType = ValidationTypeURL;
    } else if ([inputType isEqualToString:@"email"]){
        validationType = ValidationTypeEmail;
    }
    return validationType;
}

@end