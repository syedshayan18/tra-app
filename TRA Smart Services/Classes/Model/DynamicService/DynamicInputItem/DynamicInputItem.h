//
//  DynamicInputItem.h
//  TRA Smart Services
//
//  Created by Admin on 29.10.15.
//  Copyright © 2015 . All rights reserved.
//

#import "BaseDynamicModel.h"

typedef NS_ENUM(NSUInteger, InputItemType) {
    InputItemTypeTextField,
    InputItemTypePicker,
    InputItemTypeSelectFile,
    InputItemTypeDatePicker,
    InputItemTypeTextView,
    InputItemTypeTable,
    InputItemTypeTreeView,
    InputItemTypeSwitch,
    InputItemTypeSelectionView,
    InputItemTypeCRMtype,
    InputItemTypeLabel
};

typedef NS_ENUM(NSUInteger, ValidationType) {
    ValidationTypeNone,
    ValidationTypeString,
    ValidationTypeNumber,
    ValidationTypeEmail,
    ValidationTypeURL
};

@interface DynamicInputItem : BaseDynamicModel

#pragma mark - Model

@property (strong, nonatomic) NSString *dynamicInputItemID;
@property (strong, nonatomic) NSString *dynamicInputItemName;
@property (assign, nonatomic) NSInteger dynamicInputItemOrder;
@property (strong, nonatomic) NSArray *dynamicInputItemDataSource;
@property (strong, nonatomic) NSDictionary *dynamicInputItemDisplayName;
@property (strong, nonatomic) NSDictionary *dynamicInputItemPlaceholdel;
@property (assign, nonatomic) InputItemType dynamicInputItemInputType;
@property (assign, nonatomic) BOOL dynamicInputItemRequred;
@property (assign, nonatomic) ValidationType dynamicInputItemValidationType;
@property (strong, nonatomic) NSString *dynamicInputItemAdditional;
@property (strong, nonatomic) NSArray *dynamicInputItemDataContent;

@property (strong, nonatomic) id dynamicInputItemValue;
@property (assign, nonatomic) BOOL dynamicInputItemHidden;

@property (strong, nonatomic) NSSet *dynamicSecondaryItems;
@property (assign, nonatomic) BOOL displayHidden;

#pragma mark - InputClientData

@property (copy, nonatomic) NSString *clientString;
@property (copy, nonatomic) __block NSString *clientUploadedImageId;
@property (strong, nonatomic) NSDate *clientDate;
@property (assign, nonatomic) NSInteger clientSelectPicker;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
- (NSArray *)pickerDataSource;

@end
