//
//  DynamicPage.m
//  TRA Smart Services
//
//  Created by Admin on 03.11.15.
//  Copyright © 2015 . All rights reserved.
//

#import "DynamicPage.h"
#import "DynamicInputItem.h"
#import "NSDictionary+Safe.h"

@implementation DynamicPage

#pragma mark - Public

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    
    dictionary = [dictionary removeNullValues];
    
    _dynamicPageID = [dictionary valueForKey:@"_id"];
    _dynamicPageNumber = [[dictionary objectForKey:@"number"] integerValue];
    _dynamicPageInputItems = [self setupDynamicPageInputItems:[dictionary valueForKey:@"inputItems"]];
    
    return self;
}

#pragma mark - Private

- (NSArray *)setupDynamicPageInputItems:(NSArray *)inputItems
{
    NSMutableArray *dynamicPageInputItems = [[NSMutableArray alloc] init];
    for (NSDictionary *inputItem in inputItems) {
        DynamicInputItem *dynamicInputItem = [[DynamicInputItem alloc] initWithDictionary:inputItem];
        [dynamicPageInputItems addObject:dynamicInputItem];
    }
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc]initWithKey:@"dynamicInputItemOrder" ascending:YES];
    NSArray * descriptors = [NSArray arrayWithObject:valueDescriptor];
    
    return [dynamicPageInputItems sortedArrayUsingDescriptors:descriptors];
}

@end
