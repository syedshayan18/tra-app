//
//  DynamicPage.h
//  TRA Smart Services
//
//  Created by Admin on 03.11.15.
//  Copyright © 2015 . All rights reserved.
//

#import "BaseDynamicModel.h"

@interface DynamicPage : BaseDynamicModel

@property (strong, nonatomic) NSString *dynamicPageID;
@property (assign, nonatomic) NSInteger dynamicPageNumber;
@property (strong, nonatomic) NSArray *dynamicPageInputItems;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end