//
//  Question.h
//  TRA Smart Services
//
//  Created by Admin on 10.12.15.
//  Copyright © 2015 Admin. All rights reserved.
//


@interface Question : NSObject

@property (copy, nonatomic) NSString *secretQuestionType;
@property (copy, nonatomic) NSString *secretQuestionAnswer;
@property (assign, nonatomic) NSInteger secretQuestionValue;

@end
