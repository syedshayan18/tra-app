//
//  Question.m
//  TRA Smart Services
//
//  Created by Admin on 10.12.15.
//  Copyright © 2015 . All rights reserved.
//

#import "Question.h"

@implementation Question

#pragma mark - Coder

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_secretQuestionAnswer forKey:@"secretQuestionAnswer"];
    [encoder encodeObject:_secretQuestionType forKey:@"secretQuestionType"];
    [encoder encodeObject:@(_secretQuestionValue) forKey:@"secretQuestionValue"];
}

- (instancetype)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        _secretQuestionAnswer = [decoder decodeObjectForKey:@"secretQuestionAnswer"];
        _secretQuestionType = [decoder decodeObjectForKey:@"secretQuestionType"];
        _secretQuestionValue = [[decoder decodeObjectForKey:@"secretQuestionValue"] integerValue];
    }
    return self;
}

@end