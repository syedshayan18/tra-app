//
//  TransactionModel.m
//  TRA Smart Services
//
//  Created by Admin on 05.10.15.
//  Copyright © 2015 . All rights reserved.
//

#import "TransactionModel.h"
#import "NSDictionary+Safe.h"
#import "NSString+Clean.h"

@interface TransactionModel()

@end

@implementation TransactionModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super initWithDictionary:dictionary];
    
    dictionary = [dictionary removeNullValues];
    
    _title = [[dictionary valueForKey:@"title"] cleanedHtmlString];
    _type = [[dictionary valueForKey:@"type"] integerValue];
    
    NSMutableString* yourString = [[dictionary valueForKey:@"description"] mutableCopy];
    if (yourString.length) {
        CFStringTransform((CFMutableStringRef)yourString, NULL, kCFStringTransformToXMLHex, true);
    }
    
    _transationDescription = yourString;
    _transactionColor = [self colorForValue:[dictionary valueForKey:@"statusCode"]];
    _transactionImageName = [self imageNameForServiceType:[[dictionary valueForKey:@"type"] integerValue] statusCode:[dictionary valueForKey:@"statusCode"]];
    
    return self;
}

#pragma mark - Public

- (NSDictionary *)modelToDictionary
{
    NSMutableDictionary *dictionary = [[super modelToDictionary] mutableCopy];
    
    [dictionary setValue:_title forKey:@"title"];
    [dictionary setValue:_transationDescription forKey:@"description"];
    
    return dictionary;
}

- (NSString *)titleStringTransactionModel
{
    NSString *titleScreen = @"Transaction";

    switch (self.type) {
        case TransactionModelTypeCRMEnquiries:{
            titleScreen = dynamicLocalizedString(@"compliantViewController.title.enquires");
            break;
        }
        case TransactionModelTypeCRMSuggestion:{
            titleScreen = dynamicLocalizedString(@"suggestionViewController.title");
            break;
        }
        case TransactionModelTypeCRMComplainAboutTRA:{
            titleScreen = dynamicLocalizedString(@"compliantViewController.title.comtlaintTRA");
            break;
        }
        case TransactionModelTypeCRMCompliantAboutServiceProvider:{
            titleScreen = dynamicLocalizedString(@"compliantViewController.title.compliantAboutServiceProvider");
            break;
        }
        case TransactionModelTypeCoverage:{
            titleScreen = dynamicLocalizedString(@"coverageLevel.title");
            break;
        }
        case TransactionModelTypeCRMSMSSpam:{
            titleScreen = dynamicLocalizedString(@"spamReportViewControler.title.spamSMS");
            break;
        }
        case TransactionModelTypeWebReport:{
            titleScreen = dynamicLocalizedString(@"spamReportViewControler.title.spamWEB");
            break;
        }
        default:{
            break;
        }
    }
    
    return titleScreen;
}

- (BOOL)editingTransactionModel
{
    return ((self.type == TransactionModelTypeCRMSuggestion || self.type == TransactionModelTypeCRMEnquiries || self.type == TransactionModelTypeCRMComplainAboutTRA || self.type == TransactionModelTypeCRMCompliantAboutServiceProvider) && [self.statusCode isEqualToString:@"Waiting for Details"]);
}

#pragma mark - Private

- (UIColor *)colorForValue:(NSString *)value
{
    UIColor *color = [UIColor defaultGreenColor];
    if ([value isEqualToString:@"In Progress"]) {
        color = [UIColor defaultGreenColor];
    } else if ([value isEqualToString:@"On Hold"]) {
        color = [UIColor yellowColor];
    } else if ([value isEqualToString:@"Waiting for Details"]) {
        color = [UIColor redColor];
    } else if ([value isEqualToString:@"Researching"]) {
        color = [UIColor yellowColor];
    } else if ([value isEqualToString:@"Referred To Licensee"]) {
        color = [UIColor defaultGreenColor];
    } else if ([value isEqualToString:@"Implemented"]) {
        color = [UIColor defaultGreenColor];
    } else if ([value isEqualToString:@"Approved"]) {
        color = [UIColor defaultGreenColor];
    } else if ([value isEqualToString:@"Waiting to be Reviewed"]) {
        color = [UIColor defaultGreenColor];
    }
    return color;
}

- (NSString *)imageNameForServiceType:(TransactionModelType)serviceType statusCode:(NSString *)statusCode
{
    NSString *fileName = @"ic_status_approval";
    switch (serviceType) {
        case TransactionModelTypeCRMSMSSpam:
        case TransactionModelTypeCRMSuggestion:
        case TransactionModelTypeCRMEnquiries:
        case TransactionModelTypeCRMComplainAboutTRA:
        case TransactionModelTypeCRMCompliantAboutServiceProvider:{
            fileName = [self imageNamzeForStatusCRM:statusCode];
            break;
        }
        case TransactionModelTypeWebReport:{
            fileName = @"ic_global";
            break;
        }
        case TransactionModelTypeCoverage:{
            fileName = @"ic_coverage";
            break;
        }
        default:
            break;
    }
    return fileName;
}

- (NSString *)imageNamzeForStatusCRM:(NSString *)statusCode
{
    NSString *fileName = @"ic_status_approval";
    if ([statusCode isEqualToString:@"Waiting for Details"]) {
        fileName = @"ic_status_domain";
    } else if ([statusCode isEqualToString:@"On Hold"] || [statusCode isEqualToString:@"Researching"]) {
        fileName = @"ic_status_download";
    }
    return fileName;
}

@end
