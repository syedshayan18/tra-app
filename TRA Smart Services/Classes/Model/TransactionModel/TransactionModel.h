//
//  TransactionModel.h
//  TRA Smart Services
//
//  Created by Admin on 05.10.15.
//  Copyright © 2015 . All rights reserved.
//

#import "BaseServiceModel.h"

@interface TransactionModel : BaseServiceModel

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *transationDescription;

@property (assign, nonatomic) NSString *transactionImageName;
@property (strong, nonatomic) UIColor *transactionColor;


@property (assign, nonatomic) TransactionModelType type;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)modelToDictionary;
- (NSString *)titleStringTransactionModel;
- (BOOL)editingTransactionModel;

@end
