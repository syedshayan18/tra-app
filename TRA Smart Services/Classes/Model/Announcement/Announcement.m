//
//  Announcement.m
//  TRA Smart Services
//
//  Created by Admin on 21.10.15.
//  Copyright © 2015 . All rights reserved.
//

#import "Announcement.h"
#import "NSDictionary+Safe.h"
#import "NSString+Clean.h"

@implementation Announcement

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];

    dictionary = [dictionary removeNullValues];
    
    _announcementID = [dictionary valueForKey:@"_id"];
    _announcementTitle = [[dictionary valueForKey:@"title"] cleanedHtmlString];
    _announcementDescription = [[dictionary valueForKey:@"description"] cleanedHtmlString];
    _announcementLink = [dictionary valueForKey:@"link"];
    _announcementCreatedAt = [self stringToDateAnnoucement:[dictionary valueForKey:@"createdAt"]];
    _announcementPubDate = [self stringToDateAnnoucement:[dictionary valueForKey:@"pubDate"]];
    _announcementImage = [dictionary valueForKey:@"image"];
    
    return self;
}

- (BOOL)isEqual:(Announcement *)anotherAnnouncement
{
    BOOL isEqual = YES;
    if (![self.announcementID isEqualToString:anotherAnnouncement.announcementID] ||
        ![self.announcementTitle isEqualToString:anotherAnnouncement.announcementTitle] ||
        ![self.announcementDescription isEqualToString:anotherAnnouncement.announcementDescription] ||
        ![self.announcementCreatedAt isEqualToDate:anotherAnnouncement.announcementCreatedAt] ||
        ![self.announcementPubDate isEqualToDate:anotherAnnouncement.announcementPubDate]) {
        isEqual = NO;
    }
    
    return isEqual;
}

#pragma mark - Equal

- (NSUInteger)hash
{
    return [self.announcementID hash];
}

#pragma mark - Private

- (NSDate *)stringToDateAnnoucement:(NSString *)stringDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    NSDate *date = [dateFormatter dateFromString:stringDate];
    return date;
}

@end
