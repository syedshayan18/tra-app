//
//  Announcement.h
//  TRA Smart Services
//
//  Created by Admin on 21.10.15.
//  Copyright © 2015 . All rights reserved.
//

@interface Announcement : NSObject

@property (strong, nonatomic) NSString *announcementID;
@property (strong, nonatomic) NSString *announcementTitle;
@property (strong, nonatomic) NSString *announcementDescription;
@property (strong, nonatomic) NSString *announcementLink;
@property (strong, nonatomic) NSDate *announcementCreatedAt;
@property (strong, nonatomic) NSDate *announcementPubDate;
@property (strong, nonatomic) NSString *announcementImage;

@property (strong, nonatomic) UIImage *announcementLogoImage;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
- (NSUInteger)hash;
- (BOOL)isEqual:(Announcement *)anotherAnnouncement;

@end
