//
//  BaseServiceModel.m
//  TRA Smart Services
//
//  Created by Admin on 22.03.16.
//  Copyright © 2016 . All rights reserved.
//

#import "BaseServiceModel.h"

@implementation BaseServiceModel

#pragma mark - LifeCycle

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    
    dictionary = [dictionary removeNullValues];
    
    _serviceTransactionType = [[dictionary valueForKey:@"type"] integerValue];
    _transactionID = [dictionary valueForKey:@"_id"];
    
    _traSubmitDatetime = [dictionary valueForKey:@"traSubmitDatetime"];
    _modifiedDatetime = [dictionary valueForKey:@"modifiedDatetime"];
    _stateCode = [dictionary valueForKey:@"stateCode"];
    _statusCode = [dictionary valueForKey:@"statusCode"];
    _serviceStage = [dictionary valueForKey:@"serviceStage"];
    
    return self;
}

#pragma mark - Public

- (NSDictionary *)modelToDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];

    if (_serviceTransactionType) {
        [dictionary setValue:@(_serviceTransactionType) forKey:@"type"];
    }
    [dictionary setValue:_transactionID forKey:@"_id"];
    [dictionary setValue:_traSubmitDatetime forKey:@"traSubmitDatetime"];
    [dictionary setValue:_modifiedDatetime forKey:@"modifiedDatetime"];
    [dictionary setValue:_stateCode forKey:@"stateCode"];
    [dictionary setValue:_statusCode forKey:@"statusCode"];
    [dictionary setValue:_serviceStage forKey:@"serviceStage"];
    
    return dictionary;
}

@end
