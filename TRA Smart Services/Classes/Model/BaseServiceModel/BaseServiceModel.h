//
//  BaseServiceModel.h
//  TRA Smart Services
//
//  Created by Admin on 22.03.16.
//  Copyright © 2016 . All rights reserved.
//

#import "NSDictionary+Safe.h"

@interface BaseServiceModel : NSObject

@property (assign, nonatomic) TransactionModelType serviceTransactionType;
@property (strong, nonatomic) NSString *transactionID;

@property (strong, nonatomic) NSString *traSubmitDatetime;
@property (strong, nonatomic) NSString *modifiedDatetime;
@property (strong, nonatomic) NSString *stateCode;
@property (strong, nonatomic) NSString *statusCode;
@property (strong, nonatomic) NSString *serviceStage;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (NSDictionary *)modelToDictionary;

@end
