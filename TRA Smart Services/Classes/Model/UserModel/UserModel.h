//
//  UserModel.h
//  TRA Smart Services
//
//  Created by Admin on 9/30/15.
//

@interface UserModel : NSObject

@property (copy, nonatomic) NSString *firstNameEN;
@property (copy, nonatomic) NSString *lastNameEN;
@property (copy, nonatomic) NSString *firstNameAR;
@property (copy, nonatomic) NSString *lastNameAR;
@property (copy, nonatomic) NSString *contactNumber;
@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *uriForImage;
@property (copy, nonatomic) NSString *avatarImageBase64;
@property (copy, nonatomic) NSString *emirateId;

@property (assign, nonatomic) BOOL enhancedSecurity;
@property (copy, nonatomic) NSString *secretQuestionType;
@property (copy, nonatomic) NSString *secretQuestionAnswer;
@property (strong, nonatomic) NSArray *secretQuestions;

- (instancetype)initWithFirstNameEN:(NSString *)firstNameEN lastNameEN:(NSString *)lastNameEN firstNameAR:(NSString *)firstNameAR lastNameAR:(NSString *)lastNameAR emirateId:(NSString *)emirateId contactNumber:(NSString *)contactNumber email:(NSString *)email imageUri:(NSString *)uri imageBase64Data:(NSString *)imageBase64Data;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
