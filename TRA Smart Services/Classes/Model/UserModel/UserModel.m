//
//  UserModel.m
//  TRA Smart Services
//
//  Created by Admin on 9/30/15.
//

#import "UserModel.h"
#import "NSDictionary+Safe.h"

@implementation UserModel

#pragma mark - LifeCycle

- (instancetype)init
{
    return [self initWithFirstNameEN:@"" lastNameEN:@"" firstNameAR:@"" lastNameAR:@"" emirateId:@"" contactNumber:@"" email:@"" imageUri:@"" imageBase64Data:@""];
}

- (instancetype)initWithFirstNameEN:(NSString *)firstNameEN lastNameEN:(NSString *)lastNameEN firstNameAR:(NSString *)firstNameAR lastNameAR:(NSString *)lastNameAR emirateId:(NSString *)emirateId contactNumber:(NSString *)contactNumber email:(NSString *)email imageUri:(NSString *)uri imageBase64Data:(NSString *)imageBase64Data
{
    self = [super init];
    if (self) {
        _firstNameEN = firstNameEN;
        _lastNameEN = lastNameEN;
        _firstNameAR = firstNameAR;
        _lastNameAR = lastNameAR;
        _emirateId = emirateId;
        _contactNumber = contactNumber;
        _uriForImage = uri;
        _avatarImageBase64 = imageBase64Data;
        _email = email;
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    
    dictionary = [dictionary removeNullValues];
    
    _firstNameEN = [dictionary valueForKey:@"first_EN"] ? [dictionary valueForKey:@"first_EN"] : @"";
    _lastNameEN = [dictionary valueForKey:@"last_EN"] ? [dictionary valueForKey:@"last_EN"] : @"";
    _firstNameAR = [dictionary valueForKey:@"first_AR"] ? [dictionary valueForKey:@"first_AR"] : @"";
    _lastNameAR = [dictionary valueForKey:@"last_AR"] ? [dictionary valueForKey:@"last_AR"] : @"";
    _email = [dictionary valueForKey:@"email"] ? [dictionary valueForKey:@"email"] : @"";
    _emirateId = [dictionary valueForKey:@"emiratesId"] ? [dictionary valueForKey:@"emiratesId"] : @"";
    _contactNumber = [dictionary valueForKey:@"mobile"] ? [dictionary valueForKey:@"mobile"] : @"";
    _uriForImage = [dictionary valueForKey:@"image"];
    _enhancedSecurity = [[dictionary valueForKey:@"enhancedSecurity"] boolValue];
    _secretQuestionAnswer = [dictionary valueForKey:@"secretQuestionAnswer"];
    _secretQuestionType = [dictionary valueForKey:@"secretQuestionType"];
    _secretQuestions = [dictionary valueForKey:@"secretQuestions"];
    
    if ([_firstNameEN isEqualToString:@"null"]) {
        _firstNameEN = @"";
    }
    if ([_lastNameEN isEqualToString:@"null"]) {
        _lastNameEN = @"";
    }
    if ([_firstNameAR isEqualToString:@"null"]) {
        _firstNameAR = @"";
    }
    if ([_lastNameAR isEqualToString:@"null"]) {
        _lastNameAR = @"";
    }
    return self;
}

#pragma mark - Coder

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_firstNameEN forKey:@"firstNameEN"];
    [encoder encodeObject:_lastNameEN forKey:@"lastNameEN"];
    [encoder encodeObject:_firstNameAR forKey:@"firstNameAR"];
    [encoder encodeObject:_lastNameAR forKey:@"lastNameAR"];
    [encoder encodeObject:_email forKey:@"email"];
    [encoder encodeObject:_emirateId forKey:@"emirateId"];
    [encoder encodeObject:_contactNumber forKey:@"contactNumber"];
    [encoder encodeObject:_uriForImage forKey:@"uriForImage"];
    [encoder encodeObject:_avatarImageBase64 forKey:@"base64StringData"];
    [encoder encodeObject:@(_enhancedSecurity) forKey:@"enhancedSecurity"];
    [encoder encodeObject:_secretQuestionAnswer forKey:@"secretQuestionAnswer"];
    [encoder encodeObject:_secretQuestionType forKey:@"secretQuestionType"];
    [encoder encodeObject:_secretQuestions forKey:@"secretQuestions"];
}

- (instancetype)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        _firstNameEN = [decoder decodeObjectForKey:@"firstNameEN"];
        _lastNameEN = [decoder decodeObjectForKey:@"lastNameEN"];
        _firstNameAR = [decoder decodeObjectForKey:@"firstNameAR"];
        _lastNameAR = [decoder decodeObjectForKey:@"lastNameAR"];
        
        _email = [decoder decodeObjectForKey:@"email"];
        _emirateId = [decoder decodeObjectForKey:@"emirateId"];
        _contactNumber = [decoder decodeObjectForKey:@"contactNumber"];
        _uriForImage =[decoder decodeObjectForKey:@"uriForImage"];
        _avatarImageBase64 =[decoder decodeObjectForKey:@"base64StringData"];
        
        _enhancedSecurity = [[decoder decodeObjectForKey:@"enhancedSecurity"] boolValue];
        _secretQuestionAnswer = [decoder decodeObjectForKey:@"secretQuestionAnswer"];
        _secretQuestionType = [decoder decodeObjectForKey:@"secretQuestionType"];
        _secretQuestions = [decoder decodeObjectForKey:@"secretQuestions"];

    }
    return self;
}

@end
