//
//  ServiceModel.m
//  TRA Smart Services
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "ServiceModel.h"
#import "NSDictionary+Safe.h"

@implementation ServiceModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    
    dictionary = [dictionary removeNullValues];
    
    _serviceID = [[dictionary valueForKey:@"serviceID"] integerValue];
    _serviceName = [dictionary valueForKey:@"serviceName"];
    _serviceLogo = [dictionary valueForKey:@"serviceLogo"];
    _serviceOrder = [[dictionary valueForKey:@"serviceOrder"] integerValue];
    _serviceDisplayLogo = [dictionary valueForKey:@"serviceDisplayLogo"];
    _serviceEnable = [[dictionary valueForKey:@"serviceEnable"] boolValue];
    _serviceType = [[dictionary valueForKey:@"serviceType"] integerValue];
    
    return self;
}

- (instancetype)initWithTRAService:(TRAStaticService *)traService
{
    self = [self init];
    
    _serviceID = [traService.serviceInternalID integerValue];
    _serviceName = traService.serviceName;
    _serviceLogo = traService.serviceIconName;
    _serviceOrder = [traService.serviceOrder integerValue];
    _serviceDisplayLogo = traService.serviceIconName;
    _serviceEnable = [traService.serviceEnable boolValue];
    _serviceType = [traService.serviceType integerValue];
    
    return self;
}

@end
