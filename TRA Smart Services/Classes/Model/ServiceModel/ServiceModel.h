//
//  ServiceModel.h
//  TRA Smart Services
//
//  Created by Admin on 09.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "TRAStaticService.h"

@interface ServiceModel : NSObject

@property (assign, nonatomic) NSInteger serviceID;
@property (strong, nonatomic) NSString *serviceName;
@property (strong, nonatomic) NSString *serviceLogo;
@property (assign, nonatomic) NSInteger serviceOrder;
@property (strong, nonatomic) NSString *serviceDisplayLogo;
@property (assign, nonatomic) BOOL serviceEnable;
@property (assign, nonatomic) NSInteger serviceType;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
- (instancetype)initWithTRAService:(TRAStaticService *)traService;

@end
