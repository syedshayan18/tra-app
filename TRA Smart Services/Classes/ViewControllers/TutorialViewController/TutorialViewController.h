//
//
//  Created by Admin on 10.11.14.
//

static NSString *const KeyIsTutorialShowed = @"KeyIsTutorialShowed";

@interface TutorialViewController : BaseDynamicUIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) void (^didCloseViewController)();
@property (strong, nonatomic) UIImage *speedAccessCollectionViewImage;
@property (strong, nonatomic) UIImage *tabBarImage;
@property (assign, nonatomic) CGRect tabBarRect;
@property (assign, nonatomic) CGRect speedAccessCollectionViewRect;

@end
