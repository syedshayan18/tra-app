//
//
//  Created by Admin on 10.11.14.
//

#import "TutorialViewController.h"
#import "Animation.h"
#import "TutorialProfileCollectionViewCell.h"
#import "TutorialSpeedAccessCollectionViewCell.h"
#import "TutorialTabBarCollectionViewCell.h"

static NSUInteger const TutorialPageCount = 2;

static LanguageType startLanguage;

@interface TutorialViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *fakeColorShemeView;
@property (weak, nonatomic) IBOutlet UIPageControl *tutorialPageControl;

@end

@implementation TutorialViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self registerNibs];
    [self configurePageControl];
    
    if ([AppHelper isiOS9_0OrHigher]) {
        if (!startLanguage) {
            startLanguage = self.dynamicService.language;
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0 to:1. delegate:nil] forKey:nil];
    self.view.layer.opacity = 5.f;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!IS_IPAD) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.collectionView.layoutMargins = UIEdgeInsetsZero;
}

#pragma mark - IBActions

- (IBAction)closeButtonTapped:(id)sender
{
    [self storeTutorialStatus];
    if (!IS_IPAD) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    }
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"fadeAnimation"];
    self.view.layer.opacity = 0.f;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return TutorialPageCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    switch (indexPath.row) {
        case 0:{
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:TutorialProfileCollectionViewCellIdentifier forIndexPath:indexPath];
            if (!cell) {
                cell = [[TutorialProfileCollectionViewCell alloc] init];
            }
            [((TutorialProfileCollectionViewCell *)cell) prepareCell];
            break;
        }
        case 1:{
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:TutorialSpeedAccessCollectionViewCellIdentifier forIndexPath:indexPath];
            if (!cell) {
                cell = [[TutorialSpeedAccessCollectionViewCell alloc] init];
            }
            ((TutorialSpeedAccessCollectionViewCell *)cell).speedAccessCollectionViewImage = self.speedAccessCollectionViewImage;
            ((TutorialSpeedAccessCollectionViewCell *)cell).speedAccessCollectionViewRect = self.speedAccessCollectionViewRect;
            [((TutorialSpeedAccessCollectionViewCell *)cell) prepareCell];
            break;
        }
        case 2:{
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:TutorialTabBarCollectionViewCellIdentifier forIndexPath:indexPath];
            if (!cell) {
                cell = [[TutorialTabBarCollectionViewCell alloc] init];
            }
            ((TutorialTabBarCollectionViewCell *)cell).tabBarImage = self.tabBarImage;
            ((TutorialTabBarCollectionViewCell *)cell).tabBarRect = self.tabBarRect;
            [((TutorialTabBarCollectionViewCell *)cell) prepareCell];
            break;
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    if (self.dynamicService.language == LanguageTypeArabic) {
        cell.transform = CGAffineTransformScale(CGAffineTransformIdentity, -1, 1);
    } else {
        cell.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    }
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.collectionView.bounds.size;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    for (NSIndexPath *indexPath in self.collectionView.indexPathsForVisibleItems) {
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        if (ABS(cell.frame.origin.x - self.collectionView.contentOffset.x) <= 50) {
            self.tutorialPageControl.currentPage = indexPath.row;
        }
    }
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"fadeAnimation"]) {
        if (self.didCloseViewController) {
            self.didCloseViewController();
        }
        [self.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - SuperClassMethods

- (void)updateColors
{
    self.fakeColorShemeView.backgroundColor = [[self.dynamicService currentApplicationColor] colorWithAlphaComponent:0.8f];
}

- (void)localizeUI
{
    [self.collectionView reloadData];
}

- (void)setRTLArabicUI
{
    self.collectionView.transform = CGAffineTransformScale(CGAffineTransformIdentity, -1, 1);
    self.tutorialPageControl.transform = CGAffineTransformScale(CGAffineTransformIdentity, -1, 1);
}

- (void)setLTREuropeUI
{
    self.collectionView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    self.tutorialPageControl.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
}

#pragma mark - Private

- (void)registerNibs
{
    [self.collectionView registerClass:[TutorialProfileCollectionViewCell class] forCellWithReuseIdentifier:TutorialProfileCollectionViewCellIdentifier];
    [self.collectionView registerClass:[TutorialSpeedAccessCollectionViewCell class] forCellWithReuseIdentifier:TutorialSpeedAccessCollectionViewCellIdentifier];
    [self.collectionView registerClass:[TutorialTabBarCollectionViewCell class] forCellWithReuseIdentifier:TutorialTabBarCollectionViewCellIdentifier];
}

- (void)configurePageControl
{
    self.tutorialPageControl.numberOfPages = TutorialPageCount;
}

- (void)storeTutorialStatus
{
    [[NSUserDefaults standardUserDefaults] setValue:@(1) forKey:KeyIsTutorialShowed];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
