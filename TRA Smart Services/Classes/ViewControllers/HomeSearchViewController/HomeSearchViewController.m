//
//  HomeSearchViewController.m
//  TRA Smart Services
//
//  Created by Admin on 09.09.15.
//

#import "HomeSearchViewController.h"
#import "Animation.h"
#import "AppDelegate.h"
#import "TRARefreshContentView.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "TransactionsDetaisViewController.h"
#import "DetailsViewController.h"

static NSString *const HomeSearchCellIdentifier = @"homeSearchCell";
static NSString *const HomeSearchHeaderCellIdentifier = @"homeSearchHeaderCell";

static CGFloat const HeightTableViewCell = 35.f;
static NSUInteger const PageSize = 15;

@interface HomeSearchViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *conteinerSearchView;
@property (weak, nonatomic) IBOutlet UITextField *homeSearchTextField;
@property (weak, nonatomic) IBOutlet UILabel *homeSearchLabel;
@property (weak, nonatomic) IBOutlet UIImageView *fakeBackgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *conteinerView;

@property (weak, nonatomic) IBOutlet UIImageView *iconSearchImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomTableViewConstraint;
@property (weak, nonatomic) IBOutlet UILabel *notSearchDataLabel;
@property (strong, nonatomic) TRALoaderViewController *loader;
@property (assign, nonatomic) NSInteger loaderIndex;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) TRARefreshContentView *traRefreshContentView;
@property (strong, nonatomic) UIRefreshControl *bottomRefreshControl;
@property (strong, nonatomic) TRARefreshContentView *traBottomRefreshContentView;

@property (strong, nonatomic) NSArray <TRABaseService *> *dataSourceServices;
@property (strong, nonatomic) NSArray <TRABaseService *> *filteredDataSourceServices;

@property (strong, nonatomic) NSMutableArray *dataSourceTransactions;
@property (assign, nonatomic) __block NSInteger pageSearchTransactions;

@property (strong, nonatomic) NSString *searchString;
@property (strong, nonatomic) NSMutableArray *dataSourceAnnoucements;
@property (assign, nonatomic) __block NSInteger pageSearchAnnoucements;
@property (strong, nonatomic) NSMutableSet *searchDataSourceSetAnnoucements;

@end

@implementation HomeSearchViewController

#pragma mark - Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    _noresultLabel.text = @"No Result Found";
    [self prepareUI];
    [self prepareDataSource];
    [self addGestureRecognizer];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style: UIBarButtonItemStylePlain target:nil action:nil];
    [self.conteinerView.layer addAnimation:[Animation fadeAnimFromValue:0.f to:1.0f delegate:nil] forKey:nil];
    [self prepareNavigationBlock];
}

- (void)prepareNavigationBlock
{
    __weak typeof(self) weakSelf = self;
    self.didSelectTransaction = ^(TransactionModel *selectedTransactionModel){
        TransactionsDetaisViewController *detailsViewController = [weakSelf.storyboard instantiateViewControllerWithIdentifier:@"transactionsDetaisViewController"];
        detailsViewController.transactionModel = selectedTransactionModel;
        [weakSelf.navigationController pushViewController:detailsViewController animated:YES];
    };
    self.didSelectAnnouncement = ^(Announcement *selectedAnnouncement){
        DetailsViewController *detailsViewController = [weakSelf.storyboard instantiateViewControllerWithIdentifier:@"detailsViewContrrollerID"];
        detailsViewController.selectedAnnouncement = selectedAnnouncement;
        [weakSelf.navigationController pushViewController:detailsViewController animated:YES];
    };
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareNotification];
    self.navigationController.navigationBar.hidden = YES;
    
    if (!IS_IPAD) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.homeSearchTextField.text.length) {
        [self.homeSearchTextField becomeFirstResponder];
    }
    [self prepareTraRefreshControlViewes];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [self removeNotifications];
    if (!IS_IPAD) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    }
    self.navigationController.navigationBar.hidden = NO;
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HomeSearchCellIdentifier];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HeightTableViewCell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.searchType == SearchTypeAllApp ? 3 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (self.searchType) {
        case SearchTypeRationgs:
        case SearchTypeServices:
        case SearchTypeFavorites: {
            count = self.filteredDataSourceServices.count;
            break;
        }
        case SearchTypeInfoHubTransactions: {
            count = self.dataSourceTransactions.count;
            break;
        }
        case SearchTypeAnnoucements: {
            count = self.dataSourceAnnoucements.count;
            break;
        }
        case SearchTypeAllApp: {
            if (!section) {
                count = self.filteredDataSourceServices.count;
            } else if (section == 1) {
                count = self.dataSourceTransactions.count;
            } else if (section == 2) {
                count = self.dataSourceAnnoucements.count;
            }
            break;
        }
    }
    if ((!count && !self.notSearchDataLabel.alpha) || (count && self.notSearchDataLabel.alpha)) {
        self.notSearchDataLabel.alpha = count ? 0. : 1.;
        self.homeSearchLabel.alpha = count ? 1. : 0.;
        [self.notSearchDataLabel.layer addAnimation:[Animation fadeAnimFromValue:count ? 1. : 0. to:count ? 0. : 1. delegate:nil] forKey:nil];
        [self.homeSearchLabel.layer addAnimation:[Animation fadeAnimFromValue:count ? 0. : 1. to:count ? 1. : 0. delegate:nil] forKey:nil];
    }

    return count;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.searchType) {
        case SearchTypeRationgs:
        case SearchTypeServices:
        case SearchTypeFavorites: {
            if (self.didSelectService) {
                self.didSelectService(self.filteredDataSourceServices[indexPath.row]);
            }
            break;
        }
        case SearchTypeInfoHubTransactions: {
            if (self.didSelectTransaction) {
                self.didSelectTransaction(self.dataSourceTransactions[indexPath.row]);
            }
            break;
        }
        case SearchTypeAnnoucements: {
            if (self.didSelectAnnouncement) {
                self.didSelectAnnouncement(self.dataSourceAnnoucements[indexPath.row]);
            }
        }
        case SearchTypeAllApp: {
            if (!indexPath.section) {
                if (self.didSelectService) {
                    self.didSelectService(self.filteredDataSourceServices[indexPath.row]);
                }
            } else if (indexPath.section == 1) {
                if (self.didSelectTransaction) {
                    self.didSelectTransaction(self.dataSourceTransactions[indexPath.row]);
                }
            } else if (indexPath.section == 2) {
                if (self.didSelectAnnouncement) {
                    self.didSelectAnnouncement(self.dataSourceAnnoucements[indexPath.row]);
                }
            }
            break;
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self searchedTapped];
    return YES;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat progress = - self.tableView.contentOffset.y / self.refreshControl.frame.size.height;
    if (progress > 0) {
        [self.traRefreshContentView setOpacityValue:progress <= 1 ? progress : 1];
    }
    CGFloat progressBottom = self.tableView.contentSize.height < self.tableView.bounds.size.height ? self.tableView.contentOffset.y / self.bottomRefreshControl.frame.size.height : - (self.tableView.contentSize.height - self.tableView.bounds.size.height - self.tableView.contentOffset.y) / self.bottomRefreshControl.frame.size.height;

    if (progressBottom > 0) {
        [self.traBottomRefreshContentView setOpacityValue:progressBottom <= 1 ? progressBottom : 1];
    }
}

#pragma mark - IBActions

- (IBAction)closeButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)searchedTapped
{
    switch (self.searchType) {
        case SearchTypeInfoHubTransactions: {
            [self addObjectsToDataSourceIfPossibleTransactions];
            break;
        }
        case SearchTypeAnnoucements: {
            [self addObjectsToDataSourceIfPossibleAnnouncements];
            break;
        }
        case SearchTypeAllApp: {
            [self reloadFilteredTableView];
            [self addObjectsToDataSourceIfPossibleTransactions];
            [self addObjectsToDataSourceIfPossibleAnnouncements];
            break;
        }
        default:
            break;
    }
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    self.homeSearchLabel.text = dynamicLocalizedString(@"homeSearchViewController.homeSearchLabel.text");
    self.homeSearchTextField.placeholder = dynamicLocalizedString(@"homeSearchViewController.homeSearchTextField.placeholder");
//    self.notSearchDataLabel.text = dynamicLocalizedString(@"listOfDevicesViewController.devicesNotDataLabel");
    self.notSearchDataLabel.text = dynamicLocalizedString(@"homeSearchViewController.noresultLabel.text");
    
}

- (void)updateColors
{
    self.conteinerView.backgroundColor = [[self.dynamicService currentApplicationColor] colorWithAlphaComponent:0.95];
}

- (void)setRTLArabicUI
{
    [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 35, 0, 15)];
  
    self.homeSearchLabel.textAlignment = NSTextAlignmentRight;
    self.homeSearchTextField.textAlignment = NSTextAlignmentRight;
    
    self.conteinerSearchView.layer.transform = TRANFORM_3D_SCALE;
    self.homeSearchLabel.layer.transform = TRANFORM_3D_SCALE;
    self.homeSearchTextField.layer.transform = TRANFORM_3D_SCALE;
}

- (void)setLTREuropeUI
{
    [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 35)];
}

#pragma mark - KeyboardNotification

- (void)prepareNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeText:) name: UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)textFieldDidChangeText:(id)sender
{
    if (self.dataSourceServices && self.searchType != SearchTypeAllApp) {
        [self reloadFilteredTableView];
    }
}

- (void)keyboardWillAppear:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboardRect.size.height;
    
    if (!self.bottomTableViewConstraint.constant) {
        __weak typeof(self) weakSelf = self;
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:0.25 animations:^{
            weakSelf.bottomTableViewConstraint.constant = keyboardHeight;
            [weakSelf.view layoutIfNeeded];
        }];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    __weak typeof(self) weakSelf = self;
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.bottomTableViewConstraint.constant = 0;
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - Gestures

- (void)addGestureRecognizer
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.iconSearchImageView addGestureRecognizer:tapGesture];
}

- (void)tapGesture:(UITapGestureRecognizer *)gesture
{
    [self searchedTapped];
}

#pragma mark - Prepare RefreshControl

- (void)prepareRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(actionRefresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    self.refreshControl.layer.zPosition = -1;
    self.refreshControl.tintColor = [UIColor clearColor];
}

- (void)prepareBottomRefreshcontrol
{
    self.bottomRefreshControl = [[UIRefreshControl alloc] init];
    self.bottomRefreshControl.triggerVerticalOffset = 80.f;
    [self.bottomRefreshControl addTarget:self action:@selector(actionBottonRefresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = self.bottomRefreshControl;
    self.bottomRefreshControl.layer.zPosition = -1;
    self.bottomRefreshControl.tintColor = [UIColor clearColor];
}

- (void)prepareTraRefreshControlViewes
{
    if (!self.traRefreshContentView && self.searchType == SearchTypeAnnoucements) {
        self.traRefreshContentView = [[TRARefreshContentView alloc] initWithFrame:self.refreshControl.bounds];
        [self.traRefreshContentView setNeedsUpdateConstraints];
        self.traRefreshContentView.bacgroungLaeyerColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        self.traRefreshContentView.animationLaeyerColor = [UIColor whiteColor];
        [self.refreshControl addSubview:self.traRefreshContentView];
    }
    if (!self.traBottomRefreshContentView) {
        self.traBottomRefreshContentView = [[TRARefreshContentView alloc] initWithFrame:self.bottomRefreshControl.bounds];
        [self.traBottomRefreshContentView setNeedsUpdateConstraints];
        self.traBottomRefreshContentView.bacgroungLaeyerColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        self.traBottomRefreshContentView.animationLaeyerColor = [UIColor whiteColor];
        [self.bottomRefreshControl addSubview:self.traBottomRefreshContentView];
    }
}

- (void)stopAnimationRefreshControlers
{
    if (self.refreshControl.refreshing) {
        [self.traRefreshContentView stopAnimation];
        [self.refreshControl endRefreshing];
    }
    if (self.bottomRefreshControl.refreshing) {
        [self.traBottomRefreshContentView stopAnimation];
        [self.bottomRefreshControl endRefreshing];
    }
}

#pragma mark - RefreshControl Action

- (void)actionRefresh
{
    self.pageSearchAnnoucements = 0;
    [self.traRefreshContentView startAnimations];
    
    [self addObjectsToDataSourceIfPossibleAnnouncements];
}

- (void)actionBottonRefresh
{
    [self.bottomRefreshControl beginRefreshing];
    
    [self.traBottomRefreshContentView startAnimations];
    
    if (self.searchType == SearchTypeAnnoucements) {
        [self addObjectsToDataSourceIfPossibleAnnouncements];
    } else if (self.searchType == SearchTypeInfoHubTransactions) {
        [self addObjectsToDataSourceIfPossibleTransactions];
    }
}

#pragma mark - Private

- (void)reloadFilteredTableView
{
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(TRABaseService *service, NSDictionary *bindings) {
        NSString *localizedServiceName = [self serviceNameTRABaseService:service];
        
        BOOL containsString = [localizedServiceName rangeOfString:self.homeSearchTextField.text options:NSCaseInsensitiveSearch].location != NSNotFound;
        
        if ([service isKindOfClass:[TRAStaticService class]]) {
            if (![((TRAStaticService *)service).serviceEnable boolValue]) {
                containsString = NO;
            }
        }
        return containsString;
    }];
    self.filteredDataSourceServices = [[self.dataSourceServices filteredArrayUsingPredicate:predicate] mutableCopy];
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
    if (self.filteredDataSourceServices.count > 0) {
        
        _notSearchDataLabel.hidden = true;
    }
    else {
        
        _notSearchDataLabel.hidden = false;
    }
    
   
}


- (void)prepareDataSource
{
    self.filteredDataSourceServices = [[NSArray alloc] init];
    switch (self.searchType) {
        case SearchTypeRationgs:
        case SearchTypeServices:{
            self.dataSourceServices = [[[CoreDataManager sharedManager] allDBSortedArray] mutableCopy];
            break;
        }
        case SearchTypeFavorites:{
            self.dataSourceServices =  [[[CoreDataManager sharedManager] favoriteDBSortedArray] mutableCopy];
            break;
        }
        case SearchTypeInfoHubTransactions:{
            [self setupInitialParametersSearchTransactions];
            [self prepareBottomRefreshcontrol];
            break;
        }
        case SearchTypeAnnoucements:{
            [self setupInitialParametersSearchAnnoucements];
            [self prepareRefreshControl];
            [self prepareBottomRefreshcontrol];
            break;
        }
        case SearchTypeAllApp: {
            self.dataSourceServices = [[[CoreDataManager sharedManager] allDBSortedArray] mutableCopy];
            [self setupInitialParametersSearchTransactions];
            [self setupInitialParametersSearchAnnoucements];
            break;
        }
    }
}

- (void)prepareUI
{
    [self.homeSearchTextField setTintColor:[UIColor whiteColor]];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.view.backgroundColor = [UIColor lightGrayTabBarColor];
    
    if (self.fakeBackground) {
        self.fakeBackgroundImageView.image = self.fakeBackground;
    }
}

- (void)highlightingSearchText:(UILabel *)label
{
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName : [UIColor colorWithWhite:1 alpha:0.5],
                              NSFontAttributeName : label.font
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attribs];
    NSRange searchTextRange = [label.text rangeOfString:self.homeSearchTextField.text options:NSCaseInsensitiveSearch];
    [attributedText setAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} range:searchTextRange];
    label.attributedText = attributedText;
}

#pragma mark - ConfigurationscCell

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    switch (self.searchType) {
        case SearchTypeRationgs:
        case SearchTypeServices:
        case SearchTypeFavorites: {
            TRABaseService *traBaseService = self.filteredDataSourceServices[indexPath.row];
            cell.textLabel.text = [self replaceString:[self serviceNameTRABaseService:traBaseService]];
            break;
        }
        case SearchTypeInfoHubTransactions: {
            TransactionModel *selectedTransactions = self.dataSourceTransactions[indexPath.row];
            cell.textLabel.text = selectedTransactions.title;
            break;
        }
        case SearchTypeAnnoucements: {
            Announcement *selectedAnnoucement = self.dataSourceAnnoucements[indexPath.row];
            cell.textLabel.text = selectedAnnoucement.announcementTitle;
            break;
        }
        case SearchTypeAllApp: {
            if (!indexPath.section) {
                TRABaseService *traBaseService = self.filteredDataSourceServices[indexPath.row];
                cell.textLabel.text = [self replaceString:[self serviceNameTRABaseService:traBaseService]];
            } else if (indexPath.section == 1) {
                TransactionModel *selectedTransactions = self.dataSourceTransactions[indexPath.row];
                cell.textLabel.text = selectedTransactions.title;
            } else if (indexPath.section == 2) {
                Announcement *selectedAnnoucement = self.dataSourceAnnoucements[indexPath.row];
                cell.textLabel.text = selectedAnnoucement.announcementTitle;
            }
            break;
        }
    }
    if (self.dynamicService.language == LanguageTypeArabic) {
        cell.textLabel.textAlignment = NSTextAlignmentRight;
        cell.textLabel.font = [UIFont droidKufiRegularFontForSize:14];
    } else {
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.font = [UIFont latoRegularWithSize:14];
    }
    cell.textLabel.textColor = [UIColor whiteColor];
    if (self.homeSearchTextField.text.length) {
        [self highlightingSearchText:cell.textLabel];
    }
}

- (NSString *)serviceNameTRABaseService:(TRABaseService *)traBaseService
{
    NSString *serviceName;
    if ([traBaseService isKindOfClass:[TRAStaticService class]]) {
        serviceName = dynamicLocalizedString(((TRAStaticService *)traBaseService).serviceName);
    } else if ([traBaseService isKindOfClass:[TRADynamicService class]]){
        serviceName = self.dynamicService.language == LanguageTypeArabic ? ((TRADynamicService *)traBaseService).serviceNameAR : ((TRADynamicService *)traBaseService).serviceNameEN;
    }
    return serviceName;
}

- (NSString *)replaceString:(NSString *)string
{
    NSCharacterSet *charactersToRemove = [NSCharacterSet characterSetWithCharactersInString:@"\n"];
    NSString *trimmedReplacement = [[string componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@" "];
    return trimmedReplacement;
}

#pragma mark - Configuration

- (void)setupInitialParametersSearchTransactions
{
    self.pageSearchTransactions = 1;
    self.dataSourceTransactions = [[NSMutableArray alloc] init];

    [self.tableView reloadData];
}

- (void)setupInitialParametersSearchAnnoucements
{
    self.pageSearchAnnoucements = 0;
    self.dataSourceAnnoucements = [[NSMutableArray alloc] init];
    self.searchDataSourceSetAnnoucements = [[NSMutableSet alloc] init];

    [self.tableView reloadData];
}

- (void)addObjectsToDataSourceIfPossibleTransactions
{
    if (![NetworkManager sharedManager].isUserLoggined) {
        return;
    }

    if (self.homeSearchTextField.text.length) {
        [self.view endEditing:YES];
        if (![self.searchString isEqualToString:self.homeSearchTextField.text]) {
            [self setupInitialParametersSearchTransactions];
        }
        if (!self.bottomRefreshControl.refreshing) {
            if (![[self presentedViewController] isKindOfClass:[TRALoaderViewController class]]) {
                self.loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:dynamicLocalizedString(@"homeSearchViewController.homeSearchTextField.placeholder") closeButton:NO];
            }
            self.loaderIndex++;
        }
        self.searchString = self.homeSearchTextField.text;

        [self fetchSearchedTransactions];
    }
}

- (void)addObjectsToDataSourceIfPossibleAnnouncements
{
    if (self.homeSearchTextField.text.length) {
        [self.view endEditing:YES];
        if (![self.searchString isEqualToString:self.homeSearchTextField.text]) {
            [self setupInitialParametersSearchAnnoucements];
        } else if (self.pageSearchAnnoucements) {
            self.pageSearchAnnoucements = self.searchDataSourceSetAnnoucements.count / PageSize ;
        }
        if (!self.refreshControl.refreshing && !self.bottomRefreshControl.refreshing) {
            if (![[self presentedViewController] isKindOfClass:[TRALoaderViewController class]]) {
                self.loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:dynamicLocalizedString(@"homeSearchViewController.homeSearchTextField.placeholder") closeButton:NO];
            }
            self.loaderIndex++;
        }
        self.searchString = self.homeSearchTextField.text;
        
        [self fetchSearchedAnnouncementsData];
    }
}

#pragma mark - DataSource Configurations

- (void)fetchSearchedTransactions
{
    if (![NetworkManager sharedManager].isUserLoggined) {
        self.loaderIndex--;
        if (!self.loaderIndex) {
            [self.loader dismissTRALoader:YES];
        }
        return;
    }
    NSInteger pageSize = self.pageSearchTransactions == 1 ? (NSInteger)(self.tableView.frame.size.height / HeightTableViewCell) : PageSize;
    
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] traSSNoCRMServiceSearchTransactions:self.pageSearchTransactions count:pageSize orderAsc:1 searchText:self.homeSearchTextField.text responseBlock:^(id response, NSError *error, NSString *requestString) {
        if (error) {
            NSString *errorString = [response isKindOfClass:[NSString class]] ? response : error.localizedDescription;
          
            if ([[weakSelf presentedViewController] isKindOfClass:[TRALoaderViewController class]]) {
                //shayan loader issue solved
                weakSelf.loader.ratingView.hidden = true;
                [weakSelf.loader setCompletedStatus:TRACompleteStatusFailure withDescription: errorString];
            } else {
                [AppHelper alertViewWithMessage:errorString];
            }
        } else {
            if (((NSArray *)response).count) {
                weakSelf.pageSearchTransactions++;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf prepareFilteredDataSource:response];
                });
            }
            if ([[weakSelf presentedViewController] isKindOfClass:[TRALoaderViewController class]]) {
                weakSelf.loaderIndex--;
                if (!weakSelf.loaderIndex) {
                    [weakSelf.loader dismissTRALoader:YES];
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf stopAnimationRefreshControlers];
        });
    }];
}

- (void)prepareFilteredDataSource:(NSArray *)inputArray
{
    NSMutableArray *indexPathToAdd = [[NSMutableArray alloc] init];
    for (int i = (int)self.dataSourceTransactions.count; i < (int)self.dataSourceTransactions.count + inputArray.count; i++) {
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:i inSection:self.searchType == SearchTypeAllApp ? 1 : 0];
        [indexPathToAdd addObject:newIndexPath];
    }
    [self.tableView beginUpdates];
    [self.dataSourceTransactions addObjectsFromArray:inputArray];
    [self.tableView insertRowsAtIndexPaths:indexPathToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    
    [self stopAnimationRefreshControlers];
}

- (void)prepareDataSource:(NSArray *)inputArray
{
    NSMutableArray *indexPathToAdd = [[NSMutableArray alloc] init];
    for (int i = (int)self.dataSourceAnnoucements.count ; i < (int)inputArray.count; i++) {
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:i inSection:self.searchType == SearchTypeAllApp ? 2 : 0];
        [indexPathToAdd addObject:newIndexPath];
    }
    [self.tableView beginUpdates];
    self.dataSourceAnnoucements = [inputArray mutableCopy];
    [self.tableView insertRowsAtIndexPaths:indexPathToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    
    self.pageSearchAnnoucements++;
    [self stopAnimationRefreshControlers];
}
- (IBAction)clearButtonTapped:(id)sender {
    
    _homeSearchTextField.text = @"";
}

- (void)fetchSearchedAnnouncementsData
{
    NSInteger pageSize = !self.pageSearchAnnoucements ? (NSInteger)(self.tableView.frame.size.height / HeightTableViewCell) : PageSize;

    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] traSSNoCRMServiceSearchAnnoucement:PageSize offset:pageSize * self.pageSearchAnnoucements searchText:self.homeSearchTextField.text responseBlock:^(id response, NSError *error, NSString *requestURI) {
        if (error) {
            NSString *errorString = [response isKindOfClass:[NSString class]] ? response : error.localizedDescription;
            if ([[weakSelf presentedViewController] isKindOfClass:[TRALoaderViewController class]]) {
                //shayan loader issue solved
                weakSelf.loader.ratingView.hidden = true;
                [weakSelf.loader setCompletedStatus:TRACompleteStatusFailure withDescription: errorString];
            } else {
                [AppHelper alertViewWithMessage:errorString];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf stopAnimationRefreshControlers];
            });
        } else {
            [weakSelf.searchDataSourceSetAnnoucements addObjectsFromArray:response];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf prepareDataSource:[weakSelf sortDataSouseSet:weakSelf.searchDataSourceSetAnnoucements]];
            });
            if ([[weakSelf presentedViewController] isKindOfClass:[TRALoaderViewController class]]) {
                weakSelf.loaderIndex--;
                if (!weakSelf.loaderIndex) {
                    [weakSelf.loader dismissTRALoader:YES];
                }
            }
        }
    }];
}

- (NSArray *)sortDataSouseSet:(NSMutableSet *)dataSet
{
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"announcementPubDate" ascending:NO];
    return [dataSet sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sort, nil]];
}

@end
