//
//  HomeSearchViewController.h
//  TRA Smart Services
//
//  Created by Admin on 09.09.15.
//

#import "TransactionModel.h"
#import "Announcement.h"
#import "TRAStaticService.h"
#import "TRADynamicService.h"

typedef NS_ENUM(NSUInteger, SearchType) {
    SearchTypeServices,
    SearchTypeFavorites,
    SearchTypeInfoHubTransactions,
    SearchTypeAnnoucements,
    SearchTypeRationgs,
    SearchTypeAllApp
};

@interface HomeSearchViewController : BaseDynamicUIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UIImage *fakeBackground;
@property (assign, nonatomic) SearchType searchType;

@property (strong, nonatomic) void (^didSelectService)(TRABaseService *selectedServiseID);
@property (strong, nonatomic) void (^didSelectTransaction)(TransactionModel *selectedTransactionModel);
@property (weak, nonatomic) IBOutlet UILabel *noresultLabel;
@property (strong, nonatomic) void (^didSelectAnnouncement)(Announcement *selectedAnnouncement);

@end
