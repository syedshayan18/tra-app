//
//  FavouriteViewController.m
//  TRA Smart Services
//
//  Created by Admin on 14.08.15.
//

#import "AddToFavouriteViewController.h"
#import "AppDelegate.h"
#import "TRAStaticService.h"
#import "TRADynamicService.h"
#import "Animation.h"

static CGFloat const DefaultOffsetForElementConstraintInCell = 20.f;
static CGFloat const SummOfVerticalOffsetsForCell = 60.f;

@interface AddToFavouriteViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) NSMutableArray *filteredDataSource;

@end

@implementation AddToFavouriteViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNibs];
    [self fetchServiceList];
    [self prepareButtonAddToFavouriteServices];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[CoreDataManager sharedManager].managedObjectContext rollback];
}

#pragma mark - IBAction

- (void)addToFavouriteServices
{
    [[CoreDataManager sharedManager] saveContext];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = self.dynamicService.language == LanguageTypeArabic ? AddToFavouriteArabicCellIdentifier : AddToFavouriteEuroCellIdentifier;
    AddToFavouriteTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self addRemoveFavoriteService:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *serviceTitle;
    if ([self.dataSource[indexPath.row] isKindOfClass:[TRAStaticService class]]) {
        serviceTitle = ((TRAStaticService *)self.dataSource[indexPath.row]).serviceDescription;
    } else if ([self.dataSource[indexPath.row] isKindOfClass:[TRADynamicService class]]) {
        serviceTitle = self.dynamicService.language == LanguageTypeArabic ? ((TRADynamicService *)self.dataSource[indexPath.row]).serviceNameAR : ((TRADynamicService *)self.dataSource[indexPath.row]).serviceNameEN;
    }

    UIFont *font = self.dynamicService.language == LanguageTypeArabic ? [UIFont droidKufiRegularFontForSize:12.f] : [UIFont latoRegularWithSize:12.f];
    NSDictionary *attributes = @{ NSFontAttributeName :font };

    CGSize textSize = [serviceTitle sizeWithAttributes:attributes];
    CGFloat widthOfViewWithImage = 85.f;
    CGFloat labelWidth = SCREEN_WIDTH - (widthOfViewWithImage + DefaultOffsetForElementConstraintInCell * 2);
    
    NSUInteger numbersOfLines = ceil(textSize.width / labelWidth);
    CGFloat height = numbersOfLines * textSize.height + SummOfVerticalOffsetsForCell;
    
    return height;
}

- (void)configureCell:(AddToFavouriteTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIColor *pairCellColor = [[self.dynamicService currentApplicationColor] colorWithAlphaComponent:0.1f];
    cell.backgroundColor = indexPath.row % 2 ? pairCellColor : [UIColor clearColor];
    
    if ([self.dataSource[indexPath.row] isKindOfClass:[TRAStaticService class]]) {
        TRAStaticService *traService = (TRAStaticService *)self.dataSource[indexPath.row];
        cell.contentView.alpha = [traService.serviceEnable boolValue] ? 1 : 0.2;
        cell.favouriteServiceLogoImageView.image = [UIImage imageNamed:traService.serviceIconName];
        cell.favourieDescriptionLabel.text = [dynamicLocalizedString(traService.serviceName) stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        [cell setServiceFavourite:[traService.serviceIsFavorite boolValue]];
    } else if ([self.dataSource[indexPath.row] isKindOfClass:[TRADynamicService class]]) {
        TRADynamicService *traDynamicService = (TRADynamicService *)self.dataSource[indexPath.row];
        UIImage *logoPlaceholder = [UIImage imageNamed:@"ic_domain_logo"];
        [AppHelper downloadToImageView:cell.favouriteServiceLogoImageView url:traDynamicService.serviceIcon placeholderImage:logoPlaceholder];
        NSString *nameService = self.dynamicService.language == LanguageTypeArabic ? traDynamicService.serviceNameAR : traDynamicService.serviceNameEN;
        cell.favourieDescriptionLabel.text = [nameService stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        [cell setServiceFavourite:[traDynamicService.serviceIsFavorite boolValue]];
    }
    
    cell.indexPath = indexPath;
    cell.delegate = self;
}

#pragma mark - Private

- (void)updateAndDisplayDataSource
{
    [self fetchServiceList];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)registerNibs
{
    [self.tableView registerNib:[UINib nibWithNibName:@"AddToFavouriteEuroTableViewCell" bundle:nil] forCellReuseIdentifier:AddToFavouriteEuroCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"AddToFavouriteArabicTableViewCell" bundle:nil ] forCellReuseIdentifier:AddToFavouriteArabicCellIdentifier];
}

#pragma mark - CoreData

- (void)fetchServiceList
{
    self.dataSource = [[[CoreDataManager sharedManager] allDBSortedArray] mutableCopy];
}

#pragma mark - FavouriteTableViewCellDelegate

- (void)addRemoveFavoriteService:(NSIndexPath *)indexPath
{
    BOOL isFavourite;
    if ([self.dataSource[indexPath.row] isKindOfClass:[TRAStaticService class]]) {
        TRAStaticService *selectedService = self.dataSource[indexPath.row];
        if ([selectedService.serviceEnable boolValue] || [selectedService.serviceIsFavorite boolValue]) {
            selectedService.serviceIsFavorite = @(![selectedService.serviceIsFavorite boolValue]);
            isFavourite = [selectedService.serviceIsFavorite boolValue];
        }
    } else if ([self.dataSource[indexPath.row] isKindOfClass:[TRADynamicService class]]) {
        TRADynamicService *selectedService = self.dataSource[indexPath.row];
        selectedService.serviceIsFavorite = @(![selectedService.serviceIsFavorite boolValue]);
        isFavourite = [selectedService.serviceIsFavorite boolValue];
    }
    AddToFavouriteTableViewCell *cell = (AddToFavouriteTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    [cell setServiceFavourite:isFavourite];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"favourite.title");
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
    [self.tableView reloadData];
}

- (void)setRTLArabicUI
{
    [super setRTLArabicUI];
    [self fetchServiceList];
}

- (void)setLTREuropeUI
{
    [super setLTREuropeUI];
    [self fetchServiceList];
}

- (void)prepareButtonAddToFavouriteServices
{
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_nav_check"] style:UIBarButtonItemStylePlain target:self action:@selector(addToFavouriteServices)];
    self.navigationItem.rightBarButtonItems = @[addButton];
}

@end
