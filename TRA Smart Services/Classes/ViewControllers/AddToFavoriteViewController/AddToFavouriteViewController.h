//
//  FavouriteViewController.h
//  TRA Smart Services
//
//  Created by Admin on 14.08.15.
//

#import "AddToFavouriteTableViewCell.h"

@interface AddToFavouriteViewController : BaseDynamicUIViewController <UITableViewDataSource, UITableViewDelegate, AddToFavouriteTableViewCellDelegate>

@end
