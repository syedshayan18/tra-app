//
//  TermsPrivacyViewController.m
//  TRA Smart Services
//
//  Created by Admin on 22.12.15.
//  Copyright © 2015 . All rights reserved.
//

#import "TermsPrivacyViewController.h"
#import "Animation.h"

@interface TermsPrivacyViewController ()

@property (weak, nonatomic) IBOutlet UIView *segmentContainerView;
@property (weak, nonatomic) IBOutlet UIView *containerTermsView;
@property (weak, nonatomic) IBOutlet UIView *containerPrivacyView;
@property (weak, nonatomic) IBOutlet UILabel *termsTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *termsDescriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *privacyTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *privacyDescriptionTextView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UIImageView *termsLogoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *privacyLogoImageView;

@end

@implementation TermsPrivacyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self preparePrivacyContainer];
    [self prepareTermsContainer];
}

#pragma mark - SegmentViewDelegate

- (IBAction)segmentSwitch:(id)sender
{
    NSInteger selectedSegment = self.segmentedControl.selectedSegmentIndex;
    if (selectedSegment == 0) {
        [self animationShowView:self.containerTermsView];
        [self animationHideView:self.containerPrivacyView forKey:@"hidePrivacy"];
    } else  if (selectedSegment == 1) {
        [self animationShowView:self.containerPrivacyView];
        [self animationHideView:self.containerTermsView forKey:@"hideTerms"];
    }
}

#pragma mark - Animations

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.containerPrivacyView.layer animationForKey:@"hidePrivacy"]) {
        [self.containerPrivacyView.layer removeAllAnimations];
        self.containerPrivacyView.hidden = YES;
    } else if (anim == [self.containerTermsView.layer animationForKey:@"hideTerms"]) {
        [self.containerTermsView.layer removeAllAnimations];
        self.containerTermsView.hidden = YES;
    }
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"termsPrivacyViewController.title");
    [self.segmentedControl setTitle: dynamicLocalizedString(@"termsPrivacyViewController.segmentedControl.terms") forSegmentAtIndex:0];
    [self.segmentedControl setTitle: dynamicLocalizedString(@"termsPrivacyViewController.segmentedControl.privacy") forSegmentAtIndex:1];
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
    
    self.segmentContainerView.backgroundColor = self.dynamicService.currentApplicationColor;
}

- (void)setLTREuropeUI
{
    [self changeElementsAligment:NSTextAlignmentLeft];
}

- (void)setRTLArabicUI
{
    [self changeElementsAligment:NSTextAlignmentRight];
}

- (void)changeElementsAligment:(NSTextAlignment)textAlignment
{
    self.termsTitleLabel.textAlignment = textAlignment;
    self.privacyTitleLabel.textAlignment = textAlignment;
}

#pragma mark - Private

- (void)animationShowView:(UIView *)view
{
    [view.layer addAnimation:[Animation fadeAnimFromValue:0.f to:1.0f delegate:nil] forKey:nil];
    view.hidden = NO;
    view.layer.opacity = 1.f;
}

- (void)animationHideView:(UIView *)view forKey:(NSString *)key
{
    [view.layer addAnimation:[Animation fadeAnimFromValue:1.f to:0.0f delegate:nil] forKey:key];
    view.layer.opacity = 0.f;
}

- (void)prepareTermsContainer
{
    NSString *fileName = self.dynamicService.language == LanguageTypeArabic ? @"TermsAr" : @"TermsEn";
    NSURL *url = [[NSBundle mainBundle] URLForResource:fileName withExtension:@"rtf"];
    NSAttributedString *dataString = [[NSAttributedString alloc] initWithFileURL:url options:@{ NSPlainTextDocumentType : NSRTFTextDocumentType } documentAttributes:kNilOptions error:nil];
    self.termsDescriptionTextView.attributedText = dataString;
    self.termsDescriptionTextView.textAlignment = NSTextAlignmentJustified;
    
    self.termsTitleLabel.text = dynamicLocalizedString(@"termsPrivacyViewController.termsTitleLabel");
}

- (void)preparePrivacyContainer
{
    NSString *fileName = self.dynamicService.language == LanguageTypeArabic ? @"PrivacyAr" : @"PrivacyEn";
    NSURL *url = [[NSBundle mainBundle] URLForResource:fileName withExtension:@"rtf"];
    NSAttributedString *dataString = [[NSAttributedString alloc] initWithFileURL:url options:@{ NSPlainTextDocumentType : NSRTFTextDocumentType } documentAttributes:kNilOptions error:nil];
    self.privacyDescriptionTextView.attributedText = dataString;
    self.privacyDescriptionTextView.textAlignment = NSTextAlignmentJustified;

    self.privacyTitleLabel.text = dynamicLocalizedString(@"termsPrivacyViewController.privacyTitleLabel");
}

@end
