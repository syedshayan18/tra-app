//
//  AnnoucementsViewController.m
//  TRA Smart Services
//
//  Created by Admin on 18.08.15.
//

#import "AnnoucementsViewController.h"
#import "AnnoucementsTableViewCell.h"
#import "DetailsViewController.h"
#import "Announcement.h"
#import "UIImageView+AFNetworking.h"
#import "TRARefreshContentView.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "HomeSearchViewController.h"
#import "LoginViewController.h"
static NSString *const SegueToDetailsViewControllerIdentifier = @"segueToDetailsViewController";
static CGFloat const AdditionalCellOffset = 20.0f;
static CGFloat const DefaultCellOffset = 24.0f;

static NSUInteger const PageSize = 10;

@interface AnnoucementsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomTableViewConstraint;

@property (strong, nonatomic) NSMutableArray *dataSourceArray;
@property (strong, nonatomic) NSMutableSet *dataSourceSet;
@property (strong, nonatomic) NSMutableSet *searchDataSourceSet;
@property (assign, nonatomic) __block NSInteger page;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) TRARefreshContentView *traRefreshContentView;
@property (strong, nonatomic) UIRefreshControl *bottomRefreshControl;
@property (strong, nonatomic) TRARefreshContentView *traBottomRefreshContentView;
@property (copy, nonatomic) NSString *searchText;

@property (strong, nonatomic) TRALoaderViewController *loader;

@end

@implementation AnnoucementsViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNibs];
    [self prepareRefreshControl];
    [self prepareBottomRefreshcontrol];
    
}
-(void)presentLoginIfNeeded{
    
    
    if ([NetworkManager sharedManager].isUserLoggined) {
        [self addObjectsToDataSourceIfPossible];
    } else {
        UINavigationController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
        __weak typeof(self) weakSelf = self;
        ((LoginViewController *)viewController.topViewController).didCloseViewController = ^() {
            if (![NetworkManager sharedManager].isUserLoggined) {
                weakSelf.tabBarController.selectedIndex = 0;
            }
        };
        ((LoginViewController *)viewController.topViewController).didDismissed = ^() {
            if ([NetworkManager sharedManager].isUserLoggined) {
                [weakSelf addObjectsToDataSourceIfPossible];
            }
        };
        ((LoginViewController *)viewController.topViewController).shouldAutoCloseAfterLogin = YES;
        [AppHelper presentViewController:viewController onController:self.navigationController];
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //shayan info
    [self presentLoginIfNeeded];
    self.page = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppearOnInfoHub:) name:UIKeyboardWillShowNotification object:nil];
    
    [self.dataSourceArray removeAllObjects];
    [self.dataSourceSet removeAllObjects];
    [self.searchDataSourceSet removeAllObjects];
    [self.tableView reloadData];
    
    [self addObjectsToDataSourceIfPossible];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self prepareTraRefreshControlViewes];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - IBAction

- (IBAction)searchBarButtonTapped:(id)sender
{
    HomeSearchViewController *homeSearchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeSearchViewController"];
    
    homeSearchViewController.fakeBackground = [AppHelper snapshotForView:self.navigationController.view];
    homeSearchViewController.hidesBottomBarWhenPushed = YES;
    homeSearchViewController.searchType = SearchTypeAnnoucements;
        
    [self.navigationController pushViewController:homeSearchViewController animated:NO];
}

#pragma mark - UITableViewDataSource

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = self.dynamicService.language == LanguageTypeArabic ? AnnoucementsTableViewCellArabicIdentifier : AnnoucementsTableViewCellEuropeIdentifier;
    AnnoucementsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:SegueToDetailsViewControllerIdentifier sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Navigate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:SegueToDetailsViewControllerIdentifier]) {
        NSInteger cellRow = [self.tableView indexPathForSelectedRow].row;
        DetailsViewController *detailsViewController = segue.destinationViewController;\
        Announcement *currentAnnouncement = self.dataSourceArray[cellRow];
        detailsViewController.selectedAnnouncement = currentAnnouncement;
        
        if (currentAnnouncement.announcementImage.length) {
            AnnoucementsTableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
            UIImage *img = selectedCell.annoucementsImageView.image;
            detailsViewController.logoImage = img;
        }
    }
}

#pragma mark - RefreshControl Action

- (void)actionRefresh
{
    self.page = 0;
    [self.traRefreshContentView startAnimations];

    [self addObjectsToDataSourceIfPossible];
}

- (void)actionBottonRefresh
{
    [self.bottomRefreshControl beginRefreshing];

    [self.traBottomRefreshContentView startAnimations];
    [self addObjectsToDataSourceIfPossible];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat progress = - self.tableView.contentOffset.y / self.refreshControl.frame.size.height;
    if (progress > 0) {
        [self.traRefreshContentView setOpacityValue:progress <= 1 ? progress : 1];
    }
    CGFloat progressBottom = self.tableView.contentSize.height < self.tableView.bounds.size.height ? self.tableView.contentOffset.y / self.bottomRefreshControl.frame.size.height : - (self.tableView.contentSize.height - self.tableView.bounds.size.height - self.tableView.contentOffset.y) / self.bottomRefreshControl.frame.size.height;
    if (progressBottom > 0) {
        [self.traBottomRefreshContentView setOpacityValue:progressBottom <= 1 ? progressBottom : 1];
    }
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    self.navigationItem.title = dynamicLocalizedString(@"annoucements.title");
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
}

#pragma mark - Private

- (void)addObjectsToDataSourceIfPossible
{
    if (!self.refreshControl.refreshing && !self.bottomRefreshControl.refreshing) {
        self.loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:dynamicLocalizedString(@"annoucements.title") closeButton:NO];
    }
    if (self.page) {
        self.page = self.dataSourceSet.count / PageSize ;
    }
    [self fetchAnnouncementsData];
}

- (void)fetchAnnouncementsData
{
    if (!self.dataSourceSet) {
        self.dataSourceSet = [[NSMutableSet alloc] init];
    }
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] traSSNoCRMServiceGetAnnoucement: PageSize offset: self.page * PageSize responseBlock:^(id response, NSError *error) {
        if (error) {
            //shayan loader issue solved
            weakSelf.loader.ratingView.hidden = true;
            NSString *errorString = [response isKindOfClass:[NSString class]] ? response : error.localizedDescription;
            if ([[weakSelf.self presentedViewController] isKindOfClass:[TRALoaderViewController class]]) {
                
                [weakSelf.loader setCompletedStatus:TRACompleteStatusFailure withDescription: errorString];
            } else {
                [AppHelper alertViewWithMessage:errorString];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf stopAnimationRefreshControlers];
            });
        } else {
            [weakSelf.dataSourceSet addObjectsFromArray:response];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf prepareDataSource:[weakSelf sortDataSouseSet:weakSelf.dataSourceSet]];
            });
            if ([[weakSelf.self presentedViewController] isKindOfClass:[TRALoaderViewController class]]) {
                [weakSelf.loader dismissTRALoader:YES];
            }
        }
    }];
}

- (void)prepareDataSource:(NSArray *)inputArray
{
    NSMutableArray *indexPathToAdd = [[NSMutableArray alloc] init];
    for (int i = (int)self.dataSourceArray.count ; i < (int)inputArray.count; i++) {
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPathToAdd addObject:newIndexPath];
    }
    [self.tableView beginUpdates];
    self.dataSourceArray = [inputArray mutableCopy];
    [self.tableView insertRowsAtIndexPaths:indexPathToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    
    self.page++;
    [self stopAnimationRefreshControlers];
}

- (NSArray *)sortDataSouseSet:(NSMutableSet *)dataSet
{
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"announcementPubDate" ascending:NO];
    return [dataSet sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sort, nil]];
}

- (void)registerNibs
{
    [self.tableView registerNib:[UINib nibWithNibName:@"AnnoucementsTableViewCellEuropeUI" bundle:nil] forCellReuseIdentifier:AnnoucementsTableViewCellEuropeIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"AnnoucementsTableViewCellArabicUI" bundle:nil] forCellReuseIdentifier:AnnoucementsTableViewCellArabicIdentifier];
}

#pragma mark - Prepare RefreshControl

- (void)prepareRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(actionRefresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    self.refreshControl.layer.zPosition = -1;
    self.refreshControl.tintColor = [UIColor clearColor];
}

- (void)prepareBottomRefreshcontrol
{
    self.bottomRefreshControl = [[UIRefreshControl alloc] init];
    self.bottomRefreshControl.triggerVerticalOffset = 80.f;
    [self.bottomRefreshControl addTarget:self action:@selector(actionBottonRefresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = self.bottomRefreshControl;
    self.bottomRefreshControl.layer.zPosition = -1;
    self.bottomRefreshControl.tintColor = [UIColor clearColor];
}

- (void)prepareTraRefreshControlViewes
{
    if (!self.traRefreshContentView) {
        self.traRefreshContentView = [[TRARefreshContentView alloc] initWithFrame:self.refreshControl.bounds];
        [self.traRefreshContentView setNeedsUpdateConstraints];
        [self.refreshControl addSubview:self.traRefreshContentView];
    }
    if (!self.traBottomRefreshContentView) {
        self.traBottomRefreshContentView = [[TRARefreshContentView alloc] initWithFrame:self.bottomRefreshControl.bounds];
        [self.traBottomRefreshContentView setNeedsUpdateConstraints];
        [self.bottomRefreshControl addSubview:self.traBottomRefreshContentView];
    }
}

- (void)stopAnimationRefreshControlers
{
    if (self.refreshControl.refreshing) {
        [self.traRefreshContentView stopAnimation];
        [self.refreshControl endRefreshing];
    }
    if (self.bottomRefreshControl.refreshing) {
        [self.traBottomRefreshContentView stopAnimation];
        [self.bottomRefreshControl endRefreshing];
    }
}

#pragma mark - Configuration

- (void)configureCell:(AnnoucementsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.marginAnnouncementContainerConstraint.constant = indexPath.row % 2 ? DefaultCellOffset + AdditionalCellOffset : DefaultCellOffset;
    Announcement *announcement = self.dataSourceArray[indexPath.row];
    [self downloadImageCell:cell url:announcement.announcementImage];
    cell.annoucementsDateLabel.text = [AppHelper compactDateStringFrom:announcement.announcementPubDate];
    cell.annoucementsDescriptionLabel.text = announcement.announcementTitle;
    cell.annoucementsDescriptionLabel.tag = DeclineTagForFontUpdate;
}

- (void)downloadImageCell:(AnnoucementsTableViewCell *)cell url:(NSString *)url
{
    UIImage *logo = [[UIImage imageNamed:@"ic_type_apr"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    cell.annoucementsImageView.tintColor = [UIColor defaultGreenColor];
    cell.activityIndicator.color = self.dynamicService.currentApplicationColor;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    __weak AnnoucementsTableViewCell *weakCell = cell;
    [cell.annoucementsImageView setImageWithURLRequest:request placeholderImage:logo success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nonnull response, UIImage * _Nonnull image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakCell.annoucementsImageView.image = image;
        });
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nonnull response, NSError * _Nonnull error) {
    }];
}

#pragma mark - Keyboard

- (void)keyboardWillAppearOnInfoHub:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboardRect.size.height;
    CGFloat heightOfTabBar = self.tabBarController.tabBar.frame.size.height;
    CGFloat offset = keyboardHeight - heightOfTabBar;
    
    [self.view layoutIfNeeded];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.bottomTableViewConstraint.constant = offset;
        [weakSelf.view layoutIfNeeded];
    }];
}

- (void)hieddenAnimationKeyboard
{
    [self.view layoutIfNeeded];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.bottomTableViewConstraint.constant = 0;
        [weakSelf.view layoutIfNeeded];
    }];
}

@end
