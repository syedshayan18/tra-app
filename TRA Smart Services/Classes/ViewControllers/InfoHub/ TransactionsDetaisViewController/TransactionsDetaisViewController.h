//
//  TransactionsDetaisViewController.h
//  TRA Smart Services
//
//  Created by Admin on 03.12.15.
//

static NSString *const SegueTransactionsDetailsViewControllerIdentifier = @"segueTransactionsDetailsViewController";

@interface TransactionsDetaisViewController : BaseServiceViewController <UITableViewDataSource, UITableViewDelegate, UIPrintInteractionControllerDelegate>

@property (strong, nonatomic) TransactionModel *transactionModel;

@end
