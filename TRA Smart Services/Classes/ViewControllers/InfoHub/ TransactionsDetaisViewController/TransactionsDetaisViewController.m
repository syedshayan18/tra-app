//
//  TransactionsDetaisViewController.m
//  TRA Smart Services
//
//  Created by Admin on 03.12.15.
//

#import "TransactionsDetaisViewController.h"
#import "InfoHubTableViewCell.h"
#import "SpamReportViewController.h"
#import "DynamicContainerServiceViewController.h"

static NSString *const PlaceholderImageName = @"ic_type_apr";
static CGFloat const DefaultCellOffset = 22.;

static CGFloat const DefaultPageWidth = 612.;
static CGFloat const DefaultPageHeight = 792.;
static CGFloat const BorderInset = 0.;
static CGFloat const MarginInset = 30.;

@interface TransactionsDetaisViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation TransactionsDetaisViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNibs];
    [self prepareUI];
    [self prepareButtonPrint];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
}

#pragma mark - IBAction

- (IBAction)editButtonTapped:(id)sender
{
    [self performNavigationToTransactionDynamicService];
}

- (IBAction)printButtonTapped:(id)sender
{
    [self generatePdfButtonPressed];
}

#pragma mark - UITableViewDataSource

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = self.dynamicService.language == LanguageTypeArabic ? InfoHubTableViewCellArabicIdentifier : InfoHubTableViewCellEuropeIdentifier;
    InfoHubTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    [self configureTableVIweCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - Navigation 

- (void)performNavigationToTransactionDynamicService
{
    DynamicContainerServiceViewController *dynamicService = [self.storyboard instantiateViewControllerWithIdentifier:DynamicServiceIDstoryboard];
    dynamicService.transactionModel = self.transactionModel;
    [self.navigationController pushViewController:dynamicService animated:YES];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    [AppHelper titleFontForNavigationBar:self.navigationController.navigationBar];
    [self.editButton setTitle:dynamicLocalizedString(@"transactionsDetaisViewController.editButton.title") forState:UIControlStateNormal];
    [self.tableView reloadData];
    self.title = [self.transactionModel titleStringTransactionModel];
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
    self.editButton.backgroundColor = self.dynamicService.currentApplicationColor;
}

- (void)setLTREuropeUI
{
    self.descriptionLabel.textAlignment = NSTextAlignmentLeft;
}

- (void)setRTLArabicUI
{
    self.descriptionLabel.textAlignment = NSTextAlignmentRight;
}

#pragma mark - Private

#pragma mark - ConfigureCell

- (void)configureTableVIweCell:(InfoHubTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.marginInfoHubContainerConstraint.constant = DefaultCellOffset;
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.infoHubTransactionDescriptionLabel.text = self.transactionModel.statusCode;
    cell.infoHubTransactionTitleLabel.text = self.transactionModel.title;
    cell.infoHubTransactionTitleLabel.textColor = [self.dynamicService currentApplicationColor];
    
    cell.infoHubTransactionDateLabel.text = [[self.transactionModel.traSubmitDatetime componentsSeparatedByString:@" "] firstObject];
    
    UIImage *logo = [[UIImage imageNamed:self.transactionModel.transactionImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    if (self.dynamicService.colorScheme == ApplicationColorBlackAndWhite) {
        [cell.infoHubTransactionImageView setTintColor:[UIColor blackColor]];
        cell.infoHubTransactionDescriptionLabel.textColor = [UIColor blackColor];
    } else {
        [cell.infoHubTransactionImageView setTintColor:self.transactionModel.transactionColor];
        cell.infoHubTransactionDescriptionLabel.textColor = self.transactionModel.transactionColor;
    }
    cell.infoHubTransactionImageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.infoHubTransactionImageView.image = logo;
    cell.infoHubTransactionDescriptionLabel.tag = DeclineTagForFontUpdate;
}

- (void)registerNibs
{
    [self.tableView registerNib:[UINib nibWithNibName:@"InfoHubTableViewCellEuropeUI" bundle:nil] forCellReuseIdentifier:InfoHubTableViewCellEuropeIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"InfoHubTableViewCellArabicUI" bundle:nil] forCellReuseIdentifier:InfoHubTableViewCellArabicIdentifier];
}

- (void)prepareUI
{
    self.editButton.hidden = ![self.transactionModel editingTransactionModel];
    
    self.editButton.hidden = YES; //disamble edit button
    
    self.descriptionLabel.text = self.transactionModel.transationDescription;
    self.editButton.layer.shadowColor = [UIColor blackColor].CGColor;
    self.editButton.layer.shadowOpacity = 0.5;
    self.editButton.layer.shadowRadius = 1.;
    self.editButton.layer.shadowOffset = CGSizeMake(1.5, 1.5);
}

- (void)prepareButtonPrint
{
    UIImage *printImage = [UIImage imageNamed:@"ic_print"];
    UIBarButtonItem *printButton = [[UIBarButtonItem alloc] initWithImage:printImage style:UIBarButtonItemStyleDone target:self action:@selector(printButtonTapped:)];
    
    self.navigationItem.rightBarButtonItem = printButton;
}

#pragma mark - Print

-(void)printFile:(NSString *)path
{
    NSData *dataFromPath = [NSData dataWithContentsOfFile:path];
    
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    if(printController && [UIPrintInteractionController canPrintData:dataFromPath]) {
        printController.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = @"print";
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printController.printInfo = printInfo;
        printController.showsPageRange = YES;
        printController.printingItem = dataFromPath;
        
        [UINavigationBar appearance].barTintColor = self.dynamicService.currentApplicationColor;
        [UINavigationBar appearance].tintColor = [UIColor whiteColor];
        [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName : self.dynamicService.language == LanguageTypeArabic ? [UIFont droidKufiRegularFontForSize:17] : [UIFont latoRegularWithSize:17]};
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSString *errorString = [NSString stringWithFormat:@"Error: %@", error];
                [AppHelper alertViewWithMessage:errorString];
            }
        };
        [printController presentAnimated:YES completionHandler:completionHandler];
    }
}

#pragma mark - Generate PDF file

- (void)generatePdfButtonPressed
{
    NSString *fileName = @"transaction.pdf";
    NSString *paths = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *pdfFileName = [paths stringByAppendingPathComponent:fileName];
    
    [self generatePdf:pdfFileName];
    [self printFile:pdfFileName];
}

- (void)generatePdf:(NSString *)thefilePath
{
    CGFloat maxWidth = DefaultPageWidth - 2 * BorderInset - 2 * MarginInset;
    CGFloat maxHeight = DefaultPageHeight - 2 * BorderInset - 2 * MarginInset;
    CGFloat widthTitle = maxWidth * 0.25;
    CGFloat widthDescr = maxWidth * 0.75;
    CGFloat xTitle = self.dynamicService.language == LanguageTypeArabic ?  BorderInset + MarginInset + widthDescr : BorderInset + MarginInset;
    CGFloat xDescription = self.dynamicService.language == LanguageTypeArabic ? BorderInset + MarginInset : BorderInset + MarginInset + widthTitle;
    CGFloat yPosition = BorderInset + MarginInset;
    
    UIGraphicsBeginPDFContextToFile(thefilePath, CGRectZero, nil);
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, DefaultPageWidth, DefaultPageHeight), nil);
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = self.dynamicService.language == LanguageTypeArabic ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    NSDictionary *attributesHeader = @{ NSFontAttributeName: self.dynamicService.language == LanguageTypeArabic ? [UIFont latoRegularWithSize:17] : [UIFont droidKufiRegularFontForSize:17],
                                  NSParagraphStyleAttributeName: paragraphStyle };
    NSDictionary *attributesTitle = @{ NSFontAttributeName: self.dynamicService.language == LanguageTypeArabic ? [UIFont latoBoldWithSize:14] : [UIFont droidKufiBoldFontForSize:14],
                                        NSParagraphStyleAttributeName: paragraphStyle };
    NSDictionary *attributesDescription = @{ NSFontAttributeName: self.dynamicService.language == LanguageTypeArabic ? [UIFont latoRegularWithSize:14] : [UIFont droidKufiRegularFontForSize:14],
                                        NSParagraphStyleAttributeName: paragraphStyle };
    
    CGFloat headerHeigth = maxHeight / 8;
    UIImage *logo = [UIImage imageNamed:self.transactionModel.transactionImageName];
    CGSize logoSize = CGSizeMake(60, 60);
    CGRect logoFrame = CGRectMake(xTitle + (widthTitle - logoSize.width) / 2, yPosition + (headerHeigth - logoSize.height) / 2, logoSize.width, logoSize.height);
    [logo drawInRect:logoFrame];
    [self.transactionModel.titleStringTransactionModel drawInRect:CGRectMake(xDescription,  yPosition + (headerHeigth - 17) / 2, widthDescr, headerHeigth) withAttributes:attributesHeader];
    
    yPosition += headerHeigth;
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:self.transactionModel.title attributes:attributesDescription];
    CGRect rectText = [attributedText boundingRectWithSize:CGSizeMake(widthDescr, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    [self.transactionModel.title drawInRect:CGRectMake(xDescription, yPosition,  widthDescr, rectText.size.height) withAttributes:attributesDescription];
    [dynamicLocalizedString(@"transactionsDetaisViewController.pdf.title") drawInRect:CGRectMake(xTitle, yPosition, widthTitle, rectText.size.height) withAttributes:attributesTitle];
    
    yPosition += rectText.size.height;
    attributedText = [[NSAttributedString alloc] initWithString:self.transactionModel.statusCode attributes:attributesDescription];
    rectText = [attributedText boundingRectWithSize:CGSizeMake(widthDescr, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    [self.transactionModel.stateCode drawInRect:CGRectMake(xDescription, yPosition, widthDescr, rectText.size.height) withAttributes:attributesDescription];
    [dynamicLocalizedString(@"transactionsDetaisViewController.pdf.status") drawInRect:CGRectMake(xTitle, yPosition, widthTitle, rectText.size.height) withAttributes:attributesTitle];
    
    yPosition += rectText.size.height;
    attributedText = [[NSAttributedString alloc] initWithString:self.transactionModel.transationDescription attributes:attributesDescription];
    rectText = [attributedText boundingRectWithSize:CGSizeMake(widthDescr, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (rectText.size.height > maxHeight - yPosition - 20) {
        rectText = CGRectMake(0, 0, 0, maxHeight - yPosition - 20);
    }
    [self.transactionModel.transationDescription drawInRect:CGRectMake(xDescription, yPosition, widthDescr, rectText.size.height) withAttributes:attributesDescription];
    [dynamicLocalizedString(@"transactionsDetaisViewController.pdf.description") drawInRect:CGRectMake(xTitle, yPosition, widthTitle, rectText.size.height) withAttributes:attributesTitle];

    yPosition += rectText.size.height;
    attributedText = [[NSAttributedString alloc] initWithString:self.transactionModel.modifiedDatetime attributes:attributesDescription];
    rectText = [attributedText boundingRectWithSize:CGSizeMake(widthDescr, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    [dynamicLocalizedString(@"transactionsDetaisViewController.pdf.date") drawInRect:CGRectMake(xTitle, yPosition, widthTitle, rectText.size.height) withAttributes:attributesTitle];
    [self.transactionModel.modifiedDatetime drawInRect:CGRectMake(xDescription, yPosition, widthDescr, rectText.size.height) withAttributes:attributesDescription];
    
    UIGraphicsEndPDFContext();
}

@end
