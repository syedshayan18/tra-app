//
//  InfoHubNavigationController.m
//  TRA Smart Services
//
//  Created by Admin on 16.06.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "InfoHubNavigationController.h"

@implementation InfoHubNavigationController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareNavigationBar];
}

#pragma mark - Private

- (void)prepareNavigationBar
{
    [self.navigationBar setTintColor:[UIColor whiteColor]];
    [AppHelper titleFontForNavigationBar:self.navigationBar];
    self.navigationBar.translucent = YES;
    [self.navigationBar setShadowImage:[UIImage new]];
    self.interactivePopGestureRecognizer.enabled = NO;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style: UIBarButtonItemStylePlain target:nil action:nil];
}

@end
