//
//  DetailsViewController.m
//  TRA Smart Services
//
//  Created by Admin on 18.08.15.
//

#import "DetailsViewController.h"
#import "RTLController.h"
#import "Announcement.h"

@interface DetailsViewController ()

@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *screenTitleLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginBottomDescriptionTextViewConstraint;

@end

@implementation DetailsViewController

#pragma mark - Life Cicle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    RTLController *rtl = [[RTLController alloc] init];
    [rtl disableRTLForView:self.view];
    [self prepareNavigationBar];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self dispalyInformations];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    [AppHelper titleFontForNavigationBar:self.navigationController.navigationBar];
    self.title = self.titleText.length ? self.titleText : dynamicLocalizedString(@"details.title");
}

- (void)updateColors
{
    self.titleImageView.backgroundColor = [self.dynamicService currentApplicationColor];
}

- (void)setLTREuropeUI
{
    [self changeElementsAligment:NSTextAlignmentLeft];
}

- (void)setRTLArabicUI
{
    [self changeElementsAligment:NSTextAlignmentRight];
}

#pragma mark - Private

- (void)changeElementsAligment:(NSTextAlignment)textAlignment
{
    self.dateLabel.textAlignment = textAlignment;
    self.screenTitleLabel.textAlignment = textAlignment;
}

- (void)dispalyInformations
{
    self.dateLabel.text = self.selectedAnnouncement ?[AppHelper detailedDateStringFrom:self.selectedAnnouncement.announcementPubDate] : @"" ;
    self.screenTitleLabel.text = self.selectedAnnouncement ? self.selectedAnnouncement.announcementTitle : self.titleText;

    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.paragraphSpacing = 24.f;
    style.alignment = self.dynamicService.language == LanguageTypeArabic ? NSTextAlignmentRight : NSTextAlignmentLeft;

    NSAttributedString *text = [[NSAttributedString alloc] initWithString: self.selectedAnnouncement ? self.selectedAnnouncement.announcementDescription : @""
                                                               attributes:@{
                                                                            NSFontAttributeName : self.dynamicService.language == LanguageTypeArabic ? [UIFont droidKufiRegularFontForSize:12.f] : [UIFont latoRegularWithSize:12.f],
                                                                            NSParagraphStyleAttributeName : style
                                                                            }
                                ];
    self.descriptionTextView.attributedText = self.selectedAnnouncement ? text : self.contentText;
    self.descriptionTextView.textAlignment = NSTextAlignmentJustified;

    UIImage *logo = self.logoImage ? self.logoImage : [UIImage imageNamed:@"demo"];
    if (self.dynamicService.colorScheme == ApplicationColorBlackAndWhite) {
        logo = [[BlackWhiteConverter sharedManager] convertedBlackAndWhiteImage:logo];
    }
    self.titleImageView.image = logo;
}

- (void)prepareNavigationBar
{
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [AppHelper titleFontForNavigationBar:self.navigationController.navigationBar];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style: UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

@end