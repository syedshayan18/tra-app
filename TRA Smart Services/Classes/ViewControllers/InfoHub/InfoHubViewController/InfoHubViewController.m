
//
//  InfoHubViewController.m
//  TRA Smart Services
//
//  Created by Admin on 18.08.15.
//

#import "InfoHubViewController.h"
#import "InfoHubCollectionViewCell.h"
#import "InfoHubTableViewCell.h"
#import "TransactionModel.h"
#import "MBProgressHUD.h"
#import "LoginViewController.h"
#import "Announcement.h"
#import "UIImageView+AFNetworking.h"
#import "TRARefreshContentView.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "DetailsViewController.h"
#import "TransactionsDetaisViewController.h"
#import "HomeSearchViewController.h"

static CGFloat const SectionHeaderHeight = 40.0f;
static CGFloat const AdditionalCellOffset = 20.0f;
static CGFloat const DefaultCellOffset = 22.0f;
static NSUInteger const VisibleAnnouncementPreviewElementsCount = 3;
static NSString *const SegueToDetailsViewControllerIdentifier = @"segueInfoHubToDetailsViewController";

static NSString *const PlaceholderImageName = @"ic_type_apr";

static LanguageType startingLanguageType;

@interface InfoHubViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *announcementsLabel;
@property (weak, nonatomic) IBOutlet UIButton *seeMoreButton;
@property (weak, nonatomic) IBOutlet UIView *tableViewContentHolderView;
@property (weak, nonatomic) IBOutlet UIView *topContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceVerticalConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomTableViewConstraint;
@property (weak, nonatomic) IBOutlet UILabel *transactionNotDataLabel;
@property (weak, nonatomic) IBOutlet UIImageView *notransactionImageView;
@property (weak, nonatomic) IBOutlet UILabel *noTransactionLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftSeparatorSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightSeparatorSpaceConstraint;
@property (strong, nonatomic) TRARefreshContentView *traBottomRefreshContentView;
@property (strong, nonatomic) UIRefreshControl *bottomRefreshControl;

@property (strong, nonatomic) NSArray *collectionViewDataSource;
@property (strong, nonatomic) NSMutableArray *tableViewDataSource;
@property (strong, nonatomic) NSMutableArray *filteredDataSource;
@property (assign, nonatomic) __block NSInteger page;
@property (assign, nonatomic) __block NSInteger pageSearch;

@property (strong, nonatomic) TRALoaderViewController *loader;

@end

@implementation InfoHubViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    [self prepareBottomRefreshcontrol];
    [self registerNibs];
    self.filteredDataSource = [[NSMutableArray alloc] init];
    self.tableViewDataSource = [[NSMutableArray alloc] init];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //shayan info hub
      [self presentLoginIfNeeded];
    [self setupInitialParameters];
    [self addObjectsToDataSourceIfPossible];
    [self getNewsAnnouncement];
  
    [self updateUIForLeftBasedInterface: self.dynamicService.language == LanguageTypeArabic ? NO : YES];
    self.navigationController.navigationBar.hidden = NO;
}
-(void)presentLoginIfNeeded{
    
  
        if ([NetworkManager sharedManager].isUserLoggined) {
            [self addObjectsToDataSourceIfPossible];
        } else {
            UINavigationController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
            __weak typeof(self) weakSelf = self;
            ((LoginViewController *)viewController.topViewController).didCloseViewController = ^() {
                if (![NetworkManager sharedManager].isUserLoggined) {
                    weakSelf.tabBarController.selectedIndex = 0;
                }
            };
            ((LoginViewController *)viewController.topViewController).didDismissed = ^() {
                if ([NetworkManager sharedManager].isUserLoggined) {
                    [weakSelf addObjectsToDataSourceIfPossible];
                }
            };
            ((LoginViewController *)viewController.topViewController).shouldAutoCloseAfterLogin = YES;
            [AppHelper presentViewController:viewController onController:self.navigationController];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self prepareTraRefreshControlViewes];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self stopAnimationRefreshControlers];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

#pragma mark - IBAction

- (IBAction)searchBarButtonTapped:(id)sender
{
    HomeSearchViewController *homeSearchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeSearchViewController"];
    
    homeSearchViewController.fakeBackground = [AppHelper snapshotForView:self.navigationController.view];
    homeSearchViewController.hidesBottomBarWhenPushed = YES;
    homeSearchViewController.searchType = SearchTypeInfoHubTransactions;
        
    [self.navigationController pushViewController:homeSearchViewController animated:NO];
}

#pragma mark - CollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return VisibleAnnouncementPreviewElementsCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = self.dynamicService.language == LanguageTypeArabic ? InfoHubCollectionViewCellArabicIdentifier : InfoHubCollectionViewCellEuropeIdentifier;
    InfoHubCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:SegueToDetailsViewControllerIdentifier sender:nil];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellWidth = SCREEN_WIDTH * 0.78f;
    CGFloat cellHeight = collectionView.bounds.size.height;
    CGSize cellSize = CGSizeMake(cellWidth, cellHeight);
    return cellSize;
}

#pragma mark - UITableViewDataSource

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.filteredDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = self.dynamicService.language == LanguageTypeArabic ? InfoHubTableViewCellArabicIdentifier : InfoHubTableViewCellEuropeIdentifier;
    InfoHubTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    [self configureTableVIweCell:cell atIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return SectionHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(DefaultCellOffset, 0, tableView.frame.size.width - DefaultCellOffset * 2, SectionHeaderHeight)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.text = dynamicLocalizedString(@"transactions.label.text");
    headerLabel.font = self.dynamicService.language == LanguageTypeArabic ? [UIFont droidKufiBoldFontForSize:11.f] : [UIFont latoBoldWithSize:11.f];
    
    CAGradientLayer *headerGradient = [CAGradientLayer layer];
    headerGradient.frame = CGRectMake(0, 0, tableView.frame.size.width, SectionHeaderHeight);
    headerGradient.colors = @[(id)[UIColor whiteColor].CGColor, (id)[[UIColor whiteColor] colorWithAlphaComponent:0.1].CGColor];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, SectionHeaderHeight)];
    [headerView.layer addSublayer:headerGradient];
    [headerView addSubview:headerLabel];
    
    headerLabel.textAlignment = self.dynamicService.language == LanguageTypeArabic ? NSTextAlignmentRight : NSTextAlignmentLeft;
    return headerView;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:SegueTransactionsDetailsViewControllerIdentifier sender:nil];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat progressBottom = self.tableView.contentSize.height < self.tableView.bounds.size.height ? self.tableView.contentOffset.y / self.bottomRefreshControl.frame.size.height : - (self.tableView.contentSize.height - self.tableView.bounds.size.height - self.tableView.contentOffset.y) / self.bottomRefreshControl.frame.size.height;
    if (progressBottom > 0 && self.tableView.contentSize.height > self.tableView.bounds.size.height) {
        [self.traBottomRefreshContentView setOpacityValue:progressBottom <= 1 ? progressBottom : 1];
    }
}

#pragma mark - Navigate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:SegueToDetailsViewControllerIdentifier]) {
        [self prepareDetailsViewControllerWithSegue:segue];
    } else if ([segue.identifier isEqualToString:SegueTransactionsDetailsViewControllerIdentifier]) {
        [self prepareTransactionsDetailsViewControllerWithSegue:segue];
    }
}

- (void)prepareDetailsViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
    NSIndexPath *indexPath = indexPaths[0];
    NSInteger cellRow = [indexPaths[0] row];
    DetailsViewController *detailsViewController = segue.destinationViewController;
    Announcement *currentAnnouncement = self.collectionViewDataSource[cellRow];
    detailsViewController.selectedAnnouncement = currentAnnouncement;
    if (currentAnnouncement.announcementImage.length) {
        InfoHubCollectionViewCell *selectedCell = (InfoHubCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        UIImage *img = selectedCell.announcementPreviewIconImageView.image;
        detailsViewController.logoImage = img;
    }
}

- (void)prepareTransactionsDetailsViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    NSInteger cellRow = [self.tableView indexPathForSelectedRow].row;
    TransactionsDetaisViewController *detailsViewController = segue.destinationViewController;
    TransactionModel *selectedTransactions = self.filteredDataSource[cellRow];
    detailsViewController.transactionModel = selectedTransactions;
}

#pragma mark - RefreshControl Action

- (void)actionBottonRefresh
{
    [self.bottomRefreshControl beginRefreshing];
    
    [self.traBottomRefreshContentView startAnimations];
    [self addObjectsToDataSourceIfPossible];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{    
    self.navigationItem.title = dynamicLocalizedString(@"infoHub.title");
    self.announcementsLabel.text = dynamicLocalizedString(@"announcements.label.text");
    self.announcementsLabel.font = self.dynamicService.language == LanguageTypeArabic ? [UIFont droidKufiBoldFontForSize:15.f] : [UIFont latoBoldWithSize:15.f];
    [self.seeMoreButton setTitle:dynamicLocalizedString(@"seeMore.button.title") forState:UIControlStateNormal];
    self.transactionNotDataLabel.text = dynamicLocalizedString(@"infoHubViewController.transactionNotDataLabel");
    self.noTransactionLabel.text = dynamicLocalizedString(@"infoHubViewController.transactionNotDataLabel");
}

- (void)updateColors
{
    [self.tableView reloadData];
    [self.collectionView reloadData];
    
    [super updateBackgroundImageNamed:@"fav_back_orange"];
}

#pragma mark - Private

#pragma mark - ConfigureCell

- (void)configureCell:(InfoHubCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Announcement *announcement = self.collectionViewDataSource[indexPath.row];
    cell.announcementPreviewIconImageView.contentMode = UIViewContentModeScaleAspectFill;
    [cell.announcementPreviewIconImageView setTintColor:[UIColor defaultGreenColor]];
    
    cell.announcementPreviewDateLabel.text = [AppHelper compactDateStringFrom:announcement.announcementPubDate];
    cell.announcementPreviewDescriptionLabel.text = announcement.announcementTitle;
    cell.announcementPreviewDescriptionLabel.tag = DeclineTagForFontUpdate;
    [self downloadImageCell:cell url:announcement.announcementImage];
    if (self.dynamicService.language == LanguageTypeArabic) {
        cell.transform = CGAffineTransformScale(CGAffineTransformIdentity, -1, 1);
    }
}

- (void)downloadImageCell:(InfoHubCollectionViewCell *)cell url:(NSString *)url
{
    UIImage *logo = [[UIImage imageNamed:PlaceholderImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    cell.activityIndicator.color = self.dynamicService.currentApplicationColor;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    __weak InfoHubCollectionViewCell *weakCell = cell;
    [cell.announcementPreviewIconImageView setImageWithURLRequest:request placeholderImage:logo success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nonnull response, UIImage * _Nonnull image) {
            weakCell.announcementPreviewIconImageView.image = image;
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nonnull response, NSError * _Nonnull error) {

    }];
}

- (void)configureTableVIweCell:(InfoHubTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.marginInfoHubContainerConstraint.constant = DefaultCellOffset;
    if (indexPath.row % 2) {
        cell.marginInfoHubContainerConstraint.constant += AdditionalCellOffset;
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    TransactionModel *selectedTransactions = self.filteredDataSource[indexPath.row];
    cell.infoHubTransactionDescriptionLabel.text = selectedTransactions.transationDescription;
    cell.infoHubTransactionTitleLabel.text = selectedTransactions.title;
    cell.infoHubTransactionTitleLabel.textColor = [self.dynamicService currentApplicationColor];
    
    cell.infoHubTransactionDateLabel.text = [[selectedTransactions.traSubmitDatetime componentsSeparatedByString:@" "] firstObject];
    
    UIImage *logo = [[UIImage imageNamed:selectedTransactions.transactionImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    if (self.dynamicService.colorScheme == ApplicationColorBlackAndWhite) {
        [cell.infoHubTransactionImageView setTintColor:[UIColor blackColor]];
    } else {
        [cell.infoHubTransactionImageView setTintColor:selectedTransactions.transactionColor];
    }
    cell.infoHubTransactionImageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.infoHubTransactionImageView.image = logo;
    cell.infoHubTransactionDescriptionLabel.tag = DeclineTagForFontUpdate;
}

#pragma mark - Configuration

- (void)setupInitialParameters
{
    self.page = 1;
    self.pageSearch = 1;

    [self.filteredDataSource removeAllObjects];
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppearOnInfoHub:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)getNewsAnnouncement
{
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] traSSNoCRMServiceGetAnnoucement:VisibleAnnouncementPreviewElementsCount offset:0 responseBlock:^(id response, NSError *error) {
        if (error) {
            
            weakSelf.loader.ratingView.hidden = true;
            [weakSelf.loader setCompletedStatus:TRACompleteStatusFailure withDescription: error.localizedDescription];

            [response isKindOfClass:[NSString class]] ? [AppHelper alertViewWithMessage:response] : [AppHelper alertViewWithMessage:error.localizedDescription];
            
        } else {
            
            weakSelf.loader.ratingView.hidden = false;
             [weakSelf.loader setCompletedStatus:TRACompleteStatusSuccess withDescription: @"Success"];
            
            weakSelf.collectionViewDataSource = response;
            [weakSelf.collectionView reloadData];
        }
    }];
}

- (void)registerNibs
{
    [self.collectionView registerNib:[UINib nibWithNibName:@"InfoHubCollectionViewCellEuropeUI" bundle:nil] forCellWithReuseIdentifier:InfoHubCollectionViewCellEuropeIdentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:@"InfoHubCollectionViewCellArabicUI" bundle:nil] forCellWithReuseIdentifier:InfoHubCollectionViewCellArabicIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"InfoHubTableViewCellEuropeUI" bundle:nil] forCellReuseIdentifier:InfoHubTableViewCellEuropeIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"InfoHubTableViewCellArabicUI" bundle:nil] forCellReuseIdentifier:InfoHubTableViewCellArabicIdentifier];
}

- (void)updateUIForLeftBasedInterface:(BOOL)leftBased
{
    if ([AppHelper isiOS9_0OrHigher]) {
        if (!startingLanguageType) {
            startingLanguageType = self.dynamicService.language;
        }
        if (startingLanguageType == LanguageTypeArabic) {
            [self reverseDataSource];
        }
    } else {
        [self reverseDataSource];
    }
    
    self.leftSeparatorSpaceConstraint.constant = leftBased ? DefaultCellOffset : 0;
    self.rightSeparatorSpaceConstraint.constant = leftBased ? 0 : DefaultCellOffset;
    self.collectionView.transform = CGAffineTransformScale(CGAffineTransformIdentity, leftBased ? 1 : -1, 1);

    [self.tableView reloadData];
    [self.collectionView reloadData];
}

#pragma mark - DataSource Configurations

- (void)reverseDataSource
{
    self.collectionViewDataSource = [self.collectionViewDataSource reversedArray];
}

- (void)addObjectsToDataSourceIfPossible
{
    if (!self.bottomRefreshControl.refreshing) {
      //  self.loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:dynamicLocalizedString(@"transactions.label.text") closeButton:NO];
    }
    [self fetchTransactionsData];
}

- (void)fetchTransactionsData
{
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] traSSNoCRMServiceGetGetTransactions:self.page count:10 orderAsc:1 responseBlock: ^(id response, NSError *error) {
        if (error) {
            //shayan loader issue solved
            
           
                _notransactionImageView.hidden = NO;
                _noTransactionLabel.hidden = NO;
            
        
            
            weakSelf.loader.ratingView.hidden = true;
            
            NSString *errorString = [response isKindOfClass:[NSString class]] ? response : error.localizedDescription;
            if ([[weakSelf.self presentedViewController] isKindOfClass:[TRALoaderViewController class]]) {
                [weakSelf.loader setCompletedStatus:TRACompleteStatusFailure withDescription: errorString];
            } else {
                [AppHelper alertViewWithMessage:errorString];
            }
        } else {
            
            if (((NSArray *)response).count) {
                weakSelf.page++;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf prepareFilteredDataSource:response];
                    _notransactionImageView.hidden = YES;
                    _noTransactionLabel.hidden = YES;
                    [weakSelf.tableViewDataSource addObjectsFromArray:response];
                    
                });
            }
            if ([[weakSelf.self presentedViewController] isKindOfClass:[TRALoaderViewController class]]) {
                [weakSelf.loader dismissTRALoader:YES];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf stopAnimationRefreshControlers];
        });
    }];
}

- (void)prepareFilteredDataSource:(NSArray *)inputArray
{
    NSMutableArray *indexPathToAdd = [[NSMutableArray alloc] init];
    for (int i = (int)self.filteredDataSource.count; i < (int)self.filteredDataSource.count + inputArray.count; i++) {
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPathToAdd addObject:newIndexPath];
    }
    [self.tableView beginUpdates];
    [self.filteredDataSource addObjectsFromArray:inputArray];
    [self.tableView insertRowsAtIndexPaths:indexPathToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    
    if (self.filteredDataSource.count > 0){
        _notransactionImageView.hidden = NO;
        _noTransactionLabel.hidden = NO;
        
    }
    else {
        _notransactionImageView.hidden = YES;
        _noTransactionLabel.hidden = YES;
    }
    self.transactionNotDataLabel.hidden = self.filteredDataSource.count;
    
    
    [self stopAnimationRefreshControlers];
}

#pragma mark - Prepare Refreshcontrol

- (void)prepareBottomRefreshcontrol
{
    self.bottomRefreshControl = [[UIRefreshControl alloc] init];
    self.bottomRefreshControl.triggerVerticalOffset = 80.f;
    [self.bottomRefreshControl addTarget:self action:@selector(actionBottonRefresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = self.bottomRefreshControl;
    self.bottomRefreshControl.layer.zPosition = -1;
    self.bottomRefreshControl.tintColor = [UIColor clearColor];
}

- (void)prepareTraRefreshControlViewes
{
    if (!self.traBottomRefreshContentView) {
        self.traBottomRefreshContentView = [[TRARefreshContentView alloc] initWithFrame:self.bottomRefreshControl.bounds];
        [self.traBottomRefreshContentView setNeedsUpdateConstraints];
        [self.bottomRefreshControl addSubview:self.traBottomRefreshContentView];
    }
}

- (void)stopAnimationRefreshControlers
{
    [self.traBottomRefreshContentView stopAnimation];
    [self.bottomRefreshControl endRefreshing];
}

#pragma mark - Keyboard

- (void)keyboardWillAppearOnInfoHub:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = keyboardRect.size.height;
    CGFloat heightOfTabBar = self.tabBarController.tabBar.frame.size.height;
    CGFloat offset = keyboardHeight - heightOfTabBar;
    
    [self.view layoutIfNeeded];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.bottomTableViewConstraint.constant = offset;
        [weakSelf.view layoutIfNeeded];
    }];
}

- (void)hieddenAnimationKeyboard
{
    [self.view layoutIfNeeded];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.bottomTableViewConstraint.constant = 0;
        [weakSelf.view layoutIfNeeded];
    }];
}

@end
