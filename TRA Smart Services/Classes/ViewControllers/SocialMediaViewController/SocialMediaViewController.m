//
//  SocialMediaViewController.m
//  TRA Smart Services
//
//  Created by Boost Maryan on 12/1/17.
//  Copyright © 2017 Thinkmobiles. All rights reserved.
//

#import "SocialMediaViewController.h"
#import "InquiriesHomeCollectionViewCell.h"

static CGFloat const HeigthInquiriesHomeCollectionViewCell = 125.;

static NSString *const kTwitterUrlString = @"https://twitter.com/TheUAETRA";
static NSString *const kFacebookUrlString = @"https://www.facebook.com/theuaetra";
static NSString *const kYoutubeUrlString = @"https://www.youtube.com/user/TheUaetra";
static NSString *const kInstagramUrlString = @"https://www.instagram.com/theuaetra/";

typedef enum {
    Twitter,
    Facebook,
    YouTube,
    Instagram,
} SocialMedia;

@interface SocialMediaViewController ()

@property (weak, nonatomic) IBOutlet UILabel *categoryTitlesLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSArray *socialMediaIconArray;
@property (strong, nonatomic) NSArray *socialMediaTitleArray;

@end

@implementation SocialMediaViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareDataSourceCollectionView];
}

#pragma mark - Private

- (void)prepareDataSourceCollectionView
{
    self.socialMediaTitleArray = @[dynamicLocalizedString(@"Twitter"), dynamicLocalizedString(@"Facebook"),
                                 dynamicLocalizedString(@"Youtube"), dynamicLocalizedString(@"Instagram")];
    self.socialMediaIconArray = @[[UIImage imageNamed:@"ic_twitter"], [UIImage imageNamed:@"ic_facebook"], [UIImage imageNamed:@"ic_youtube"], [UIImage imageNamed:@"ic_instagram"]];
}

- (void)openSocialMediaLinkWithType:(SocialMedia)socialMediaType
{
    NSURL *socialURL = [NSURL new];
    
    switch (socialMediaType) {
        case Twitter:
            socialURL = [NSURL URLWithString:kTwitterUrlString];
            
            break;
        case Facebook:
            socialURL = [NSURL URLWithString:kFacebookUrlString];
        
            break;
        case YouTube:
            socialURL = [NSURL URLWithString:kYoutubeUrlString];
            
            break;
        case Instagram:
            socialURL = [NSURL URLWithString:kInstagramUrlString];
            
            break;
        default:
            break;
    }
    
    [[UIApplication sharedApplication] openURL:socialURL];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.socialMediaTitleArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    InquiriesHomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"InquiriesHomeCollectionViewCell" forIndexPath:indexPath];
    [self configureCell:cell indexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    switch (indexPath.row) {
        case 0:
            [self openSocialMediaLinkWithType:Twitter];
            break;
        case 1:
            [self openSocialMediaLinkWithType:Facebook];
            break;
        case 2:
            [self openSocialMediaLinkWithType:YouTube];
            break;
        case 3:
            [self openSocialMediaLinkWithType:Instagram];
            break;
        default:
            break;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat offset = self.collectionView.frame.size.width / 4. - 60.;
    return UIEdgeInsetsMake(0.0, offset, 0.0, offset);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize screenSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
    CGFloat collectionViewWidth = screenSize.width;
    CGFloat minimumSpacingForCell = 10.;
    CGFloat offset = self.collectionView.frame.size.width / 4. - 60.;
    
    CGFloat cellWidth = collectionViewWidth / 2. - minimumSpacingForCell - offset;
    CGFloat cellHeight = HeigthInquiriesHomeCollectionViewCell;
    
    if (self.socialMediaIconArray.count % 2 && indexPath.row == self.socialMediaIconArray.count - 1) {
        cellWidth = collectionViewWidth - 2 * offset;
    }
    
    CGSize cellSize = CGSizeMake(cellWidth, cellHeight);
    return cellSize;
}

#pragma mark - ConfigureCell

- (void)configureCell:(InquiriesHomeCollectionViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    cell.inquriesTitleLabel.text = self.socialMediaTitleArray[indexPath.row];
    cell.inquiriesIconImageView.image = self.socialMediaIconArray[indexPath.row];
    
    UIColor *colorTintCell = self.dynamicService.currentApplicationColor;
    
    cell.inquriesTitleLabel.textColor = colorTintCell;
    cell.inquiriesIconImageView.tintColor = colorTintCell;
    [AppHelper addHexagonBorderForLayer:cell.polygonView.layer color:colorTintCell width:1];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    [AppHelper titleFontForNavigationBar:self.navigationController.navigationBar];
    self.title = dynamicLocalizedString(@"SocialMedia");;
    self.categoryTitlesLabel.text = dynamicLocalizedString(@"inquiriesHomeViewController.categoriTitlesLabel");
}

- (void)updateColors
{
     [super updateBackgroundImageNamed:@"fav_back_orange"];
}

- (void)setLTREuropeUI
{
    //[self changeElementsAligment:NSTextAlignmentLeft];
}

- (void)setRTLArabicUI
{
    //[self changeElementsAligment:NSTextAlignmentRight];
}


@end
