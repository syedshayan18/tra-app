//
//  BaseMembershipViewController.m
//  TRA Smart Services
//
//  Created by Admin on 21.07.15.
//

#import "TextFieldNavigator.h"
#import "BaseMembershipViewController.h"

static CGFloat const HeightTextField = 70.f;

@interface BaseMembershipViewController ()

@property (assign, nonatomic) CGFloat keyboardHeight;
@property (assign, nonatomic) CGFloat offSetTextFildY;

@end

@implementation BaseMembershipViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareNotification];

    [self prepareNavigationBar];
    [self prepareUIElements];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self scrollViewDidScroll:self.scrollView];
}

- (void)dealloc
{
    [self removeNotifications];
}

#pragma mark - Public

- (void)configureTextField:(LeftInsetTextField *)textField withImageName:(NSString *)imageName
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        [textField setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
    UIImage *leftImage = [UIImage imageNamed:imageName];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:leftImage];
    [imageView setImage:leftImage];
    imageView.tintColor = [self.dynamicService currentApplicationColor];
    textField.rightView = nil;
    textField.leftView = nil;
    if (self.dynamicService.language != LanguageTypeArabic) {
        textField.leftViewMode = UITextFieldViewModeAlways;
        textField.leftView = imageView;
    } else {
        textField.rightViewMode = UITextFieldViewModeAlways;
        textField.rightView = imageView;
    }
}

- (void)prepareNavigationBar
{
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName:self.dynamicService.language == LanguageTypeArabic ? [UIFont droidKufiRegularFontForSize:20] : [UIFont latoRegularWithSize: 20]};
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style: UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)returnKeyDone
{
    
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.scrollView == scrollView && self.scrollLogoImage) {
        if (scrollView.contentOffset.y >= 0) {
            [self.scrollLogoImage setContentOffset:CGPointMake(0, scrollView.contentOffset.y)];
        } else {
            [self.scrollLogoImage setContentOffset:CGPointMake(0, 0)];
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [TextFieldNavigator findNextTextFieldFromCurrent:textField];
    
    if (textField.returnKeyType == UIReturnKeyNext) {
        CGRect frame = [self.scrollView.subviews[0] convertRect:textField.bounds fromView:textField];
        
        CGFloat offsetTextField = frame.origin.y;
        CGFloat lineEventScroll = self.scrollView.frame.size.height + self.scrollView.contentOffset.y - self.keyboardHeight - 2 * HeightTextField + 20.f;
        
        if (offsetTextField  > lineEventScroll) {
            [self.scrollView setContentOffset:CGPointMake(0, self.scrollView.contentOffset.y + HeightTextField) animated:YES];
        }
        CGFloat offsetForScrollViewY = self.scrollView.frame.size.height - self.scrollView.contentOffset.y - self.keyboardHeight;
        if (lineEventScroll < self.offSetTextFildY - HeightTextField) {
            [self.scrollView setContentOffset:CGPointMake(0, self.offSetTextFildY - offsetForScrollViewY - self.scrollView.contentOffset.y + HeightTextField + 10.f) animated:YES];
        }
    }
    if (textField.returnKeyType == UIReturnKeyDone) {
        [self.scrollView setContentOffset:CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.frame.size.height ) animated:YES];
        return YES;
    }
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    CGRect frame = [self.scrollView.subviews[0] convertRect:textField.bounds fromView:textField];
    
    self.offSetTextFildY = frame.origin.y + textField.frame.size.height;
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self visibleRectView:textField];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField layoutIfNeeded];
}

#pragma mark - SuperClass Methods

- (void)updateColors
{
    [self updateColorsSeparatorTextField:self.view];
}

#pragma mark - Private

- (void)updateColorsSeparatorTextField:(UIView *)view
{
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[BottomBorderTextField class]]) {
            [AppHelper setStyleForTextField:(BottomBorderTextField *)subView];
            ((BottomBorderTextField *)subView).requredIndicatorColor = self.dynamicService.colorScheme == ApplicationColorBlackAndWhite ? [UIColor blackColor] : [UIColor redColor];
        }
        [self updateColorsSeparatorTextField:subView];
    }
}

- (void)prepareUIElements
{
    self.scrollView.delegate = self;
    self.mainButton.layer.cornerRadius = 2.5f;
    [self.mainButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.mainButton setBackgroundColor:[self.dynamicService currentApplicationColor]];
    self.mainButton.layer.shadowColor = [UIColor blackColor].CGColor;
    self.mainButton.layer.shadowOpacity = 0.5;
    self.mainButton.layer.shadowRadius = 1.;
    self.mainButton.layer.shadowOffset = CGSizeMake(1.5, 1.5);
}

#pragma mark - Keyboard

- (void)prepareNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillAppear:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyboardHeight = keyboardRect.size.height;
    [self animationKeyboardHide:NO];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [self animationKeyboardHide:YES];
}

#pragma mark - Animation

- (void)animationKeyboardHide:(BOOL)hidden
{
    [self.view layoutIfNeeded];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.scrollView.contentInset = UIEdgeInsetsMake(0, 0, hidden ? 0 : self.keyboardHeight, 0);
        [weakSelf.view layoutIfNeeded];
    }];
}

- (void)visibleRectView:(UIView *)view
{
    CGRect frame = [self.scrollView.subviews[0] convertRect:view.bounds fromView:view];
    CGFloat offsetLine = self.keyboardHeight;
    
    CGFloat frameTopPositionY = frame.origin.y + view.frame.size.height;
    CGFloat lineEventScroll = self.scrollView.frame.size.height + self.scrollView.contentOffset.y - offsetLine;
    if (lineEventScroll < frameTopPositionY) {
        [self.scrollView setContentOffset:CGPointMake(0, frameTopPositionY - self.scrollView.frame.size.height + offsetLine) animated:YES];
    }
    if (frame.origin.y < self.scrollView.contentOffset.y) {
        [self.scrollView setContentOffset:CGPointMake(0, frame.origin.y - 10.f) animated:YES];
    }
}

@end