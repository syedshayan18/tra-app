//
//  FeedbackViewController.m
//  TRA Smart Services
//
//  Created by Admin on 07.12.15.
//  Copyright © 2015 . All rights reserved.
//

#import "FeedbackViewController.h"
#import "RatingView.h"
#import "Animation.h"

static NSString *const LoaderBackgroundOrange = @"img_bg_1";

@interface FeedbackViewController ()

@property (weak, nonatomic) IBOutlet RatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *titleScreenLabel;

@end

@implementation FeedbackViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.ratingView.delegate = self;
    self.ratingView.keyboardKeyDone = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0.f to:1.0f delegate:nil] forKey:nil];
    self.view.layer.opacity = 1.0f;

    [self.tabBarController.tabBar setHidden:YES];
    [self.navigationController setNavigationBarHidden:YES];
    self.titleScreenLabel.text = self.titleScreen;
    [self.ratingView makeActivKeyboard];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - IBAction

- (IBAction)tappedCloseButton:(id)sender
{
    [self.containerView.layer addAnimation:[Animation fadeAnimFromValue:1.f to:0.0f delegate:self] forKey:@"hideViewController"];
    self.containerView.layer.opacity = 0.0f;
    self.closeButton.userInteractionEnabled = NO;
}

#pragma mark - RatingViewDelegate

- (void)ratingChanged:(NSInteger)rating description:(NSString *)string
{
    if (!self.serviceRatingName) {
        self.serviceRatingName = [self serviceNameForCurrentService];
    }
    
    [[NetworkManager sharedManager] traSSNoCRMServicePOSTFeedback:string forSerivce:self.serviceRatingName withRating:rating requestResult:^(id response, NSError *error) {}];
    
    [self tappedCloseButton:nil];
}

#pragma mark - Animations

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.containerView.layer animationForKey:@"hideViewController"]) {
        [self.containerView.layer removeAllAnimations];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    
}

- (void)updateColors
{
    if (self.dynamicService.colorScheme == ApplicationColorBlackAndWhite) {
        self.backgroundImageView.image = [[BlackWhiteConverter sharedManager] convertedBlackAndWhiteImage:[UIImage imageNamed:LoaderBackgroundOrange]];
    } else {
        NSString *name = [NSString stringWithFormat:@"img_bg_%i", (int)self.dynamicService.colorScheme];
        self.backgroundImageView.image = [UIImage imageNamed:name];
    }
}

#pragma mark - Private

- (NSString *)serviceNameForCurrentService
{
    NSString *serviceNameForRequest;
    switch (self.selectedServiceID) {
        case ServiceTypeGetDomainData: {
            serviceNameForRequest = ServiceTypeGetDomainDataStringName;
            break;
        }
        case ServiceTypeGetDomainAvaliability: {
            serviceNameForRequest = ServiceTypeGetDomainAvaliabilityStringName;
            break;
        }
        case ServiceTypeSearchMobileIMEI: {
            serviceNameForRequest = ServiceTypeSearchMobileIMEIStringName;
            break;
        }
        case ServiceTypeSearchMobileBrand: {
            serviceNameForRequest = ServiceTypeSearchMobileBrandStringName;
            break;
        }
        case ServiceTypeFeedback: {
            serviceNameForRequest = ServiceTypeFeedbackStringName;
            break;
        }
        case ServiceTypeSMSSpamReport: {
            serviceNameForRequest = ServiceTypeSMSSpamReportStringName;
            break;
        }
        case ServiceTypeHelpSalim: {
            serviceNameForRequest = ServiceTypeHelpSalimStringName;
            break;
        }
        case ServiceTypeVerification: {
            serviceNameForRequest = ServiceTypeVerificationStringName;
            break;
        }
        case ServiceTypeCoverage: {
            serviceNameForRequest = ServiceTypeCoverageStringName;
            break;
        }
        case ServiceTypeInternetSpeedTest: {
            serviceNameForRequest = ServiceTypeInternetSpeedTestStringName;
            break;
        }
        case ServiceTypeCompliantAboutServiceProvider: {
            serviceNameForRequest = ServiceTypeCompliantAboutServiceProviderStringName;
            break;
        }
        case ServiceTypeSuggestion: {
            serviceNameForRequest = ServiceTypeSuggestionStringName;
            break;
        }
        case ServiceTypeCompliantAboutServiceProviderEnquires: {
            serviceNameForRequest = ServiceTypeCompliantAboutServiceProviderEnquiresStringName;
            break;
        }
        case ServiceTypeCompliantAboutServiceProviderTRA: {
            serviceNameForRequest = ServiceTypeCompliantAboutServiceProviderTRAStringName;
            break;
        }
    }
    return serviceNameForRequest.capitalizedString;
}

@end
