//
//  FeedbackViewController.h
//  TRA Smart Services
//
//  Created by Admin on 07.12.15.
//  Copyright © 2015  All rights reserved.
//

#import "RatingView.h"

static NSString *const FeedbackViewControllerIdentifier = @"feedbackViewController";

@interface FeedbackViewController :  BaseDynamicUIViewController <RatingViewDelegate>

@property (strong, nonatomic) NSString *serviceRatingName;
@property (assign, nonatomic) NSInteger selectedServiceID;
@property (strong, nonatomic) NSString *titleScreen;

@end
