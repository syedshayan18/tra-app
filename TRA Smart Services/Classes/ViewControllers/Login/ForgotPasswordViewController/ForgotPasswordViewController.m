//
//  ForgotPasswordViewController.m
//  TRA Smart Services
//
//  Created by Admin on 21.07.15.
//

#import "ForgotPasswordViewController.h"
#import "Animation.h"
#import "ChangeForgotPasswordViewController.h"

static NSString *const ChangeForgotPasswordSegueIdentifier = @"ChangeForgotPasswordSegue";

@interface ForgotPasswordViewController ()

@property (weak, nonatomic) IBOutlet LeftInsetTextField *userEmailTextField;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (strong, nonatomic) NSString *uidResponse;

@end

@implementation ForgotPasswordViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.userEmailTextField.requredIndicatorEnable = YES;
}

#pragma mark - IBActions

- (IBAction)restorePasswordPressed:(id)sender
{
    [self.view endEditing:YES];

    if (!self.userEmailTextField.text.length) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.Email")]];
    } else if (![self.userEmailTextField.text isValidEmailUseHardFilter:NO]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatEmail")];
    } else {
        TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];
        __weak typeof(self) weakSelf = self;
        [[NetworkManager sharedManager] traSSForgotPasswordForEmail:self.userEmailTextField.text requestResult:^(id response, NSError *error) {
            if (error) {
                //shayan loader issue solved
                loader.ratingView.hidden = true;
                [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[response isKindOfClass:[NSString class]] ? response : error.localizedDescription];
            } else {
                
                loader.ratingView.hidden = false;
                [loader setCompletedStatus:TRACompleteStatusSuccess withDescription:nil];

                if ([response isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *responseDictionary = (NSDictionary *)response;
                    if ([[responseDictionary valueForKey:@"status"] boolValue] && [responseDictionary valueForKey:@"uid"]) {
                        self.uidResponse = [responseDictionary valueForKey:@"uid"];
                        [weakSelf performSegueWithIdentifier:ChangeForgotPasswordSegueIdentifier sender:self];
                    }
                }
            }
        }];
    }
}

- (void)closeButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:ChangeForgotPasswordSegueIdentifier]) {
        ChangeForgotPasswordViewController *viewController = segue.destinationViewController;
        viewController.uidResponse = self.uidResponse;
    }
}

#pragma mark - Private

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"restorePassword.title");
    [self.mainButton setTitle:dynamicLocalizedString(@"restorePassword.button.restore") forState:UIControlStateNormal];
    self.informationLabel.text = dynamicLocalizedString(@"restorePassword.information.label.text");
}

- (void)updateColors
{
    [super updateColors];
    
    [self configureTextField:self.userEmailTextField withImageName:@"ic_mail"];
    UIColor *color = [self.dynamicService currentApplicationColor];
    [super updateBackgroundImageNamed:@"res_img_bg"];
    self.userEmailTextField.textColor = color;
    self.userEmailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:dynamicLocalizedString(@"restorePassword.PlaceholderText.email") attributes:@{NSForegroundColorAttributeName: color}];
}

- (void)setRTLArabicUI
{
    self.userEmailTextField.textAlignment = NSTextAlignmentRight;
}

- (void)setLTREuropeUI
{
    self.userEmailTextField.textAlignment = NSTextAlignmentLeft;
}

@end
