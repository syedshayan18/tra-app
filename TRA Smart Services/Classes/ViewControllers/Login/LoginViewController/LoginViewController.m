//
//  LoginViewController.m
//  TRA Smart Services
//
//  Created by Admin on 20.07.15.
//

#import "LoginViewController.h"
#import "Animation.h"
#import "ForgotPasswordViewController.h"
#import "AppDelegate.h"
#import "TextFieldNavigator.h"
#import "KeychainStorage.h"
#import "ServicesSelectTableViewCell.h"
#import "RegisterViewController.h"

static NSString *const IconNameCheckActive = @"ic_check_act";
static NSString *const IconNameCheckDisactive = @"ic_check_disact";

static CGFloat const HeightSelectTableViewCell = 50.f;
static NSString *const LoginToRegisterSegueIdentifier = @"LoginToRegisterSegue";

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet LeftInsetTextField *userNameTextField;
@property (weak, nonatomic) IBOutlet LeftInsetTextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *rememberMeButton;

@property (weak, nonatomic) IBOutlet UIView *rememberMeSeparator;
@property (weak, nonatomic) IBOutlet UIView *loginSeparator;
@property (weak, nonatomic) IBOutlet UIView *verticalSeparator;
@property (weak, nonatomic) IBOutlet UIView *containerRegisterView;
@property (weak, nonatomic) IBOutlet UIView *containerActionButtonView;
@property (weak, nonatomic) IBOutlet UIView *rememberContainerView;

@property (weak, nonatomic) IBOutlet UILabel *rememberMeLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTableViewConstraint;

@property (strong, nonatomic) NSArray *dataSourceQuery;
@property (assign, nonatomic) NSInteger selectedIndexAccountType;

@property (strong, nonatomic) KeychainStorage *storage;
@property (assign, nonatomic) BOOL isViewControllerPresented;
@property (assign, nonatomic) BOOL rememberMe;

@end

@implementation LoginViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.rememberMe = YES;
    [self setEnableRememberMe:self.rememberMe];
    self.storage = [[KeychainStorage alloc] init];
    [self prepareNavigationBarButton];
    [self prepareDataSourceQuery];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.selectedIndexAccountType = 0;
    if (!self.isViewControllerPresented) {
        [self.view.layer addAnimation:[Animation fadeAnimFromValue:0.f to:1.0f delegate:nil] forKey:nil];
        self.view.layer.opacity = 1.0f;
        self.isViewControllerPresented = YES;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (self.navigationController.viewControllers.count == 1) {
        if (self.didDismissed) {
            self.didDismissed();
        }
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifierCell = self.dynamicService.language == LanguageTypeArabic ? selectProviderCellArabicUIIdentifier : selectProviderCellEuropeUIIdentifier;
    ServicesSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierCell forIndexPath:indexPath];
    if (indexPath.row) {
        cell.requiredIndicatorLabel.hidden = YES;
        cell.selectProviderLabel.text = self.dataSourceQuery[indexPath.row];
        cell.selectProviderLabel.textColor = [UIColor blackColor];
    } else {
        cell.requiredIndicatorLabel.hidden = NO;
        cell.requiredIndicatorLabel.textColor = [DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite ? [UIColor blackColor] : [UIColor redColor];
        cell.selectProviderImage.tintColor = [self.dynamicService currentApplicationColor];
        cell.selectProviderImage.image = self.heightTableViewConstraint.constant == HeightSelectTableViewCell ? [UIImage imageNamed:@"selectTableDn"] : [UIImage imageNamed:@"selectTableUp"];
        
        cell.selectProviderLabel.textColor = [self.dynamicService currentApplicationColor];
        if (self.selectedIndexAccountType > 0) {
            cell.selectProviderLabel.text = self.dataSourceQuery[self.selectedIndexAccountType];
        } else {
            cell.selectProviderLabel.text = self.dataSourceQuery[indexPath.row];
        }
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceQuery.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HeightSelectTableViewCell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if (self.heightTableViewConstraint.constant == HeightSelectTableViewCell) {
        self.selectedIndexAccountType = 0;
        [self animationSelectTableView:YES];
    } else {
        if (indexPath.row) {
            self.selectedIndexAccountType = indexPath.row;
            [self.tableView reloadData];
        }
        [self animationSelectTableView:NO];
    }
}

#pragma mark - IBActions

- (IBAction)registerButtonPressed:(id)sender
{
    [self performSegueWithIdentifier:LoginToRegisterSegueIdentifier sender:sender];
}

- (IBAction)loginButtonPressed:(id)sender
{
    if (!self.userNameTextField.text.length && !self.passwordTextField.text.length) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.EmptyInputParameters")];
    } else if (!self.userNameTextField.text.length) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.Email")]];
    } else if (!self.passwordTextField.text.length) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.Password")]];
    } else if (![self.userNameTextField.text isValidEmailUseHardFilter:NO]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatEmail")];
    } else {
        TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];
        loader.closeWithoutAnimation = YES;
        
        __weak typeof(self) weakSelf = self;
        void(^UpdateAvatar)(UserModel *user) = ^(UserModel *user) {
            [[NetworkManager sharedManager] traSSGetImageWithPath:user.uriForImage withCompletition:^(BOOL success, UIImage *response) {
                if (success) {
                    NSData *imageData = UIImageJPEGRepresentation(response, 1.0);
                    NSString *encodedString = [imageData base64EncodedStringWithOptions:kNilOptions];
                    UserModel *user = [[KeychainStorage new] loadCustomObjectWithKey:userModelKey];
                    user.avatarImageBase64 = encodedString;
                    [[KeychainStorage new] saveCustomObject:user key:userModelKey];
                }
                if (weakSelf.shouldAutoCloseAfterLogin) {
                    weakSelf.didCloseViewController = nil;
                    [weakSelf closeButtonPressed];
                }
                [loader dismissTRALoader:YES];
            }];
        };
        
        void(^GetUserProfile)() = ^() {
            [[NetworkManager sharedManager] traSSGetUserProfileResult:^(id response, NSError *error) {
                if ([response isKindOfClass:[NSDictionary class]]) {
                    UserModel *user = [[UserModel alloc] initWithDictionary:response];
                    [[KeychainStorage new] saveCustomObject:user key:userModelKey];
                    [[KeychainStorage new] saveCustomObject:@(self.selectedIndexAccountType) key:KeychainStorageKeyAccountType];

                    UpdateAvatar(user);
                } else if ([response isKindOfClass:[NSString class]]){
                    [loader dismissTRALoader:YES];
                    [AppHelper alertViewWithMessage:response];
                } else {
                    [loader dismissTRALoader:YES];
                    [AppHelper alertViewWithMessage:@"invalid data response"];
                }
            }];
        };
        
        [[NetworkManager sharedManager] traSSLoginUsername:self.userNameTextField.text password:self.passwordTextField.text accountType:self.selectedIndexAccountType secretQuesionType:0 secretQuestionAnswer:@"" requestResult:^(id response, NSError *error) {
            if (error) {
                [loader dismissTRALoader:YES];
                [response isKindOfClass:[NSString class]] ? [AppHelper alertViewWithMessage:response] : [AppHelper alertViewWithMessage:error.localizedDescription];
            } else {
                if (weakSelf.rememberMe) {
                    [weakSelf.storage storePassword:weakSelf.passwordTextField.text forUser:weakSelf.userNameTextField.text];
                }
                GetUserProfile();
            }
        }];
    }

}

- (void)closeButtonPressed
{
    self.navigationController.navigationBar.hidden = YES;
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1.f to:0.0f delegate:self] forKey:@"dismissView"];
    self.view.layer.opacity = 0.0f;
    if (self.didCloseViewController) {
        self.didCloseViewController();
    }
}

- (IBAction)rememberMeButtonTapped:(id)sender
{
    self.rememberMe = !self.rememberMe;
    [self setEnableRememberMe:self.rememberMe];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:LoginToRegisterSegueIdentifier]) {
        RegisterViewController *registerViewController = segue.destinationViewController;
        registerViewController.didDismissed = self.didDismissed;
        registerViewController.didCloseViewController = self.didCloseViewController;
    }
}

#pragma mark - Animations

- (void)animationDidStop:(nonnull CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"dismissView"]) {
        [self.view.layer removeAllAnimations];
        [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)animationSelectTableView:(BOOL)selected
{
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CGFloat heightTableView = HeightSelectTableViewCell;
    if (selected) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        heightTableView = self.dataSourceQuery.count > 5 ? HeightSelectTableViewCell * 5 :  HeightSelectTableViewCell * self.dataSourceQuery.count;
        self.tableView.scrollEnabled = self.dataSourceQuery.count > 5 ? YES : NO;
    }
    [self.view layoutIfNeeded];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.heightTableViewConstraint.constant = heightTableView;
        [weakSelf.view layoutIfNeeded];
    }];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - Private

- (void)updateUI:(NSTextAlignment)textAlignment
{
    [self configureTextField:self.userNameTextField withImageName:@"ic_mail"];
    [self configureTextField:self.passwordTextField withImageName:@"ic_pass"];
    
    self.userNameTextField.textAlignment = textAlignment;
    self.passwordTextField.textAlignment = textAlignment;
    self.rememberMeLabel.textAlignment = textAlignment;
}

#pragma mark - UI

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"login.title");
    self.userNameTextField.placeholder = dynamicLocalizedString(@"login.placeHolderText.username");
    self.passwordTextField.placeholder = dynamicLocalizedString(@"login.placeHolderText.password");
    [self.loginButton setTitle:dynamicLocalizedString(@"login.button.login") forState:UIControlStateNormal];
    [self.forgotPasswordButton setTitle:dynamicLocalizedString(@"login.button.forgotPassword") forState:UIControlStateNormal];
    [self.registerButton setTitle:dynamicLocalizedString(@"login.button.registerButton") forState:UIControlStateNormal];
    self.rememberMeLabel.text = dynamicLocalizedString(@"login.rememberMeLabel");
}

- (void)updateColors
{
    [super updateColors];

    UIColor *color = [self.dynamicService currentApplicationColor];
    [self.loginButton setBackgroundColor:color];
    [self.registerButton setBackgroundColor:color];
    //shayan commented
    //[self.registerButton setTitleColor:color forState:UIControlStateNormal];

    [self.registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    [self.forgotPasswordButton setTitleColor:color forState:UIControlStateNormal];
    
    [self.userNameTextField.leftView setTintColor:color];
    [self.userNameTextField.rightView setTintColor:color];
    self.userNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:dynamicLocalizedString(@"register.placeHolderText.email") attributes:@{NSForegroundColorAttributeName: UIColor.grayColor}];
    self.userNameTextField.textColor = color;
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:dynamicLocalizedString(@"login.placeHolderText.password") attributes:@{NSForegroundColorAttributeName: UIColor.grayColor}];
    self.passwordTextField.textColor = color;
    
    self.loginSeparator.backgroundColor = color;
    self.verticalSeparator.backgroundColor = color;
    self.rememberMeLabel.textColor = color;
    self.rememberMeSeparator.backgroundColor = color;
    self.rememberMeButton.tintColor = color;
    
    [super updateBackgroundImageNamed:@"res_img_bg"];
}

- (void)setLTREuropeUI
{
    [self updateUI:NSTextAlignmentLeft];
    [self transformUILayer:CATransform3DIdentity];
}

- (void)setRTLArabicUI
{
    [self updateUI:NSTextAlignmentRight];
    [self transformUILayer:TRANFORM_3D_SCALE];
}

- (void)prepareNavigationBarButton
{
    [self.navigationController setButtonWithImageNamed:CloseButtonImageName andActionDelegate:self tintColor:[UIColor whiteColor] position:ButtonPositionModeLeft selector:@selector(closeButtonPressed)];
}

- (void)transformUILayer:(CATransform3D)animCATransform3D
{
    self.rememberContainerView.layer.transform = animCATransform3D;
    self.rememberMeLabel.layer.transform = animCATransform3D;
    self.rememberMeButton.layer.transform = animCATransform3D;
}

- (void)prepareDataSourceQuery
{
    self.dataSourceQuery = @[dynamicLocalizedString(@"login.accountType.individualsServices"),
                             dynamicLocalizedString(@"login.accountType.businessServices")];
}

- (void)setEnableRememberMe:(BOOL)enable
{
    UIImage *icon = [UIImage imageNamed:enable ? IconNameCheckActive : IconNameCheckDisactive];
    [self.rememberMeButton setImage:icon forState:UIControlStateNormal];
}

@end
