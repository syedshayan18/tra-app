//
//  RegisterViewController.h
//  TRA Smart Services
//
//  Created by Admin on 21.07.15.
//

#import "BaseMembershipViewController.h"

@interface RegisterViewController : BaseMembershipViewController <UIGestureRecognizerDelegate, UIScrollViewDelegate, LeftInsetTextFieldDelegate>

@property (strong, nonatomic) void (^didCloseViewController)();
@property (strong, nonatomic) void (^didDismissed)();

@end
