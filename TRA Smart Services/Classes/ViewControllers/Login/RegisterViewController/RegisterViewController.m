//
//  RegisterViewController.m
//  TRA Smart Services
//
//  Created by Admin on 21.07.15.
//

#import "RegisterViewController.h"
#import "TextFieldNavigator.h"
#import "ServicesSelectTableViewCell.h"

static NSString *const IconNameCheckActive = @"ic_check_act";
static NSString *const IconNameCheckDisactive = @"ic_check_disact";

static NSString *const DividerForID = @"-";
static NSString *const DefautCharPhoneTextField = @"x";
static NSString *const DefautStringPhoneTextField = @"+9715xxxxxxxx";

typedef NS_ENUM(NSUInteger, ContainerSelectQeryEditType) {
    ContainerSelectQueryViewDisable,
    ContainerSelectQueryViewEnabled,
    ContainerSelectQueryViewEditQuery
};

@interface RegisterViewController ()

@property (strong, nonatomic) IBOutlet UIView *mainContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@property (weak, nonatomic) IBOutlet LeftInsetTextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet LeftInsetTextField *emiratesIDTextField;
@property (weak, nonatomic) IBOutlet LeftInsetTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet LeftInsetTextField *mobileTextField;
@property (weak, nonatomic) IBOutlet LeftInsetTextField *emailTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaseRegisterConteinerUIView;
@property (weak, nonatomic) IBOutlet UIView *registerContainer;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomRegisterContainerConstraint;

@property (assign, nonatomic) CGFloat offSetTextFildY;
@property (assign, nonatomic) CGFloat keyboardHeight;

@end

@implementation RegisterViewController

#pragma mark - LifeCycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareRegisterConteinerUIView];
    self.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    self.emiratesIDTextField.subDelegate = self;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 7) {
        [self prepareBeginEditingTextFieldPhone:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    if (textField.tag == 7) {
       return [self prepareTextFieldPhone:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.mobileTextField && [self.mobileTextField.text isEqualToString:DefautStringPhoneTextField]) {
        self.mobileTextField.text = @"";
    }
}

#pragma mark - IBActions

- (IBAction)registerButtonPress:(id)sender
{
    if (![self isInputParametersInvalid]) {
        [self.view endEditing:YES];
        __weak typeof(self) weakSelf = self;
        TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];
        [[NetworkManager sharedManager] traSSRegisterUsername:self.emailTextField.text
                                                  firstNameEN:self.dynamicService.language == LanguageTypeArabic ? @"" : self.firstNameTextField.text
                                                   lastNameEN:self.dynamicService.language == LanguageTypeArabic ? @"" : self.lastNameTextField.text
                                                  firstNameAR:self.dynamicService.language == LanguageTypeArabic ? self.firstNameTextField.text : @""
                                                   lastNameAR:self.dynamicService.language == LanguageTypeArabic ? self.lastNameTextField.text : @""
                                                   emiratesID:[self.emiratesIDTextField.text stringByReplacingOccurrencesOfString:DividerForID withString:@""]
                                                  mobilePhone:[self deleteOneCharPlusToStrind:self.mobileTextField.text]
                                                requestResult:^(id response, NSError *error) {
            if (error) {
                //shayan loader issue solved
                loader.ratingView.hidden = true;
                [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[response isKindOfClass:[NSString class]] ? response : error.localizedDescription];
            }  else {
                loader.TRALoaderDidClose = ^{
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                };
                loader.ratingView.hidden = false;

                [loader setCompletedStatus:TRACompleteStatusSuccess withDescription:nil];
            }
        }];
    }
}

- (void)doneFilteringButtonTapped
{
    [self.mobileTextField becomeFirstResponder];
}

- (IBAction)didChangeEmiratesID:(UITextField *)sender
{
    [self prepareEmiratesIDWithTextField:sender];
}

#pragma mark - LeftInsetTextFieldDelegate

- (void)textFieldDidDelete:(UITextField *)textField
{
    if ((textField.text.length == 4 || textField.text.length == 9 || textField.text.length == 17) && textField.tag == 6){
        textField.text = [textField.text substringToIndex:textField.text.length - 1];
    }
}


#pragma mark - Superclass methods

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"register.title");
    [self.registerButton setTitle:dynamicLocalizedString(@"register.button.register") forState:UIControlStateNormal];
    [self prepareRequiredIndicatorTextFieldLabel:self.registerContainer];
}

- (void)updateColors
{
    [super updateColors];
    
    UIColor *color = [self.dynamicService currentApplicationColor];
    [super updateBackgroundImageNamed:@"res_img_bg"];
    
    [self.registerButton setBackgroundColor:[self.dynamicService currentApplicationColor]];
    
    self.mobileTextField.attributedPlaceholder = [self placeholderWithString:dynamicLocalizedString(@"register.placeHolderText.phone")];
    self.firstNameTextField.attributedPlaceholder = [self placeholderWithString:dynamicLocalizedString(@"register.placeHolderText.firstName")];
    self.lastNameTextField.attributedPlaceholder = [self placeholderWithString:dynamicLocalizedString(@"register.placeHolderText.lastName")];
    self.emailTextField.attributedPlaceholder = [self placeholderWithString:dynamicLocalizedString(@"register.placeHolderText.email")];
    self.emiratesIDTextField.attributedPlaceholder = [self placeholderWithString:dynamicLocalizedString(@"register.placeHolderText.emirateID")];
    
    self.mobileTextField.textColor = color;
    self.firstNameTextField.textColor = color;
    self.lastNameTextField.textColor = color;
    self.emailTextField.textColor = color;
    self.emiratesIDTextField.textColor = color;
}

- (void)setLTREuropeUI
{
    [self updateUI:NSTextAlignmentLeft];
}

- (void)setRTLArabicUI
{
    [self updateUI:NSTextAlignmentRight];
}

#pragma mark - Private

- (void)updateUI:(NSTextAlignment)textAlignment
{
    [self configureTextField:self.firstNameTextField withImageName:@"ic_user_login"];
    [self configureTextField:self.emiratesIDTextField withImageName:@"ic_id"];
    [self configureTextField:self.lastNameTextField withImageName:@"ic_user_login"];
    [self configureTextField:self.mobileTextField withImageName:@"ic_phone_reg"];
    [self configureTextField:self.emailTextField withImageName:@"ic_mail"];

    self.firstNameTextField.textAlignment = textAlignment;
    self.emiratesIDTextField.textAlignment = textAlignment;
    self.lastNameTextField.textAlignment = textAlignment;
    self.mobileTextField.textAlignment = textAlignment;
    self.emailTextField.textAlignment = textAlignment;
}

- (NSAttributedString *)placeholderWithString:(NSString *)string
{
    NSDictionary *attributes = @{NSForegroundColorAttributeName:UIColor.grayColor};
    return [[NSAttributedString alloc] initWithString:string attributes:attributes];
}

- (void)prepareRegisterConteinerUIView
{
    self.verticalSpaseRegisterConteinerUIView.constant = self.logoImageView.frame.size.height - self.navigationController.navigationBar.frame.size.height - self.navigationController.navigationBar.frame.origin.y;

    self.emailTextField.requredIndicatorEnable = YES;
    self.lastNameTextField.requredIndicatorEnable = YES;
}

- (NSString *)deleteOneCharPlusToStrind:(NSString *)string
{
    if ([self.mobileTextField.text isEqualToString:DefautStringPhoneTextField] || !self.mobileTextField.text.length) {
        return @"";
    }
    NSRange rangeX = [string rangeOfString:@"+"];
    NSMutableString *stringMutable = [NSMutableString stringWithString:string];
    if (rangeX.location == 0) {
        [stringMutable deleteCharactersInRange:rangeX];
    }
    return stringMutable;
}

#pragma mark - TextFieldCalculations

- (void)prepareEmiratesIDWithTextField:(UITextField *)textField
{
    if (textField.text.length == 3 || textField.text.length == 8 || textField.text.length == 16) {
        NSString *textToShow = [textField.text stringByAppendingString:DividerForID];
        textField.text = textToShow;
    }
    if (textField.text.length >= 18) {
        NSString *phone = [textField.text substringToIndex:18];
        textField.text = phone;
    }
    
    if ((textField.text.length == 4 && ([textField.text rangeOfString:DividerForID].location == NSNotFound))) {
        NSString *firstPart = [textField.text substringToIndex:3];
        firstPart = [[firstPart stringByAppendingString:DividerForID] stringByAppendingString:[textField.text substringFromIndex:3]];
        textField.text = firstPart;
    } else if ((textField.text.length == 9 && [[textField.text substringFromIndex:4] rangeOfString:DividerForID].location == NSNotFound)) {
        NSString *firstPart = [textField.text substringToIndex:8];
        firstPart = [[firstPart stringByAppendingString:DividerForID] stringByAppendingString:[textField.text substringFromIndex:8]];
        textField.text = firstPart;
    } else if ((textField.text.length == 17 && [[textField.text substringFromIndex:9] rangeOfString:DividerForID].location == NSNotFound)) {
        NSString *firstPart = [textField.text substringToIndex:16];
        firstPart = [[firstPart stringByAppendingString:DividerForID] stringByAppendingString:[textField.text substringFromIndex:16]];
        textField.text = firstPart;
    }
}

- (void)prepareBeginEditingTextFieldPhone:(UITextField *)textField
{
    if (textField.text.length == 0) {
        textField.text = DefautStringPhoneTextField;
    }
    [self selectTextForInput:textField atRange:NSMakeRange(5, 0)];
}

- (BOOL)prepareTextFieldPhone:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (range.length > 0) {
        if (range.location > 4) {
            textField.text = [self string:textField.text replaceCharactersInRange:range withString:DefautCharPhoneTextField];
            
            NSRange rangeX = [textField.text rangeOfString:DefautCharPhoneTextField];
            [self selectTextForInput:textField atRange:NSMakeRange(rangeX.location, 0)];
        }
    } else if (string.length == 1 && [string isValidNumber]) {
        NSRange rangeX = [textField.text rangeOfString:DefautCharPhoneTextField];
        if (rangeX.location != NSNotFound ) {
            textField.text = [self string:textField.text replaceCharactersInRange:rangeX withString:string];
            
            [self selectTextForInput:textField atRange:NSMakeRange(rangeX.location + 1, 0)];
        }
    }
    return NO;
}

- (void)selectTextForInput:(UITextField *)input atRange:(NSRange)range
{
    UITextPosition *start = [input positionFromPosition:[input beginningOfDocument] offset:range.location];
    UITextPosition *end = [input positionFromPosition:start offset:range.length];
    [input setSelectedTextRange:[input textRangeFromPosition:start toPosition:end]];
}

- (NSString *)string:(NSString *)stringText replaceCharactersInRange:(NSRange)range withString:(NSString *)string
{
    NSMutableString *stringMutable = [NSMutableString stringWithString:stringText];
    [stringMutable replaceCharactersInRange:range withString:string];
    return stringMutable;
}

#pragma mark - ValidationMethods

- (BOOL)isInputParametersInvalid
{
    if (!self.lastNameTextField.text.length && !self.emailTextField.text.length) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.EmptyInputParameters")];
    } else if (!self.emailTextField.text.length) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.Email")]];
    } else if (!self.lastNameTextField.text.length) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.LastNameEN")]];
    } else if (![self.emailTextField.text isValidEmailUseHardFilter:NO]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatEmail")];
    } else if (self.firstNameTextField.text.length && self.firstNameTextField.text.length > 32) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooLong"), dynamicLocalizedString(@"message.EmptyInputParameter.FirstNameEN")]];
    } else if (self.firstNameTextField.text.length && self.firstNameTextField.text.length < 3) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooShort"), dynamicLocalizedString(@"message.EmptyInputParameter.FirstNameEN")]];
    } else if (self.lastNameTextField.text.length && self.lastNameTextField.text.length > 32) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooLong"), dynamicLocalizedString(@"message.EmptyInputParameter.LastNameEN")]];
    } else if (self.lastNameTextField.text.length && self.lastNameTextField.text.length < 3) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooShort"), dynamicLocalizedString(@"message.EmptyInputParameter.LastNameEN")]];
        
    } else if (self.lastNameTextField.text.length && ![self.lastNameTextField.text isValidNameArabicAndEnglish]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatLastName")];
    } else if (self.firstNameTextField.text.length && ![self.firstNameTextField.text isValidNameArabicAndEnglish]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatFirstName")];
        
    } else if (![self.emiratesIDTextField.text isValidIDEmirates] && self.emiratesIDTextField.text.length) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatIDEmirates")];
    } else if (![self.mobileTextField.text isValidPhoneNumber] && self.mobileTextField.text.length && ![self.mobileTextField.text isEqualToString:DefautStringPhoneTextField]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidPhoneNumberFormat")];
    } else {
        return NO;
    }
    return YES;
}

- (void)prepareRequiredIndicatorTextFieldLabel:(UIView *)view
{
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UILabel class]] && subView.tag == 87) {
            [(UILabel *)subView setTextAlignment: self.dynamicService.language == LanguageTypeArabic ? NSTextAlignmentLeft : NSTextAlignmentRight];
            [(UILabel *)subView setTextColor:[DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite ? [UIColor blackColor] : [UIColor redColor]];
        }
        [self prepareRequiredIndicatorTextFieldLabel:subView];
    }
}

@end
