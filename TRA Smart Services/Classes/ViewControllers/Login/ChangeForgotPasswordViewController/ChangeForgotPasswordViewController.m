//
//  ChangePasswordViewController.m
//  TRA Smart Services
//
//  Created by  on 23.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "ChangeForgotPasswordViewController.h"
#import "OffsetTextField.h"

@interface ChangeForgotPasswordViewController ()

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet LeftInsetTextField *verifityOTPTextField;
@property (weak, nonatomic) IBOutlet LeftInsetTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet LeftInsetTextField *comfirmPasswordTextField;

@end

@implementation ChangeForgotPasswordViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - IBAction

- (IBAction)tappedChangeForgetPasswordButton:(id)sender
{
    [self.view endEditing:YES];

    if ([NetworkManager sharedManager].networkStatus <= 0) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.NoInternetConnection")];
    } else if (![self isInputParametersInvalid]) {
        __weak typeof(self) weakSelf = self;
        TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];
        [[NetworkManager sharedManager] traSSForgotChangePasswordUID:self.uidResponse verifityOTP:self.verifityOTPTextField.text newPassword:self.passwordTextField.text requestResult:^(id response, NSError *error) {
            if (error) {
                //shayan loader issue solved
                loader.ratingView.hidden = true;
                [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[response isKindOfClass:[NSString class]] ? response : error.localizedDescription];
            }  else {
                loader.TRALoaderDidClose = ^{
                    [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                };
                loader.ratingView.hidden = false;

                [loader setCompletedStatus:TRACompleteStatusSuccess withDescription:nil];
            }
        }];
    }
}

- (IBAction)showHidePassword:(UIButton *)sender
{
    [sender setSelected:!sender.isSelected];
    [sender setTintColor:sender.isSelected ? self.dynamicService.currentApplicationColor :[UIColor grayColor]];
    [(UITextField *)[sender superview] setSecureTextEntry:!sender.isSelected];
    [(UITextField *)[sender superview] becomeFirstResponder];
}

#pragma mark - SuperclassMethods

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"changeForgotPasswordViewController.title");
    [self.mainButton setTitle:dynamicLocalizedString(@"changeForgotPasswordViewController.changeForgotPasswordButton.text") forState:UIControlStateNormal];
    self.descriptionLabel.text = dynamicLocalizedString(@"changeForgotPasswordViewController.descriptionLabel.text");
}

- (void)updateColors
{
    [super updateColors];
    
    UIColor *color = [self.dynamicService currentApplicationColor];
    [super updateBackgroundImageNamed:@"res_img_bg"];
    self.verifityOTPTextField.textColor = color;
    self.passwordTextField.textColor = color;
    self.comfirmPasswordTextField.textColor = color;
    self.verifityOTPTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:dynamicLocalizedString(@"changeForgotPasswordViewController.verifityOTP.placecholder.text") attributes:@{NSForegroundColorAttributeName: color}];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:dynamicLocalizedString(@"changePassword.placeHolder.password") attributes:@{NSForegroundColorAttributeName: color}];
    self.comfirmPasswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:dynamicLocalizedString(@"changePassword.placeHolder.retypePassword") attributes:@{NSForegroundColorAttributeName: color}];
}

- (void)setRTLArabicUI
{
    [self updateUI:NSTextAlignmentRight];
}

- (void)setLTREuropeUI
{
    [self updateUI:NSTextAlignmentLeft];
}

#pragma mark - Private

- (void)configureButtonShowHidePasswordTextField:(UITextField *)textField
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [button setImage:[UIImage imageNamed:@"btn_eye"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_eye"] forState:UIControlStateSelected];
    [button setTintColor:[UIColor grayColor]];
    [button addTarget:self action:@selector(showHidePassword:) forControlEvents:UIControlEventTouchUpInside];
    if (self.dynamicService.language == LanguageTypeArabic) {
        textField.leftView = nil;
        textField.leftViewMode = UITextFieldViewModeAlways;
        textField.leftView = button;
    } else {
        textField.rightView = nil;
        textField.rightViewMode = UITextFieldViewModeAlways;
        textField.rightView = button;
    }
}

- (void)updateUI:(NSTextAlignment)textAlignment
{
    [self configureTextField:self.verifityOTPTextField withImageName:@"ic_pass"];
    [self configureTextField:self.passwordTextField withImageName:@"ic_pass"];
    [self configureTextField:self.comfirmPasswordTextField withImageName:@"ic_pass"];
    
    [self configureButtonShowHidePasswordTextField:self.verifityOTPTextField];
    [self configureButtonShowHidePasswordTextField:self.passwordTextField];
    [self configureButtonShowHidePasswordTextField:self.comfirmPasswordTextField];
    
    self.verifityOTPTextField.textAlignment = textAlignment;
    self.passwordTextField.textAlignment = textAlignment;
    self.comfirmPasswordTextField.textAlignment = textAlignment;
}

#pragma mark - ValidationMethods

- (BOOL)passwordValidation
{
    if (self.passwordTextField.text.length < 8) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.PasswordIsTooShort8")];
    } else if (self.passwordTextField.text.length >= 32) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.PasswordIsTooLong32")];
    } else if (![self.passwordTextField.text isEqualToString:self.comfirmPasswordTextField.text]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.PasswordsNotEqual")];
    } else if (![self.passwordTextField.text isValidPasswordFormat]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.PasswordPowerful")];
    } else {
        return NO;
    }
    return YES;
}

- (BOOL)isInputParametersInvalid
{
    if (!self.verifityOTPTextField.text.length && !self.passwordTextField.text.length && !self.comfirmPasswordTextField.text.length) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.EmptyInputParameters")];
    } else if (!self.verifityOTPTextField.text.length) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.verifityOTP")]];
    } else if (!self.passwordTextField.text.length) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.Password")]];
    } else if (!self.comfirmPasswordTextField.text.length) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.ComfirmPassword")]];
    } else if (self.verifityOTPTextField.text.length > 32) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.InvalidFormatTooLong"), dynamicLocalizedString(@"message.EmptyInputParameter.verifityOTP")]];
    } else if (![self passwordValidation]){
        return NO;
    }
    return YES;
}

@end
