//
//  ChangeForgotPasswordViewController.h
//  TRA Smart Services
//
//  Created by  on 23.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "BaseMembershipViewController.h"

@interface ChangeForgotPasswordViewController : BaseMembershipViewController

@property (strong, nonatomic) NSString *uidResponse;

@end
