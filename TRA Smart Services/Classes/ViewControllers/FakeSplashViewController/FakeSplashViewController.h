//
//  FakeSplashViewController.h
//  TRA Smart Services
//
//  Created by Admin on 01.12.15.
//

static NSString *const fakeSplashViewControllerIdentifier = @"fakeSplashViewControllerID";

@interface FakeSplashViewController : BaseDynamicUIViewController

- (void)stopAnimationFakeSplash;
- (void)resumeProgressLoad;
- (void)pauseProgressLoad;

@end
