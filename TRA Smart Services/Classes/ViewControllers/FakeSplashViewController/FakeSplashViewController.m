//
//  FakeSplashViewController.m
//  TRA Smart Services
//
//  Created by Admin on 01.12.15.
//

#import "FakeSplashViewController.h"
#import "AutoLoginService.h"
#import "FingerPrintAuth.h"
#import "KeychainStorage.h"
#import "SettingViewController.h"
#import "ServiceModel.h"
#import "DynamicServiceModel.h"
#import "NSTimer+Pausable.h"
#import "UIImage+animatedGIF.h"

@interface FakeSplashViewController ()

@property (weak, nonatomic) IBOutlet UILabel *loadingUpdatesLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIImageView *gifImageView;

@property (strong, nonatomic) FingerPrintAuth *touchAuth;
@property (strong, nonatomic) AutoLoginService *autoLogger;
@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) CGFloat progress;

@property (strong, nonatomic) NSArray *servicesList;
@property (strong, nonatomic) dispatch_semaphore_t startapSemaphore;

@end

@implementation FakeSplashViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *gifURL = [[NSBundle mainBundle] URLForResource:@"splash" withExtension:@"gif"];
    self.gifImageView.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:gifURL]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareWillUI];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationController setNavigationBarHidden:NO];

    if ([self.timer isValid]) {
        [self.timer invalidate];
    }
    self.timer = nil;
}

#pragma mark - Public 

- (void)stopAnimationFakeSplash
{
    [self.progressView setProgress:1 animated:YES];
    __weak typeof(self) weakSelf = self;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf.navigationController popToRootViewControllerAnimated:NO];
    });
}

- (void)resumeProgressLoad
{
    [self.timer resume];
}

- (void)pauseProgressLoad
{
    [self.timer pause];
}

#pragma mark - SuperClassMethods

- (void)updateColors
{
    self.progressView.trackTintColor = [UIColor groupTableViewBackgroundColor];
    self.progressView.progressTintColor = self.dynamicService.currentApplicationColor;
    
    [super updateBackgroundImageNamed:@"launch_back"];
}

- (void)localizeUI
{
    self.loadingUpdatesLabel.text = dynamicLocalizedString(@"fakeSplashViewController.hloadingUpdatesLabel.text");
}

- (void)setRTLArabicUI
{
    self.progressView.layer.transform = CATransform3DMakeScale( - 1, 4.5, 1);
    self.loadingUpdatesLabel.textAlignment = NSTextAlignmentRight;
}

- (void)setLTREuropeUI
{
    self.progressView.layer.transform = CATransform3DMakeScale(1, 4.5, 1);
    self.loadingUpdatesLabel.textAlignment = NSTextAlignmentLeft;
}

#pragma mark - Network mathods

- (void)performGetServicesEnableToAutoLogin
{
    [[NetworkManager sharedManager] traSSNoCRMServiceGetAllServicesEnable:^(id response, NSError *error) {
        if (error) {
            [self preparePresentMessegeErrorLoader:[self hendelServiceError:error response:response]];
        } else {
            [[CoreDataManager sharedManager] prepareCoreDataWintResponse:response];
            [self performAutoLogin];
        }
    }];
    
    [[NetworkManager sharedManager] traSSCRMDynamicServiceGetPDF:^(id response, NSError *error) {
        DynamicServiceModel *model = response;
        NSString *arPDF = model.dynamicServiceARPDF;
        NSString *enPDF = model.dynamicServiceENPDF;
        [[NSUserDefaults standardUserDefaults] setObject:arPDF forKey:@"arPDF"];
        [[NSUserDefaults standardUserDefaults] setObject:enPDF forKey:@"enPDF"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}

- (void)performAutoLogin
{
    if ([KeychainStorage userName].length) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:KeyUseTouchIDIdentification]) {
            [self.timer pause];
            self.touchAuth = [[FingerPrintAuth alloc] init];
            [self.touchAuth authentificationsWithTouchWithViewController:self];
        } else {
            self.autoLogger = [[AutoLoginService alloc] init];
            
            __weak typeof(self) weakSelf = self;
            self.autoLogger.autoLoginServiceResalt = ^{
                [weakSelf stopAnimationFakeSplash];
            };
            [self.autoLogger performAutoLoginIfPossible];
        }
    } else {
        [self stopAnimationFakeSplash];
    }
}

#pragma mark - Private

- (void)prepareWillUI
{
    self.progress = 0;
    self.progressView.progress = 0;
    
    [self.tabBarController.tabBar setHidden:YES];
    [self.navigationController setNavigationBarHidden:YES];
    if ([self.timer isValid]) {
        [self.timer invalidate];
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(animationProgresView:) userInfo:nil repeats:YES];
    [self performGetServicesEnableToAutoLogin];
}

- (void)preparePresentMessegeErrorLoader:(NSString *)errorString
{
    TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:nil closeButton:YES];
    __weak typeof(self) weakSelf = self;
    loader.TRALoaderDidClose = ^{
        [weakSelf prepareWillUI];
    };
    //shayan loader issue solved
    loader.ratingView.hidden = true;
    [loader setCompletedStatus:TRACompleteStatusFailure withDescription:errorString];
}

- (void)animationProgresView:(id)sender
{
    self.progress += 0.1;
    if (self.progressView.progress < 1) {
        [self.progressView setProgress:exp( - 3 / self.progress) animated:YES];
    }
}

- (NSString *)hendelServiceError:(NSError *)error response:(id)response
{
    NSString *errorString = error.localizedDescription;
    
    NSInteger statusCode = ((NSHTTPURLResponse *)error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey]).statusCode;
    if (error.code == NSURLErrorNotConnectedToInternet) {
        errorString = dynamicLocalizedString(@"message.NoInternetConnection");
    } else if (error.code == NSURLErrorTimedOut) {
        errorString = dynamicLocalizedString(@"message.error.serverTimedOut");
    } else if (error.code == NSURLErrorCancelled) {
        errorString = dynamicLocalizedString(@"message.OperationCanceledByUser");
    } else if (statusCode == 426) {
        errorString = dynamicLocalizedString(@"message.error.serviceIsDisable");
        [[CoreDataManager sharedManager] prepareCoreDataWintResponse:response];
    } else if (statusCode == 416) {
        errorString = dynamicLocalizedString(@"message.error.apiVersionIsUnsupported");
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.error.apiVersionIsUnsupported") delegate:self otherButtonTitles:dynamicLocalizedString(@"uiElement.cancelButton.title")];
    }else if ([response isKindOfClass:[NSString class]]) {
        errorString = response;
    }
    return errorString;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (!buttonIndex) {
        NSString *appstoreString = @"https://itunes.apple.com/us/app/uae-tra/id930647801";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appstoreString]];
    }
}

@end
