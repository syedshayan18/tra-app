//
//  FavouriteViewController.h
//  TRA Smart Services
//
//  Created by Admin on 14.08.15.
//

#import "FavouriteTableViewCell.h"

static CGFloat const AnimationDuration = 0.3f;

@interface FavouriteViewController : BaseDynamicUIViewController <UITableViewDelegate, UITableViewDataSource, FavouriteTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerSegmentControlView;

@property (strong, nonatomic) CALayer *arcDeleteZoneLayer;
@property (strong, nonatomic) CALayer *contentFakeIconLayer;
@property (strong, nonatomic) CALayer *shadowFakeIconLayer;

- (CGFloat)headerHeight;

@end
