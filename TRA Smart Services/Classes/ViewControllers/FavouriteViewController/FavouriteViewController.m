//
//  FavouriteViewController.m
//  TRA Smart Services
//
//  Created by Admin on 14.08.15.
//

#import "UIImageView+AFNetworking.h"
#import "FavouriteViewController.h"
#import "AppDelegate.h"
#import "TRAStaticService.h"
#import "TRADynamicService.h"
#import "Animation.h"
#import "ServiceInfoViewController.h"
#import "FavouriteViewController+DeleteAction.h"
#import "SpamReportViewController.h"
#import "FeedbackViewController.h"
#import "HomeSearchViewController.h"
#import "DynamicContainerServiceViewController.h"
#import "LoginViewController.h"

static CGFloat const DefaultOffsetForElementConstraintInCell = 20.f;
static CGFloat const SummOfVerticalOffsetsForCell = 85.f;
static CGFloat const HeightHeiderAddService = 70.;

static NSString *const ServiceInfoListSegueIdentifier = @"serviceInfoListSegue";
static NSString *const AddToFavoriteSegueIdentifier = @"addToFavoriteSegue";
static NSString *const FavoriteToHomeSearchSegueIdentifier = @"FavoriteToHomeSearchSegue";

@interface FavouriteViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeiderAddServiceConstraint;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UIView *placeHolderView;
@property (weak, nonatomic) IBOutlet UIButton *addFavouriteButton;
@property (weak, nonatomic) IBOutlet UIButton *addServiceHiddenButton;
@property (weak, nonatomic) IBOutlet UIView *headerAddServiceView;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *actionDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *addServiceHiddenImageView;

@property (strong, nonatomic) NSMutableArray <TRABaseService *> *dataSource;
@property (strong, nonatomic) NSMutableArray <TRABaseService *> *filteredDataSource;
@property (strong, nonatomic) NSMutableArray <TRABaseService *> *dataSourceFavorites;
@property (strong, nonatomic) NSMutableArray <TRABaseService *> *dataSourceRating;

@property (assign, nonatomic) BOOL removeProcessIsActive;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation FavouriteViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNibs];
    self.headerAddServiceView.hidden = YES;
    
    [AppHelper addHexagoneOnView:self.addServiceHiddenImageView];
    [self prepareNavigationBar];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self fetchFavouriteList];
    [self.tableView reloadData];

    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.dataSource.count) {
        self.headerAddServiceView.hidden = NO;
    }
}

#pragma mark - Custom Accessors

- (void)setDataSource:(NSMutableArray *)dataSource
{
    _dataSource = dataSource;
    
    [self showPlaceHolderIfNeeded];
}

- (CGFloat)headerHeight
{
    return self.headerAddServiceView.frame.size.height;
}

#pragma mark - IBActions

- (IBAction)addFavouriteButtonPress:(id)sender
{
    if (self.segmentControl.selectedSegmentIndex == 0) {
        [self performSegueWithIdentifier:AddToFavoriteSegueIdentifier sender:self];
    }
}

- (IBAction)searchBarButtonTapped:(id)sender
{
    HomeSearchViewController *homeSearchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeSearchViewController"];
    
    homeSearchViewController.fakeBackground = [AppHelper snapshotForView:self.navigationController.view];
    homeSearchViewController.hidesBottomBarWhenPushed = YES;
    
    __weak typeof(self) weakSelf = self;
    if (self.segmentControl.selectedSegmentIndex == 0) {
        homeSearchViewController.searchType = SearchTypeFavorites;
        homeSearchViewController.didSelectService = ^(TRABaseService *selectedServise){
            [UIView performWithoutAnimation:^{
                if ([selectedServise isKindOfClass:[TRAStaticService class]]) {
                    [weakSelf performNavigationToServiceWithIndex:[((TRAStaticService *)selectedServise).serviceInternalID integerValue]];
                } else if ([selectedServise isKindOfClass:[TRADynamicService class]]) {
                    DynamicServiceModel *dynamicServiceModel = [[DynamicServiceModel alloc] initWithTRADynamicServise:(TRADynamicService *)selectedServise];
//                    [self performNavigationToDynamicService:dynamicServiceModel];
                    [self presentDynamicService:dynamicServiceModel];
                }
            }];
        };
    } else {
        homeSearchViewController.searchType = SearchTypeRationgs;
        homeSearchViewController.didSelectService = ^(TRABaseService *selectedServise){
            [UIView performWithoutAnimation:^{
                [weakSelf showRatingViewController:selectedServise];
            }];
        };
    }
    [self.navigationController pushViewController:homeSearchViewController animated:NO];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = self.dynamicService.language == LanguageTypeArabic ? FavouriteArabicCellIdentifier : FavouriteEuroCellIdentifier;
    FavouriteTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *serviceTitle;
    if ([self.dataSource[indexPath.row] isKindOfClass:[TRAStaticService class]]) {
        serviceTitle = ((TRAStaticService *)self.dataSource[indexPath.row]).serviceDescription;
    } else if ([self.dataSource[indexPath.row] isKindOfClass:[TRADynamicService class]]) {
        serviceTitle = self.dynamicService.language == LanguageTypeArabic ? ((TRADynamicService *)self.dataSource[indexPath.row]).serviceNameAR : ((TRADynamicService *)self.dataSource[indexPath.row]).serviceNameEN;
    }
    
    UIFont *font = self.dynamicService.language == LanguageTypeArabic ? [UIFont droidKufiRegularFontForSize:12.f] : [UIFont latoRegularWithSize:12.f];
    NSDictionary *attributes = @{ NSFontAttributeName :font };

    CGSize textSize = [serviceTitle sizeWithAttributes:attributes];
    CGFloat widthOfViewWithImage = 85.f;
    CGFloat labelWidth = SCREEN_WIDTH - (widthOfViewWithImage + DefaultOffsetForElementConstraintInCell * 2);
    
    NSUInteger numbersOfLines = ceil(textSize.width / labelWidth);
    CGFloat height = numbersOfLines * textSize.height + SummOfVerticalOffsetsForCell;
    
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.segmentControl.selectedSegmentIndex) {
        return;
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if ([self.dataSource[indexPath.row] isKindOfClass:[TRAStaticService class]]) {
        TRAStaticService *serviceToGo = (TRAStaticService *)self.dataSource[indexPath.row];
        NSUInteger navigationIndex = [serviceToGo.serviceInternalID integerValue];
        if ([serviceToGo.serviceEnable boolValue]) {
            [self performNavigationToServiceWithIndex:navigationIndex];
        }
    } else if ([self.dataSource[indexPath.row] isKindOfClass:[TRADynamicService class]]) {
        TRADynamicService *traDynamicService = (TRADynamicService *)self.dataSource[indexPath.row];
        DynamicServiceModel *dynamicServiceModel = [[DynamicServiceModel alloc] initWithTRADynamicServise:traDynamicService];
//        [self performNavigationToDynamicService:dynamicServiceModel];
        [self presentDynamicService:dynamicServiceModel];
    }
}

#pragma mark - SegmentViewDelegate

- (IBAction)segmentSwitch:(id)sender
{
    NSInteger selectedSegment = self.segmentControl.selectedSegmentIndex;
    if (selectedSegment == 0) {
        [self animationHeiderAddServiceView];
    } else  if (selectedSegment == 1) {
        [self animationHeiderAddServiceView];
    }
}

#pragma mark - Navigations

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:ServiceInfoListSegueIdentifier]) {
        [self prepareServiceInfiViewControllerSegue:segue];
    }
}

- (void)prepareServiceInfiViewControllerSegue:(UIStoryboardSegue *)segue
{
    ServiceInfoViewController *serviceInfoController = segue.destinationViewController;
    serviceInfoController.hidesBottomBarWhenPushed = YES;
    if ([self.dataSource[self.selectedIndexPath.row] isKindOfClass:[TRAStaticService class]]) {
        serviceInfoController.selectedServiceID = [((TRAStaticService *)self.dataSource[self.selectedIndexPath.row]).serviceInternalID integerValue];
        serviceInfoController.titleScreen = dynamicLocalizedString(((TRAStaticService *)self.dataSource[self.selectedIndexPath.row]).serviceName);
    } else if ([self.dataSource[self.selectedIndexPath.row] isKindOfClass:[TRADynamicService class]]) {
        TRADynamicService *traDynamicService = (TRADynamicService *)self.dataSource[self.selectedIndexPath.row];
        serviceInfoController.dynamicServiceModel = [[DynamicServiceModel alloc] initWithTRADynamicServise:traDynamicService];
    }
    
    serviceInfoController.fakeBackground = [AppHelper snapshotForView:self.tabBarController.view];
}

- (void)performNavigationToServiceWithIndex:(NSUInteger)navigationIndex
{
    NSArray *serviceIdentifiers = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ServiceViewControllersIdentifiers" ofType:@"plist"]];
    
    UIViewController *selectedService = [self.storyboard instantiateViewControllerWithIdentifier:serviceIdentifiers[navigationIndex]];

    if (navigationIndex == 5) {
        ((SpamReportViewController *)selectedService).selectSpamReport = SpamReportTypeSMS;
    } else if (navigationIndex == 6) {
        ((SpamReportViewController *)selectedService).selectSpamReport = SpamReportTypeWeb;
    }

    if (selectedService) {
        if ([selectedService isKindOfClass:[BaseServiceViewController class]]) {
            [(BaseServiceViewController *)selectedService setServiceID:navigationIndex];
        }
        [self.navigationController pushViewController:selectedService animated:YES];
    }
}

- (void)presentDynamicService:(DynamicServiceModel *)dynamicServiceModel
{
    UIViewController *selectedDynamicService = [self.storyboard instantiateViewControllerWithIdentifier:DynamicServiceIDstoryboard];
    ((DynamicContainerServiceViewController *)selectedDynamicService).dynamicServiceModel = dynamicServiceModel;
    [self.navigationController pushViewController:selectedDynamicService animated:YES];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    [self.addServiceHiddenButton setTitle:dynamicLocalizedString(@"favourite.button.addFav.title") forState:UIControlStateNormal];
    self.navigationItem.title = dynamicLocalizedString(@"favourite.title");
    self.informationLabel.text = dynamicLocalizedString(@"favourite.notification");
    self.actionDescriptionLabel.text = dynamicLocalizedString(@"favourite.button.addFav.title");
    
    [self.segmentControl setTitle: dynamicLocalizedString(@"favorite.segmentedControl.favorites") forSegmentAtIndex:0];
    [self.segmentControl setTitle: dynamicLocalizedString(@"favorite.segmentedControl.rating") forSegmentAtIndex:1];
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
    
    UIColor *color = [self.dynamicService currentApplicationColor];
    [self.addFavouriteButton setTintColor:color];
    self.actionDescriptionLabel.textColor = color;
    
    [self.addServiceHiddenButton setTitleColor:color forState:UIControlStateNormal];
    self.addServiceHiddenImageView.tintColor = color;
    
    [AppHelper addHexagonBorderForLayer:self.addServiceHiddenImageView.layer color:color width:2.0f];
    self.containerSegmentControlView.backgroundColor = color;
}

- (void)setRTLArabicUI
{
    [super setRTLArabicUI];
    
    [self.addServiceHiddenButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 20.0, 0.0, -20.0)];
    [self performActionForUpdateUIWithTransform:TRANFORM_3D_SCALE];
}

- (void)setLTREuropeUI
{
    [super setLTREuropeUI];
    
    [self.addServiceHiddenButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, -20.0, 0.0, 20.0)];
    [self performActionForUpdateUIWithTransform:CATransform3DIdentity];
}

#pragma mark - Private

#pragma mark - ConfigureCell

- (void)configureCell:(FavouriteTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIColor *pairCellColor = [[self.dynamicService currentApplicationColor] colorWithAlphaComponent:0.1f];
    cell.backgroundColor = indexPath.row % 2 ? [UIColor clearColor] : pairCellColor;
    
    if ([self.dataSource[indexPath.row] isKindOfClass:[TRAStaticService class]]) {
        TRAStaticService *traService = (TRAStaticService *)self.dataSource[indexPath.row];
        cell.contentView.alpha = [traService.serviceEnable boolValue] ? 1 : 0.2;
        cell.favouriteServiceLogoImageView.image = [UIImage imageNamed:traService.serviceIconName];
        cell.favourieDescriptionLabel.text = [dynamicLocalizedString(traService.serviceName) stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    } else if ([self.dataSource[indexPath.row] isKindOfClass:[TRADynamicService class]]) {
        TRADynamicService *traDynamicService = (TRADynamicService *)self.dataSource[indexPath.row];
        UIImage *logoPlaceholder = [UIImage imageNamed:@"ic_domain_logo"];
        [AppHelper downloadToImageView:cell.favouriteServiceLogoImageView url:traDynamicService.serviceIcon placeholderImage:logoPlaceholder];
        NSString *nameService = self.dynamicService.language == LanguageTypeArabic ? traDynamicService.serviceNameAR : traDynamicService.serviceNameEN;
        cell.favourieDescriptionLabel.text = [nameService stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    }
    cell.typeCell = self.segmentControl.selectedSegmentIndex == 1 ? TypeCellRating : TypeCellFavorites;
    cell.delegate = self;
}

- (void)showRatingViewController:(id)selectedService
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FeedbackViewController *feedback = [storyboard instantiateViewControllerWithIdentifier:FeedbackViewControllerIdentifier];
    feedback.modalPresentationStyle = UIModalPresentationOverFullScreen;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    if ([selectedService isKindOfClass:[TRAStaticService class]]) {
        if ([((TRAStaticService *)selectedService).serviceEnable boolValue]) {
            feedback.selectedServiceID = [((TRAStaticService *)selectedService).serviceInternalID integerValue];
            feedback.titleScreen = dynamicLocalizedString(((TRAStaticService *)selectedService).serviceName);
        }
    } else if ([selectedService isKindOfClass:[TRADynamicService class]]) {
        feedback.serviceRatingName = ((TRADynamicService *)selectedService).serviceID;
        feedback.titleScreen = self.dynamicService.language == LanguageTypeArabic ? ((TRADynamicService *)selectedService).serviceNameAR : ((TRADynamicService *)selectedService).serviceNameEN;
    }
    
    [self presentViewController:feedback animated:NO completion:nil];
}

- (void)updateAndDisplayDataSource
{
    [self fetchFavouriteList];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)registerNibs
{
    [self.tableView registerNib:[UINib nibWithNibName:@"FavouriteEuroTableViewCell" bundle:nil] forCellReuseIdentifier:FavouriteEuroCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"FavouriteArabicTableViewCell" bundle:nil ] forCellReuseIdentifier:FavouriteArabicCellIdentifier];
}

#pragma mark - UICustomization

- (void)showPlaceHolderIfNeeded
{
    if (self.dataSource.count) {
        self.tableView.hidden = NO;
        self.headerAddServiceView.hidden = NO;
        self.headerAddServiceView.layer.opacity = 1.0f;
        
        self.placeHolderView.hidden = YES;
    } else {
        self.tableView.hidden = YES;
        
        self.headerAddServiceView.layer.opacity = 0.0f;
        [self.headerAddServiceView.layer addAnimation:[Animation fadeAnimFromValue:1.f to:0.0f delegate:self] forKey:@"keyAnimationHidePlaceHolder"];
        self.placeHolderView.hidden = NO;
        self.placeHolderView.layer.opacity = 1.0f;
        [self.placeHolderView.layer addAnimation:[Animation fadeAnimFromValue:0.f to:1.f delegate:nil] forKey:nil];
    }
}

#pragma mark - CoreData

- (void)fetchFavouriteList
{
    self.dataSourceRating = [[[CoreDataManager sharedManager] allDBSortedArray] mutableCopy];
    self.dataSourceFavorites = [[[CoreDataManager sharedManager] favoriteDBSortedArray] mutableCopy];
    
    self.dataSource = self.segmentControl.selectedSegmentIndex == 0 ? self.dataSourceFavorites : self.dataSourceRating;
}

#pragma mark - FavouriteTableViewCellDelegate

- (void)favouriteServiceInfoButtonDidPressedInCell:(FavouriteTableViewCell *)cell
{
    self.selectedIndexPath = [self.tableView indexPathForCell:cell];
    if ([self.dataSource[self.selectedIndexPath.row] isKindOfClass:[TRAStaticService class]]) {
        TRAStaticService *traService = (TRAStaticService *)self.dataSource[self.selectedIndexPath.row];
        if ([traService.serviceEnable boolValue]) {
            [self performSegueWithIdentifier:ServiceInfoListSegueIdentifier sender:self];
        }
    } else if ([self.dataSource[self.selectedIndexPath.row] isKindOfClass:[TRADynamicService class]]) {
        [self performSegueWithIdentifier:ServiceInfoListSegueIdentifier sender:self];
    }
}

- (void)favouriteServiceInfoRatingDidPressedInCell:(FavouriteTableViewCell *)cell
{
    self.selectedIndexPath = [self.tableView indexPathForCell:cell];
    [self showRatingViewController:self.dataSource[self.selectedIndexPath.row]];
}

#pragma mark - GestureRecognizer on FavouriteTableViewCell
//shayan
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        TRABaseService *serviceToRemoveFromFav = self.dataSource[indexPath.row];
        serviceToRemoveFromFav.serviceIsFavorite = @(![serviceToRemoveFromFav.serviceIsFavorite boolValue]);
        serviceToRemoveFromFav.serviceFavoriteOrder = nil;
        
        [self.dataSource removeObjectAtIndex:indexPath.row];
        [self showPlaceHolderIfNeeded];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
    [self updateFavoriteOrder];
    [[CoreDataManager sharedManager] saveContext];
    
 
    self.removeProcessIsActive = NO;
    [self animateDeleteZoneDisapearing];
    [self.tableView reloadData];
        
    
}

- (void)favouriteServiceDeleteButtonDidReceiveGestureRecognizerInCell:(FavouriteTableViewCell *)cell gesture:(UILongPressGestureRecognizer *)gesture
{
    CGPoint location = [gesture locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    FavouriteTableViewCell *selectedCell = cell;

    static UIView *snapshotView;
    static NSIndexPath *sourceIndexPath;
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan: {
            self.removeProcessIsActive = YES;
            sourceIndexPath = indexPath;
            [selectedCell markRemoveButtonSelected:YES];
            snapshotView = [selectedCell snapshot];
            
            __block CGPoint center = selectedCell.center;
            snapshotView.center = center;
            snapshotView.alpha = 0.f;
            [self.tableView addSubview:snapshotView];
            
            [self drawDeleteAreaOnTable:self.tableView];
            
            snapshotView.backgroundColor = [UIColor whiteColor];
            snapshotView.layer.masksToBounds = NO;
            snapshotView.layer.shadowOffset = CGSizeMake(1, 2);
            snapshotView.layer.shadowRadius = 2.5;
            snapshotView.layer.shadowOpacity = 0.3;
            
            [UIView animateWithDuration:0.25f animations:^{
                center.y = location.y;
                snapshotView.center = center;
                snapshotView.transform = CGAffineTransformMakeScale(0.95, 0.95);
                snapshotView.alpha = 0.98f;
                selectedCell.alpha = 0.;
            }];
            break;
        }
        case UIGestureRecognizerStateChanged: {
            if (self.removeProcessIsActive) {
                selectedCell.hidden = YES;
                CGPoint center = snapshotView.center;
                center.y = location.y;
                snapshotView.center = center;
                
                if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                    if (![self isCellInRemoveAreaWithCenter:CGPointMake(0, CGRectGetMinY(snapshotView.frame))]) {
                        [self.dataSource exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                        [self.tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                        sourceIndexPath = indexPath;
                    }
                }
                if ([self isCellInRemoveAreaWithCenter:CGPointMake(0, center.y - self.tableView.contentOffset.y)]) {
                    [self selectDeleteZone:YES];
                } else {
                    [self selectDeleteZone:NO];
                }
            }
            break;
        }
        default: {
            if (self.removeProcessIsActive) {
                FavouriteTableViewCell *cell = (FavouriteTableViewCell *)[self.tableView cellForRowAtIndexPath:sourceIndexPath];
                cell.hidden = NO;
                cell.alpha = 0.0f;
                [cell markRemoveButtonSelected:NO];
                
                if ([self isCellInRemoveAreaWithCenter:snapshotView.center]) {
                    TRABaseService *serviceToRemoveFromFav = self.dataSource[sourceIndexPath.row];
                    serviceToRemoveFromFav.serviceIsFavorite = @(![serviceToRemoveFromFav.serviceIsFavorite boolValue]);
                    serviceToRemoveFromFav.serviceFavoriteOrder = nil;
                    
                    [self.dataSource removeObjectAtIndex:sourceIndexPath.row];
                    [self showPlaceHolderIfNeeded];
                    [self.tableView deleteRowsAtIndexPaths:@[sourceIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                }
                [self updateFavoriteOrder];
                [[CoreDataManager sharedManager] saveContext];

                sourceIndexPath = nil;
                [snapshotView removeFromSuperview];
                snapshotView = nil;
                self.removeProcessIsActive = NO;
                [self animateDeleteZoneDisapearing];
                [self.tableView reloadData];
            }
            break;
        }
    }
}

- (void)updateFavoriteOrder
{
    for (NSInteger i = 0; i < self.dataSource.count; i++) {
        TRABaseService *traBaseService = self.dataSource[i];
        traBaseService.serviceFavoriteOrder = @(i);
    }
}

- (BOOL)isCellInRemoveAreaWithCenter:(CGPoint)center
{
    BOOL isInRemoveArea = NO;
    
    CGFloat heightOFDeleteRect = SCREEN_HEIGHT * 0.185f;
    CGFloat startY = self.tableView.bounds.size.height - heightOFDeleteRect;
    if (center.y > startY) {
        isInRemoveArea = YES;
    }
    return isInRemoveArea;
}

#pragma mark - Animation

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{    
    if (anim == [self.arcDeleteZoneLayer animationForKey:@"disapearAnimation"]) {
        [self.arcDeleteZoneLayer removeAllAnimations];
        [self removeDeleteZone];
    } else if (anim == [self.headerAddServiceView.layer animationForKey:@"keyAnimationHidePlaceHolder"] ) {
        self.headerAddServiceView.hidden = YES;
    } else if (anim == [self.placeHolderView.layer animationForKey:@"hidePlaceHolder"]) {
        self.placeHolderView.hidden = YES;
    }
}

- (void)animationHeiderAddServiceView
{
    if (self.dataSourceFavorites.count == 0 && self.segmentControl.selectedSegmentIndex == 0) {
        self.headerAddServiceView.hidden = YES;
        self.placeHolderView.alpha = 0.;
    }
    self.dataSource = self.segmentControl.selectedSegmentIndex == 0 ? self.dataSourceFavorites : self.dataSourceRating;

    __weak typeof(self) weakSelf = self;
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.heightHeiderAddServiceConstraint.constant = weakSelf.segmentControl.selectedSegmentIndex == 0 ? HeightHeiderAddService : 0;
        self.placeHolderView.alpha = 1.;

        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)performActionForUpdateUIWithTransform:(CATransform3D)transform
{
    self.addServiceHiddenButton.layer.transform = transform;
    self.headerAddServiceView.layer.transform = transform;
    
    [self fetchFavouriteList];
    [self.tableView reloadData];
}

- (void)prepareNavigationBar
{
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [AppHelper titleFontForNavigationBar:self.navigationController.navigationBar];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style: UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

@end
