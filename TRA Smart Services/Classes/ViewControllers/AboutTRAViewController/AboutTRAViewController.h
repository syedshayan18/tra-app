//
//  AboutTRAViewController.h
//  TRA Smart Services
//
//  Created by Admin on 04.12.15.
//

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AboutTRAViewController : BaseDynamicUIViewController <UIPickerViewDataSource, UIPickerViewDelegate, MFMailComposeViewControllerDelegate>

@end
