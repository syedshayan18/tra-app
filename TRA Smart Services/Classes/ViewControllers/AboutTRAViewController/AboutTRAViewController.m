//
//  AboutTRAViewController.m
//  TRA Smart Services
//
//  Created by Admin on 04.12.15.
//

#import "AboutTRAViewController.h"
#import "Animation.h"
#import "ContactItemModel.h"
#import <MapKit/MapKit.h>

static CGFloat const HeightForToolbars = 44.;

@interface AboutTRAViewController ()

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UIView *segmentContainerView;
@property (weak, nonatomic) IBOutlet UIView *containerAboutView;
@property (weak, nonatomic) IBOutlet UIView *containerContactUsView;

@property (weak, nonatomic) IBOutlet UITextView *aboutContentText;
@property (weak, nonatomic) IBOutlet UILabel *aboutDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *aboutLogoImage;

@property (weak, nonatomic) IBOutlet BottomBorderTextField *contactUsSelectedTextField;
@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) NSArray *dataSource;
@property (weak, nonatomic) IBOutlet MKMapView *contactUsMapView;
@property (weak, nonatomic) IBOutlet UILabel *contactUsAdressTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactUsAdressDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *contactUsAdressContainerView;
@property (weak, nonatomic) IBOutlet UIView *contactUsContactInfoContainerView;
@property (weak, nonatomic) IBOutlet UILabel *contactUsContactInfoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactUsPhoneTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactUsEmailTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *contactUsPhoneDescriptionButton;
@property (weak, nonatomic) IBOutlet UIButton *contactUsEmailDescriptionButton;
@property (strong, nonatomic) ContactItemModel *currentContactItemModel;

@end

@implementation AboutTRAViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - SegmentViewDelegate

- (IBAction)segmentSwitch:(id)sender
{
    NSInteger selectedSegment = self.segmentedControl.selectedSegmentIndex;
    if (selectedSegment == 0) {
        [self animationShowView:self.containerAboutView];
        [self animationHideView:self.containerContactUsView forKey:@"hideContactUs"];
    } else  if (selectedSegment == 1) {
        if ([NetworkManager sharedManager].networkStatus <= 0) {
            [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.NoInternetConnection")];
            self.segmentedControl.selectedSegmentIndex = 0;
        } else {
            if (!self.dataSource) {
                [self prepareDataSource];
            }
            [self animationShowView:self.containerContactUsView];
            [self animationHideView:self.containerAboutView forKey:@"hideContactUs"];
        }
    }
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.dataSource.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    ContactItemModel *selectedContactItemModel = self.dataSource[row];
    return selectedContactItemModel.contactItemTitle;
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    ContactItemModel *selectedContactItemModel = self.dataSource[row];

    self.contactUsSelectedTextField.text = selectedContactItemModel.contactItemTitle;
    [self prepareUIContactUs:selectedContactItemModel];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.contactUsSelectedTextField) {
        return NO;
    }
    return YES;
}

#pragma mark - IBAction

- (IBAction)contactUsPhoneDescriptionButtonTapped:(id)sender
{
    if (self.currentContactItemModel.contactItemPhone.length) {
        NSString *phoneNumber = [self.currentContactItemModel.contactItemPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
        phoneNumber = [NSString stringWithFormat:@"tel:%@", phoneNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
}

- (IBAction)contactUsEmailDescriptionButtonTapped:(id)sender
{
    if([MFMailComposeViewController canSendMail]) {
        [UINavigationBar appearance].barTintColor = self.dynamicService.currentApplicationColor;
        [UINavigationBar appearance].tintColor = [UIColor whiteColor];
        [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName : self.dynamicService.language == LanguageTypeArabic ? [UIFont droidKufiRegularFontForSize:17] : [UIFont latoRegularWithSize:17]};
        
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        [mailCont setSubject:self.currentContactItemModel.contactItemEmail];
        [mailCont setMessageBody:@"" isHTML:NO];
        
        mailCont.navigationBar.tintColor = [UIColor whiteColor];
        [self presentViewController:mailCont animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }];
    }
}

- (void)tappedDoneButtonPickerView
{
    [self.containerContactUsView endEditing:YES];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Animations

 - (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.containerContactUsView.layer animationForKey:@"hideContactUs"]) {
        [self.containerContactUsView.layer removeAllAnimations];
        self.containerContactUsView.hidden = YES;
        
    } else if (anim == [self.containerAboutView.layer animationForKey:@"hideAbout"]) {
        [self.containerAboutView.layer removeAllAnimations];
        self.containerAboutView.hidden = YES;
    }
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"aboutTRAViewController.title");
    [self.segmentedControl setTitle: dynamicLocalizedString(@"aboutTRAViewController.segmentedControl.About") forSegmentAtIndex:0];
    [self.segmentedControl setTitle: dynamicLocalizedString(@"aboutTRAViewController.segmentedControl.ContactUs") forSegmentAtIndex:1];
    
    [self prepareAboutTRAContainer];
    
    self.contactUsAdressTitleLabel.text = dynamicLocalizedString(@"aboutTRAViewController.contactUsAdressTitleLabel.text");
    self.contactUsContactInfoTitleLabel.text = dynamicLocalizedString(@"aboutTRAViewController.contactUsContactInfoTitleLabel.text");
    self.contactUsPhoneTitleLabel.text = dynamicLocalizedString(@"aboutTRAViewController.contactUsPhoneTitleLabel.text");
    self.contactUsEmailTitleLabel.text = dynamicLocalizedString(@"aboutTRAViewController.contactUsEmailTitleLabel.text");
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
    
    self.segmentContainerView.backgroundColor = self.dynamicService.currentApplicationColor;
    [AppHelper setStyleForTextField:self.contactUsSelectedTextField];
    
    self.contactUsAdressDescriptionLabel.textColor = self.dynamicService.currentApplicationColor;
    self.contactUsPhoneDescriptionButton.tintColor = self.dynamicService.currentApplicationColor;
    self.contactUsEmailDescriptionButton.tintColor = self.dynamicService.currentApplicationColor;
}

- (void)setLTREuropeUI
{
    [self changeElementsAligment:NSTextAlignmentLeft];
    self.contactUsPhoneDescriptionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.contactUsEmailDescriptionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [self transformUILayer:CATransform3DIdentity];
}

- (void)setRTLArabicUI
{
    [self changeElementsAligment:NSTextAlignmentRight];
    self.contactUsPhoneDescriptionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.contactUsEmailDescriptionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    [self transformUILayer:TRANFORM_3D_SCALE];
}

- (void)changeElementsAligment:(NSTextAlignment)textAlignment
{
    self.aboutTitleLabel.textAlignment = textAlignment;
    
    self.contactUsSelectedTextField.textAlignment = textAlignment;
    self.contactUsAdressDescriptionLabel.textAlignment = textAlignment;
    self.contactUsAdressTitleLabel.textAlignment = textAlignment;
    self.contactUsContactInfoTitleLabel.textAlignment = textAlignment;
    self.contactUsPhoneTitleLabel.textAlignment = textAlignment;
    self.contactUsEmailTitleLabel.textAlignment = textAlignment;
}

- (void)transformUILayer:(CATransform3D)animCATransform3D
{
    self.contactUsAdressContainerView.layer.transform = animCATransform3D;
    self.contactUsContactInfoContainerView.layer.transform = animCATransform3D;
    self.contactUsAdressDescriptionLabel.layer.transform = animCATransform3D;
    self.contactUsAdressTitleLabel.layer.transform = animCATransform3D;
    self.contactUsContactInfoTitleLabel.layer.transform = animCATransform3D;
    self.contactUsPhoneTitleLabel.layer.transform = animCATransform3D;
    self.contactUsPhoneDescriptionButton.layer.transform = animCATransform3D;
    self.contactUsEmailTitleLabel.layer.transform = animCATransform3D;
    self.contactUsEmailDescriptionButton.layer.transform = animCATransform3D;
}

#pragma mark - Private

- (void)animationShowView:(UIView *)view
{
    [view.layer addAnimation:[Animation fadeAnimFromValue:0.f to:1.0f delegate:nil] forKey:nil];
    view.hidden = NO;
    view.layer.opacity = 1.f;
}

- (void)animationHideView:(UIView *)view forKey:(NSString *)key
{
    [view.layer addAnimation:[Animation fadeAnimFromValue:1.f to:0.0f delegate:nil] forKey:key];
    view.layer.opacity = 0.f;
}

- (void)prepareAboutTRAContainer
{
    NSString *fileName = self.dynamicService.language == LanguageTypeArabic ? @"AboutAr" : @"AboutEn";
    NSURL *url = [[NSBundle mainBundle] URLForResource:fileName withExtension:@"rtf"];
    NSAttributedString *dataString = [[NSAttributedString alloc] initWithFileURL:url options:@{ NSPlainTextDocumentType : NSRTFTextDocumentType } documentAttributes:kNilOptions error:nil];
    self.aboutContentText.attributedText = dataString;
    self.aboutContentText.textAlignment = NSTextAlignmentJustified;
    
    self.aboutLogoImage.image =  [UIImage imageNamed:@"demo"];
    self.aboutTitleLabel.text = dynamicLocalizedString(@"aboutTRAViewController.aboutTitleLabel.text");
}

- (void)prepareUIContactUs:(ContactItemModel *)contactItemModel
{
    self.currentContactItemModel = contactItemModel;
    self.contactUsSelectedTextField.text = contactItemModel.contactItemTitle;
    self.contactUsAdressDescriptionLabel.text = contactItemModel.contactItemPoBox;
    [self.contactUsPhoneDescriptionButton  setTitle:contactItemModel.contactItemPhone forState:UIControlStateNormal] ;
    [self.contactUsEmailDescriptionButton setTitle:contactItemModel.contactItemEmail forState:UIControlStateNormal] ;
    
    [self prapareMapCoordinate:contactItemModel];
}
     
- (void)prapareMapCoordinate:(ContactItemModel *)contactItemModel
{
    CLLocationCoordinate2D annotationCoord;
    
    annotationCoord.latitude = [contactItemModel.contactItemLocationLatitude floatValue];
    annotationCoord.longitude = [contactItemModel.contactItemLocationLongitude floatValue];
    
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = annotationCoord;
    annotationPoint.title = contactItemModel.contactItemTitle;
    annotationPoint.subtitle = @"";
    [self.contactUsMapView addAnnotation:annotationPoint];
    
    self.contactUsMapView.centerCoordinate = annotationCoord;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (annotationCoord, 1000, 1000);
    [self.contactUsMapView setRegion:region animated:NO];
}

- (void)configurePickerController
{
    self.pickerView = [[UIPickerView alloc] init];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    self.contactUsSelectedTextField.inputView = self.pickerView;
    [self configureKeyboardToolbar:self.contactUsSelectedTextField];
}

- (void)configureKeyboardToolbar:(UITextField *)textField
{
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, HeightForToolbars)];
    toolBar.backgroundColor = [UIColor clearColor];
    toolBar.tintColor = [[DynamicUIService service] currentApplicationColor];
    
    UIBarButtonItem *barItemCustomDone = [[UIBarButtonItem alloc] initWithTitle:dynamicLocalizedString(@"uiElement.keyboardButtom.title") style:UIBarButtonItemStyleDone target:self action:@selector(tappedDoneButtonPickerView)];

    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
 
    [toolBar setItems:@[flexibleSpace, barItemCustomDone]];
    [textField setInputAccessoryView:toolBar];
}

- (void)prepareDataSource
{
    TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:dynamicLocalizedString(@"aboutTRAViewController.title") closeButton:YES];
    __weak typeof(self) weakSelf = self;
    loader.TRALoaderWillClose = ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!weakSelf.dataSource) {
                weakSelf.segmentedControl.selectedSegmentIndex = 0;
                weakSelf.containerAboutView.hidden = NO;
                weakSelf.containerAboutView.alpha = 1.;
                weakSelf.containerContactUsView.hidden = YES;
            }
        });
    };
    [[NetworkManager sharedManager] traSSNoCRMServiceGetContacts:^(id response, NSError *error) {
        if (error) {
            NSString *errorString = [response isKindOfClass:[NSString class]] ? response : error.localizedDescription;
            //shayan loader issue solved
            loader.ratingView.hidden = true;
            [loader setCompletedStatus:TRACompleteStatusFailure withDescription: errorString];
        } else {
            weakSelf.dataSource = response;
            dispatch_async(dispatch_get_main_queue(), ^{
                [loader dismissTRALoader:YES];
                [weakSelf prepareContactUsContainer];
            });
        }
    }];
}

- (void)prepareContactUsContainer
{
    if (self.dataSource.count) {
        if (self.dataSource.count > 1) {
            [self configurePickerController];
            [self prepareContactUsSelectedTextField];
        } else {
            self.contactUsSelectedTextField.userInteractionEnabled = NO;
        }
        [self prepareUIContactUs:(ContactItemModel *)self.dataSource[0]];
    }
}

- (void)prepareContactUsSelectedTextField
{
    UIImage *rightImage = [UIImage imageNamed:@"selectTableDn"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:rightImage];
    [imageView setImage:rightImage];
    imageView.tintColor = [self.dynamicService currentApplicationColor];
    self.contactUsSelectedTextField.rightView = nil;
    self.contactUsSelectedTextField.leftView = nil;
    if (self.dynamicService.language == LanguageTypeArabic) {
        self.contactUsSelectedTextField.leftViewMode = UITextFieldViewModeAlways;
        self.contactUsSelectedTextField.leftView = imageView;
    } else {
        self.contactUsSelectedTextField.rightViewMode = UITextFieldViewModeAlways;
        self.contactUsSelectedTextField.rightView = imageView;
    }
}

@end
