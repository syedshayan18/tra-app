//
//  EditUserProfileViewController.m
//  TRA Smart Services
//
//  Created by Admin on 10.09.15.
//

#import "EditUserProfileViewController.h"

#import "ServicesSelectTableViewCell.h"
#import "KeychainStorage.h"
#import "TextFieldNavigator.h"
#import "BlackImageView.h"

static NSString *const IconNameCheckActive = @"ic_check_act";
static NSString *const IconNameCheckDisactive = @"ic_check_disact";

static CGFloat const HeightContainerSelectQueryViewConstraintDisable = 42.;
static CGFloat const HeightContainerSelectQueryViewConstraintEnabled = 109.;
static CGFloat const HeightContainerSelectQueryViewConstraintEditQuery = 166.;
static CGFloat const MarginConstraintBottomSelectTableView = 73.;

static CGFloat const HeightSelectTableViewCell = 35.f;
static CGFloat const HeightTextFieldAndSeparator = 42.f;

typedef NS_ENUM(NSUInteger, ContainerSelectQeryEditType) {
    ContainerSelectQueryViewDisable,
    ContainerSelectQueryViewEnabled,
    ContainerSelectQueryViewEditQuery
};

@interface EditUserProfileViewController ()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet BlackImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *changePhotoButton;

@property (weak, nonatomic) IBOutlet UILabel *firstNameENLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameENLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstNameARLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameARLabel;

@property (weak, nonatomic) IBOutlet BottomBorderTextField *firstNameENTextfield;
@property (weak, nonatomic) IBOutlet BottomBorderTextField *lastNameENTextField;
@property (weak, nonatomic) IBOutlet BottomBorderTextField *firstNameARTextfield;
@property (weak, nonatomic) IBOutlet BottomBorderTextField *lastNameARTextField;

@property (weak, nonatomic) IBOutlet UserProfileActionView *userActionView;

@property (weak, nonatomic) IBOutlet UIButton *enableSelectQueryButton;
@property (weak, nonatomic) IBOutlet UILabel *enableSelectQueryLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet ContextMenuTextField *replySecretQueryTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTableViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContainerSelectQueryViewConstraint;
@property (weak, nonatomic) IBOutlet UILabel *securityQuestionLabel;
@property (weak, nonatomic) IBOutlet UILabel *securityAnswerLabel;
@property (weak, nonatomic) IBOutlet UIView *containerCheckBoxQueryView;
@property (assign, nonatomic) ContainerSelectQeryEditType selectQuery;
@property (strong, nonatomic) NSArray *dataSourceQuery;
@property (assign, nonatomic) NSInteger selectedIndexQuery;

@property (strong, nonatomic) NSArray *dataSource;
@property (assign, nonatomic) CGPoint textFieldTouchPoint;
@property (strong, nonatomic) NSString *selectedEmirate;
@property (assign, nonatomic) CGFloat offSetTextFildY;

@property (strong, nonatomic) ServicesSelectTableViewCell *headerCell;
@property (strong, nonatomic) UserModel *user;

@end

@implementation EditUserProfileViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareUI];
    [self fillData];
    [self prepareNotification];
}

- (void)dealloc
{
    [self removeNotifications];
}

#pragma mark - IBActions

- (IBAction)changePhotoButtonTapped:(id)sender
{
//    [self selectImagePickerController:UIImagePickerControllerSourceTypePhotoLibrary];

    [self configureActionSheet];
}

- (IBAction)enableSelectQueryButtonTapped:(id)sender
{
    if (self.selectQuery == ContainerSelectQueryViewDisable) {
        self.selectQuery = ContainerSelectQueryViewEnabled;
        [self setEnableSecretQuery:YES];
        [self.view endEditing:YES];
    } else {
        self.selectQuery = ContainerSelectQueryViewDisable;
        [self setEnableSecretQuery:NO];
        self.selectedIndexQuery = 0;
        [self.tableView reloadData];
        self.replySecretQueryTextField.text = @"";
    }
    [self animationSelectTableView:NO];
}

#pragma mark - Custom Accessors

- (void)setSelectImage:(UIImage *)selectImage
{
    [super setSelectImage:selectImage];
    
    if (!selectImage) {
        if (self.user.avatarImageBase64.length) {
            NSData *data = [[NSData alloc] initWithBase64EncodedString:self.user.avatarImageBase64 options:kNilOptions];
            UIImage *image = [UIImage imageWithData:data];
            self.logoImageView.image = image;
            self.selectImage = image;
        } else {
            self.logoImageView.image = [UIImage imageNamed:DefaultLogoImageName];
        }
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifierCell = self.dynamicService.language == LanguageTypeArabic ? selectProviderCellArabicUIIdentifier : selectProviderCellEuropeUIIdentifier;
    ServicesSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierCell forIndexPath:indexPath];
    if (indexPath.row) {
        cell.selectProviderLabel.text =  ((Question *)self.dataSourceQuery[indexPath.row]).secretQuestionType;
        cell.selectProviderLabel.textColor = [UIColor blackColor];
    } else {
        cell.selectProviderImage.tintColor = [self.dynamicService currentApplicationColor];
        cell.selectProviderImage.image = self.heightTableViewConstraint.constant == HeightSelectTableViewCell ?  [UIImage imageNamed:@"selectTableDn"] :  [UIImage imageNamed:@"selectTableUp"];
        
        cell.selectProviderLabel.textColor = self.dynamicService.currentApplicationColor;
        if (self.selectedIndexQuery > 0) {
            cell.selectProviderLabel.text =  ((Question *)self.dataSourceQuery[self.selectedIndexQuery]).secretQuestionType;
        } else {
            cell.selectProviderLabel.text =  ((Question *)self.dataSourceQuery[indexPath.row]).secretQuestionType;
        }
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceQuery.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HeightSelectTableViewCell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if (self.heightTableViewConstraint.constant == HeightSelectTableViewCell) {
        self.selectedIndexQuery = 0;
        self.replySecretQueryTextField.text = @"";
        self.selectQuery = ContainerSelectQueryViewEnabled;
        [self animationSelectTableView:YES];
    } else {
        if (indexPath.row) {
            self.selectedIndexQuery = indexPath.row;
            self.selectQuery = ContainerSelectQueryViewEditQuery;
            [self.tableView reloadData];
        }
        [self animationSelectTableView:NO];
    }
}

#pragma mark - ImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [super imagePickerController:picker didFinishPickingMediaWithInfo:info];
    self.logoImageView.image = self.selectImage;
}

#pragma mark - UserProfileActionViewDelegate

- (void)buttonCancelDidTapped
{
    [self.view endEditing:YES];

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)buttonResetDidTapped
{
    [self.view endEditing:YES];

    [self fillData];
}

- (void)buttonSaveDidTapped
{
    [self.view endEditing:YES];
    
    if (self.firstNameENTextfield.text.length && self.firstNameENTextfield.text.length > 32) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooLong"), dynamicLocalizedString(@"message.EmptyInputParameter.FirstNameEN")]];
    } else if (self.firstNameENTextfield.text.length && self.firstNameENTextfield.text.length < 3) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooShort"), dynamicLocalizedString(@"message.EmptyInputParameter.FirstNameEN")]];
    } else if (self.lastNameENTextField.text.length && self.lastNameENTextField.text.length > 32) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooLong"), dynamicLocalizedString(@"message.EmptyInputParameter.LastNameEN")]];
    } else if (self.lastNameARTextField.text.length && self.lastNameENTextField.text.length < 3) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooShort"), dynamicLocalizedString(@"message.EmptyInputParameter.LastNameEN")]];
    }  else if (self.firstNameARTextfield.text.length && self.firstNameARTextfield.text.length > 32) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooLong"), dynamicLocalizedString(@"message.EmptyInputParameter.FirstNameAR")]];
    } else if (self.firstNameARTextfield.text.length && self.firstNameARTextfield.text.length < 3) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooShort"), dynamicLocalizedString(@"message.EmptyInputParameter.FirstNameAR")]];
    } else if (self.lastNameARTextField.text.length && self.lastNameARTextField.text.length > 32) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooLong"), dynamicLocalizedString(@"message.EmptyInputParameter.LastNameAR")]];
    } else if (self.lastNameARTextField.text.length && self.lastNameARTextField.text.length < 3) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@\n%@", dynamicLocalizedString(@"message.InvalidFormatTooShort"), dynamicLocalizedString(@"message.EmptyInputParameter.LastNameAR")]];
        
    } else if (self.lastNameARTextField.text.length && ![self.lastNameARTextField.text isValidNameArabic]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatLastNameInArabicLetters")];
    } else if (self.firstNameARTextfield.text.length && ![self.firstNameARTextfield.text isValidNameArabic]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatFirstNameInArabicLetters")];

    } else if (self.lastNameENTextField.text.length && ![self.lastNameENTextField.text isValidName]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatLastNameInEnglishLetters")];
    } else if (self.firstNameENTextfield.text.length && ![self.firstNameENTextfield.text isValidName]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatFirstNameInEnglishLetters")];
        
    } else {
        NSString *encodedString = @"";
        if (self.selectImage) {
            NSData *imageData = UIImageJPEGRepresentation(self.selectImage, 1.0);
            encodedString = [imageData base64EncodedStringWithOptions:kNilOptions];
        }
    
        self.user.firstNameEN = self.firstNameENTextfield.text;
        self.user.lastNameEN = self.lastNameENTextField.text;
        self.user.firstNameAR = self.firstNameARTextfield.text;
        self.user.lastNameAR = self.lastNameARTextField.text;
        self.user.avatarImageBase64 = encodedString;
        
        if (self.replySecretQueryTextField.text.length) {
            self.user.enhancedSecurity = YES;
            self.user.secretQuestionAnswer = self.replySecretQueryTextField.text;
            self.user.secretQuestionType = [NSString stringWithFormat:@"%i", (int)((Question *)self.dataSourceQuery[self.selectedIndexQuery]).secretQuestionValue];
        }
        
        __weak typeof(self) weakSelf = self;
        TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];
        loader.TRALoaderDidClose = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
        [[NetworkManager sharedManager] traSSUpdateUserProfile:self.user requestResult:^(id response, NSError *error) {
            if (error || ![response isKindOfClass:[NSDictionary class]]) {
                //shayan loader issue solved
                loader.ratingView.hidden = true;
                
                [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[response isKindOfClass:[NSString class]] ? response : error.localizedDescription];
            } else {
                UserModel *updateUser = [[UserModel alloc] initWithDictionary:response];
                updateUser.avatarImageBase64 = self.user.avatarImageBase64;
                [[KeychainStorage new] saveCustomObject:updateUser key:userModelKey];
              loader.ratingView.hidden = false;
                [loader setCompletedStatus:TRACompleteStatusSuccess withDescription:nil];
                
                Question *secretQuestion = weakSelf.dataSourceQuery[weakSelf.selectedIndexQuery];
                secretQuestion.secretQuestionAnswer = weakSelf.replySecretQueryTextField.text;
                
                [[KeychainStorage new] saveCustomObject:secretQuestion key:SecretQuestion];
            }
        }];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [TextFieldNavigator findNextTextFieldFromCurrent:textField];
    
    if (textField.returnKeyType == UIReturnKeyNext) {
        CGRect frame = [self.containerView convertRect:textField.bounds fromView:textField];
        
        CGFloat offsetTextField = frame.origin.y;
        CGFloat lineEventScroll = self.scrollView.frame.size.height + self.scrollView.contentOffset.y - self.keyboardHeight - 2 * HeightTextFieldAndSeparator + 20.f;
        
        if (offsetTextField  > lineEventScroll) {
            [self.scrollView setContentOffset:CGPointMake(0, self.scrollView.contentOffset.y + HeightTextFieldAndSeparator) animated:YES];
        }
        CGFloat offsetForScrollViewY = self.scrollView.frame.size.height  - self.scrollView.contentOffset.y - self.keyboardHeight;
        if (lineEventScroll < self.offSetTextFildY - HeightTextFieldAndSeparator) {
            [self.scrollView setContentOffset:CGPointMake(0, self.offSetTextFildY - offsetForScrollViewY - self.scrollView.contentOffset.y + HeightTextFieldAndSeparator + 10.f) animated:YES];
        }
    }
    if (textField.returnKeyType == UIReturnKeyDone) {
        CGFloat offsetForScrollViewY = self.scrollView.contentSize.height - self.scrollView.frame.size.height;
        [self.scrollView setContentOffset:CGPointMake(0, offsetForScrollViewY ) animated:YES];
        return YES;
    }
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    CGFloat selectegStateTextField = 0;
    if (textField.tag == 7 && self.selectQuery == HeightContainerSelectQueryViewConstraintDisable) {
        selectegStateTextField = 3 * HeightTextFieldAndSeparator;
    }
    if (textField.tag == 111) {
        selectegStateTextField = HeightTextFieldAndSeparator;
    }
    
    CGRect frame = [self.containerView convertRect:textField.bounds fromView:textField];
    
    self.offSetTextFildY = frame.origin.y + textField.frame.size.height + selectegStateTextField;
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self vizibleTextFieldChangeKeyboard];
    return YES;
}

#pragma mark - Keyboard

- (void)prepareNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)removeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillAppear:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyboardHeight = keyboardRect.size.height;
    [self vizibleTextFieldChangeKeyboard];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"userProfile.title");
    [self.userActionView localizeUI];
    
    self.firstNameENLabel.text = dynamicLocalizedString(@"editUserProfileViewController.firstNameENLabel");
    self.lastNameENLabel.text = dynamicLocalizedString(@"editUserProfileViewController.lastNameENLabel");
    self.firstNameARLabel.text = dynamicLocalizedString(@"editUserProfileViewController.firstNameARLabel");
    self.lastNameARLabel.text = dynamicLocalizedString(@"editUserProfileViewController.lastNameARLabel");
    
    [self.changePhotoButton setTitle:dynamicLocalizedString(@"editUserProfileViewController.changePhotoButtonTitle") forState:UIControlStateNormal];
    self.securityAnswerLabel.text = dynamicLocalizedString(@"editUserProfileViewController.securityAnswerLabel.text");
    self.securityQuestionLabel.text = dynamicLocalizedString(@"editUserProfileViewController.securityQuestionLabel.text");
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
    [self.userActionView updateColorUI];
    [self setEnableSecretQuery:self.selectQuery == ContainerSelectQueryViewDisable ? NO : YES];
    if (self.selectImage) {
        self.logoImageView.image = self.selectImage;
    }
    
    UIColor *color = self.dynamicService.currentApplicationColor;
    self.firstNameENTextfield.textColor = color;
    self.lastNameENTextField.textColor = color;
    self.firstNameARTextfield.textColor = color;
    self.lastNameARTextField.textColor = color;
    
    self.replySecretQueryTextField.textColor = color;
    self.enableSelectQueryLabel.textColor = color;
    [self.tableView reloadData];
    self.enableSelectQueryButton.tintColor = color;
    
    [self updateColorsSeparatorTextField:self.view];
}

- (void)setRTLArabicUI
{
    [self updateUIaligment:NSTextAlignmentRight];
    
    [self.userActionView setRTLStyle];
    [self transformUILayer:TRANFORM_3D_SCALE];
}

- (void)setLTREuropeUI
{
    [self updateUIaligment:NSTextAlignmentLeft];
    
    [self.userActionView setLTRStyle];
    [self transformUILayer:CATransform3DIdentity];
}

#pragma mark - Private

- (void)updateColorsSeparatorTextField:(UIView *)view
{
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[BottomBorderTextField class]]) {
            [AppHelper setStyleForTextField:(BottomBorderTextField *)subView];
            ((BottomBorderTextField *)subView).requredIndicatorColor = self.dynamicService.colorScheme == ApplicationColorBlackAndWhite ? [UIColor blackColor] : [UIColor redColor];
        }
        [self updateColorsSeparatorTextField:subView];
    }
}

- (void)transformUILayer:(CATransform3D)animCATransform3D
{
    self.containerCheckBoxQueryView.layer.transform = animCATransform3D;
    self.enableSelectQueryLabel.layer.transform = animCATransform3D;
    self.enableSelectQueryButton.layer.transform = animCATransform3D;
}

- (void)updateUIaligment:(NSTextAlignment)aligment
{
    self.firstNameENLabel.textAlignment = aligment;
    self.lastNameENLabel.textAlignment = aligment;
    self.firstNameARLabel.textAlignment = aligment;
    self.lastNameARLabel.textAlignment = aligment;

    self.firstNameENTextfield.textAlignment = aligment;
    self.lastNameENTextField.textAlignment = aligment;
    self.firstNameARTextfield.textAlignment = aligment;
    self.lastNameARTextField.textAlignment = aligment;

    self.securityAnswerLabel.textAlignment = aligment;
    self.securityQuestionLabel.textAlignment = aligment;
    self.replySecretQueryTextField.textAlignment = aligment;
    self.enableSelectQueryLabel.textAlignment = aligment;
}

- (void)prepareUI
{
    self.userActionView.delegate = self;
    [AppHelper addHexagoneOnView:self.logoImageView];
    [AppHelper addHexagonBorderForLayer:self.logoImageView.layer color:[UIColor whiteColor] width:3.0];
    self.logoImageView.tintColor = [UIColor whiteColor];
}

- (void)fillData
{
    self.user = [[KeychainStorage new] loadCustomObjectWithKey:userModelKey];
    
    self.firstNameENTextfield.text = self.user.firstNameEN;
    self.lastNameENTextField.text = self.user.lastNameEN;
    self.firstNameARTextfield.text = self.user.firstNameAR;
    self.lastNameARTextField.text = self.user.lastNameAR;
    
    if (self.user.avatarImageBase64.length) {
        NSData *data = [[NSData alloc] initWithBase64EncodedString:self.user.avatarImageBase64 options:kNilOptions];
        UIImage *image = [UIImage imageWithData:data];
        self.logoImageView.image = image;
        self.selectImage = image;
    } else {
        self.logoImageView.image = [UIImage imageNamed:DefaultLogoImageName];
    }
    if (self.user.enhancedSecurity) {
        [self setEnableSecretQuery:YES];
        self.selectQuery = ContainerSelectQueryViewEditQuery;
        self.heightContainerSelectQueryViewConstraint.constant = HeightContainerSelectQueryViewConstraintEditQuery;
        for (int i = 0; i < self.user.secretQuestions.count; i++) {
            NSDictionary *questionDictionary = self.user.secretQuestions[i];
            if ([questionDictionary valueForKey:@"value"] == self.user.secretQuestionType) {
                self.selectedIndexQuery = i + 1;
                [self.tableView reloadData];
            }
        }
        self.replySecretQueryTextField.text = self.user.secretQuestionAnswer;
    } else {
        [self setEnableSecretQuery:NO];
        self.selectQuery = ContainerSelectQueryViewDisable;
        self.heightContainerSelectQueryViewConstraint.constant = HeightContainerSelectQueryViewConstraintDisable;
    }
    
    if (self.user.secretQuestions.count) {
        [self prepareDataSourceQueryWithQuestion:self.user.secretQuestions];
    }
}

- (void)setEnableSecretQuery:(BOOL)enable
{
    UIImage *icon = [UIImage imageNamed:enable ? IconNameCheckActive : IconNameCheckDisactive];
    [self.enableSelectQueryButton setImage:icon forState:UIControlStateNormal];
}

#pragma mark - Animation

- (void)animationSelectTableView:(BOOL)selected
{
    CGFloat heigthContainerSelectQueryView;
    switch (self.selectQuery) {
        case ContainerSelectQueryViewDisable:{
            heigthContainerSelectQueryView = HeightContainerSelectQueryViewConstraintDisable;
            break;
        }
        case ContainerSelectQueryViewEnabled:{
            heigthContainerSelectQueryView = HeightContainerSelectQueryViewConstraintEnabled;
            break;
        }
        case ContainerSelectQueryViewEditQuery:{
            heigthContainerSelectQueryView = HeightContainerSelectQueryViewConstraintEditQuery;
            break;
        }
    }
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CGFloat heightTableView = HeightSelectTableViewCell;
    if (selected) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        heightTableView = self.dataSourceQuery.count > 5 ? HeightSelectTableViewCell * 5 :  HeightSelectTableViewCell * self.dataSourceQuery.count;
        if (heightTableView > HeightContainerSelectQueryViewConstraintEditQuery - MarginConstraintBottomSelectTableView) {
            heigthContainerSelectQueryView = heightTableView + MarginConstraintBottomSelectTableView;
            self.tableView.scrollEnabled = YES;
        }
    }
    [self.view layoutIfNeeded];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.heightTableViewConstraint.constant = heightTableView;
        weakSelf.heightContainerSelectQueryViewConstraint.constant = heigthContainerSelectQueryView;
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (!selected && weakSelf.selectQuery == ContainerSelectQueryViewEditQuery) {
            [weakSelf.replySecretQueryTextField becomeFirstResponder];
        }
    }];
    [self.tableView reloadData];
}

- (void)vizibleTextFieldChangeKeyboard
{
    CGFloat offsetForScrollViewY = self.scrollView.frame.size.height - self.scrollView.contentOffset.y - self.keyboardHeight;
    CGFloat lineEventScroll = self.scrollView.frame.size.height + self.scrollView.contentOffset.y - self.keyboardHeight - 2 * HeightTextFieldAndSeparator + 20.f;
    if (lineEventScroll < self.offSetTextFildY - HeightTextFieldAndSeparator) {
        [self.scrollView setContentOffset:CGPointMake(0, self.offSetTextFildY - offsetForScrollViewY - self.scrollView.contentOffset.y + HeightTextFieldAndSeparator + 10.f) animated:YES];
    }
}

- (void)prepareDataSourceQueryWithQuestion:(NSArray *)questions
{
    NSMutableArray *questionsToDispay = [[NSMutableArray alloc] init];
    Question *question = [Question new];
    question.secretQuestionValue = -1;
    question.secretQuestionType = dynamicLocalizedString(@"register.placeHolderText.selectQueryTableView");
    
    [questionsToDispay addObject:question];
    
    for (NSDictionary *dic in questions) {
        Question *question = [Question new];
        question.secretQuestionValue = [[dic valueForKey:@"value"] integerValue];
        question.secretQuestionType = [dic valueForKey:@"label"];
        [questionsToDispay addObject:question];
    }
    
    self.dataSourceQuery = questionsToDispay;
    [self.tableView reloadData];
}

@end
