//
//  ChangePasswordViewController.m
//  TRA Smart Services
//
//  Created by Admin on 9/11/15.
//

#import "ChangePasswordViewController.h"

#import "UserProfileActionView.h"
#import "SettingViewController.h"

#import "UIImage+DrawText.h"
#import "KeychainStorage.h"
#import "BlackImageView.h"

@interface ChangePasswordViewController () <UserProfileActionViewDelegate>

@property (weak, nonatomic) IBOutlet BlackImageView *userLogoImageView;

@property (weak, nonatomic) IBOutlet UILabel *changeLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldPasswordLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UILabel *retypePasswordLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordRequirementsLabel;

@property (weak, nonatomic) IBOutlet LeftInsetTextField *oldPasswordTextField;
@property (weak, nonatomic) IBOutlet LeftInsetTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet LeftInsetTextField *retypePasswordTextField;

@property (weak, nonatomic) IBOutlet UserProfileActionView *actionView;

@property (strong, nonatomic) TRALoaderViewController *loader;

@end

@implementation ChangePasswordViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareUserView];
    self.actionView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self fillData];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[self.dynamicService currentApplicationColor] inRect:CGRectMake(0, 0, 1, 1)] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    [self.actionView localizeUI];
    self.title = dynamicLocalizedString(@"userProfile.changePassword");
    self.changeLabel.text = dynamicLocalizedString(@"userProfile.changePassword");
    self.oldPasswordLabel.text = dynamicLocalizedString(@"changePassword.oldPassword");
    self.passwordLabel.text = dynamicLocalizedString(@"changePassword.password");
    self.retypePasswordLabel.text = dynamicLocalizedString(@"changePassword.retypePassword");
    self.passwordRequirementsLabel.text = dynamicLocalizedString(@"message.PasswordPowerful");
    
    self.passwordTextField.attributedPlaceholder = [self placeholderWithString:dynamicLocalizedString(@"changePassword.placeHolder.password")];
    self.oldPasswordTextField.attributedPlaceholder = [self placeholderWithString:dynamicLocalizedString(@"changePassword.placeHolder.oldPassword")];
    self.retypePasswordTextField.attributedPlaceholder = [self placeholderWithString:dynamicLocalizedString(@"changePassword.placeHolder.retypePassword")];
    
   // self.title = dynamicLocalizedString(@"userProfile.title");
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
    [self.actionView updateColorUI];
    [self prepareSeparatorView:self.view];
    UIColor *color = self.dynamicService.currentApplicationColor;
    self.oldPasswordTextField.textColor = color;
    self.passwordTextField.textColor = color;
    self.retypePasswordTextField.textColor = color;
}

- (void)prepareNavigationBar
{
    [super prepareNavigationBar];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style: UIBarButtonItemStylePlain target:nil action:nil];
    [AppHelper titleFontForNavigationBar:self.navigationController.navigationBar];
}

- (void)setRTLArabicUI
{
    [self updateUI:NSTextAlignmentRight];
    
    [self.actionView setRTLStyle];
}

- (void)setLTREuropeUI
{
    [self updateUI:NSTextAlignmentLeft];
    
    [self.actionView setLTRStyle];
}

#pragma mark - Private Metods

- (void)fillData
{
    UserModel *user = [[KeychainStorage new] loadCustomObjectWithKey:userModelKey];
    if (user.avatarImageBase64.length) {
        NSData *data = [[NSData alloc] initWithBase64EncodedString:user.avatarImageBase64 options:kNilOptions];
        UIImage *image = [UIImage imageWithData:data];
        self.userLogoImageView.image = image;
    } else {
        self.userLogoImageView.image = [UIImage imageNamed:DefaultLogoImageName];
    }
}

- (void)prepareUserView
{
    [AppHelper addHexagoneOnView:self.userLogoImageView];
    [AppHelper addHexagonBorderForLayer:self.userLogoImageView.layer color:[UIColor whiteColor] width:3.0];
    self.userLogoImageView.tintColor = [UIColor whiteColor];
}

- (void)updateUI:(NSTextAlignment)textAlignment
{
    self.oldPasswordLabel.textAlignment = textAlignment;
    self.passwordLabel.textAlignment = textAlignment;
    self.retypePasswordLabel.textAlignment = textAlignment;
    self.oldPasswordTextField.textAlignment = textAlignment;
    self.passwordTextField.textAlignment = textAlignment;
    self.retypePasswordTextField.textAlignment = textAlignment;
    self.passwordRequirementsLabel.textAlignment = textAlignment;
    
    [self configureTextField:self.oldPasswordTextField];
    [self configureTextField:self.passwordTextField];
    [self configureTextField:self.retypePasswordTextField];
}

- (void)configureTextField:(UITextField *)textField
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        [textField setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [button setImage:[UIImage imageNamed:@"btn_eye"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_eye"] forState:UIControlStateSelected];
    [button setTintColor:[UIColor grayColor]];
    [button addTarget:self action:@selector(showHidePassword:) forControlEvents:UIControlEventTouchUpInside];
    textField.rightView = nil;
    textField.leftView = nil;
    if (self.dynamicService.language == LanguageTypeArabic) {
        textField.leftViewMode = UITextFieldViewModeAlways;
        textField.leftView = button;
    } else {
        textField.rightViewMode = UITextFieldViewModeAlways;
        textField.rightView = button;
    }
}

#pragma mark - Actions

- (IBAction)showHidePassword:(UIButton *)sender
{
    [sender setSelected:!sender.isSelected];
    [sender setTintColor:sender.isSelected ? self.dynamicService.currentApplicationColor :[UIColor grayColor]];
    [(UITextField *)[sender superview] setSecureTextEntry:!sender.isSelected];
    [(UITextField *)[sender superview] becomeFirstResponder];
}

#pragma mark - UserProfileActionViewDelegate

- (void)buttonCancelDidTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)buttonResetDidTapped
{
    self.oldPasswordTextField.text = @"";
    self.passwordTextField.text = @"";
    self.retypePasswordTextField.text = @"";
}

- (void)buttonSaveDidTapped
{
    if ([self isInputParametersValid]) {
        self.loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];

        __weak typeof(self) weakSelf = self;
        [[NetworkManager sharedManager] traSSChangePassword:self.passwordTextField.text confirmPassword:self.retypePasswordTextField.text requestResult:^(id response, NSError *error) {
            if (error) {
                
                weakSelf.loader.ratingView.hidden = true;
                [self.loader setCompletedStatus:TRACompleteStatusFailure withDescription:[response isKindOfClass:[NSString class]] ? response : error.localizedDescription];
            } else {
                [[KeychainStorage new] storePassword:self.passwordTextField.text forUser:[KeychainStorage userName]];
                [weakSelf performLogout];
            }
        }];
    }
}

- (BOOL)isInputParametersValid
{
    if (!self.passwordTextField.text.length || !self.retypePasswordTextField.text.length) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.EmptyInputParameters")];
    } else if (self.passwordTextField.text.length < 8) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.PasswordIsTooShort8")];
    } else if (self.passwordTextField.text.length >= 32) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.PasswordIsTooLong32")];
    } else if (![self.passwordTextField.text isEqualToString:self.retypePasswordTextField.text]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.NewPasswordsNotEqual")];
    } else if (![self.passwordTextField.text isValidPasswordFormat]) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.PasswordPowerful")];
    } else {
        return YES;
    }
    return NO;
}

#pragma mark - Private

- (void)performLogout
{
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] traSSLogout:^(id response, NSError *error) {
        if (error) {
            //shayan loader issue solved
            weakSelf.loader.ratingView.hidden = true;
            [self.loader setCompletedStatus:TRACompleteStatusFailure withDescription:response];
        } else {
            KeychainStorage *storage = [[KeychainStorage alloc] init];
            [storage removeStoredCredentials];
            [storage saveCustomObject:[UserModel new] key:userModelKey];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KeyUseTouchIDIdentification];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.loader.TRALoaderDidClose = ^{
                [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            };
            [self.loader dismissTRALoader:YES];
        }
    }];
}

- (NSAttributedString *)placeholderWithString:(NSString *)string
{
      NSDictionary *attributes = @{NSForegroundColorAttributeName:[UIColor grayColor]};
//    NSDictionary *attributes = @{NSForegroundColorAttributeName:[self.dynamicService currentApplicationColor]};
    return [[NSAttributedString alloc] initWithString:string attributes:attributes];
}

- (void)prepareSeparatorView:(UIView *)view
{
    for (UIView *subView in view.subviews) {
        if (subView.tag == 77) {
            subView.backgroundColor = self.dynamicService.currentApplicationColor;
        }
        [self prepareSeparatorView:subView];
    }
}

@end
