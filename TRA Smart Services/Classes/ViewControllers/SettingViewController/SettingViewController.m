//
//  SettingViewController.m
//  TRA Smart Services
//
//  Created by Admin on 22.07.15.
//

#import "SettingViewController.h"
#import "RTLController.h"
#import "UIImage+DrawText.h"
#import "DetailsViewController.h"
#import "SocialMediaViewController.h"
#import "KeychainStorage.h"
#import <LocalAuthentication/LocalAuthentication.h>

#import "TutorialViewController.h"

static NSInteger const ThemeColorBlackAndWhite = 4;
static CGFloat const OptionScaleSwitch = 1.;
static NSString *const SegueToAboutTRAViewControllerIdentifier = @"segueToAboutTRAViewController";
static NSString *const SegueToTermsPrivacyViewControllerIdentifier = @"segueToTermsPrivacyViewController";
static NSString *const SegueToSocialMediaViewControllerIdentifier = @"segueToSocialMediaViewController";

static NSString *const KeyForOptionColor = @"currentNumberColorTheme";

@interface SettingViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *conteinerView;
@property (weak, nonatomic) IBOutlet UIView *conteinerButtonColorThemeView;
@property (weak, nonatomic) IBOutlet UIView *containerSliderView;

@property (weak, nonatomic) IBOutlet UISwitch *screenLockNotificationSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *appTutorialScreensSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *themeColorBlackAndWhiteSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *touchIDSwitch;

@property (weak, nonatomic) IBOutlet UIButton *themeBlueButton;
@property (weak, nonatomic) IBOutlet UIButton *themeOrangeButton;
@property (weak, nonatomic) IBOutlet UIButton *themeGreenButton;

@property (weak, nonatomic) IBOutlet SegmentView *languageSegmentControl;
@property (weak, nonatomic) IBOutlet SegmentView *tutorialSegmentControl;
@property (weak, nonatomic) IBOutlet UISlider *slider;

@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
@property (weak, nonatomic) IBOutlet UILabel *colorImpairedLabel;
@property (weak, nonatomic) IBOutlet UILabel *screenLockNotificationTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *screenLockNotificationDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *appTutorialScreensTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *appTutorialScreensDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *fontSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *colorThemeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *colorThemeDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutTRATitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutTRADetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionBuildLabel;
@property (weak, nonatomic) IBOutlet UILabel *releaseLabel;
@property (weak, nonatomic) IBOutlet UILabel *useTouchIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *touchIDDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *termsPrivacyTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *termsPrivacyDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *faqTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *faqDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *socialMediaLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftFontSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *centerFontSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightFontSizeLabel;
@property (weak, nonatomic) IBOutlet UIButton *onTutorialButton;
@property (weak, nonatomic) IBOutlet UIButton *offTutorialButton;

@end

@implementation SettingViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    RTLController *rtl = [[RTLController alloc] init];
    [rtl disableRTLForView:self.view];
    
    [self prepareNavigationController];
    [self prepareSegmentsView];
    [self prepareUISwitchSettingViewController];
    [self prepareNavigationBar];
    [self prepareFontSizeSlider];
    [self updateFontSizeSliderColor];
    [self prepareFingerPrintSwitch];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

-(void)updateTutorialSwitchColor {
    
    NSNumber *toturialNumber =  [[NSUserDefaults standardUserDefaults]objectForKey: KeyIsTutorialShowed];
    
    if ([toturialNumber intValue] == 2) {
        
        [self tutorialColorOn];
    }
    else {
        
        [self tutorialColorOff];
        
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateTutorialSwitchColor];
   
    [self updateColorForDescriptioveTextSize];
    [self updateLanguageSegmentControlPosition];
    [self updateFontSizeControl];
    [self makeActiveColorTheme:self.dynamicService.colorScheme];
    self.touchIDSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:KeyUseTouchIDIdentification] && [NetworkManager sharedManager].isUserLoggined;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (IBAction)onToturialButtonTapped:(id)sender {
    
    
    [[NSUserDefaults standardUserDefaults] setValue:@(2) forKey:KeyIsTutorialShowed];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self tutorialColorOn];

    //shayan commented original code
   // self.tabBarController.selectedIndex = self.dynamicService.language == LanguageTypeArabic ? 4 : 0;
    
    UIViewController *currentViewController = self.tabBarController.selectedViewController;
    if ([currentViewController isKindOfClass:[UINavigationController class]]) {
        [(UINavigationController *)currentViewController popToRootViewControllerAnimated:NO];
    }

}

- (IBAction)offToturialButtonTapped:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setValue:@(1) forKey:KeyIsTutorialShowed];
    [self tutorialColorOff];
    
}

-(void)tutorialColorOn{
    
    _onTutorialButton.backgroundColor = [self.dynamicService currentApplicationColor];
    [_onTutorialButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [_offTutorialButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _offTutorialButton.backgroundColor = [UIColor lightGrayColor];
}

-(void)tutorialColorOff {
    
    
    
    _onTutorialButton.backgroundColor = [UIColor lightGrayColor];
    [_onTutorialButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [_offTutorialButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _offTutorialButton.backgroundColor = [self.dynamicService currentApplicationColor];
    
    
}
- (void)viewDidLayoutSubviews
{
    self.screenLockNotificationSwitch.onTintColor = [self.dynamicService currentApplicationColor];
    self.appTutorialScreensSwitch.onTintColor = [self.dynamicService currentApplicationColor];
    
    self.themeColorBlackAndWhiteSwitch.onTintColor = [self.dynamicService currentApplicationColor];
    self.touchIDSwitch.onTintColor = [self.dynamicService currentApplicationColor];
}

#pragma mark - IBActions

- (IBAction)showTutorialValueDidChanged:(UISwitch *)sender
{
    if (sender.isOn) {
        [[NSUserDefaults standardUserDefaults] setValue:@(2) forKey:KeyIsTutorialShowed];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.tabBarController.selectedIndex = self.dynamicService.language == LanguageTypeArabic ? 4 : 0;
        
        UIViewController *currentViewController = self.tabBarController.selectedViewController;
        if ([currentViewController isKindOfClass:[UINavigationController class]]) {
            [(UINavigationController *)currentViewController popToRootViewControllerAnimated:NO];
        }
    }
    sender.on = NO;
}

- (IBAction)useTouchIDValueChanged:(UISwitch *)sender
{
    if ([KeychainStorage userName].length && [NetworkManager sharedManager].isUserLoggined) {
        [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:KeyUseTouchIDIdentification];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        sender.on = NO;
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"setting.loginRequired.message")];
    }
}

- (void)selectThemeColorBlackAndWhiteSwitch:(UISwitch *)switchState
{
   
    
    
    
    switchState.isOn ? [self selectColorTheme:ThemeColorBlackAndWhite] : [self selectColorTheme:[self currentColorThemaUserDefaults]];
    
    //shayan logic of color changed to impaired
    if (switchState.isOn && _offTutorialButton.currentTitleColor == [UIColor whiteColor])  {
        
        [_offTutorialButton setBackgroundColor:[self.dynamicService currentApplicationColor]];
        [_onTutorialButton setTitleColor:UIColor.whiteColor forState: UIControlStateNormal];
        [_onTutorialButton setBackgroundColor:UIColor.grayColor];
        
        
    }
}

- (IBAction)selectedThemes:(UISwitch *)sender
{
    [self selectColorTheme:sender.tag];

    [self.themeColorBlackAndWhiteSwitch setOn:NO animated:YES];
    [self setCurrentColorThemaUserDefaults:sender.tag];
}

- (IBAction)sliderDidChangeValue:(UISlider *)sender
{
    sender.value = roundf(sender.value / sender.maximumValue * 2) * sender.maximumValue / 2;
    switch ((int)sender.value) {
        case 0: {
            if (self.dynamicService.fontSize == ApplicationFontSmall) {
                return;
            }
            [self.dynamicService saveCurrentFontSize:ApplicationFontSmall];
            break;
        }
        case 1: {
            if (self.dynamicService.fontSize == ApplicationFontUndefined) {
                return;
            }
            [self.dynamicService saveCurrentFontSize:ApplicationFontUndefined];
            break;
        }
        case 2: {
            if (self.dynamicService.fontSize == ApplicationFontBig) {
                return;
            }
            [self.dynamicService saveCurrentFontSize:ApplicationFontBig];
            break;
        }
    }
    [AppHelper updateFontsOnTabBar];
    [self updateColorForDescriptioveTextSize];
}

- (void)selctedSliderValueFont:(UITapGestureRecognizer *)gesture
{
    UIView *view = gesture.view;
    CGPoint tapPoint = [gesture locationInView:view];
    
    CGFloat sliderWidth = self.slider.frame.size.width;
    NSInteger selecteValue = -1;
    if (tapPoint.x < sliderWidth / 3) {
        selecteValue = 0;
    } else if (tapPoint.x > sliderWidth / 3 && tapPoint.x < (sliderWidth / 3) * 2) {
        selecteValue = 1;
    } else {
        selecteValue = 2;
    }
    
    if (selecteValue >= 0 && [view isKindOfClass:[UISlider class]]) {
        [self.slider setValue:selecteValue];
        [self sliderDidChangeValue:(UISlider *)view];
    }
}

- (IBAction)aboutTRAButtonTapped:(id)sender
{
    [self performSegueWithIdentifier:SegueToAboutTRAViewControllerIdentifier sender:nil];
}

- (IBAction)termsPrivacyButtonTapped:(id)sender
{
    [self performSegueWithIdentifier:SegueToTermsPrivacyViewControllerIdentifier sender:nil];
}
- (IBAction)socialMediaTapped:(id)sender
{
    [self performSegueWithIdentifier:SegueToSocialMediaViewControllerIdentifier sender:nil];
}

#pragma mark - SegmentViewDelegate

- (void)segmentControlDidPressedItem:(NSUInteger)item inSegment:(SegmentView *)segment
{
    
 
    switch (item) {
        case 0: {
            if ((self.dynamicService.language == LanguageTypeEnglish)||(self.dynamicService.language == LanguageTypeDefault)) {
                [self changeLanguageTo:LanguageTypeArabic];
            }
            break;
        }
        case 1: {
            if (self.dynamicService.language == LanguageTypeArabic) {
                [self changeLanguageTo:LanguageTypeEnglish];
            }
            break;
        }
    }
}

- (void)changeLanguageTo:(LanguageType)languageType
{
    [self transformAnimationContainerView];
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf.dynamicService setLanguage:languageType];
        [weakSelf updateLanguageSegmentControlPosition];
        [weakSelf localizeUI];
        [weakSelf updateSubviewForParentViewIfPossible:self.view fontSizeInclude:NO];
    });
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        [self prepareDynamicLocalizeShortcutItems];
    }
}

#pragma mark - Private

#pragma mark - UIPreparation

- (void)prepareUISwitchSettingViewController
{
    [self.themeColorBlackAndWhiteSwitch addTarget:self action:@selector(selectThemeColorBlackAndWhiteSwitch:) forControlEvents:UIControlEventValueChanged];

    [self prepareUISwitch:self.themeColorBlackAndWhiteSwitch];
    [self prepareUISwitch:self.appTutorialScreensSwitch];
    [self prepareUISwitch:self.screenLockNotificationSwitch];
    [self prepareUISwitch:self.touchIDSwitch];
}

- (void)prepareUISwitch:(UISwitch *) prepareSwitch
{
    prepareSwitch.backgroundColor = [UIColor grayBorderTextFieldTextColor];
    prepareSwitch.layer.cornerRadius = prepareSwitch.bounds.size.height / 2;
    prepareSwitch.tintColor = [UIColor grayBorderTextFieldTextColor];
}

#pragma mark - UITransform / Animations

- (void)transformUILayerSwitchView:(CATransform3D)animCATransform3DSwitch
{
    self.screenLockNotificationSwitch.layer.transform = animCATransform3DSwitch;
    self.appTutorialScreensSwitch.layer.transform = animCATransform3DSwitch;
    self.themeColorBlackAndWhiteSwitch.layer.transform = animCATransform3DSwitch;
    self.touchIDSwitch.layer.transform = animCATransform3DSwitch;
}

- (void)transformUILayer:(CATransform3D)animCATransform3D
{
    //shayan coded
    self.tutorialSegmentControl.layer.transform = animCATransform3D;
    
    
    self.appTutorialScreensTitleLabel.layer.transform = animCATransform3D;
    self.appTutorialScreensDetailsLabel.layer.transform = animCATransform3D;
    
    self.conteinerView.layer.transform = animCATransform3D;
    self.conteinerButtonColorThemeView.layer.transform = animCATransform3D;
    self.languageSegmentControl.layer.transform = animCATransform3D;
   
    self.languageLabel.layer.transform = animCATransform3D;
    self.fontSizeLabel.layer.transform = animCATransform3D;
    self.colorImpairedLabel.layer.transform = animCATransform3D;
    self.screenLockNotificationTitleLabel.layer.transform = animCATransform3D;
    self.screenLockNotificationDetailsLabel.layer.transform = animCATransform3D;
    
    self.colorThemeTitleLabel.layer.transform = animCATransform3D;
    self.colorThemeDetailsLabel.layer.transform = animCATransform3D;
    self.aboutTRATitleLabel.layer.transform = animCATransform3D;
    self.aboutTRADetailsLabel.layer.transform = animCATransform3D;
    self.leftFontSizeLabel.layer.transform = animCATransform3D;
    self.centerFontSizeLabel.layer.transform = animCATransform3D;
    self.rightFontSizeLabel.layer.transform = animCATransform3D;
    self.versionBuildLabel.layer.transform = animCATransform3D;
    self.releaseLabel.layer.transform = animCATransform3D;
    self.useTouchIDLabel.layer.transform = animCATransform3D;
    self.offTutorialButton.layer.transform = animCATransform3D;
    self.onTutorialButton.layer.transform= animCATransform3D;
    self.touchIDDescriptionLabel.layer.transform = animCATransform3D;
    self.termsPrivacyTitleLabel.layer.transform = animCATransform3D;
    self.termsPrivacyDescriptionLabel.layer.transform = animCATransform3D;
    self.faqTitleLabel.layer.transform = animCATransform3D;
    self.faqDescriptionLabel.layer.transform = animCATransform3D;
    self.socialMediaLabel.layer.transform = animCATransform3D;
}

- (void)setTextAligmentLabelSettingViewController:(NSTextAlignment)textAlignment
{
    
    self.languageLabel.textAlignment = textAlignment;
    self.fontSizeLabel.textAlignment = textAlignment;
    self.colorImpairedLabel.textAlignment = textAlignment;
    self.screenLockNotificationTitleLabel.textAlignment = textAlignment;
    self.screenLockNotificationDetailsLabel.textAlignment = textAlignment;
    self.appTutorialScreensTitleLabel.textAlignment = textAlignment;
    self.appTutorialScreensDetailsLabel.textAlignment = textAlignment;
    self.colorThemeTitleLabel.textAlignment = textAlignment;
    self.colorThemeDetailsLabel.textAlignment = textAlignment;
    self.aboutTRATitleLabel.textAlignment = textAlignment;
    self.aboutTRADetailsLabel.textAlignment = textAlignment;
    self.rightFontSizeLabel.textAlignment = textAlignment == NSTextAlignmentLeft ? NSTextAlignmentRight : NSTextAlignmentLeft;
    self.leftFontSizeLabel.textAlignment = textAlignment;
    self.versionBuildLabel.textAlignment = textAlignment;
    self.releaseLabel.textAlignment = textAlignment;
    self.useTouchIDLabel.textAlignment = textAlignment;
    self.touchIDDescriptionLabel.textAlignment = textAlignment;
    self.termsPrivacyDescriptionLabel.textAlignment = textAlignment;
    self.termsPrivacyTitleLabel.textAlignment = textAlignment;
    self.faqTitleLabel.textAlignment = textAlignment;
    self.faqDescriptionLabel.textAlignment = textAlignment;
    self.socialMediaLabel.textAlignment = textAlignment;
}

- (void)transformAnimationContainerView
{
    UIView *protectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    protectionView.tag = 101;
    [self.view addSubview:protectionView];
    CAAnimation *anim = [self transformAnimation];
    anim.delegate = self;
    anim.removedOnCompletion = NO;
    [self.conteinerView.layer addAnimation:anim forKey:@"transformView"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.conteinerView.layer animationForKey:@"transformView"]) {
        [self.conteinerView.layer removeAllAnimations];
        [[self.view viewWithTag:101] removeFromSuperview];
    }
}

- (CAAnimation *)transformAnimation
{
    CATransform3D rotationAndPerspectiveTransform = self.conteinerView.layer.transform;
    if (self.dynamicService.language == LanguageTypeArabic ) {
        rotationAndPerspectiveTransform.m34 = 1.0 / 500.0;
        rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform,  45 * M_PI , 0.0f, 1.0f, 0.0f);
    } else {
        rotationAndPerspectiveTransform.m34 = 1.0 / -500.0;
        rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, -45 * M_PI , 0.0f, -1.0f, 0.0f);
    }
    
    CABasicAnimation *transformAnim = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnim.fromValue = [NSValue valueWithCATransform3D:self.conteinerView.layer.transform];
    transformAnim.toValue = [NSValue valueWithCATransform3D:rotationAndPerspectiveTransform];
    
    CAKeyframeAnimation *scaleAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    CATransform3D startScale = CATransform3DScale (self.conteinerView.layer.transform, 1, 0, 0);
    CATransform3D midScale = CATransform3DScale (self.conteinerView.layer.transform, 0.8, 0, 0);
    CATransform3D endScale = CATransform3DScale (self.conteinerView.layer.transform, 1, 0, 0);
    if (self.dynamicService.language == LanguageTypeArabic) {
        startScale = CATransform3DScale (self.conteinerView.layer.transform, 1, 1, 1);
        midScale = CATransform3DScale (self.conteinerView.layer.transform, 0.8, 0.8, 0.8);
        endScale = CATransform3DScale (self.conteinerView.layer.transform, 1, 1, 1);
    }
    scaleAnimation.values = @[
                              [NSValue valueWithCATransform3D:startScale],
                              [NSValue valueWithCATransform3D:midScale],
                              [NSValue valueWithCATransform3D:endScale],
                              ];
    scaleAnimation.keyTimes = @[[NSNumber numberWithFloat:0.0f],
                                [NSNumber numberWithFloat:0.5f],
                                [NSNumber numberWithFloat:0.9f]
                                ];
    scaleAnimation.timingFunctions = @[
                                       [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut],
                                       [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                       ];
    scaleAnimation.fillMode = kCAFillModeForwards;
    scaleAnimation.removedOnCompletion = NO;
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[transformAnim, scaleAnimation];
    group.duration = 0.6f;
    
    return group;
}

#pragma mark - Controls

- (void)prepareSegmentsView
{
    self.tutorialSegmentControl.delegate = self;
    self.tutorialSegmentControl.segmentItemsAttributes = @[@{NSFontAttributeName : [UIFont droidKufiBoldFontForSize:12]}, @{NSFontAttributeName : [UIFont latoBoldWithSize:12]}];
    self.languageSegmentControl.delegate = self;
    self.languageSegmentControl.segmentItemsAttributes = @[@{NSFontAttributeName : [UIFont droidKufiBoldFontForSize:12]}, @{NSFontAttributeName : [UIFont latoBoldWithSize:12]}];
}

- (void)selectColorTheme:(NSInteger)numberTheme
{
    switch (numberTheme) {
        case 1: {
            [self makeActiveColorTheme:ApplicationColorBlue];
            [self.dynamicService saveCurrentColorScheme:ApplicationColorBlue];
            break;
        }
        case 0:
        case 2: {
            [self makeActiveColorTheme:ApplicationColorOrange];
            [self.dynamicService saveCurrentColorScheme:ApplicationColorOrange];
            break;
        }
        case 3: {
            [self makeActiveColorTheme:ApplicationColorGreen];
            [self.dynamicService saveCurrentColorScheme:ApplicationColorGreen];
            break;
        }
        case 4: {
            [self makeActiveColorTheme:ApplicationColorBlackAndWhite];
            [self.dynamicService saveCurrentColorScheme:ApplicationColorBlackAndWhite];
            break;
        }
    }
    [AppHelper updateTabBarTintColor];
    [AppHelper updateNavigationBarColor];
}

- (void)buttonColorThemeEnable:(BOOL)enabled
{
    self.themeBlueButton.enabled = enabled;
    self.themeGreenButton.enabled = enabled;
    self.themeOrangeButton.enabled = enabled;
    
    UIImage *themeBlueButtonImage = [[UIImage imageNamed:@"btn_theme_bl"] imageWithRenderingMode:enabled ? UIImageRenderingModeAutomatic : UIImageRenderingModeAlwaysTemplate];
    UIImage *themeOrangeButtonImage = [[UIImage imageNamed:@"btn_theme_rng"] imageWithRenderingMode:enabled ? UIImageRenderingModeAutomatic : UIImageRenderingModeAlwaysTemplate];
    UIImage *themeGreenButtonImage = [[UIImage imageNamed:@"btn_theme_grn"] imageWithRenderingMode:enabled ? UIImageRenderingModeAutomatic : UIImageRenderingModeAlwaysTemplate];
    
    [self.themeBlueButton setImage:themeBlueButtonImage forState:UIControlStateNormal];
    [self.themeOrangeButton setImage:themeOrangeButtonImage forState:UIControlStateNormal];
    [self.themeGreenButton setImage:themeGreenButtonImage forState:UIControlStateNormal];
}

- (void)makeActiveColorTheme:(ApplicationColor)selectedColor
{
    [self buttonColorThemeEnable:YES];
    self.themeColorBlackAndWhiteSwitch.on = NO;
    [self.dynamicService setColorScheme:selectedColor];
    
    switch (selectedColor) {
        case ApplicationColorBlue: {
            [self.themeBlueButton setImage:[UIImage imageNamed:@"btn_theme_bl_act"] forState:UIControlStateNormal];
            break;
        }
        case ApplicationColorDefault:
        case ApplicationColorOrange: {
            [self.themeOrangeButton setImage:[UIImage imageNamed:@"btn_theme_rng_act"] forState:UIControlStateNormal];
            break;
        }
        case ApplicationColorGreen: {
            [self.themeGreenButton setImage:[UIImage imageNamed:@"btn_theme_grn_act"] forState:UIControlStateNormal];
            break;
        }
        case ApplicationColorBlackAndWhite: {
            self.themeColorBlackAndWhiteSwitch.on = YES;
            [self buttonColorThemeEnable:NO];
            break;
        }
    }
    [self updateColors];
}

#pragma mark - NavigationBar

- (void)prepareNavigationController
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style: UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)prepareNavigationBar
{
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

- (void)prepareTitle
{
    self.title = dynamicLocalizedString(@"tabBarMenu_item_5");
    [AppHelper titleFontForNavigationBar:self.navigationController.navigationBar];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    [self prepareTitle];
    
   
    [self.onTutorialButton setTitle:dynamicLocalizedString(@"settings.onTitle") forState:UIControlStateNormal];
    [self.offTutorialButton setTitle:dynamicLocalizedString(@"settings.offTitle") forState:UIControlStateNormal];
    
    
    self.tutorialSegmentControl.segmentItems = @[dynamicLocalizedString(@"settings.switchControl.tutorial.off"), dynamicLocalizedString(@"settings.switchControl.tutorial.on")];


    self.leftFontSizeLabel.text = dynamicLocalizedString(@"settings.label.fontsize.description.small");
    self.centerFontSizeLabel.text = dynamicLocalizedString(@"settings.label.fontsize.description.normal");
    self.rightFontSizeLabel.text = dynamicLocalizedString(@"settings.label.fontsize.description.large");
    self.languageLabel.text = dynamicLocalizedString(@"settings.label.language");
    self.fontSizeLabel.text = dynamicLocalizedString(@"settings.label.fontsize");
    self.colorImpairedLabel.text = dynamicLocalizedString(@"settings.label.colorImpaired");
    self.screenLockNotificationTitleLabel.text = dynamicLocalizedString(@"settings.label.screenLockNotificationTitle");
    self.screenLockNotificationDetailsLabel.text = dynamicLocalizedString(@"settings.label.screenLockNotificationDetauils");
    self.appTutorialScreensTitleLabel.text = dynamicLocalizedString(@"settings.label.appTutorialScreensTitle");
    self.appTutorialScreensDetailsLabel.text = dynamicLocalizedString(@"settings.label.appTutorialScreensDetails");
    self.colorThemeTitleLabel.text = dynamicLocalizedString(@"settings.label.colorThemeTitle");
    self.colorThemeDetailsLabel.text = dynamicLocalizedString(@"settings.label.colorThemeDetails");
    self.aboutTRATitleLabel.text = dynamicLocalizedString(@"settings.label.aboutTRATitle");
    self.aboutTRADetailsLabel.text = dynamicLocalizedString(@"settings.label.aboutTRADetails");
    self.termsPrivacyTitleLabel.text = dynamicLocalizedString(@"settings.termsPrivacyTitleLabel");
    self.termsPrivacyDescriptionLabel.text = dynamicLocalizedString(@"settings.termsPrivacyDescriptionLabel");
    self.languageSegmentControl.segmentItems = @[dynamicLocalizedString(@"settings.switchControl.language.arabic"), dynamicLocalizedString(@"setting.switchControl.language.english")];
    self.releaseLabel.text = dynamicLocalizedString(@"setting.label.release");
    self.touchIDDescriptionLabel.text = dynamicLocalizedString(@"setting.label.touchIDDEscription");
    self.useTouchIDLabel.text = dynamicLocalizedString(@"setting.label.touchID");
    self.faqTitleLabel.text = dynamicLocalizedString(@"setting.faqTitleLabel");
    self.faqDescriptionLabel.text = dynamicLocalizedString(@"setting.faqDescriptionLabel");
    self.socialMediaLabel.text = dynamicLocalizedString(@"SocialMedia");
    
    //v 1.0 build <version number>.<number of week>.<weakBuild>
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *dateComponent = [calender components:(NSCalendarUnitWeekOfYear | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:[NSDate date]];
    self.versionBuildLabel.text = [NSString stringWithFormat:dynamicLocalizedString(@"setting.label.releaseVersion"), [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"], 1, [dateComponent weekOfYear], 1];
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
    
    self.screenLockNotificationSwitch.onTintColor = [self.dynamicService currentApplicationColor];
    self.appTutorialScreensSwitch.onTintColor = [self.dynamicService currentApplicationColor];
    self.themeColorBlackAndWhiteSwitch.onTintColor = [self.dynamicService currentApplicationColor];
    self.touchIDSwitch.onTintColor = [self.dynamicService currentApplicationColor];
    
[self updateTutorialSwitchColor];
    self.languageSegmentControl.segmentSelectedBacrgroundColor = [self.dynamicService currentApplicationColor];
     self.tutorialSegmentControl.segmentSelectedBacrgroundColor = [self.dynamicService currentApplicationColor];
    [self.tutorialSegmentControl setNeedsLayout];
    [self.languageSegmentControl setNeedsLayout];
    [self updateFontSizeSliderColor];
    [self updateColorForDescriptioveTextSize];
}

- (void)setRTLArabicUI
{
    [self transformUILayer:TRANFORM_3D_SCALE];
    [self transformUILayerSwitchView:CATransform3DMakeScale(- OptionScaleSwitch, OptionScaleSwitch, 1)];
    [self setTextAligmentLabelSettingViewController:NSTextAlignmentRight];
}

- (void)setLTREuropeUI
{
    [self transformUILayer:CATransform3DIdentity];
    [self transformUILayerSwitchView:CATransform3DMakeScale(OptionScaleSwitch, OptionScaleSwitch, 1)];
    [self setTextAligmentLabelSettingViewController:NSTextAlignmentLeft];
}

#pragma mark - UIUpdate

- (void)prepareDynamicLocalizeShortcutItems
{
    NSArray *dataString = @[@"compliantViewController.title.compliantAboutServiceProvider", @"compliantViewController.title.comtlaintTRA", @"homeSearchViewController.homeSearchTextField.placeholder"];
    
    NSArray <UIApplicationShortcutItem *> *existingShortcutItems = [[UIApplication sharedApplication] shortcutItems];
    if (existingShortcutItems.count == 3) {
        for (NSInteger anIndex = 0; anIndex < existingShortcutItems.count; anIndex++) {
            UIApplicationShortcutItem *anExistingShortcutItem = [existingShortcutItems objectAtIndex: anIndex];
            NSMutableArray <UIApplicationShortcutItem *> *updatedShortcutItems = [existingShortcutItems mutableCopy];
            UIMutableApplicationShortcutItem *aMutableShortcutItem = [anExistingShortcutItem mutableCopy];
            [aMutableShortcutItem setLocalizedTitle:dynamicLocalizedString(dataString[anIndex])];
            [updatedShortcutItems replaceObjectAtIndex: anIndex withObject: aMutableShortcutItem];
            [[UIApplication sharedApplication] setShortcutItems: updatedShortcutItems];
        }
    }
}

- (void)updateLanguageSegmentControlPosition
{
    switch (self.dynamicService.language) {
        case LanguageTypeDefault:
        case LanguageTypeArabic : {
            [self.languageSegmentControl setSegmentItemSelectedWithTag:1];
            break;
        }
        case LanguageTypeEnglish: {
            [self.languageSegmentControl setSegmentItemSelectedWithTag:0];
            break;
        }
    }
}

-(void)updateTutorialSegmentControlPosition {
    switch (self.dynamicService.language) {
        case LanguageTypeDefault:
        case LanguageTypeArabic : {
            [self.tutorialSegmentControl setSegmentItemSelectedWithTag:1];
            break;
        }
        case LanguageTypeEnglish: {
            [self.tutorialSegmentControl setSegmentItemSelectedWithTag:0];
            break;
        }
    }
    
}

- (void)updateFontSizeControl
{
    switch (self.dynamicService.fontSize) {
        case ApplicationFontUndefined: {
            [self.slider setValue:1];
            break;
        }
        case ApplicationFontSmall: {
            [self.slider setValue:0];
            break;
        }
        case ApplicationFontBig: {
            [self.slider setValue:2];
            break;
        }
    }
}

- (void)setCurrentColorThemaUserDefaults:(NSInteger)currentNumberColorTheme
{
    [[NSUserDefaults standardUserDefaults] setInteger:currentNumberColorTheme forKey:KeyForOptionColor];
}

- (NSInteger)currentColorThemaUserDefaults
{
    NSInteger result = (NSInteger)[[NSUserDefaults standardUserDefaults] integerForKey:KeyForOptionColor];
    return result;
}

- (void)prepareFingerPrintSwitch
{
    LAContext *context = [[LAContext alloc] init];
    
    if (![context canEvaluatePolicy: LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil])
    {
        self.touchIDSwitch.enabled = NO;
    }
}

#pragma mark - Slider Setup

- (void)prepareFontSizeSlider
{
    UIImage *img = [UIImage imageNamed:@"filled13-2"];
    [self.slider setThumbImage:img forState:UIControlStateNormal];
    [self.slider setThumbImage:img forState:UIControlStateSelected];
    [self.slider setThumbImage:img forState:UIControlStateHighlighted];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selctedSliderValueFont:)];
    [self.slider addGestureRecognizer:tapGestureRecognizer];
}

- (void)updateFontSizeSliderColor
{
    UIImage *currentSliderMaximumImage = self.slider.currentMaximumTrackImage;
    
    CGRect rect = CGRectMake(0, 0, currentSliderMaximumImage.size.width, currentSliderMaximumImage.size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [[self.dynamicService currentApplicationColor] setFill];
    UIRectFill(rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.slider setMaximumTrackImage:image forState:UIControlStateNormal];
    [self.slider setMinimumTrackImage:image forState:UIControlStateNormal];
    
    [[UISlider appearance] setTintColor:[UIColor itemGradientTopColor]];
    
    for (UIView *point in self.containerSliderView.subviews) {
        if (![point isKindOfClass:[UISlider class]]) {
            point.layer.cornerRadius = point.frame.size.height / 2;
            point.backgroundColor = [self.dynamicService currentApplicationColor];
        }
    }
}

- (void)updateColorForDescriptioveTextSize
{
    UIColor *textColor = [self.dynamicService currentApplicationColor];
    UIColor *defaultColor = [UIColor lightGraySettingTextColor];
    
    self.rightFontSizeLabel.textColor = defaultColor;
    self.leftFontSizeLabel.textColor = defaultColor;
    self.centerFontSizeLabel.textColor = defaultColor;
    
    switch (self.dynamicService.fontSize) {
        case ApplicationFontUndefined: {
            self.centerFontSizeLabel.textColor = textColor;
            break;
        }
        case ApplicationFontSmall: {
            self.leftFontSizeLabel.textColor = textColor;
            break;
        }
        case ApplicationFontBig: {
            self.rightFontSizeLabel.textColor = textColor;
            break;
        }
    }
}

@end
