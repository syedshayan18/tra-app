//
//  ServiceInfoViewController.m
//  TRA Smart Services
//
//  Created by Admin on 18.08.15.
//

#import "ServiceInfoViewController.h"
#import "ServiceInfoCollectionViewCell.h"
#import "Animation.h"
#import "ServiceDetailedInfoViewController.h"

static NSUInteger const RowCount = 2;
static NSUInteger const ColumsCount = 4;

static NSString *const ServiceDetailsSegueIdentifier = @"serviceDetailsSegue";

static NSString *const ARLanguageKey = @"AR";
static NSString *const ENLanguageKey = @"EN";

@interface ServiceInfoViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleScreenLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIImageView *fakeBackgroundImageView;

@property (strong, nonatomic) NSArray *dataSource;
@property (assign, nonatomic) BOOL isControllerPresented;

@property (strong, nonatomic) NSDictionary *currentServiceLocalizedDataSource;

@end

@implementation ServiceInfoViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.fakeBackground) {
        self.fakeBackgroundImageView.image = self.fakeBackground;
    }
    self.titleScreenLabel.text = self.titleScreen;
    [self reverseDataSourceIfNeeded];
    [self prepareDataSource];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self animateAppearence];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ServiceInfoCollectionViewCell *cell =  (ServiceInfoCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    NSDictionary *selectedElement = self.dataSource[indexPath.row];
    
    cell.serviceInfologoImageView.image = [UIImage imageNamed:[selectedElement valueForKey:@"serviceInfoItemLogoActName"]];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        cell.serviceInfologoImageView.image = [UIImage imageNamed:[selectedElement valueForKey:@"serviceInfoItemLogoName"]];
        [collectionView reloadItemsAtIndexPaths:@[indexPath]];
    });
    
    NSDictionary *dataSource = self.dataSource[indexPath.row];
    NSString *detailsText = [self.currentServiceLocalizedDataSource valueForKey:[dataSource valueForKey:@"serviceResponseKey"]];
    
    if ([detailsText isEqualToString:@"No document is required."] ||[detailsText isEqualToString:@"Not applicable."] ||[detailsText isEqualToString:@"No fees."] || [detailsText isEqualToString:@"Not applicable"]  ) {

        
    }
    else {
           [self performSegueWithIdentifier:ServiceDetailsSegueIdentifier sender:self];
    }
 
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ServiceInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ServiceInfoCollectionViewCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[ServiceInfoCollectionViewCell alloc] init];
    }
    
    
    NSDictionary *dataSource = self.dataSource[indexPath.row];
    NSString *detailsText = [self.currentServiceLocalizedDataSource valueForKey:[dataSource valueForKey:@"serviceResponseKey"]];
    
    if ([detailsText isEqualToString:@"No document is required."] ||[detailsText isEqualToString:@"Not applicable."] ||[detailsText isEqualToString:@"No fees."] || [detailsText isEqualToString:@"Not applicable"]  ) {
        [self configureCell:cell atIndexPath:indexPath];
        cell.serviceInfologoImageView.alpha = 0.3;
        cell.serviceInfoTitleLabel.alpha = 0.3;
        cell.serviceInfologoImageView.userInteractionEnabled = NO;
       
    }
    else {
        cell.serviceInfologoImageView.alpha = 1;
        cell.serviceInfoTitleLabel.alpha = 1;

        [self configureCell:cell atIndexPath:indexPath];
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize screenSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
    CGFloat originY = self.collectionView.frame.origin.y;
    CGFloat collectionViewHeight = screenSize.height - originY - 20.;
    CGFloat collectionViewWidth = screenSize.width;
    CGFloat numbersOfRow = ColumsCount;
    CGFloat numbersOfColums = RowCount;
    CGFloat minimumSpacingForCell = 10.;
    CGFloat minimumSpacingForLines = 20.;

    CGFloat cellWidth = (collectionViewWidth - minimumSpacingForCell * numbersOfColums) / numbersOfColums;
    CGFloat cellHeight = (collectionViewHeight - minimumSpacingForLines * numbersOfRow) / numbersOfRow;
    
    if (self.dataSource.count % 2 && indexPath.row == self.dataSource.count - 1) {
        cellWidth = collectionViewWidth;
    }
    
    CGSize cellSize = CGSizeMake(cellWidth, cellHeight);
    return cellSize;
}

#pragma mark - IbActions

- (IBAction)closeButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - Animations

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:ServiceDetailsSegueIdentifier]) {
        NSDictionary *dataSource = self.dataSource[((NSIndexPath *)[[self.collectionView indexPathsForSelectedItems] firstObject]).row];
        NSString *detailsText = [self.currentServiceLocalizedDataSource valueForKey:[dataSource valueForKey:@"serviceResponseKey"]];
      

        ServiceDetailedInfoViewController *serviceInfoController = segue.destinationViewController;
        serviceInfoController.fakeBackground = [AppHelper snapshotForView:self.navigationController.view];
        serviceInfoController.hidesBottomBarWhenPushed = YES;
        serviceInfoController.dataSource = dataSource;
        serviceInfoController.detailedInfoText = detailsText;
    }
}

#pragma mark - Superclass

- (void)localizeUI
{
    [self.collectionView reloadData];
}

- (void)updateColors
{
    self.backgroundView.backgroundColor = [[self.dynamicService currentApplicationColor] colorWithAlphaComponent:0.97f];
}

#pragma mark - Networking

- (void)downloadStaticServiceInfo
{
    TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:dynamicLocalizedString(@"favouriteCell.infoButton.title") closeButton:NO];
    
    NSString *languageCode = self.dynamicService.language == LanguageTypeArabic ? ARLanguageKey : ENLanguageKey;
    NSString *serviceRequestName = [self serviceNameForCurrentService];
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] traSSNoCRMServiceGetServiceAboutInfo:serviceRequestName languageCode:languageCode responseBlock:^(id response, NSError *error) {
        if (error) {
            [response isKindOfClass:[NSString class]] ? [AppHelper alertViewWithMessage:response] : [AppHelper alertViewWithMessage:error.localizedDescription];
        } else if ([response isKindOfClass:[NSDictionary class]]) {
            weakSelf.currentServiceLocalizedDataSource = response;
            [_collectionView reloadData];
        }
        [loader dismissTRALoader:YES];
    }];
}

- (void)downloadDinamicServiceInfo
{
    TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:dynamicLocalizedString(@"favouriteCell.infoButton.title") closeButton:NO];
    
    __weak typeof(self) weakSelf = self;
    [[NetworkManager sharedManager] traSSNoCRMServiceGetDynamicServiceAboutInfo:self.dynamicServiceModel.dynamicServiceID responseBlock:^(id response, NSError *error) {
        if (error) {
            [response isKindOfClass:[NSString class]] ? [AppHelper alertViewWithMessage:response] : [AppHelper alertViewWithMessage:error.localizedDescription];
        } else if ([response isKindOfClass:[NSDictionary class]]) {
            weakSelf.dynamicServiceModel = [[DynamicServiceModel alloc] initWithDictionary:response];
            [weakSelf prepareCurrentServiceLocalizedDataSource];
        }
        [loader dismissTRALoader:YES];
    }];
}

#pragma mark - Private

- (void)prepareDataSource
{
    if (self.dynamicServiceModel) {
         self.titleScreenLabel.text = [self.dynamicServiceModel localizableString:self.dynamicServiceModel.dynamicServiceName];
        if (self.dynamicServiceModel.dynamicServiceInfo) {
            [self prepareCurrentServiceLocalizedDataSource];
        } else {
            [self downloadDinamicServiceInfo];
        }
    } else  {
        [self downloadStaticServiceInfo];
    }
}

- (void)prepareCurrentServiceLocalizedDataSource
{
    NSMutableDictionary *serviceInfo = [[NSMutableDictionary alloc] init];
    for (NSDictionary *selectedElement in self.dataSource) {
        NSString *serviceInfoTitleKey = [selectedElement valueForKey:@"serviceResponseKey"];
        NSDictionary *localizationDictionary = [self.dynamicServiceModel.dynamicServiceInfo valueForKey:serviceInfoTitleKey];
        NSString *serviceInfoDescription = [localizationDictionary valueForKey:[DynamicUIService service].language == LanguageTypeArabic ? ARLanguageKey : ENLanguageKey];
        [serviceInfo setValue:serviceInfoDescription forKey:serviceInfoTitleKey];
    }
    self.currentServiceLocalizedDataSource = serviceInfo;
}

- (void)configureCell:(ServiceInfoCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *selectedElement = self.dataSource[indexPath.row];
    cell.serviceInfoTitleLabel.text = dynamicLocalizedString([selectedElement valueForKey:@"serviceInfoItemName"]);
    cell.serviceInfologoImageView.image = [UIImage imageNamed:[selectedElement valueForKey:@"serviceInfoItemLogoName"]];
}

- (void)reverseDataSourceIfNeeded
{
    self.dataSource = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ServiceInfoList" ofType:@"plist"]];
    if (self.dynamicService.language == LanguageTypeArabic) {
        self.dataSource = [self.dataSource reversedArrayByElementsInGroup:RowCount];
    }
}

- (void)animateAppearence
{
    if (!self.isControllerPresented) {
        [self.backgroundView.layer addAnimation:[Animation fadeAnimFromValue:0 to:1 delegate:nil] forKey:nil];
        self.isControllerPresented = YES;
    } else {
        [self.collectionView reloadData];
    }
}

- (NSString *)serviceNameForCurrentService
{
    NSString *serviceNameForRequest;
    switch (self.selectedServiceID) {
        case ServiceTypeGetDomainData: {
            serviceNameForRequest = ServiceTypeGetDomainDataStringName;
            break;
        }
        case ServiceTypeGetDomainAvaliability: {
            serviceNameForRequest = ServiceTypeGetDomainAvaliabilityStringName;
            break;
        }
        case ServiceTypeSearchMobileIMEI: {
            serviceNameForRequest = ServiceTypeSearchMobileIMEIStringName;
            break;
        }
        case ServiceTypeSearchMobileBrand: {
            serviceNameForRequest = ServiceTypeSearchMobileBrandStringName;
            break;
        }
        case ServiceTypeFeedback: {
            serviceNameForRequest = ServiceTypeFeedbackStringName;
            break;
        }
        case ServiceTypeSMSSpamReport: {
            serviceNameForRequest = ServiceTypeSMSSpamReportStringName;
            break;
        }
        case ServiceTypeHelpSalim: {
            serviceNameForRequest = ServiceTypeHelpSalimStringName;
            break;
        }
        case ServiceTypeVerification: {
            serviceNameForRequest = ServiceTypeVerificationStringName;
            break;
        }
        case ServiceTypeCoverage: {
            serviceNameForRequest = ServiceTypeCoverageStringName;
            break;
        }
        case ServiceTypeInternetSpeedTest: {
            serviceNameForRequest = ServiceTypeInternetSpeedTestStringName;
            break;
        }
        case ServiceTypeCompliantAboutServiceProvider: {
            serviceNameForRequest = ServiceTypeCompliantAboutServiceProviderStringName;
            break;
        }
        case ServiceTypeSuggestion: {
            serviceNameForRequest = ServiceTypeSuggestionStringName;
            break;
        }
        case ServiceTypeCompliantAboutServiceProviderEnquires: {
            serviceNameForRequest = ServiceTypeCompliantAboutServiceProviderEnquiresStringName;
            break;
        }
        case ServiceTypeCompliantAboutServiceProviderTRA: {
            serviceNameForRequest = ServiceTypeCompliantAboutServiceProviderTRAStringName;
            break;
        }
    }
    return serviceNameForRequest;
}

@end
