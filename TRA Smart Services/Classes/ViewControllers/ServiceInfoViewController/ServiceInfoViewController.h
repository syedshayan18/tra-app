//
//  ServiceInfoViewController.h
//  TRA Smart Services
//
//  Created by Admin on 18.08.15.
//

#import "DynamicServiceModel.h"

@interface ServiceInfoViewController : BaseDynamicUIViewController

@property (strong, nonatomic) UIImage *fakeBackground;
@property (assign, nonatomic) NSInteger selectedServiceID;
@property (strong, nonatomic) NSString *titleScreen;
@property (strong, nonatomic) DynamicServiceModel *dynamicServiceModel;

@end
