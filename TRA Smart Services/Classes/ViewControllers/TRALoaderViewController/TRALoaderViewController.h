//
//  TRALoaderViewController.h
//  TRA Smart Services
//
//  Created by Admin on 21.09.15.
//

#import "BaseDynamicUIViewController.h"

#import "RatingView.h"

static CGFloat const TRAAnimationDuration = 2;

typedef NS_ENUM(NSInteger, TRACompleteStatus) {
    TRACompleteStatusFailure = 0,
    TRACompleteStatusSuccess = 1,
};

@interface TRALoaderViewController : BaseDynamicUIViewController <RatingViewDelegate>

@property (strong, nonatomic) void (^TRALoaderWillClose)();
@property (strong, nonatomic) void (^TRALoaderDidClose)();

@property (weak, nonatomic) IBOutlet RatingView *ratingView;

@property (strong, nonatomic) NSString *serviceRatingName;
@property (assign, nonatomic) BOOL closeWithoutAnimation;

+ (TRALoaderViewController *)presentLoaderOnViewController:(UIViewController *)presenter requestName:(NSString *)requestName closeButton:(BOOL)button;

- (void)dismissTRALoader:(BOOL)animated;
- (void)setCompletedStatus:(TRACompleteStatus)status withDescription:(NSString *)description;

@end
