//
//  BaseServiceViewController.m
//  TRA Smart Services
//
//  Created by Admin on 31.08.15.
//

#import "BaseServiceViewController.h"
#import "LoginViewController.h"
#import "ServiceInfoViewController.h"

static CGFloat const HeightForToolbars = 44.f;

static CGFloat HeightSelectViewConstraint = 40.;
static CGFloat TopSelectViewConstraint = 20.;

@interface BaseServiceViewController()

@property (weak, nonatomic) id textView;

@end

@implementation BaseServiceViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.translucent = YES;
    
    [self prepareNotification];
    if (self.serviceID > 0) {
        [self addInfoButtonToNavigationBar];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style: UIBarButtonItemStylePlain target:nil action:nil];
    [AppHelper titleFontForNavigationBar:self.navigationController.navigationBar];
}

#pragma mark - Public

- (NSString *)hendelServiceError:(NSError *)error response:(id)response
{
    NSString *errorString = error.localizedDescription;
    
    NSInteger statusCode = ((NSHTTPURLResponse *)error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey]).statusCode;
    if (error.code == NSURLErrorNotConnectedToInternet) {
        errorString = dynamicLocalizedString(@"message.NoInternetConnection");
    } else if (error.code == NSURLErrorTimedOut) {
        errorString = dynamicLocalizedString(@"message.error.serverTimedOut");
    } else if (error.code == NSURLErrorCancelled) {
        errorString = dynamicLocalizedString(@"message.OperationCanceledByUser");
    } else if ( statusCode == 426) {
        errorString = dynamicLocalizedString(@"message.error.serviceIsDisable");
        [[CoreDataManager sharedManager] prepareCoreDataWintResponse:response];
        [self.navigationController popToRootViewControllerAnimated:NO];
    } else if (statusCode == 416) {
        errorString = dynamicLocalizedString(@"message.error.apiVersionIsUnsupported");
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.error.apiVersionIsUnsupported") delegate:self otherButtonTitles:dynamicLocalizedString(@"uiElement.cancelButton.title")];
    } else if ([response isKindOfClass:[NSString class]]) {
        errorString = response;
    }
    return errorString;
}

- (void)presentLoginIfNeededAndPopToRootController:(UIViewController *)controller
{
    if (![NetworkManager sharedManager].isUserLoggined) {
        UINavigationController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
        __weak typeof(self) weakSelf = self;
        ((LoginViewController *)viewController.topViewController).didCloseViewController = ^() {
            if (![NetworkManager sharedManager].isUserLoggined) {
                if (controller) {
                    [weakSelf.navigationController popToViewController:controller animated:NO];
                } else {
                    [weakSelf.navigationController popToRootViewControllerAnimated:NO];
                }
            }
        };
        ((LoginViewController *)viewController.topViewController).shouldAutoCloseAfterLogin = YES;
        [AppHelper presentViewController:viewController onController:self.navigationController];
    }
}

- (void)prefixLoginSend:(void (^)(void))sendIsLoginedBlock
{
    if ([NetworkManager sharedManager].isUserLoggined) {
        sendIsLoginedBlock();
    } else {
        UINavigationController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
        ((LoginViewController *)viewController.topViewController).didDismissed = ^() {
            if ([NetworkManager sharedManager].isUserLoggined) {
                sendIsLoginedBlock();
            }
        };
        ((LoginViewController *)viewController.topViewController).shouldAutoCloseAfterLogin = YES;
        [AppHelper presentViewController:viewController onController:self.navigationController];
    }
}

- (void)configureKeyboardButtonDone:(id)textView
{
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, HeightForToolbars)];
    toolBar.backgroundColor = [UIColor clearColor];
    toolBar.tintColor = [[DynamicUIService service] currentApplicationColor];
    
    UIBarButtonItem *barItemCustomDone = [[UIBarButtonItem alloc] initWithTitle:dynamicLocalizedString(@"uiElement.keyboardButtom.title") style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonTapped)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolBar setItems:@[flexibleSpace, barItemCustomDone]];
    
    [textView setInputAccessoryView:toolBar];
    self.textView = textView;
}

- (void)animationSelectView:(UIView *)view hidden:(BOOL)hidden
{
    NSLayoutConstraint *heightViewConstraint = [self constraintAttributeHeigthForView:view];
    if (heightViewConstraint.constant == (hidden ? 0. : HeightSelectViewConstraint)) {
        return;
    }
    NSLayoutConstraint *topViewConstraint = [self constraintAttributeTopForView:view];
    
    view.alpha = hidden ? 1. : 0;
    [self.view layoutIfNeeded];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3  delay:hidden ? 0. : 0.15 options:UIViewAnimationOptionCurveEaseOut animations:^{
        if (hidden) {
            view.alpha = 0.;
        } else {
            heightViewConstraint.constant = HeightSelectViewConstraint;
            topViewConstraint.constant = TopSelectViewConstraint;
        }
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {}];
    
    [UIView animateWithDuration:0.3  delay:hidden ? 0.15 : 0. options:UIViewAnimationOptionCurveEaseOut animations:^{
        if (hidden) {
            heightViewConstraint.constant = 0;
            topViewConstraint.constant = 0;
        } else {
            view.alpha = 1.;
        }
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {}];
}

- (NSLayoutConstraint *)constraintAttributeHeigthForView:(UIView *)view
{
    for (NSLayoutConstraint *constraint in view.constraints) {
        if (constraint.firstItem == view && constraint.firstAttribute == NSLayoutAttributeHeight && constraint.constant == view.frame.size.height) {
            return constraint;
        }
    }
    return nil;
}

- (void)addInfoButtonToNavigationBar
{
    UIImage *infoImage = [UIImage imageNamed:@"ic_info_sml"];
    UIBarButtonItem *infoButton = [[UIBarButtonItem alloc] initWithImage:infoImage style:UIBarButtonItemStyleDone target:self action:@selector(showServiceInfo)];
    self.navigationItem.rightBarButtonItem = infoButton;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (!buttonIndex) {
        NSString *appstoreString = @"https://itunes.apple.com/us/app/uae-tra/id930647801";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appstoreString]];
    }
}

#pragma mark - TextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    CGRect caretRect = [textView caretRectForPosition:textView.selectedTextRange.start];
    CGRect frame = [self.view convertRect:textView.bounds fromView:textView];
    
    CGFloat curcorPointY = frame.origin.y + caretRect.origin.y + caretRect.size.height - textView.contentOffset.y;
    CGFloat const keyPositionY = SCREEN_HEIGHT - self.keyboardHeight;
    if (curcorPointY >= keyPositionY) {
        [textView setContentOffset: CGPointMake(0, frame.origin.y + caretRect.origin.y + caretRect.size.height - keyPositionY + 3.0f)];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{    
    [textField layoutIfNeeded];
}

#pragma mark - Superclass methods

- (void)updateColors
{
    [self updateColors:self.view];
}

#pragma mark - Action

- (void)doneButtonTapped
{
    [self.view endEditing:YES];
}

#pragma mark - Private

- (NSLayoutConstraint *)constraintAttributeTopForView:(UIView *)view
{
    for (NSLayoutConstraint *constraint in view.superview.constraints) {
        if (constraint.firstItem == view && constraint.firstAttribute == NSLayoutAttributeTop) {
            return constraint;
        }
    }
    return nil;
}

- (void)updateColors:(UIView *) view
{
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)subView;
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button setBackgroundColor:[self.dynamicService currentApplicationColor]];
            button.layer.shadowColor = [UIColor blackColor].CGColor;
            button.layer.shadowOpacity = 0.5;
            button.layer.shadowRadius = 1.;
            button.layer.shadowOffset = CGSizeMake(1.5, 1.5);
        } else if ([subView isKindOfClass:[BottomBorderTextField class]]) {
            [AppHelper setStyleForTextField:(BottomBorderTextField *)subView];
            ((BottomBorderTextField *)subView).requredIndicatorColor = [DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite ? [UIColor blackColor] : [UIColor redColor];

        }  else if ([subView isKindOfClass:[BottomBorderTextView class]]) {
            [AppHelper setStyleForTextView:(BottomBorderTextView *)subView];
            ((BottomBorderTextView *)subView).requredIndicatorColor = [DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite ? [UIColor blackColor] : [UIColor redColor];
        }
        [self updateColors:subView];
    }
}

- (void)showServiceInfo
{
    [self.view endEditing:YES];

    ServiceInfoViewController *serviceInfoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"serviceInfoIdentifier"];
    serviceInfoViewController.hidesBottomBarWhenPushed = YES;
    serviceInfoViewController.selectedServiceID = self.serviceID;
    serviceInfoViewController.fakeBackground = [AppHelper snapshotForView:self.navigationController.view];
    serviceInfoViewController.titleScreen = self.title;
    serviceInfoViewController.dynamicServiceModel = self.dynamicServiceModel;

    [self.navigationController pushViewController:serviceInfoViewController animated:NO];
}

#pragma mark - Keyboard

- (void)prepareNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillAppear:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyboardHeight = keyboardRect.size.height - self.tabBarController.tabBar.frame.size.height;
    
    [self animationKeyboardHide:NO];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [self.textView setContentOffset: CGPointZero];
    
    [self animationKeyboardHide:YES];
}

- (void)animationKeyboardHide:(BOOL)hidden
{
    if ((!hidden && self.bottomContentScrollViewConstraint.constant != self.keyboardHeight) || (hidden && self.bottomContentScrollViewConstraint != 0)) {
        [self.view layoutIfNeeded];
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:0.3 animations:^{
            weakSelf.bottomContentScrollViewConstraint.constant = hidden ? 0 : self.keyboardHeight;
            [weakSelf.view layoutIfNeeded];
        }];
    }
}

@end
