//
//  BaseServiceViewController.h
//  TRA Smart Services
//
//  Created by Admin on 31.08.15.
//

#import "BottomBorderTextField.h"
#import "BottomBorderTextView.h"
#import "DynamicServiceModel.h"

@interface BaseServiceViewController : BaseDynamicUIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomContentScrollViewConstraint;
@property (assign, nonatomic) NSInteger serviceID;
@property (assign, nonatomic) CGFloat keyboardHeight;
@property (strong, nonatomic) DynamicServiceModel *dynamicServiceModel;

- (void)presentLoginIfNeededAndPopToRootController:(UIViewController *)controller;
- (void)prefixLoginSend:(void (^)(void))sendIsLoginedBlock;
- (void)updateColors;
- (void)configureKeyboardButtonDone:(UITextView *)textView;
- (NSString *)hendelServiceError:(NSError *)error response:(id)response;
- (void)animationSelectView:(UIView *)view hidden:(BOOL)hidden;
- (NSLayoutConstraint *)constraintAttributeHeigthForView:(UIView *)view;
- (void)addInfoButtonToNavigationBar;

@end