//
//  DynamicListServicesViewController.m
//  TRA Smart Services
//
//  Created by Admin on 21.06.16.
//  Copyright © 2016 . All rights reserved.
//

#import "DynamicListServicesViewController.h"
#import "MenuCollectionViewCell.h"
#import "DynamicContainerServiceViewController.h"

static CGFloat const CellSpacing = 5.f;
static CGFloat const RowCount = 2.f;
static CGFloat const CellSubmenuHeight = 140.f;
static NSInteger const MinRowServicesMain = 0;

@interface DynamicListServicesViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *dynamicServiceDataSorce;

@end

@implementation DynamicListServicesViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dynamicServiceDataSorce = [self.dynamicServiceModel.dynamicServiceItems mutableCopy];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.dynamicServiceDataSorce.count / RowCount < MinRowServicesMain ? MinRowServicesMain : (NSInteger)ceilf(self.dynamicServiceDataSorce.count / RowCount);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSUInteger itemRowisSection = (NSUInteger)RowCount;
    
    NSUInteger elementsCount = self.dynamicServiceDataSorce.count;
    NSInteger elementCorrectionNoVisibleCount = elementsCount % (NSUInteger)RowCount;
    elementsCount = elementsCount < MinRowServicesMain * RowCount ? (MinRowServicesMain * RowCount - elementCorrectionNoVisibleCount) : elementsCount;
    
    NSUInteger rowCount = (NSUInteger)RowCount;
    NSUInteger elementsVisibleCount = self.dynamicServiceDataSorce.count;
    NSUInteger indexElementsRowCount = (elementsVisibleCount / rowCount) * rowCount;
    if (section * rowCount >= indexElementsRowCount && section * rowCount < elementsVisibleCount ) {
        itemRowisSection = elementsVisibleCount % rowCount;
    }
    
    return itemRowisSection;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:MenuCollectionViewCellIdentifier forIndexPath:indexPath];

    [self configureMainCell:(MenuCollectionViewCell *)cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = CellSubmenuHeight;
    CGSize contentSize = self.collectionView.frame.size;
    CGFloat rowCountActual = RowCount;
    
    CGSize cellSize = CGSizeMake((contentSize.width - (CellSpacing * (rowCountActual + 1))) / rowCountActual, cellHeight);
    return cellSize;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat minimumInteritemSpacing = CellSpacing;
    
    NSUInteger rowCount = (NSUInteger)RowCount;
    NSUInteger elementsVisibleCount = self.dynamicServiceDataSorce.count;
    NSUInteger indexElementsRowCount = (elementsVisibleCount / rowCount) * rowCount;
    if (section * RowCount >= indexElementsRowCount && section * RowCount < elementsVisibleCount) {
        CGFloat cellWidth = (self.collectionView.frame.size.width - (CellSpacing * (RowCount + 1))) / RowCount;
        NSInteger rowCountActual = elementsVisibleCount % rowCount;
        
        minimumInteritemSpacing = (self.collectionView.frame.size.width - rowCountActual * cellWidth) / (rowCountActual );
        if (rowCountActual == 2) {
            minimumInteritemSpacing = CellSpacing; //minimumInteritemSpacing / 2.;
        }
    }
    return minimumInteritemSpacing;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat cellSpacing = CellSpacing;
    
    NSUInteger rowCount = (NSUInteger)RowCount;
    NSUInteger elementsVisibleCount = self.dynamicServiceDataSorce.count;
    NSUInteger indexElementsRowCount = (elementsVisibleCount / rowCount) * rowCount;
    if (section * RowCount >= indexElementsRowCount && section * RowCount < elementsVisibleCount) {
        CGFloat cellWidth = (self.collectionView.frame.size.width - (CellSpacing * (RowCount + 1))) / RowCount;
        NSInteger rowCountActual = elementsVisibleCount % rowCount;
        
        cellSpacing = (self.collectionView.frame.size.width - rowCountActual * cellWidth) / (rowCountActual * 2);
        if (rowCountActual == 2) {
            cellSpacing = CellSpacing * 2 + cellWidth;//cellSpacing * 1.5;
        }
    }
    
    return UIEdgeInsetsMake(0, cellSpacing, 0, cellSpacing);
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row + indexPath.section * RowCount < self.dynamicServiceDataSorce.count) {
        
        DynamicServiceModel *selectDynamicServiceModel = self.dynamicServiceDataSorce[indexPath.row + indexPath.section * (NSInteger)RowCount];
        
        if (selectDynamicServiceModel.dynamicServiceItems.count) {
            DynamicListServicesViewController *dynamicListServicesViewController = [[DynamicListServicesViewController alloc] init];
            dynamicListServicesViewController.dynamicServiceModel = selectDynamicServiceModel;
            
            [self.navigationController pushViewController:dynamicListServicesViewController animated:YES];
        } else {
            DynamicContainerServiceViewController *dynamicContainerServiceViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"dynamicServiceID"];
            dynamicContainerServiceViewController.dynamicServiceModel = selectDynamicServiceModel;
            
            [self.navigationController pushViewController:dynamicContainerServiceViewController animated:YES];
        }
    }
}

#pragma mark - SuperClass Methods

- (void)localizeUI
{
    self.title = [self.dynamicServiceModel localizableString:self.dynamicServiceModel.dynamicServiceName];
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"img_bg_service"];
}

#pragma mark - Configurations for Cells

- (void)configureMainCell:(MenuCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.menuTitleLabel.textColor = self.dynamicService.currentApplicationColor;
    cell.menuTitleLabel.tag = DeclineTagForFontUpdate;
    cell.widthMenuTitleLabelConstraint.constant = [self widthMenuCollectionViewCellTitleLabel];
    if (indexPath.row + indexPath.section * RowCount < self.dynamicServiceDataSorce.count) {
        [self configureCellDynamicService:cell atIndexPath:indexPath];
    }
}

- (void)configureCellDynamicService:(MenuCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [AppHelper addHexagonBorderForLayer:cell.polygonView.layer color:self.dynamicService.currentApplicationColor width:1];
    DynamicServiceModel *dynamicServiceModel = self.dynamicServiceDataSorce[indexPath.row + indexPath.section * (NSInteger)RowCount];
    cell.menuTitleLabel.text = [dynamicServiceModel localizableString:dynamicServiceModel.dynamicServiceName];
    UIImage *logo = [UIImage imageNamed:@"ic_domain_logo"];
    [AppHelper downloadToImageView:cell.itemLogoImageView url:dynamicServiceModel.dynamicServiceIcon placeholderImage:logo];
    cell.itemLogoImageView.tintColor = self.dynamicService.currentApplicationColor;
}

- (CGFloat)widthMenuCollectionViewCellTitleLabel
{
    return (self.collectionView.frame.size.width - (CellSpacing * (RowCount + 1))) / RowCount;
}

@end
