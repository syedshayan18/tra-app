//
//  SpamRaportViewController.m
//  TRA Smart Services
//
//  Created by Admin on 13.08.15.
//

#import "SpamReportViewController.h"
#import "LoginViewController.h"
#import "ServicesSelectTableViewCell.h"
#import "PlaceholderTextView.h"
#import "FeedbackViewController.h"
#import "SpamReportProviderCollectionViewCell.h"

static NSInteger const BlockServiceNumber = 7726;
//shayan original value 132 Service Provider title added thats why vertical constraint value added
static CGFloat const verticalTopReportTextFieldConstreintSpamSMS = 180.f;
static CGFloat const phoneNumberVerticalConstraintSpamSMS = 170.f;
static CGFloat const verticalTopReportTextFieldConstreintSpamWeb = 30.f;
static CGFloat const phoneNumberVerticalConstraintSpamWeb = 20.f;


@interface SpamReportViewController ()
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOrLinkLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneNumberVerticalConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneNumberLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionLeadingConstraint;

@property (weak, nonatomic) IBOutlet BottomBorderTextField *reportTextField;
@property (weak, nonatomic) IBOutlet PlaceholderTextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *reportButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalTopReportTextFieldConstreint;
@property (weak, nonatomic) IBOutlet UIView *separatorSelectedProvider;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) NSArray *iconProviderDataSource;
@property (strong, nonatomic) NSArray *selectProviderDataSource;
@property (assign, nonatomic) NSInteger selectedProvider;
@property (strong, nonatomic) NSArray *keyForServerProvider;
@end

@implementation SpamReportViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
  //  self.reportTextField.text = @"+971 5";
    self.keyForServerProvider = @[@"du", @"Etisalat"]; //shoudnt be localized

    self.selectedProvider = 0;
    [self prepareDataSource];
    //shayan
  //  self.reportTextField.requredIndicatorEnable = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
  if  (self.dynamicService.language == LanguageTypeArabic) {
      
      [self.view removeConstraint:_descriptionLeadingConstraint];
      [self.view removeConstraint:_phoneNumberLeadingConstraint];
        
    }

    [super viewWillAppear:animated];
    
    [self didChangeReportType:self.selectSpamReport];
    [self configureKeyboardButtonDone:self.descriptionTextView];
}

#pragma mark - IBAction

- (void)showRatingViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FeedbackViewController *feedback = [storyboard instantiateViewControllerWithIdentifier:FeedbackViewControllerIdentifier];
    feedback.modalPresentationStyle = UIModalPresentationOverFullScreen;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    feedback.serviceRatingName = ServiceTypeSMSSpamReportStringName.capitalizedString;
    
    [self presentViewController:feedback animated:NO completion:nil];
}

- (IBAction)responseSpam:(id)sender
{
    [self.view endEditing:YES];
    if (self.selectSpamReport == SpamReportTypeWeb) {
        if (![self.reportTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length) {
            [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.Link")]];
        }  else  if (![self.reportTextField.text isValidURL]) {
            [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatURL")];
        } else {
            [self helpSalimReport];
        }
    } else if (self.selectSpamReport == SpamReportTypeSMS) {
        if (![self.reportTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length) {
            [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.SpammerNummerAlias")]];
        } else if (!self.selectedProvider) {
            [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.PleaseChooseServiceProvider")];
        } else if ([self.reportTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length < 4) {
            [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.ReferenceNumberIsTooShort")];
        }
//        else if ([self.reportTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 15) {
//
//            [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.ReferenceNumberIsTooLong")];
//        }
      
        else if (![self.reportTextField.text hasPrefix:@"+971 5"] && ![self.reportTextField.text hasPrefix:@"+9715"] && ![self.reportTextField.text hasPrefix:@"009715"] && ![self.reportTextField.text hasPrefix:@"00971 5"]) {
            
            [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.incorrectPhoneFormat")];
        }
            else {
            [self sendSMSMessage];
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
//    if (textField.text.length == 14 && ![string isEqualToString:@""]){
//        
//                return NO;
//        
//            }
   
//    else if (textField.text.length ==14 && ![string isEqualToString:@""]){
//
//        return NO;
//
//    }
//
    return YES;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.iconProviderDataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SpamReportProviderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SpamReportProviderCollectionViewCellIdentifier forIndexPath:indexPath];
    cell.iconProvider = self.iconProviderDataSource[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedProvider = indexPath.row + 1;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellWidth = collectionView.bounds.size.width / 2.;
    CGFloat cellHeight = collectionView.bounds.size.height;
    
    return CGSizeMake(cellWidth, cellHeight);
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self visibleRectView:textView];
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self visibleRectView:textView];
}

#pragma mark - SuperclassMethods

- (void)localizeUI
{
    
    self.numberOrLinkLabel.text = dynamicLocalizedString(self.selectSpamReport == SpamReportTypeSMS ? @"spamReportViewControler.phoneNumberLabel.label" : @"spamReportViewControler.linkLabel.label");
    
    self.descriptionLabel.text = dynamicLocalizedString(@"spamReportViewControler.descriptionTextView.label");
    
    self.titleLabel.text = dynamicLocalizedString(@"spamReportViewControler.title.serviceProvider");
    self.title = dynamicLocalizedString(self.selectSpamReport == SpamReportTypeSMS ? @"spamReportViewControler.title.spamSMS" : @"spamReportViewControler.title.spamWEB");
    
    //original code shayan commented
    
        self.reportTextField.placeholder = dynamicLocalizedString(self.selectSpamReport == SpamReportTypeSMS ?@"+971 5xxxxxxxx" : @"spamReportViewControler.reportTextField.reportWeb");
    
//    self.reportTextField.placeholder = dynamicLocalizedString(self.selectSpamReport == SpamReportTypeSMS ?@"spamReportViewControler.reportTextField.reportSMS" : @"spamReportViewControler.reportTextField.reportWeb");
    [self.reportButton setTitle:dynamicLocalizedString(@"spamReportViewControler.reportButton.title") forState:UIControlStateNormal];
    self.descriptionTextView.placeholder = dynamicLocalizedString(@"spamReportViewControler.descriptionTextView.placeholder");
    [self prepareDataSource];
}

- (void)updateColors
{
    [super updateColors];
    
    UIColor *color = [self.dynamicService currentApplicationColor];
    
    self.reportButton.backgroundColor = color;
    self.descriptionTextView.textColor = color;
    self.reportTextField.textColor = color;
    self.separatorSelectedProvider.backgroundColor = color;
    self.titleLabel.textColor = color;
    [super updateBackgroundImageNamed:@"img_bg_service"];
    [self.reportButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [AppHelper setStyleForTextView:self.descriptionTextView];
}

- (void)setRTLArabicUI
{
    [self updateUIElementsWithTextAlignment:NSTextAlignmentRight];
}

- (void)setLTREuropeUI
{
    [self updateUIElementsWithTextAlignment:NSTextAlignmentLeft];
}

#pragma mark - Networking

- (void)helpSalimReport
{
    TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];
    
    loader.serviceRatingName = ServiceTypeHelpSalimStringName.capitalizedString;

    [[NetworkManager sharedManager] traSSNoCRMServicePOSTHelpSalim:self.reportTextField.text notes:self.descriptionTextView.text requestResult:^(id response, NSError *error) {
        if (error) {
            //shayan loader issue solved
            loader.ratingView.hidden = YES;
            [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[self hendelServiceError:error response:response]];
        } else {
            loader.TRALoaderDidClose = ^{

                [self.navigationController
                 popToRootViewControllerAnimated:NO];

            };
            loader.ratingView.hidden = NO;

            [loader setCompletedStatus:TRACompleteStatusSuccess withDescription:nil];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, TRAAnimationDuration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        });
    }];
}

- (void)POSTSpamReport
{
    TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];
    loader.serviceRatingName = ServiceTypeSMSSpamReportStringName.capitalizedString;

    [[NetworkManager sharedManager] traSSNoCRMServicePOSTSMSSpamReport:self.reportTextField.text notes:self.descriptionTextView.text requestResult:^(id response, NSError *error) {
        if (error) {
            //shayan loader issue solved
            loader.ratingView.hidden = true;
            [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[self hendelServiceError:error response:response]];
        } else {
            loader.TRALoaderDidClose = ^{
                [self.navigationController popToRootViewControllerAnimated:NO];
            };
            loader.ratingView.hidden = NO;

            [loader setCompletedStatus:TRACompleteStatusSuccess withDescription:nil];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, TRAAnimationDuration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        });
    }];
}

- (void)POSTSMSBlock
{
    NSString *provider = self.keyForServerProvider[self.selectedProvider - 1];
    [[NetworkManager sharedManager] traSSNoCRMServicePOSTSMSBlock:self.reportTextField.text phoneProvider:[NSString stringWithFormat:@"%i", (int)BlockServiceNumber] providerType:provider notes:self.descriptionTextView.text requestResult:^(id response, NSError *error) {
    }];
}

#pragma mark - SMSMessage

- (void)sendSMSMessage
{
    if([MFMessageComposeViewController canSendText]) {
        [UINavigationBar appearance].barTintColor = self.dynamicService.currentApplicationColor;
        [UINavigationBar appearance].tintColor = [UIColor whiteColor];
        [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName : self.dynamicService.language == LanguageTypeArabic ? [UIFont droidKufiRegularFontForSize:17] : [UIFont latoRegularWithSize:17]};
        
        NSArray *recipents = @[[NSString stringWithFormat:@"%i", (int)BlockServiceNumber]];
        NSString *message = [NSString stringWithFormat:@"b %@", self.reportTextField.text];
        
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        [messageController setRecipients:recipents];
        [messageController setBody:message];
        
        messageController.navigationBar.tintColor = [UIColor whiteColor];
        
        [self presentViewController:messageController animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }];
    } else {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.FailedToInitializeMFController")];
    }
}

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultSent: {
            __weak typeof(self) weakSelf = self;
            [controller dismissViewControllerAnimated:YES completion:^{
                [weakSelf POSTSpamReport];
            }];
            break;
        }
        case MessageComposeResultFailed: {
            [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.FailedToSendSMS")];
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        default: {
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
        }
    }
}

#pragma mark - Private

- (void)updateUIElementsWithTextAlignment:(NSTextAlignment)alignment
{
    self.descriptionLabel.textAlignment = alignment;
    self.numberOrLinkLabel.textAlignment = alignment;
    self.descriptionTextView.textAlignment = alignment;
    self.reportTextField.textAlignment = alignment;
    
}

- (void)didChangeReportType:(SpamReportType)select
{
   
//    [self presentLoginIfNeededAndPopToRootController:[self.navigationController viewControllers][self.navigationController.viewControllers.count - 2]];

    if (select == SpamReportTypeWeb) {
        self.titleLabel.hidden = true;
        self.collectionView.hidden = YES;
        self.separatorSelectedProvider.hidden = YES;
        self.verticalTopReportTextFieldConstreint.constant = verticalTopReportTextFieldConstreintSpamWeb;
        self.phoneNumberVerticalConstraint.constant = phoneNumberVerticalConstraintSpamWeb;
        self.reportTextField.keyboardType = UIKeyboardTypeURL;
    } else {
        self.collectionView.hidden = NO;
        self.separatorSelectedProvider.hidden = NO;
        self.verticalTopReportTextFieldConstreint.constant = verticalTopReportTextFieldConstreintSpamSMS;
        self.phoneNumberVerticalConstraint.constant = phoneNumberVerticalConstraintSpamSMS;
    }
}

- (void)prepareDataSource
{
    self.selectProviderDataSource = @[
                                      dynamicLocalizedString(@"providerType.selectProvider.text"),
                                      dynamicLocalizedString(@"providerType.Du"),
                                      dynamicLocalizedString(@"providerType.Etisalat"),
                                      ];
    self.iconProviderDataSource = @[[UIImage imageNamed:@"logo_du"], [UIImage imageNamed:@"logo_elisalat"]];
}

- (void)prepareButtonRating
{
    UIBarButtonItem *infoButton = self.navigationItem.rightBarButtonItem;
    
    UIImage *infoImage = [UIImage imageNamed:@"btn_nav_rate"];
    UIBarButtonItem *ratingButton = [[UIBarButtonItem alloc] initWithImage:infoImage style:UIBarButtonItemStyleDone target:self action:@selector(showRatingViewController)];
    
    self.navigationItem.rightBarButtonItems = @[infoButton, ratingButton];
}

- (void)visibleRectView:(UIView *)view
{
    CGRect frame = [self.scrollView.subviews[0] convertRect:view.bounds fromView:view];
    CGFloat offsetLine = 0.;
    
    CGFloat frameTopPositionY = frame.origin.y + view.frame.size.height;
    CGFloat lineEventScroll = self.scrollView.frame.size.height + self.scrollView.contentOffset.y - offsetLine;
    if (lineEventScroll < frameTopPositionY) {
        [self.scrollView setContentOffset:CGPointMake(0, frameTopPositionY - self.scrollView.frame.size.height + offsetLine) animated:YES];
    }
    if (frame.origin.y < self.scrollView.contentOffset.y) {
        [self.scrollView setContentOffset:CGPointMake(0, frame.origin.y - 10.f) animated:YES];
    }
}

@end
