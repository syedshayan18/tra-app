//
//  BaseSelectImageViewController.h
//  TRA Smart Services
//
//  Created by Admin on 14.08.15.
//

@protocol BaseSelectImageProtocol <NSObject>

@optional
- (void)clearImageAttach;

@end

@interface BaseSelectImageViewController : BaseServiceViewController <BaseSelectImageProtocol, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIImage *selectImage;

- (void)configureActionSheet;
- (void)addAttachButtonTextField:(UITextField *)textField;
- (void)configureImageAttach:(BOOL)imageAttach;
- (void)selectImagePickerController:(UIImagePickerControllerSourceType)sourceType;

@end
