//
//  BaseSelectImageViewController.m
//  TRA Smart Services
//
//  Created by Admin on 14.08.15.
//

#import "BaseSelectImageViewController.h"
#import "UIImage+Color.h"
#import "UIImage+Compression.h"

static NSString *const ImageNameButtonAttachClear = @"btn_attach";
static NSString *const ImageNameButtonAttachData = @"btn_attach_file";

@interface BaseSelectImageViewController ()

@property (strong, nonatomic) UIImage *buttonAttachImage;
@property (strong, nonatomic) UIButton *attachButton;

@end

@implementation BaseSelectImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.buttonAttachImage = [UIImage imageNamed:ImageNameButtonAttachClear];
}

#pragma mark - Public

- (void)addAttachButtonTextField:(UITextField *)textField
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        [textField setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }

    textField.rightView = nil;
    textField.leftView = nil;
    
    self.attachButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.attachButton setImage:self.buttonAttachImage forState:UIControlStateNormal];
    [self.attachButton addTarget:self action:@selector(selectImage:) forControlEvents:UIControlEventTouchUpInside];
    self.attachButton.backgroundColor = [UIColor clearColor];
    self.attachButton.tintColor = [self.dynamicService currentApplicationColor];
    [self.attachButton setFrame:CGRectMake(0, 0, textField.frame.size.height, textField.frame.size.height)];
    
    if (self.dynamicService.language == LanguageTypeArabic) {
        textField.leftView = self.attachButton;
        textField.leftViewMode = UITextFieldViewModeAlways;
    } else {
        textField.rightView = self.attachButton;
        textField.rightViewMode = UITextFieldViewModeAlways;
    }
}

- (void)configureActionSheet
{
    UIAlertController *selectImageController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:dynamicLocalizedString(@"selectImageActionSheet.cancel") style:UIAlertActionStyleCancel handler:nil];
    [selectImageController addAction:cancelAction];
    
    __weak typeof(self) weakSelf = self;
    UIAlertAction *selectImageAction = [UIAlertAction actionWithTitle:dynamicLocalizedString(@"selectImageActionSheet.selectImage") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf selectImagePickerController:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    [selectImageController addAction:selectImageAction];
    
    UIAlertAction *cameraImageAction = [UIAlertAction actionWithTitle:dynamicLocalizedString(@"selectImageActionSheet.cameraImage") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [weakSelf selectImagePickerController:UIImagePickerControllerSourceTypeCamera];
         } else {
            [AppHelper alertViewWithMessage:@"message.NoCameraPermissionsGranted"];
        }
    }];
    [selectImageController addAction:cameraImageAction];
    if (self.selectImage) {
        UIAlertAction *removeAttachAction = [UIAlertAction actionWithTitle:dynamicLocalizedString(@"selectImageActionSheet.removeFile") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            if ([weakSelf respondsToSelector:@selector(clearImageAttach)]) {
                [weakSelf clearImageAttach];
            }
            weakSelf.buttonAttachImage = [UIImage imageNamed:ImageNameButtonAttachClear];
            if (weakSelf.selectImage) {
                weakSelf.selectImage = nil;
            }
        }];
        [selectImageController addAction:removeAttachAction];
    }
    
    [self presentViewController:selectImageController animated:YES completion:nil];
}

- (void)configureImageAttach:(BOOL)imageAttach
{
    self.buttonAttachImage = imageAttach ? [UIImage imageNamed:ImageNameButtonAttachData] : [UIImage imageNamed:ImageNameButtonAttachClear];
}

- (void)selectImagePickerController:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = sourceType;
    
    picker.navigationBar.barTintColor = self.dynamicService.currentApplicationColor;
    picker.navigationBar.tintColor = [UIColor whiteColor];
    picker.navigationBar.translucent = YES;
    [AppHelper titleFontForNavigationBar:picker.navigationBar];
    picker.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - CustomAccessors

- (void)setSelectImage:(UIImage *)selectImage
{
    _selectImage = selectImage;
    self.buttonAttachImage = selectImage ? [UIImage imageNamed:ImageNameButtonAttachData] : [UIImage imageNamed:ImageNameButtonAttachClear];
}

#pragma mark - Private

- (void)setButtonAttachImage:(UIImage *)buttonAttachImage
{
    _buttonAttachImage = buttonAttachImage;
    [self.attachButton setImage:self.buttonAttachImage forState:UIControlStateNormal];
}

#pragma mark - ImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.selectImage = [UIImage compressImage:info[UIImagePickerControllerEditedImage] compressLessMByte:2];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    self.buttonAttachImage = [UIImage imageNamed:ImageNameButtonAttachData];
}

#pragma mark - IBAction

- (IBAction)selectImage:(id)sender
{
    [self configureActionSheet];
}

@end