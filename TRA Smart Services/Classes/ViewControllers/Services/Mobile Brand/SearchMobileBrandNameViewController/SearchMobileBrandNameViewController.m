//
//  SearchMobileBrandNameViewController.m
//  TRA Smart Services
//
//  Created by Admin on 25.08.15.
//

#import "SearchMobileBrandNameViewController.h"
#import "FeedbackViewController.h"
#import "ListOfDevicesViewController.h"

static NSString *const ListDeviceSegue = @"listOfDevicesSegue";

@interface SearchMobileBrandNameViewController ()

@property (weak, nonatomic) IBOutlet BottomBorderTextField *brandNameTextField;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *requiredIndicatorLabel;

@end

@implementation SearchMobileBrandNameViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.brandNameTextField.text = @"";
}

#pragma mark - IBActions

- (void)showRatingViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FeedbackViewController *feedback = [storyboard instantiateViewControllerWithIdentifier:FeedbackViewControllerIdentifier];
    feedback.modalPresentationStyle = UIModalPresentationOverFullScreen;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    feedback.serviceRatingName = ServiceTypeSearchMobileBrandStringName.capitalizedString;
    
    [self presentViewController:feedback animated:NO completion:nil];
}


- (IBAction)searchButtonTapped:(id)sender
{
    if (self.brandNameTextField.text.length) {
        [self.view endEditing:YES];
        TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];
        __weak typeof(self) weakSelf = self;
        [[NetworkManager sharedManager] traSSNoCRMServicePerformSearchByMobileBrand:self.brandNameTextField.text requestResult:^(id response, NSError *error) {
            if (error) {
                //shayan loader issue solved
                loader.ratingView.hidden = true;
                [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[self hendelServiceError:error response:response]];
            } else {
                [loader dismissTRALoader:YES];
                [weakSelf performSegueWithIdentifier:ListDeviceSegue sender:response];
            }
        }];
    } else {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.EnterBrandName")]];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:ListDeviceSegue]) {
        ListOfDevicesViewController *cont = segue.destinationViewController;
        cont.dataSource = sender;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - SuperclassMethods

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"searchMobileBrandNameViewController.title");
    [self.searchButton setTitle:dynamicLocalizedString(@"searchMobileBrandNameViewController.searchButton") forState:UIControlStateNormal];
    self.brandNameTextField.placeholder = dynamicLocalizedString(@"searchMobileBrandNameViewController.searchTextField.placeholder");
}

- (void)updateColors
{
    [super updateColors];
    
    self.requiredIndicatorLabel.textColor = [DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite ? [UIColor blackColor] : [UIColor redColor];    
    [super updateBackgroundImageNamed:@"img_bg_service"];
}

- (void)setRTLArabicUI
{
    self.brandNameTextField.textAlignment = NSTextAlignmentRight;
    self.requiredIndicatorLabel.textAlignment = NSTextAlignmentLeft;
}

- (void)setLTREuropeUI
{
    self.brandNameTextField.textAlignment = NSTextAlignmentLeft;
    self.requiredIndicatorLabel.textAlignment = NSTextAlignmentRight;
}

#pragma mark - Private

- (void)prepareUI
{
    self.searchButton.layer.cornerRadius = 8;
    self.searchButton.layer.borderWidth = 1;
}

@end
