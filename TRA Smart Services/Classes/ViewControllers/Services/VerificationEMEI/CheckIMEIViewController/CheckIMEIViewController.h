//
//  CheckIMEIViewController.h
//  TRA Smart Services
//
//  Created by Admin on 13.08.15.
//

#import "BarcodeCodeReader.h"

@protocol IMEIDelegate <NSObject>

- (void)IMEIsuccess:(NSString *)IMEI;
@end

@interface CheckIMEIViewController : BaseServiceViewController <BarcodeCodeReaderDelegate, UIAlertViewDelegate>

@property (assign, nonatomic) BOOL needTransparentNavigationBar;

@property (nonatomic, weak) id <IMEIDelegate> delegate;


@property (strong, nonatomic) void (^didFinishWithResult)(NSString *result);

@end
