//
//  CoverageReportViewController.m
//  TRA Smart Services
//
//  Created by Admin on 13.08.15.
//

#import <MapKit/MapKit.h>
#import "CoverageReportViewController.h"
#import "CarrierInfo.h"

@interface CoverageReportViewController () <MKMapViewDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet BottomBorderTextField *addressTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UISlider *signalLevelSlider;
@property (weak, nonatomic) IBOutlet UIButton *reportSignalButton;
@property (weak, nonatomic) IBOutlet UIButton *detectLocationButton;
@property (weak, nonatomic) IBOutlet UILabel *selectedSignalLevel;

@property (assign, nonatomic) CGFloat userSignalStrength;

@property (strong, nonatomic) TRALoaderViewController *loader;
@property (assign, nonatomic) BOOL needToCaptureLocation;

@property (strong, nonatomic) CLLocation *selectedLocation;
@property (strong, nonatomic) CLLocation *currentLocation;


@property (strong, nonatomic) CLGeocoder *geocoder;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) CarrierInfo *carrierInfo;

@property (weak, nonatomic) UIAlertController *locationEnableRequestController;


@end

@implementation CoverageReportViewController
@synthesize selectedLocation = _selectedLocation;

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    
    self.geocoder = [[CLGeocoder alloc] init];
    self.userSignalStrength = -1;
    self.carrierInfo = [CarrierInfo carrierInfoWithUpdateInfoBlock:^{
        if (_userSignalStrength < 0) {
            self.signalLevelSlider.value = self.carrierInfo.signalStrength;
        }
    }];
    
    [self.carrierInfo startUpdating];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(becomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [self prepareButtonTitle];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.signalLevelSlider.value = self.carrierInfo.barsSignalStrength;
    self.selectedSignalLevel.text = self.selectedSignalLevel.text = [NSString stringWithFormat:@"%@ - %@", dynamicLocalizedString(@"coverageLevel.title"), [self titleForSignalStrength:(int)self.carrierInfo.barsSignalStrength]];
    [self sliderDidChnageValue:self.signalLevelSlider];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
   
}

- (void)becomeActive:(NSNotification *)notification {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationEnableRequestController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.carrierInfo stopUpdating];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}



#pragma mark - CustomAccessors

- (void)setCurrentLocation:(CLLocation *)currentLocation {
    _currentLocation = currentLocation;
    if (self.selectedLocation == currentLocation) {
        [self updateAdress:currentLocation];
    }
}

- (void)setSelectedLocation:(CLLocation *)selectedLocation {
    _selectedLocation = selectedLocation;
    [self updateAdress:_selectedLocation];
}

- (CLLocation *)selectedLocation {
    if (!_selectedLocation) {
        return self.currentLocation;
    }
    return _selectedLocation;
}

- (CGFloat)userSignalStrength {
    if (_userSignalStrength >= 0) {
        return _userSignalStrength;
    }
    return self.carrierInfo.barsSignalStrength;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    self.currentLocation = userLocation.location;
}

- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error {
    NSLog(@"%@", error.localizedDescription);
}



#pragma mark - IBAction

- (IBAction)reportSignalButtonTapped:(id)sender
{
    if (self.selectedLocation || self.addressTextField.text.length) {
        TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];
        loader.serviceRatingName = ServiceTypeCoverageStringName.capitalizedString;
        [[NetworkManager sharedManager] traSSNoCRMServicePOSTPoorCoverageAtLatitude:self.selectedLocation.coordinate.latitude longtitude:self.selectedLocation.coordinate.longitude address:self.addressTextField.text signalPower:self.signalLevelSlider.value  requestResult:^(id response, NSError *error) {
            if (error) {
                //shayan loader issue solved
                loader.ratingView.hidden = true;
                [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[self hendelServiceError:error response:response]];
            } else {
                loader.TRALoaderDidClose = ^{
                    [self.navigationController popToRootViewControllerAnimated:NO];
                };
                 loader.ratingView.hidden = NO;

                [loader setCompletedStatus:TRACompleteStatusSuccess withDescription:nil];
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, TRAAnimationDuration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            });
        }];
    } else {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), dynamicLocalizedString(@"message.EmptyInputParameter.Adress")]];
    }
}

- (IBAction)mapTapGesture:(UITapGestureRecognizer *)sender {
    MKPointAnnotation *annotation;
    if (self.mapView.annotations.count) {
        annotation = self.mapView.annotations.firstObject;
        [self.mapView removeAnnotations:self.mapView.annotations];
    } else {
        annotation = [[MKPointAnnotation alloc] init];
    }
    CLLocationCoordinate2D coordinate = [self.mapView convertPoint:[sender locationInView:self.mapView] toCoordinateFromView:self.mapView];
    annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = coordinate;
    [self.mapView addAnnotation:annotation];
    self.selectedLocation = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    [self.mapView setUserTrackingMode:MKUserTrackingModeNone animated:YES];
    MKCoordinateRegion region = self.mapView.region;
    region.center = coordinate;
    [self.mapView setRegion:region animated:YES];
}

- (IBAction)sliderDidChnageValue:(UISlider *)sender
{
    sender.value = roundf(sender.value / sender.maximumValue * 5) * sender.maximumValue / 5;
    self.userSignalStrength = sender.value;
    self.selectedSignalLevel.text = [NSString stringWithFormat:@"%@ - %@", dynamicLocalizedString(@"coverageLevel.title"), [self titleForSignalStrength:(int)sender.value]];
}

- (NSString *)titleForSignalStrength:(int)strength {
    NSString *value;
    switch (strength) {
        case 1: {
            value = dynamicLocalizedString(@"coverageReport.very_weak");
            break;
        }
        case 2: {
            value = dynamicLocalizedString(@"coverageReport.weak");
            break;
        }
        case 3: {
            value = dynamicLocalizedString(@"coverageReport.good");
            break;
        }
        case 4: {
            value = dynamicLocalizedString(@"coverageReport.strong");
            break;
        }
        case 5: {
            value = dynamicLocalizedString(@"coverageReport.veryStrong");
            break;
        }
        default:
            value = dynamicLocalizedString(@"coverageReport.very_weak");
            break;
    }
    return value;
}

- (IBAction)detectLocationButtonTapped:(id)sender
{
    [self checkAndRequestAuthorizationStatus];
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    [self.mapView removeAnnotations:self.mapView.annotations];
    self.selectedLocation = nil;
    self.userSignalStrength = -1;
    self.signalLevelSlider.value = self.userSignalStrength;
    self.selectedSignalLevel.text = [NSString stringWithFormat:@"%@ - %@", dynamicLocalizedString(@"coverageLevel.title"), [self titleForSignalStrength:(int)self.userSignalStrength]];
    [self updateAdress:self.selectedLocation];
}

- (void)MBProgressHUDCancelButtonDidPressed
{
    [self.loader dismissTRALoader:YES];
    [[NetworkManager sharedManager] cancelAllOperations];
    [[LocationManager sharedManager] stopUpdatingLocation];
    [self.activityIndicator stopAnimating];
}

#pragma mark - SuperclassMethods

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"coverageLevel.title");
    self.selectedSignalLevel.text = [NSString stringWithFormat:@"%@ - %@", dynamicLocalizedString(@"coverageLevel.title"), dynamicLocalizedString(@"coverageReport.very_weak")];
    [self.reportSignalButton setTitle:dynamicLocalizedString(@"coverageReport.reportSignalButton.title") forState:UIControlStateNormal];
    [self.detectLocationButton setTitle:dynamicLocalizedString(@"coverageReport.detectLocationButton.title") forState:UIControlStateNormal];
    self.addressTextField.placeholder = dynamicLocalizedString(@"coverageReport.addressTextField");
}

- (void)updateColors
{
    [super updateColors];
    
    UIColor *color = [self.dynamicService currentApplicationColor];
    self.activityIndicator.color = color;
    self.signalLevelSlider.minimumTrackTintColor = color;
    self.selectedSignalLevel.textColor = [self.dynamicService currentApplicationColor];
    [super updateBackgroundImageNamed:@"img_bg_service"];
}

- (void)setRTLArabicUI
{
    self.signalLevelSlider.layer.transform = CATransform3DMakeScale( - 1, 1, 1);
}

- (void)setLTREuropeUI
{
    self.signalLevelSlider.layer.transform = CATransform3DIdentity;
}

#pragma mark - Private



- (void)prepareButtonTitle
{
    [self minimumScaleFactorTitleButton:self.reportSignalButton];
    //shayan
   // [self minimumScaleFactorTitleButton:self.detectLocationButton];
}

- (void)minimumScaleFactorTitleButton:(UIButton *)button
{
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    button.titleLabel.minimumScaleFactor = 0.7f;
}

- (void)checkAndRequestAuthorizationStatus {
    if (![CLLocationManager locationServicesEnabled]) {
        [self requestLocationEnabling];
        return;
    }
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    switch (status) {
        case kCLAuthorizationStatusAuthorizedAlways:
            
            
            return;
            break;
        case kCLAuthorizationStatusDenied:
            [self openAppSettings];
            break;
        default:
            [self requestAuthorizeLocationService];
            break;
    }
}

- (void)openAppSettings {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Location service disabled for this app" message:@"For detect your current location please oppen Settings.app and enable location service for this app" preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [controller addAction:[UIAlertAction actionWithTitle:@"Open Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
            return ;
        }
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }]];
    
    [self presentViewController:controller animated:YES completion:^{
        
    }];
    
    self.locationEnableRequestController = controller;
    
}

- (void)updateAdress:(CLLocation *)location {
    if (self.geocoder.isGeocoding) {
        [self.geocoder cancelGeocode];
    }
    __weak typeof(self) weakSelf = self;
    [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        CLPlacemark *placemark = placemarks.firstObject;
        NSMutableString *addres = [[NSMutableString alloc] init];
        [addres appendFormat:@"%@", placemark.name.length ? placemark.name : @""];
        [addres appendFormat:placemark.name.length ? @", %@" : @"%@", placemark.country.length ? placemark.country : @""];
        weakSelf.addressTextField.text = addres;
    }];
}

- (void)requestLocationEnabling {
    NSLog(@"request enable");
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Location service disabled" message:@"For detect your current location please oppen Settings.app and enable location service" preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:controller animated:YES completion:^{
        
    }];
    
    self.locationEnableRequestController = controller;
}

- (void)requestAuthorizeLocationService {
    if (!self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
    }
    [self.locationManager requestWhenInUseAuthorization];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
}

@end
