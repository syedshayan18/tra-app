//
//  DynamicalServiceViewController.m
//  TRA Smart Services
//
//  Created by Admin on 06.11.15.
//

#import "UIImage+Compression.h"

//viewControllers
#import "DynamicPageServiceViewController.h"

//models
#import "DynamicPage.h"

static CGFloat const HeigthPageSwichControl = 50.;

static CGFloat const HeigthDynamicButton = 80.;
static CGFloat const HeigthDynamicDefault = 60.;
static CGFloat const heigthDynamicTextView = 55.;
static CGFloat const heigthDynamicTreeView = 200.;

@interface DynamicPageServiceViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSMutableArray <DynamicInputItem *> *dataSource;
@property (strong, nonatomic) NSMutableArray <DynamicInputItem *> *pastStateIndexes;


@end

@implementation DynamicPageServiceViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNibs];
    [self prepareDataSource];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (self.totalPageCount - 1) == self.selectedPage.dynamicPageNumber ? 2 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section ? 1 : self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section) {
        UITableViewCell *cellButton = [self prepareDynamicButtonTableViewCell];
        return cellButton;
    } else {
        BaseDynamicTableViewCell *cell;
        DynamicInputItem *dynamicInputItem = self.dataSource[indexPath.row];
        switch (dynamicInputItem.dynamicInputItemInputType) {
            case InputItemTypeLabel: {
                cell = [self.tableView dequeueReusableCellWithIdentifier:DynamicLabelTableViewCell.identifier];
                break;
            }
            case InputItemTypeCRMtype:
            case InputItemTypeTextField:
            case InputItemTypePicker:
            case InputItemTypeDatePicker:
            case InputItemTypeSelectFile: {
                cell = [self.tableView dequeueReusableCellWithIdentifier:DynamicTextFieldTableViewCell.identifier];
                break;
            }
            case InputItemTypeTextView: {
                cell = [self.tableView dequeueReusableCellWithIdentifier:DynamicTextViewTableViewCell.identifier];
                break;
            }
            case InputItemTypeTable: {
                cell = [self.tableView dequeueReusableCellWithIdentifier:DynamicWorkQueueTableviewCell.identifier];
                break;
            }
            case InputItemTypeTreeView: {
                cell = [self.tableView dequeueReusableCellWithIdentifier:DynamicTreeViewTableViewCell.identifier];
                break;
            }
            case InputItemTypeSwitch: {
                cell = [tableView dequeueReusableCellWithIdentifier:DynamicSwitchTableViewCell.identifier];
            
                break;
            }
            case InputItemTypeSelectionView: {
                cell = [tableView dequeueReusableCellWithIdentifier:DynamicSelectionViewTableViewCell.identifier];
               
                ((DynamicSelectionViewTableViewCell *)cell).currentViewController = self;
             
            }
        }
        cell.dynamicInputItem = dynamicInputItem;
        cell.delegate = self;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section) {
        return HeigthDynamicButton;
    } else {
        DynamicInputItem *dynamicInputItem = self.dataSource[indexPath.row];
        CGFloat heightCell = HeigthDynamicDefault;
        switch (dynamicInputItem.dynamicInputItemInputType) {
            case InputItemTypeTextView:{
                heightCell = heigthDynamicTextView;
                break;
            }
            case InputItemTypeTable:{
                heightCell = SCREEN_HEIGHT - [[UIApplication sharedApplication] statusBarFrame].size.height - self.navigationController.navigationBar.frame.size.height - HeigthPageSwichControl - HeigthDynamicDefault - self.tabBarController.tabBar.frame.size.height;
                break;
            }
            case InputItemTypeTreeView:{
                heightCell = heigthDynamicTreeView;
                break;
            }
            case InputItemTypeLabel : {
                CGFloat height = [self heigthDynamicLabelTableViewCell:indexPath];
                heightCell = height > heightCell ? height : heightCell;
                break;
            }
            default:
                break;
        }
        return  heightCell;
    }
}

#pragma mark - BaseDynamicTableViewCellDelegate

- (void)dynamicCellDidBecomeFirstResponder:(BaseDynamicTableViewCell *)cell
{
    self.selectedIndexPath = [self.tableView indexPathForCell:cell];
    [self updateDataSourceDynamicIntutItem:self.selectedIndexPath.row];
}

#pragma mark - DynamicTextFieldTableViewCellDelegate

- (void)dynamicCellDidTappedAttachmentButton:(DynamicTextFieldTableViewCell *)cell
{
    self.selectedIndexPath = [self.tableView indexPathForCell:cell];
    [self configureActionSheet];
}

- (void)dynamicCellTextFieldDidBecomeFirstResponder:(BottomBorderTextField *)textField cell:(DynamicTextFieldTableViewCell *)cell;
{
    self.selectedIndexPath = [self.tableView indexPathForCell:cell];
}

#pragma mark - DynamicButtonTableViewCellDelegate

- (void)dynamicButtonDidTappedInCell:(DynamicButtonTableViewCell *)cell
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(pageServiceViewControllerPOSTButtonDidTapped)]) {
        [self.delegate pageServiceViewControllerPOSTButtonDidTapped];
    }
}

#pragma mark - ImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.selectImage = [UIImage compressImage:info[UIImagePickerControllerEditedImage] compressLessMByte:2];
    [picker dismissViewControllerAnimated:YES completion:^{
        if (self.selectImage) {
            [self uploadImageForItem:self.dataSource[self.selectedIndexPath.row]];
        }
    }];
}

- (void)clearImageAttach
{
    if ([self.dataSource[self.selectedIndexPath.row] clientUploadedImageId]) {
        [self.dataSource[self.selectedIndexPath.row] setClientUploadedImageId:nil];
        [self.tableView reloadRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark - Networking UploadImage

- (void)uploadImageForItem:(DynamicInputItem *)item
{
    NSData *imageData = [UIImage dataCompressImage:self.selectImage compressLessMByte:2];
    NSString *base64PhotoString = [imageData base64EncodedStringWithOptions:kNilOptions];
    item.clientUploadedImageId = [ImagePrefixBase64String stringByAppendingString:base64PhotoString];
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    
//    __weak typeof(self) weakSelf = self;
//    TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:@"Upload Attachment" closeButton:NO];
//    
//    [[NetworkManager sharedManager] traSSCRMDynamicServicePOSTImage:[UIImage compressImage:self.selectImage compressLessMByte:2] imagePOSTResult:^(id response, NSError *error) {
//        if (response && !error) {
//            item.clientUploadedImageId = [response valueForKey:@"attachmentId"];
//            [loader dismissTRALoader:YES];
//        } else {
//            [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[self hendelServiceError:error response:response]];
//        }
//        self.selectImage = nil;
//        [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
//    }];
}

#pragma mark - Superclass

- (void)localizeUI
{
    [self.tableView reloadData];
}

- (void)updateColors
{
    
}

#pragma mark - Private
#pragma mark - Logic Show-Hide dynamicPageInputItems

- (void)prepareDataSource
{
    self.dataSource = [[NSMutableArray alloc] init];
    for (DynamicInputItem *item in self.selectedPage.dynamicPageInputItems) {
        if (!item.displayHidden) {
            [self.dataSource addObject:item];
            if (item.clientSelectPicker) {
                [self updateSecondaryHiddenDynamicInputItem:item];
            }
        }
    }
}

- (void)updateDataSourceDynamicIntutItem:(NSInteger)index
{
    DynamicInputItem *inputItem = self.dataSource[index];
    if (!inputItem.dynamicInputItemDataSource.count) {
        return;
    }
    
    [self updateSecondaryHiddenDynamicInputItem:inputItem];
    
    self.pastStateIndexes = [self.dataSource mutableCopy];
    
    [self prepareDataSource];
    [self updateDataTableView];
}

- (void)updateSecondaryHiddenDynamicInputItem:(DynamicInputItem *)inputItem
{
    NSArray *showArray = @[];
    NSArray *hideArray = @[];
    if (inputItem.clientSelectPicker) {
        NSDictionary *valueDictionary = inputItem.dynamicInputItemDataSource[inputItem.clientSelectPicker - 1];
        showArray = [valueDictionary valueForKey:@"show"];
        hideArray = [valueDictionary valueForKey:@"hide"];
    }
    for (NSNumber *numberSecondary in inputItem.dynamicSecondaryItems) {
        DynamicInputItem *inputItemSecondary = self.selectedPage.dynamicPageInputItems[[numberSecondary integerValue]];
        [self prepareInputItemHiddenDefault:inputItemSecondary];
    }
    for (NSNumber *numberSecondary in showArray) {
        DynamicInputItem *inputItemSecondary = self.selectedPage.dynamicPageInputItems[[numberSecondary integerValue]];
        inputItemSecondary.displayHidden = NO;
    }
    for (NSNumber *numberSecondary in hideArray) {
        DynamicInputItem *inputItemSecondary = self.selectedPage.dynamicPageInputItems[[numberSecondary integerValue]];
        inputItemSecondary.displayHidden = YES;
    }
}

- (void)prepareInputItemHiddenDefault:(DynamicInputItem *)dynamicInputItem
{
    dynamicInputItem.displayHidden = dynamicInputItem.dynamicInputItemHidden;
    for (NSNumber *numberSecondary in dynamicInputItem.dynamicSecondaryItems) {
        DynamicInputItem *inputItemSecondary = self.selectedPage.dynamicPageInputItems[[numberSecondary integerValue]];
        if (inputItemSecondary != dynamicInputItem) {
            [self prepareInputItemHiddenDefault:inputItemSecondary];
        }
    }
}

- (void)updateDataTableView
{
    NSMutableArray *removeIndexPaths = [[NSMutableArray alloc] init];
    for (DynamicInputItem *item in self.pastStateIndexes) {
        if (![self.dataSource containsObject:item]) {
            NSUInteger index = [self.pastStateIndexes indexOfObject:item];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [removeIndexPaths addObject:indexPath];
            [self clearClientDataDynamicInputItem:item];
        }
    }
    NSMutableArray *addedIndexPaths = [[NSMutableArray alloc] init];
    for (DynamicInputItem *item in self.dataSource) {
        if (![self.pastStateIndexes containsObject:item]) {
            NSUInteger index = [self.dataSource indexOfObject:item];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [addedIndexPaths addObject:indexPath];
        }
    }
    NSMutableArray *reloadIndexPaths = [[NSMutableArray alloc] init];
    if (removeIndexPaths.count && addedIndexPaths.count) {
        if (removeIndexPaths.count > addedIndexPaths.count) {
            for (NSInteger i = addedIndexPaths.count - 1; i >= 0; i--) {
                [reloadIndexPaths addObject:addedIndexPaths[i]];
                [removeIndexPaths removeObjectAtIndex:i];
            }
            [addedIndexPaths removeAllObjects];
        } else {
            for (NSInteger i = removeIndexPaths.count - 1; i >= 0; i--) {
                [reloadIndexPaths addObject:removeIndexPaths[i]];
                [addedIndexPaths removeObjectAtIndex:i];
            }
            [removeIndexPaths removeAllObjects];
        }
    }
    [self.tableView beginUpdates];
    if (reloadIndexPaths.count) {
        [self.tableView reloadRowsAtIndexPaths:reloadIndexPaths withRowAnimation:UITableViewRowAnimationBottom];
    }
    if (removeIndexPaths.count) {
        [self.tableView deleteRowsAtIndexPaths:removeIndexPaths withRowAnimation:UITableViewRowAnimationTop];
    }
    if (addedIndexPaths.count) {
        [self.tableView insertRowsAtIndexPaths:addedIndexPaths withRowAnimation:UITableViewRowAnimationTop];
    }
    [self.tableView endUpdates];
}

- (void)clearClientDataDynamicInputItem:(DynamicInputItem *)item
{
    item.clientSelectPicker = 0;
    item.clientString = @"";
}

#pragma mark - PrepareDynamicCell

- (UITableViewCell *)prepareDynamicButtonTableViewCell
{
    DynamicButtonTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:DynamicButtonTableViewCell.identifier];
    cell.dynamicButton.backgroundColor = [DynamicUIService service].currentApplicationColor;
    [cell.dynamicButton setTitle:self.buttonTitle forState:UIControlStateNormal];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    return cell;
}

- (void)registerNibs
{
    [DynamicTextViewTableViewCell registerWithTableView:self.tableView];
    [DynamicTextFieldTableViewCell registerWithTableView:self.tableView];
    [DynamicButtonTableViewCell registerWithTableView:self.tableView];
    [DynamicWorkQueueTableviewCell registerWithTableView:self.tableView];
    [DynamicTreeViewTableViewCell registerWithTableView:self.tableView];
    [DynamicSelectionViewTableViewCell registerWithTableView:self.tableView];
    [DynamicSwitchTableViewCell registerWithTableView:self.tableView];
    [DynamicLabelTableViewCell registerWithTableView:self.tableView];
}

- (CGFloat)heigthDynamicLabelTableViewCell:(NSIndexPath *)indexPath
{
    CGFloat marginWidth = 32.;
    CGFloat marginHeigth = 20.;
    
    static DynamicLabelTableViewCell *sizingCell;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:DynamicLabelTableViewCell.identifier];
    });
    
    DynamicInputItem *dynamicInputItem = self.dataSource[indexPath.row];
    
    sizingCell.dynamicInputItem = dynamicInputItem;
    CGFloat fixedWidth = SCREEN_WIDTH - marginWidth;
    CGSize newSize = [sizingCell.descriptionLabel sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    
    return newSize.height + marginHeigth;
}

@end
