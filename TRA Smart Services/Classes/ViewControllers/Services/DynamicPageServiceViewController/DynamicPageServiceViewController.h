//
//  DynamicalServiceViewController.h
//  TRA Smart Services
//
//  Created by Admin  on 06.11.15.
//

#import "BaseSelectImageViewController.h"

//views
#import "DynamicTextFieldTableViewCell.h"
#import "DynamicTextViewTableViewCell.h"
#import "DynamicButtonTableViewCell.h"
#import "DynamicWorkQueueTableviewCell.h"
#import "DynamicTreeViewTableViewCell.h"
#import "DynamicSelectionViewTableViewCell.h"
#import "DynamicSwitchTableViewCell.h"
#import "DynamicLabelTableViewCell.h"

@class DynamicPage;

@protocol DynamicPageServiceViewControllerDelegate <NSObject>

@optional
- (void)pageServiceViewControllerPOSTButtonDidTapped;

@end

@interface DynamicPageServiceViewController : BaseSelectImageViewController <UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, DynamicTextFieldTableViewCellDelegate, DynamicButtonTableViewCellDelegate, BaseDynamicTableViewCellDelegate>

@property (weak, nonatomic) id <DynamicPageServiceViewControllerDelegate> delegate;

@property (strong, nonatomic) DynamicPage *selectedPage;
@property (copy, nonatomic) NSString *buttonTitle;
@property (assign, nonatomic) NSInteger totalPageCount;

@end