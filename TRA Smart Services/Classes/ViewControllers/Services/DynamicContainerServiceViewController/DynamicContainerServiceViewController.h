//
//  DynamicContainerServiceViewController.h
//  TRA Smart Services
//
//  Created by Admin on 06.11.15.
//

#import "DynamicTextFieldTableViewCell.h"
#import "DynamicTextViewTableViewCell.h"
#import "DynamicButtonTableViewCell.h"
#import "DynamicPageServiceViewController.h"
#import "NavigationPageView.h"

static NSString *const DynamicServiceIDstoryboard = @"dynamicServiceID";

@interface DynamicContainerServiceViewController : BaseServiceViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate, NavigationPageViewDelegate, DynamicPageServiceViewControllerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) TransactionModel *transactionModel;

@end
