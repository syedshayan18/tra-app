//
//  DynamicContainerServiceViewController.m
//  TRA Smart Services
//
//  Created by Admin on 06.11.15.
//

#import "DynamicContainerServiceViewController.h"
#import "TRALoaderViewController.h"
#import "DynamicPage.h"
#import "TransactionModel.h"
#import "LoginViewController.h"

static CGFloat const PageSwitchControlViewHeight = 150.;

@interface DynamicContainerServiceViewController ()

@property (weak, nonatomic) IBOutlet NavigationPageView *pageSwitchControlView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageSwitchControlViewHeightConstraint;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (assign, nonatomic) NSInteger selectedPage;

@end

@implementation DynamicContainerServiceViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setInitialParameters];
    [self downloadDynamicServiceDetails];
    [self addInfoButtonToNavigationBar];
}

#pragma mark - SwitchControlViewDelegate

- (void)selectedNavigationPage:(NSInteger)selectPage
{
    DynamicPageServiceViewController *currentViewController = [self.pageViewController.viewControllers objectAtIndex:0];
    DynamicPageServiceViewController *requiredViewController = (DynamicPageServiceViewController *)[self viewControllerAtIndex:selectPage - 1];
    
    if (currentViewController.selectedPage.dynamicPageNumber != requiredViewController.selectedPage.dynamicPageNumber) {
        DynamicPage *page = self.dynamicServiceModel.dynamicServicePages[currentViewController.selectedPage.dynamicPageNumber];
        for (DynamicInputItem *item in page.dynamicPageInputItems) {
            if (![self validateDynamicInputItem:item]) {
                self.pageSwitchControlView.elementSelected = currentViewController.selectedPage.dynamicPageNumber + 1;
                return;
            }
        }
        NSInteger currentViewControllerIndex = currentViewController.selectedPage.dynamicPageNumber;
        NSInteger tappedViewControllerIndex = requiredViewController.selectedPage.dynamicPageNumber;
        
        UIPageViewControllerNavigationDirection animationDirection = currentViewControllerIndex < tappedViewControllerIndex ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse;
        
        [self.pageViewController setViewControllers:@[requiredViewController] direction:animationDirection animated:YES completion:nil];
    }
}

#pragma mark - UIPageViewControllerDataSource

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    DynamicPageServiceViewController *nextViewController = (DynamicPageServiceViewController *)viewController;
    NSInteger index = nextViewController.selectedPage.dynamicPageNumber - 1;
    if (index && index > self.dynamicServiceModel.dynamicServicePages.count - 1) {
        return nil;
    } else {
        return [self viewControllerAtIndex:index];
    }
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    DynamicPageServiceViewController *prevViewController = (DynamicPageServiceViewController *)viewController;
    NSInteger index = prevViewController.selectedPage.dynamicPageNumber + 1;
    
    if (index && index > self.dynamicServiceModel.dynamicServicePages.count - 1) {
        return nil;
    } else {
        return [self viewControllerAtIndex:index];
    }
}

#pragma mark - UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed) {
        DynamicPageServiceViewController *currentViewController = [self.pageViewController.viewControllers objectAtIndex:0];
        self.pageSwitchControlView.elementSelected = currentViewController.selectedPage.dynamicPageNumber + 1;
    }
}

#pragma mark - DynamicPageServiceViewControllerDelegate

- (void)pageServiceViewControllerPOSTButtonDidTapped
{
    
    if ( self.dynamicServiceModel.dynamicServiceNeedAuth == NO) {
        NSString *arPDF = self.dynamicServiceModel.urlAR;
        NSString *enPDF = self.dynamicServiceModel.urlEN;
        
        if (![arPDF isEqualToString:@""] && ![enPDF isEqualToString:@""] ) {
            NSString *currentPDF = [DynamicUIService service].language == LanguageTypeArabic ? arPDF : enPDF;
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:currentPDF]];
            return;
        }
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    for (DynamicPage *page in self.dynamicServiceModel.dynamicServicePages) {
        for (DynamicInputItem *item in page.dynamicPageInputItems) {
            
            if (![self validateDynamicInputItem:item]) {
                return;
            }
            if (item.displayHidden && item.dynamicInputItemInputType != InputItemTypeCRMtype) {
                continue;
            }
            id value;
            switch (item.dynamicInputItemInputType) {
                case InputItemTypeCRMtype: {
                    value = item.dynamicInputItemValue;
                    break;
                }
                case InputItemTypeTextView:
                case InputItemTypeTextField: {
                    value = item.clientString;
                    break;
                }
                case InputItemTypeDatePicker: {
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
                    NSString *dateString = [formatter stringFromDate:item.clientDate];
                    value = dateString;
                    break;
                }
                case InputItemTypeSelectFile: {
                    if (![item.clientUploadedImageId isEqualToString:@"image"]) {
                        value = item.clientUploadedImageId;
                    }
                    break;
                }
                case InputItemTypePicker:
                case InputItemTypeSwitch:
                case InputItemTypeSelectionView: {
                    if (item.clientSelectPicker) {
                        value = [item.dynamicInputItemDataSource[item.clientSelectPicker - 1] valueForKey:@"value"];
                    }
                    break;
                }
                case InputItemTypeTable: {
                    //todo
                    break;
                }
                case InputItemTypeTreeView: {
                    //todo
                    break;
                }
                case InputItemTypeLabel :{
                    break;
                }
            }
            [parameters setValue:value forKey:item.dynamicInputItemName];
        }
    }
    
    __weak typeof(self) weakSelf = self;
    [self prefixLoginSend:^{
        [weakSelf sendWithParameters:parameters];
    }];
}

#pragma mark - Superclass

- (void)localizeUI
{
    self.title = [self.dynamicServiceModel localizableString:self.dynamicServiceModel.dynamicServiceName];
    [self.pageSwitchControlView updateUILocalize];
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"img_bg_service"];
    [self.pageSwitchControlView updateUIColor];
}

#pragma mark - Private

#pragma mark - Validation

- (BOOL)validateDynamicInputItem:(DynamicInputItem *)item
{
    NSString *string = [item.clientString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (item.dynamicInputItemRequred && !item.displayHidden && (item.dynamicInputItemInputType == InputItemTypeTextField || item.dynamicInputItemInputType == InputItemTypeTextView) && !string.length ) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:dynamicLocalizedString(@"message.EmptyInputParameter.String"), [item localizableString:item.dynamicInputItemDisplayName]]];
        return NO;
    } else if (item.dynamicInputItemRequred && !item.displayHidden && (item.dynamicInputItemInputType == InputItemTypeSelectionView || item.dynamicInputItemInputType == InputItemTypeSwitch) && !item.clientSelectPicker) {
        [AppHelper alertViewWithMessage:[NSString stringWithFormat:@"%@ %@", dynamicLocalizedString(@"message.PleaseChoose"), [item localizableString:item.dynamicInputItemDisplayName]]];
        return NO;
    }
    
    BOOL isValid = YES;
    switch (item.dynamicInputItemValidationType) {
        case ValidationTypeEmail: {
            isValid = [string isValidEmailUseHardFilter:NO];
            if (!isValid) {
                [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatEmail")];
            }
            break;
        }
        case ValidationTypeNumber: {
            isValid = [string isValidNumber];
            if (!isValid) {
                [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.IncorrectInputDataFormat.Number")];
            }
            break;
        }
        case ValidationTypeURL : {
            isValid = [string isValidURL];
            if (!isValid) {
                [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.InvalidFormatURL")];
            }
            break;
        }
        case ValidationTypeString : {
            isValid = [string isValidString];
            if (!isValid) {
                [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.IncorrectInputDataFormat.String")];
            }
            break;
        }
        case ValidationTypeNone: {
            break;
        }
    }
    return isValid;
}

#pragma mark - DynamicPageViewControllerPreparaton

- (UIViewController *)viewControllerAtIndex:(NSInteger)controllerIndex
{
    DynamicPageServiceViewController *dynamicPageViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DynamicPageServiceViewController class])];
    dynamicPageViewController.selectedPage = self.dynamicServiceModel.dynamicServicePages[controllerIndex];
    dynamicPageViewController.totalPageCount = self.dynamicServiceModel.dynamicServicePages.count;
    dynamicPageViewController.delegate = self;
    dynamicPageViewController.buttonTitle = [self.dynamicServiceModel localizableString:self.dynamicServiceModel.dynamicServiceRequestButtonTitle];
    
    return dynamicPageViewController;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Preparation

- (void)prepareLanguageSelectorSwitch
{
    if (self.dynamicServiceModel.dynamicServicePages.count > 1) {
        self.pageSwitchControlViewHeightConstraint.constant = PageSwitchControlViewHeight;
        self.pageSwitchControlView.delegate = self;
        
        self.pageSwitchControlView.elementsCount = self.dynamicServiceModel.dynamicServicePages.count;
        
    } else {
        self.pageSwitchControlViewHeightConstraint.constant = 0.;
    }
}

- (void)prepareFirstPageViewController
{
    self.pageViewController = self.childViewControllers[0];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    [self.pageViewController setViewControllers:@[[self viewControllerAtIndex:self.selectedPage]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (void)setInitialParameters
{
    self.pageSwitchControlView.elementsCount = 0;
    self.selectedPage = 0;
}

#pragma mark - Network Methods

- (void)downloadDynamicServiceDetails
{
    NSString *requestName = [self.dynamicServiceModel localizableString:self.dynamicServiceModel.dynamicServiceName];
    TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:dynamicLocalizedString(requestName) closeButton:NO];
    
    __weak typeof(self) weakSelf = self;
    void(^ResponseDynamicServiceModel)(id response, NSError *error) = ^(id response, NSError *error){
        if (error) {
            //shayan loader issue solved
            loader.ratingView.hidden = true;
            [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[self hendelServiceError:error response:response]];
        } else {
            if ([response isKindOfClass:[DynamicServiceModel class]]) {
                if (((DynamicServiceModel *)response).dynamicServicePages.count) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        weakSelf.dynamicServiceModel = response;
                        [weakSelf prepareLanguageSelectorSwitch];
                        [weakSelf prepareFirstPageViewController];
                    });
                } else {
                    [AppHelper alertViewWithMessage:dynamicLocalizedString(@"api.message.serverError") delegate:weakSelf];
                }
            }
            [loader dismissTRALoader:YES];
        }
    };
    
    if (self.transactionModel) {
        [[NetworkManager sharedManager] traSSCRMServiceGetTransactionByID:self.transactionModel.transactionID type:self.transactionModel.type responseBlock:ResponseDynamicServiceModel];
    } else {
        [[NetworkManager sharedManager] traSSCRMServiceGetServiceDetails:self.dynamicServiceModel.dynamicServiceID responseBlock:ResponseDynamicServiceModel];
        
    }
}

- (void)sendWithParameters:(NSDictionary *)parameters
{
    TRALoaderViewController *loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:[self.dynamicServiceModel localizableString:self.dynamicServiceModel.dynamicServiceRequestButtonTitle] closeButton:NO];
    
    loader.serviceRatingName = self.dynamicServiceModel.dynamicServiceID;
    
    __weak typeof(self) weakSelf = self;
    void(^ResponseSendDynamicService)(id response, NSError *error) = ^(id response, NSError *error) {
        if (error) {
            //shayan loader issue solved
            loader.ratingView.hidden = true;
            [loader setCompletedStatus:TRACompleteStatusFailure withDescription:[self hendelServiceError:error response:response]];
        } else {
            
            loader.TRALoaderDidClose = ^{
                [weakSelf.navigationController popToRootViewControllerAnimated:NO];
            };
            loader.ratingView.hidden = false;

            [loader setCompletedStatus:TRACompleteStatusSuccess withDescription:nil];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, TRAAnimationDuration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            //loader.ratingView.hidden = NO;
        });
    };
    
    if (self.transactionModel) {
        NSMutableDictionary *tansactionParametrs = [[self.transactionModel modelToDictionary] mutableCopy];
        [tansactionParametrs addEntriesFromDictionary:parameters];
        [tansactionParametrs setValue:self.dynamicServiceModel.dynamicServiceID forKey:@"_id"];
        
        [[NetworkManager sharedManager] traSSCRMDynamicServicePutEditTransactions:self.transactionModel.transactionID parametrs:tansactionParametrs responseBlock:ResponseSendDynamicService];
    } else {
        [[NetworkManager sharedManager] traSSCRMDynamicServicePOSTServiceReportWithServiceID:self.dynamicServiceModel.dynamicServiceID parameters:parameters postResponse:ResponseSendDynamicService];
    }
}

@end
