//
//  ViewController.h
//  testPentagonCells
//
//  Created by Admin on 30.07.15.
//

typedef NS_ENUM(NSUInteger, NavigationTransitionType) {
    NavigationTransitionTypeDefault,
    NavigationTransitionTypeComplaintASP,
    NavigationTransitionTypeComplainTRA,
    NavigationTransitionTypeSearch
};

#import "HomeTopBarView.h"

@interface HomeViewController : BaseDynamicUIViewController <UICollectionViewDataSource, UICollectionViewDelegate, HomeTopBarViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, UICollectionViewDelegateFlowLayout>

@property (assign, nonatomic) NavigationTransitionType navigationTransitionType;

- (void)navigationTransitionViewController:(NavigationTransitionType)navigationType;

@end