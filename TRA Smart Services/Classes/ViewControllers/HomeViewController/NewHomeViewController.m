//
//  NewHomeViewController.m
//  TRA Smart Services
//
//  Created by Shayan Ali on 13/09/2018.
//  Copyright © 2018 UAE TRA. All rights reserved.
//

#import "NewHomeViewController.h"
#import "HomeTopBarView.h"
#import "DynamicServiceModel.h"
#import "MenuCollectionViewCell.h"
#import "CategoryCollectionViewCell.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "NotificationViewController.h"
#import "UserProfileViewController.h"
#import "HomeSearchViewController.h"
#import "KeychainStorage.h"
#import "TutorialViewController.h"
#import "DynamicServiceModel.h"
#import "DynamicContainerServiceViewController.h"
#import "FakeSplashViewController.h"
#import "SpamReportViewController.h"
#import "ServiceModel.h"
#import "TRADynamicService.h"
#import "UIImageView+AFNetworking.h"
#import "DynamicListServicesViewController.h"
#import "NewHomeMenuCollectionViewCell.h"
#import "HeaderViewCollectionReusableView.h"

static CGFloat const CellSpacing = 5.f;
static CGFloat const RowCount = 4.f;
static CGFloat const CellSubmenuHeight = 140.f;
static CGFloat const ZigZagViewTag = 1001;
static CGFloat const TopViewHeightMultiplierValue = 0.11f;
static CGFloat const MaxScaleFactorForSpeedAccessCell = 1.0f;
static NSString *const HomeBarcodeReaderSegueIdentifier = @"HomeBarcodeReaderSegue";
static NSString *const HomeCheckDomainSegueIdentifier = @"HomeCheckDomainSegue";
static NSString *const HomeToHelpSalimSequeIdentifier = @"HomeToHelpSalimSeque";
static NSString *const HomeToCoverageSwgueIdentifier = @"HomeToCoverageSegue";
static NSString *const HomeToSpamReportSegueidentifier = @"HomeToSpamReportSegue";
static NSString *const HomeToSearchBrandNameSegueIdentifier = @"homeToListOfDeviceBrands"; //@"HomeToSearchBrandNameSegue";
static NSString *const HomeToNotificationSegueIdentifier = @"HomeToNotificationSegue";
static NSString *const HomeToUserProfileSegueIdentifier = @"UserProfileSegue";
static NSString *const HomeToHomeSearchSegueIdentifier = @"HomeToHomeSearchSegue";
static NSString *const HomeToDynamicContainerSegueIdentifier = @"HomeToDynamicContainer";
static NSString *const HomeToComplaintEnquiresSequeIdentifier = @"HomeToComplaintEnquiresSeque";
static NSString *const HomeToComplaintTRASequeIdentifier = @"HomeToComplaintTRASeque";
static NSString *const HomeToDynamicListServicesSegueIdentifier = @"HomeToDynamicListServicesSegue";

static LanguageType startLanguage;

@interface NewHomeViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *speedAccessCollectionViewTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet HomeTopBarView *topView;


@property (strong, nonatomic) NSMutableArray *servicesModelDataSource;
@property (strong, nonatomic) NSMutableArray *speedAccessDataSource;
@property (strong, nonatomic) NSMutableArray *otherServiceDataSource;
@property (strong, nonatomic) NSMutableArray *dynamicServiceDataSorce;

@property (assign, nonatomic) CGFloat lastContentOffset;
@property (assign, nonatomic) BOOL isScrollintToTop;
@property (assign, nonatomic) BOOL stopAnimate;
@property (assign, nonatomic) BOOL isFirstTimeLoaded;
@property (assign, nonatomic) NSInteger selectedServiceIDHomeSearchViewController;

@property (strong, nonatomic) CAShapeLayer *collectionViewMaskLayer;
@property (strong, nonatomic) DynamicServiceModel *selectedDynamicService;

@end

@implementation NewHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(10, 0, 10, 0);
    
    
    startLanguage = self.dynamicService.language;
    self.selectedServiceIDHomeSearchViewController = -1;
    self.collectionView.decelerationRate = UIScrollViewDecelerationRateNormal;
    
    [self disableInteractiveGesture];
    [self prepareTopBar];
    [AppHelper updateNavigationBarColor];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self prepareFekeSplashViewController];
    // Do any additional setup after loading the view.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
   // [self prepareZigZagView];
    [self.topView setNeedsLayout];
}
- (void)disableInteractiveGesture
{
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}
- (void)prepareFekeSplashViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FakeSplashViewController *fakeSplashViewController = [storyboard instantiateViewControllerWithIdentifier:fakeSplashViewControllerIdentifier];
    fakeSplashViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self.navigationController pushViewController:fakeSplashViewController animated:NO];
}

- (void)prepareDataSource
{
    self.dynamicServiceDataSorce = [[[CoreDataManager sharedManager] dynamicServiceModelArray] mutableCopy];
    self.servicesModelDataSource = [[[CoreDataManager sharedManager] fetchServiceList] mutableCopy];
    
    self.speedAccessDataSource = [self prepareArrayServicesModelWithServicesDictionary:[[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SpeedAccessServices" ofType:@"plist"]]];
    self.otherServiceDataSource = [self prepareArrayServicesModelWithServicesDictionary:[[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"OtherServices" ofType:@"plist"]]];
    
    if (self.dynamicService.language == LanguageTypeArabic) {
        [self reverseDataSource];
    }
}
- (NSMutableArray *)prepareArrayServicesModelWithServicesDictionary:(NSMutableArray *)servicesDictionatry
{
    NSMutableArray *servicesModel = [[NSMutableArray alloc] init];
    for (NSDictionary *serviceDictionary in servicesDictionatry) {
        ServiceModel *serviceModel = [[ServiceModel alloc] initWithDictionary:serviceDictionary];
        for (TRAStaticService *traService in self.servicesModelDataSource) {
            if ([serviceModel.serviceName isEqualToString:traService.serviceName]) {
                serviceModel.serviceEnable = [traService.serviceEnable boolValue];
            }
        }
        [servicesModel addObject:serviceModel];
    }
    return servicesModel;
}

- (void)reverseDataSource
{
    self.speedAccessDataSource = [[self.speedAccessDataSource reversedArray] mutableCopy];
    self.otherServiceDataSource = [self.otherServiceDataSource reversedArrayByElementsInGroup:RowCount];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    
    [self.topView setStartApearenceAnimationParameters];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.topView animateTopViewApearence];
    [self presentToturial];
    // [self presetTutorialViewControllerLogic];
    [self LoadDynamicDataSource];
    self.topView.disableFakeButtonLayersDrawing = YES;
}
//shayan coded
-(void)presentToturial{
    
    NSInteger tutorialShowed = (NSInteger)[[NSUserDefaults standardUserDefaults] integerForKey:KeyIsTutorialShowed];
    TutorialViewController *tutorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialID"];
    if (tutorialShowed == 2) {
        
        
        [AppHelper presentViewController:tutorialViewController onController:self.navigationController];
        
    }
    
    
    
}
-(void)LoadDynamicDataSource {
    
    if (self.dynamicService.language == LanguageTypeArabic) {
        
       
        _menuLabels = [NSArray arrayWithObjects:@"شكاوى حول دو أو اتصالات", @"الرسائل التسويقية", @"حجب موقع", @"تغطية", @"اقتراح", @"شكوى على الهيئة",nil];
        
        _enquiryLabels = [NSArray arrayWithObjects:@"استفسارات",@"تحقق من صحة هاتفك", @"تحقق من نطاق",nil];
        
        
        self.IFSALabels = [NSArray arrayWithObjects:@"إصدار تصاريح الطيف الترددي للتواصل مع المستخدمين",@"إصدار تصاريح الطيف الترددي للخدمات الراديوية الثابتة", @"إصدار تصاريح الطيف الترددي للخدمات الراديوية المساندة في الإنتاج الإذاعي والفعاليات الخاصة",nil];
        
        self.customsLabel = [NSArray arrayWithObjects:@"التخليص الجمركي",nil];
        
//        _sectionNames = [NSArray arrayWithObjects:@"الشكاوى والمراجعات والتقارير", @"البحث عن الاستفسارات", @"إصدار تصاريح الطيف الترددي", @"الجمارك",nil];
        _sectionNames = [NSArray arrayWithObjects:@"الشكاوى والمراجعات والتقارير", @"البحث عن الاستفسارات", @"إصدارالتصاريح", @"الجمارك",nil];
          [_collectionView reloadData ];
            }
    
    else {
        
        _menuLabels = [NSArray arrayWithObjects:@"Complaints about Du or Etisalat", @"Report SMS Spam", @"Report Websites", @"Review Coverage Near You", @"General Suggestions", @"Complaints About TRA",nil];
        
        _enquiryLabels = [NSArray arrayWithObjects:@"TRA Enquiries",@"Verify Your Phone’s Authenticity", @"Domain Check",nil];
        
        self.IFSALabels = [NSArray arrayWithObjects:@"IFSA For Programme",@"IFSA For Fixed", @"IFSA For Private",nil];
        
        self.customsLabel = [NSArray arrayWithObjects:@"Customs Clearance",nil];
        
        _sectionNames = [NSArray arrayWithObjects:@"Complains, Reviews & Reports", @"Look-ups & Enquiries", @"IFSA", @"Customs",nil];
        [_collectionView reloadData ];
    }
    
}

//- (void)prepareDataSource
//{
//
//
//    if (self.dynamicService.language == LanguageTypeArabic) {
//        [self reverseDataSource];
//    }
//}
- (void)prepareTopBar
{
    self.topView.delegate = self;
    self.topView.informationButtonImage = [UIImage imageNamed:@"ic_lamp"];
    self.topView.searchButtonImage = [UIImage imageNamed:@"ic_search"];
    self.topView.notificationButtonImage = [UIImage imageNamed:@"ic_not"];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self prepareDataSource];
    [self updateUserProfileImage];
    self.navigationController.navigationBar.hidden = YES;
    
//    [self prepareDataSource];
    //my coded
    //    __weak typeof(self) weakSelf = self;
    //    if ([NetworkManager sharedManager].isUserLoggined) {
    //
    //    } else {
    //        UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
    //        LoginViewController *loginViewController = (LoginViewController *)navController.topViewController;
    //        loginViewController.shouldAutoCloseAfterLogin = YES;
    //        loginViewController.didDismissed = ^() {
    //            [weakSelf.topView animateTopViewApearence];
    //             [self prepareDataSource];
    //        };
    //        [AppHelper presentViewController:navController onController:self.navigationController];
    //    }
    //
    
    
  
    [self navigationTransitionViewController:self.navigationTransitionType];
}

#pragma mark - HomeTopBarViewDelegate

- (void)topBarInformationButtonDidPressedInView:(HomeTopBarView *)parentView
{
    [self performSegueWithIdentifier:@"InnovationsSegue" sender:self];
}

- (void)topBarNotificationButtonDidPressedInView:(HomeTopBarView *)parentView
{
    [self performSegueWithIdentifier:HomeToNotificationSegueIdentifier sender:self];
}

- (void)topBarSearchButtonDidPressedInView:(HomeTopBarView *)parentView
{
    [self performSegueWithIdentifier:HomeToHomeSearchSegueIdentifier sender:self];
}

- (void)topBarLogoImageDidTouched:(HomeTopBarView *)parentView
{
    [self.topView setStartApearenceAnimationParameters];
    
    __weak typeof(self) weakSelf = self;
    if ([NetworkManager sharedManager].isUserLoggined) {
        [self performSegueWithIdentifier:HomeToUserProfileSegueIdentifier sender:self];
    } else {
        UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
        LoginViewController *loginViewController = (LoginViewController *)navController.topViewController;
        loginViewController.shouldAutoCloseAfterLogin = YES;
        loginViewController.didDismissed = ^() {
            [weakSelf.topView animateTopViewApearence];
            [weakSelf updateUserProfileImage];
        };
        [AppHelper presentViewController:navController onController:self.navigationController];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView == self.collectionView) {
        
//        [self animationMaskMainCollectionView];
//
       [self detectScrollDirectioninScrollView:scrollView];
        CGFloat heighOFScreen = SCREEN_HEIGHT;
        
        CGFloat minimumAllowedY = (heighOFScreen * TopViewHeightMultiplierValue) / 2;
        CGFloat contentOffsetY = scrollView.contentOffset.y;
        
        
        if ( (- self.speedAccessCollectionViewTopSpaceConstraint.constant + contentOffsetY < minimumAllowedY && contentOffsetY >= 0)|| (- self.speedAccessCollectionViewTopSpaceConstraint.constant + contentOffsetY > 0 && contentOffsetY < 0)) {
            self.speedAccessCollectionViewTopSpaceConstraint.constant -= contentOffsetY;
            [self.collectionView setContentOffset:CGPointZero];
        } else {
            self.speedAccessCollectionViewTopSpaceConstraint.constant = contentOffsetY >= 0 ? - minimumAllowedY : 0;
        }
        
        [self.topView stopCABasicAnimations];
        
       CGFloat progress = - self.speedAccessCollectionViewTopSpaceConstraint.constant / minimumAllowedY;
        [self animateTopLogoWithProgress:progress];
        [self animateSpeedAcceesCollectionViewCellWithScaleFactor:progress];
        [self.topView animateBottomWireMovingWithProgress:progress];
        [self.topView animateFakeButtonsLayerMovingWithProgress:progress];
        [self.topView animateOpacityChangesForBottomLayers:progress];
        
        self.lastContentOffset = scrollView.contentOffset.y;
       [self.scrollView setContentOffset:CGPointMake(0, - 0.3 * (self.collectionView.contentOffset.y + self.speedAccessCollectionViewTopSpaceConstraint.constant))];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if (!decelerate) {
        [self animationDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self animationDidScroll:scrollView];
}

#pragma mark - Animations vs Calculation for Animations

- (void)animationDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.collectionView) {
        self.stopAnimate = YES;
        CGFloat heighOFScreen = SCREEN_HEIGHT;
        CGFloat minimumAllowedY = (heighOFScreen * TopViewHeightMultiplierValue) / 2;
        CGFloat constantValue = self.isScrollintToTop ? minimumAllowedY : 0;
        
        if (- self.speedAccessCollectionViewTopSpaceConstraint.constant == 0 || - self.speedAccessCollectionViewTopSpaceConstraint.constant == minimumAllowedY) {
            self.stopAnimate = NO;
        } else {
            [self.view layoutIfNeeded];
            __weak typeof(self) weakSelf = self;
            CGFloat duration =  0.3 * ABS(self.speedAccessCollectionViewTopSpaceConstraint.constant + constantValue) / minimumAllowedY;
            
            [UIView animateWithDuration:duration
                             animations:^{
                                 weakSelf.speedAccessCollectionViewTopSpaceConstraint.constant = - constantValue;
                                 [weakSelf.view layoutIfNeeded];
                             } completion:^(BOOL finished) {
                                 weakSelf.stopAnimate = NO;
                             }];
            
            self.topView.duration =  duration;
            [CATransaction begin];
            [CATransaction setDisableActions:YES];
            [self.topView scaleLogo:!self.isScrollintToTop];
            [self speedAcceesCollectionViewCellScale:!self.isScrollintToTop duration:duration];
            [self.topView animateBottomElementsMovingToTop:self.isScrollintToTop];
            [self.topView animateFakeButtonsLayerMovingToTop:self.isScrollintToTop];
            [CATransaction setDisableActions:NO];
            [CATransaction commit];
        }
    }
}

//- (void)animationMaskMainCollectionView
//{
//    CGRect frame = self.collectionViewMaskLayer.frame;
//    frame.origin = CGPointMake(0, -CGRectGetMaxY(self.speedAccessCollectionView.bounds) + self.menuCollectionView.contentOffset.y );
//
//    [CATransaction begin];
//    [CATransaction setDisableActions:YES];
//    self.collectionViewMaskLayer.frame = frame;
//    [CATransaction setDisableActions:NO];
//    [CATransaction commit];
//}

- (void)animateTopLogoWithProgress:(CGFloat)progress
{
    CGFloat scalePercent = progress == 1 ? progress : 1 - (1 - LogoScaleMinValue) * progress;
    
    if (scalePercent < 1 && scalePercent) {
        [self.topView animateLogoScaling:scalePercent];
    }
}

- (void)animateSpeedAcceesCollectionViewCellWithScaleFactor:(CGFloat)scaleFactor
{
    CGFloat scalePercent = scaleFactor == 1 ? scaleFactor : 1 - (1 - MaxScaleFactorForSpeedAccessCell) * scaleFactor;

    if (scalePercent < 1 && scalePercent) {
        for (NewHomeMenuCollectionViewCell *cell in [self.collectionView visibleCells]) {
            CATransform3D transformation = CATransform3DIdentity;
            transformation = CATransform3DScale(transformation, scalePercent, scalePercent, 1);
            cell.layer.transform  = transformation;
        }
    }
}

- (void)speedAcceesCollectionViewCellScale:(BOOL)animate duration:(CGFloat)duration
{
    NewHomeMenuCollectionViewCell *cell = (NewHomeMenuCollectionViewCell *)[[self.collectionView visibleCells] firstObject];
    CATransform3D startTransform = cell.layer.transform;

    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    CATransform3D transformation = CATransform3DIdentity;
    transformation = animate ? CATransform3DIdentity : CATransform3DScale(transformation, MaxScaleFactorForSpeedAccessCell, MaxScaleFactorForSpeedAccessCell, 1);
    scaleAnimation.fromValue = [NSValue valueWithCATransform3D:startTransform];
    scaleAnimation.toValue = [NSValue valueWithCATransform3D:transformation];
    scaleAnimation.duration = duration;

    for (NewHomeMenuCollectionViewCell *cell in [self.collectionView visibleCells]) {
        [cell.layer addAnimation:scaleAnimation forKey:nil];
        cell.layer.transform  = transformation;
    }
}

- (void)detectScrollDirectioninScrollView:(UIScrollView *)scrollView
{
    if (self.lastContentOffset > scrollView.contentOffset.y) {
        self.isScrollintToTop = NO;
    } else if (self.lastContentOffset < scrollView.contentOffset.y) {
        self.isScrollintToTop = YES;
    }
}

- (void)animateSpeedAccessCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CAKeyframeAnimation *moveAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    moveAnim.values = @ [
                         [NSValue valueWithCGPoint:CGPointMake(cell.center.x, 0)],
                         [NSValue valueWithCGPoint:CGPointMake(cell.center.x, 0)],
                         [NSValue valueWithCGPoint:cell.center]
                         ];
    moveAnim.keyTimes = @[@(0), @(0.1 * indexPath.row), @(1)];
    
    CABasicAnimation *opacityAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnim.fromValue = @(0);
    opacityAnim.toValue = @(1);
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[moveAnim, opacityAnim];
    group.duration = 0.15 + 0.05 * indexPath.row;
    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [cell.layer addAnimation:group forKey:nil];
    
    BOOL shouldBeScaled = self.collectionView.frame.origin.y < CGRectGetMaxY(self.topView.frame);
    
    CATransform3D transformation = CATransform3DIdentity;
    transformation = shouldBeScaled ? CATransform3DScale(transformation, MaxScaleFactorForSpeedAccessCell, MaxScaleFactorForSpeedAccessCell, 1) : CATransform3DIdentity;
    cell.layer.transform = transformation;
}
- (void)animationWithStartY:(NSInteger)startY stopY:(NSInteger)stopY duration:(CGFloat)duration andLayer:(CALayer *)layer
{
    CABasicAnimation *topToDownAnimation = [CABasicAnimation animationWithKeyPath:@"position.y"];
    topToDownAnimation.fromValue = @(startY);
    topToDownAnimation.toValue = @(stopY);
    topToDownAnimation.duration = duration;
    [layer addAnimation:topToDownAnimation forKey:nil];
}

- (void)animateOtherCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CABasicAnimation *opacityAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnim.fromValue = @(0);
    opacityAnim.toValue = @(1);
    opacityAnim.duration = 0.15 + 0.05 * indexPath.row;
    [cell.layer addAnimation:opacityAnim forKey:nil];
    
    CGPoint center = cell.center;
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        [self animationWithStartY:center.y - 5 stopY:center.y duration:0.2 andLayer:cell.layer];
    }];
    [self animationWithStartY:center.y + 50 stopY:center.y - 5 duration:0.2 andLayer:cell.layer];
    [CATransaction commit];
}

#pragma mark - NavigationTransitionViewController

- (void)navigationTransitionViewController:(NavigationTransitionType)navigationType
{
    switch (navigationType) {
            //        case NavigationTransitionTypeComplaintASP:{
            //            CompliantViewController *compliantViewController = [self.storyboard   instantiateViewControllerWithIdentifier:@"compliantID"];
            //            compliantViewController.serviceID = 10;
            //            [self.navigationController pushViewController:compliantViewController animated:NO];
            //            break;
            //        }
            //        case NavigationTransitionTypeComplainTRA:{
            //            CompliantViewController *compliantViewController = [self.storyboard   instantiateViewControllerWithIdentifier:@"compliantID"];
            //            compliantViewController.serviceID = 13;
            //            [self.navigationController pushViewController:compliantViewController animated:NO];
            //            break;
            //        }
        case NavigationTransitionTypeSearch:{
            [self performSegueWithIdentifier:HomeToHomeSearchSegueIdentifier sender:self];
            break;
        }
        default:
            break;
    }
    self.navigationTransitionType = NavigationTransitionTypeDefault;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareNotificationViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    NotificationViewController *notificationViewController = segue.destinationViewController;
    notificationViewController.fakeBackground = [AppHelper snapshotForView:self.tabBarController.view];
    notificationViewController.hidesBottomBarWhenPushed = YES;
}
- (void)prepareHomeSearchViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    HomeSearchViewController *homeSearchViewController = segue.destinationViewController;
    homeSearchViewController.fakeBackground = [AppHelper snapshotForView:self.tabBarController.view];
    homeSearchViewController.hidesBottomBarWhenPushed = YES;
    homeSearchViewController.searchType = SearchTypeAllApp;
    
    __weak typeof(self) weakSelf = self;
    homeSearchViewController.didSelectService = ^(TRABaseService *selectedServise){
        
       
        [UIView performWithoutAnimation:^{
            if ([selectedServise isKindOfClass:[TRAStaticService class]]) {
                [weakSelf sevriceSwitchPerformSegue:[((TRAStaticService *)selectedServise).serviceInternalID integerValue]];
            } else if ([selectedServise isKindOfClass:[TRADynamicService class]]) {
                weakSelf.selectedDynamicService = [[DynamicServiceModel alloc] initWithTRADynamicServise:(TRADynamicService *)selectedServise];
                [self performSegueWithIdentifier:HomeToDynamicContainerSegueIdentifier sender:nil];
            }
        }];
    };
}

- (void)sevriceSwitchPerformSegue:(NSInteger)serviceID
{
    if (![NetworkManager sharedManager].networkStatus) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.NoInternetConnection")];
        return;
    }
    
    //shayan coded for login wall on menu
    else if (![[NetworkManager sharedManager] isUserLoggined]){
        
        UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
        LoginViewController *loginViewController = (LoginViewController *)navController.topViewController;
        loginViewController.shouldAutoCloseAfterLogin = YES;
        loginViewController.didDismissed = ^() {
            
            [self.navigationController popViewControllerAnimated:YES];
            
        };
        [AppHelper presentViewController:navController onController:self.navigationController];
        
        return;
        
    }
    
    
    switch (serviceID) {
        case 2: {
            [self performSegueWithIdentifier:HomeBarcodeReaderSegueIdentifier sender:@(serviceID)];
            break;
        }
        case 3: {
            [self performSegueWithIdentifier:HomeToSearchBrandNameSegueIdentifier sender:@(serviceID)];
            break;
        }
        case 5:
        case 6: {
            [self performSegueWithIdentifier:HomeToSpamReportSegueidentifier sender:@(serviceID)];
            break;
        }
        case 7: {
            [self performSegueWithIdentifier:HomeCheckDomainSegueIdentifier sender:@(serviceID)];
            break;
        }
        case 8: {
            [self performSegueWithIdentifier:HomeToCoverageSwgueIdentifier sender:@(serviceID)];
            break;
        }
        case 12:{
            [self performSegueWithIdentifier:HomeToComplaintEnquiresSequeIdentifier sender:@(serviceID)];
            break;
        }
        case 13:{
            [self performSegueWithIdentifier:HomeToComplaintTRASequeIdentifier sender:@(serviceID)];
            break;
        }
    }
}
- (void)prepareDynamicContainerServiceViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    DynamicContainerServiceViewController *dynamicContainerServiceViewController = segue.destinationViewController;
    dynamicContainerServiceViewController.dynamicServiceModel = self.selectedDynamicService;
}

- (void)prepareSpamReportViewControllerWithSegue:(UIStoryboardSegue *)segue senderID:(NSNumber *)selectedID
{
    SpamReportViewController *viewController = segue.destinationViewController;

    if ([selectedID intValue] == 5) {
        viewController.selectSpamReport = SpamReportTypeSMS;
    } else if ([selectedID intValue] == 6) {
        viewController.selectSpamReport = SpamReportTypeWeb;
    }
}
- (void)prepareDynamicListServicesViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    DynamicListServicesViewController *dynamicListServicesViewController = segue.destinationViewController;
    self.selectedDynamicService = self.dynamicServiceDataSorce[0];
    dynamicListServicesViewController.dynamicServiceModel = self.selectedDynamicService;
    
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.destinationViewController isKindOfClass:[BaseServiceViewController class]]&& [sender isKindOfClass:[NSNumber class]]) {
        [(BaseServiceViewController *)segue.destinationViewController setServiceID:[sender integerValue]];
    }
    
    if ([segue.identifier isEqualToString:HomeToNotificationSegueIdentifier]) {
        [self prepareNotificationViewControllerWithSegue:segue];
    } else if ([segue.identifier isEqualToString:HomeToHomeSearchSegueIdentifier]) {
        [self prepareHomeSearchViewControllerWithSegue:segue];
    } else if ([segue.identifier isEqualToString:HomeToDynamicContainerSegueIdentifier]) {
        [self prepareDynamicContainerServiceViewControllerWithSegue:segue];
    } else if ([segue.identifier isEqualToString:HomeToSpamReportSegueidentifier]) {
        [self prepareSpamReportViewControllerWithSegue:segue senderID:sender];
    } else if ([segue.identifier isEqualToString:HomeToDynamicListServicesSegueIdentifier]) {
        [self prepareDynamicListServicesViewControllerWithSegue:segue];
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NewHomeMenuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    
    if (indexPath.section == 0) {
        
        NSString *imageName = [NSString stringWithFormat:@"%ld.jpg",indexPath.row+1];
        
        cell.menuImageView.image = [UIImage imageNamed:imageName];
        cell.menuLabel.text = _menuLabels[indexPath.row];
    }
    else if (indexPath.section == 1) {
        
        cell.menuLabel.text = _enquiryLabels[indexPath.row];
        NSString *imageName = [NSString stringWithFormat:@"%ld.jpg",indexPath.row+7];
        
        cell.menuImageView.image = [UIImage imageNamed:imageName];
    }
    
    else if (indexPath.section == 2) {
        cell.menuLabel.text = _IFSALabels[indexPath.row];
        NSString *imageName = [NSString stringWithFormat:@"%ld.jpg",indexPath.row+10];
        
        cell.menuImageView.image = [UIImage imageNamed:imageName];
        
    }
    else {
        NSString *imageName = [NSString stringWithFormat:@"%ld.jpg",indexPath.row+13];
        
        cell.menuImageView.image = [UIImage imageNamed:imageName];
        
        cell.menuLabel.text = _customsLabel[indexPath.row];
    }
    
    
    
    return cell;
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
            [self otherServiceCollectionViewCellSelectedAtIndexPath:0];
            
         
            
        }
        
        if (indexPath.row == 1) {
             [self sevriceSwitchPerformSegue:5];
          
            
    
        }
        
        else if (indexPath.row == 2) {
            
            
                [self sevriceSwitchPerformSegue:6];
           
           
        }
        else if (indexPath.row == 3) {
            [self sevriceSwitchPerformSegue:8];
            
        }
        else if (indexPath.row == 4) {
            
            [self otherServiceCollectionViewCellSelectedAtIndexPath:2];
        }
        
        
        else if (indexPath.row == 5) {
            
            [self otherServiceCollectionViewCellSelectedAtIndexPath:1];

            
        }
        
    }
    else if (indexPath.section == 1) {
        
        if (indexPath.row == 0) {
            
            [self otherServiceCollectionViewCellSelectedAtIndexPath:3];

        }
        
        if (indexPath.row == 1) {
            

                [self sevriceSwitchPerformSegue:2];
        }
        if (indexPath.row == 2) {
            
          
            [self sevriceSwitchPerformSegue:7];
        }
       
    }
    else if (indexPath.section == 2) {
        
        if (indexPath.row
            == 0) {
            
            [self otherServiceCollectionViewCellSelectedAtIndexPath:4];
        }
        
        else if (indexPath.row == 1) {
            
            [self otherServiceCollectionViewCellSelectedAtIndexPath:6];
        }
        
       
        else if (indexPath.row == 2) {
            
            [self otherServiceCollectionViewCellSelectedAtIndexPath:5];
        }
    }
    
    else {
        
        [self otherServiceCollectionViewCellSelectedAtIndexPath:7];
    }
    
}

- (void)otherServiceCollectionViewCellSelectedAtIndexPath:(int)arrayIndex
{
    __weak typeof(self) weakSelf = self;
    if ([NetworkManager sharedManager].isUserLoggined) {
        
        
        self.selectedDynamicService = self.dynamicServiceDataSorce[arrayIndex];
        NSString *segueIdentifier = self.selectedDynamicService.dynamicServiceItems.count ? HomeToDynamicListServicesSegueIdentifier : HomeToDynamicContainerSegueIdentifier;
        [self performSegueWithIdentifier:segueIdentifier sender:nil];
        
    } else {
        UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
        LoginViewController *loginViewController = (LoginViewController *)navController.topViewController;
        loginViewController.shouldAutoCloseAfterLogin = YES;
        loginViewController.didDismissed = ^() {
            
            if ([NetworkManager sharedManager].isUserLoggined) {
                
                NSString *segueIdentifier = weakSelf.selectedDynamicService.dynamicServiceItems.count ? HomeToDynamicListServicesSegueIdentifier : HomeToDynamicContainerSegueIdentifier;
                [self performSegueWithIdentifier:segueIdentifier sender:nil];
                
                
            }
            
        };
        [AppHelper presentViewController:navController onController:self.navigationController];
    }
    
   
    
   /* if (indexPath.row + indexPath.section * RowCount < self.otherServiceDataSource.count) {
        ServiceModel *selectedServiceDetails = self.otherServiceDataSource[indexPath.row];
        if (selectedServiceDetails.serviceEnable) {
            [self sevriceSwitchPerformSegue:selectedServiceDetails.serviceID];
        }
    } else if (self.dynamicServiceDataSorce.count && indexPath.row + indexPath.section * RowCount < self.otherServiceDataSource.count + self.dynamicServiceDataSorce.count) {
        
        NSLog(@"indexPath:%d",indexPath.row);
        NSLog(@"Section:%d",indexPath.section);
        NSLog(@"rowCount:%d",(NSInteger)RowCount);
        NSLog(@"arraycount:%d",self.otherServiceDataSource.count);
        
        self.selectedDynamicService = self.dynamicServiceDataSorce[0];
        
        NSString *segueIdentifier = self.selectedDynamicService.dynamicServiceItems.count ? HomeToDynamicListServicesSegueIdentifier : HomeToDynamicContainerSegueIdentifier;
        [self performSegueWithIdentifier:segueIdentifier sender:nil];
    }*/
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return CGSizeMake((collectionView.frame.size.width/3)-15, 150);
    
    
    
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        HeaderViewCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        
        if (self.dynamicService.colorScheme == ApplicationColorBlackAndWhite) {
            
            
            
           // headerView.sectionTitle.textColor = UIColor.blackColor;
            headerView.sectionTitle.text = _sectionNames[indexPath.section];
           // headerView.sectionImage.image = nil;
            
            headerView.sectionImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"section%ld",indexPath.section]];
            
            headerView.sectionImage.image = [[BlackWhiteConverter sharedManager] convertedBlackAndWhiteImage:headerView.sectionImage.image];

        }
        else {
            
             headerView.sectionTitle.textColor = UIColor.whiteColor;
            headerView.sectionTitle.text = _sectionNames[indexPath.section];
            
            headerView.sectionImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"section%ld",indexPath.section]];
            
        }
        
        reusableview = headerView;
        
        
    }
    
    if (kind == UICollectionElementKindSectionFooter) {
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        
        reusableview = footerview;
    }
    
    return reusableview;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 4;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (section == 0) {
        return _menuLabels.count;
    }
    else if (section == 1 || section == 2) {
        
        return 3;
    }
    else {
        
        return 1;
    }
    
    
    
}
#pragma mark - TopBar

- (void)updateUserProfileImage
{
    UserModel *user = [[KeychainStorage new] loadCustomObjectWithKey:userModelKey];
    if (user.avatarImageBase64.length && [NetworkManager sharedManager].isUserLoggined) {
        NSData *data = [[NSData alloc] initWithBase64EncodedString:user.avatarImageBase64 options:kNilOptions];
        UIImage *image = [UIImage imageWithData:data];
        self.topView.logoImage = image;
    } else {
        self.topView.logoImage = [UIImage imageNamed:DefaultLogoImageName];
    }
}

#pragma mark - SuperclassMethods

- (void)localizeUI
{
    
}

- (void)updateColors
{
    
    
    UIImage *movableImage = [UIImage imageNamed:@"background"];
    if (self.dynamicService.colorScheme == ApplicationColorBlackAndWhite) {
        movableImage = [[BlackWhiteConverter sharedManager] convertedBlackAndWhiteImage:movableImage];
        
        self.backgroundImage.image = movableImage;
    }
    
    self.backgroundImage.image = movableImage;

   // self.movableImageView.image = movableImage;
    
    [self.topView updateUIColor];
}

- (void)setRTLArabicUI
{
    [self transformTopView:TRANFORM_3D_SCALE];
}
- (void)transformTopView:(CATransform3D)transform
{
    self.topView.layer.transform = transform;
    self.topView.avatarView.layer.transform = transform;
}

- (void)setLTREuropeUI
{
    [self transformTopView:CATransform3DIdentity];
}

@end
