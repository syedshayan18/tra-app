//
//  HeaderViewCollectionReusableView.h
//  CollectionViewExample
//
//  Created by Shayan Ali on 12/09/2018.
//  Copyright © 2018 Shayan Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderViewCollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIImageView *sectionImage;
@property (weak, nonatomic) IBOutlet UILabel *sectionTitle;

@end
