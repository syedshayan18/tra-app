//
//  ViewController.m
//  testPentagonCells
//
//  Created by Admin on 30.07.15.
//

#import "HomeViewController.h"
#import "MenuCollectionViewCell.h"
#import "CategoryCollectionViewCell.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "NotificationViewController.h"
#import "UserProfileViewController.h"
#import "HomeSearchViewController.h"
#import "KeychainStorage.h"
#import "TutorialViewController.h"
#import "DynamicServiceModel.h"
#import "DynamicContainerServiceViewController.h"
#import "FakeSplashViewController.h"
#import "SpamReportViewController.h"
#import "ServiceModel.h"
#import "TRADynamicService.h"
#import "UIImageView+AFNetworking.h"
#import "DynamicListServicesViewController.h"

static CGFloat const CellSpacing = 5.f;
static CGFloat const RowCount = 4.f;
static CGFloat const CellSubmenuHeight = 140.f;
static CGFloat const ZigZagViewTag = 1001;
static CGFloat const TopViewHeightMultiplierValue = 0.18f;
static CGFloat const MaxScaleFactorForSpeedAccessCell = 0.9f;
//shayan original value 6
static NSInteger const MinRowServicesMain = 3;

static NSString *const HomeBarcodeReaderSegueIdentifier = @"HomeBarcodeReaderSegue";
static NSString *const HomeCheckDomainSegueIdentifier = @"HomeCheckDomainSegue";
static NSString *const HomeToHelpSalimSequeIdentifier = @"HomeToHelpSalimSeque";
static NSString *const HomeToCoverageSwgueIdentifier = @"HomeToCoverageSegue";
static NSString *const HomeToSpamReportSegueidentifier = @"HomeToSpamReportSegue";
static NSString *const HomeToSearchBrandNameSegueIdentifier = @"homeToListOfDeviceBrands"; //@"HomeToSearchBrandNameSegue";
static NSString *const HomeToNotificationSegueIdentifier = @"HomeToNotificationSegue";
static NSString *const HomeToUserProfileSegueIdentifier = @"UserProfileSegue";
static NSString *const HomeToHomeSearchSegueIdentifier = @"HomeToHomeSearchSegue";
static NSString *const HomeToDynamicContainerSegueIdentifier = @"HomeToDynamicContainer";
static NSString *const HomeToComplaintEnquiresSequeIdentifier = @"HomeToComplaintEnquiresSeque";
static NSString *const HomeToComplaintTRASequeIdentifier = @"HomeToComplaintTRASeque";
static NSString *const HomeToDynamicListServicesSegueIdentifier = @"HomeToDynamicListServicesSegue";

static LanguageType startLanguage;

@interface HomeViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *speedAccessCollectionViewTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *menuCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *speedAccessCollectionView;
@property (weak, nonatomic) IBOutlet HomeTopBarView *topView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *movableImageView;

@property (strong, nonatomic) NSMutableArray *servicesModelDataSource;
@property (strong, nonatomic) NSMutableArray *speedAccessDataSource;
@property (strong, nonatomic) NSMutableArray *otherServiceDataSource;
@property (strong, nonatomic) NSMutableArray *dynamicServiceDataSorce;

@property (assign, nonatomic) CGFloat lastContentOffset;
@property (assign, nonatomic) BOOL isScrollintToTop;
@property (assign, nonatomic) BOOL stopAnimate;
@property (assign, nonatomic) BOOL isFirstTimeLoaded;
@property (assign, nonatomic) NSInteger selectedServiceIDHomeSearchViewController;

@property (strong, nonatomic) CAShapeLayer *collectionViewMaskLayer;
@property (strong, nonatomic) DynamicServiceModel *selectedDynamicService;

@end

@implementation HomeViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    startLanguage = self.dynamicService.language;
    self.selectedServiceIDHomeSearchViewController = -1;
    self.menuCollectionView.decelerationRate = UIScrollViewDecelerationRateNormal;
    
    [self disableInteractiveGesture];
    [self prepareTopBar];
    [AppHelper updateNavigationBarColor];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self prepareFekeSplashViewController];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self addMaskMainCollectionView];
    [self prepareZigZagView];
    [self.topView setNeedsLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
        
    [self updateUserProfileImage];
    self.navigationController.navigationBar.hidden = YES;
    
     [self prepareDataSource];
    //my coded
//    __weak typeof(self) weakSelf = self;
//    if ([NetworkManager sharedManager].isUserLoggined) {
//
//    } else {
//        UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
//        LoginViewController *loginViewController = (LoginViewController *)navController.topViewController;
//        loginViewController.shouldAutoCloseAfterLogin = YES;
//        loginViewController.didDismissed = ^() {
//            [weakSelf.topView animateTopViewApearence];
//             [self prepareDataSource];
//        };
//        [AppHelper presentViewController:navController onController:self.navigationController];
//    }
//
    
    
    if (self.isFirstTimeLoaded) {
        [self.speedAccessCollectionView reloadData];
        [self.menuCollectionView reloadData];
    }
    self.isFirstTimeLoaded = YES;
    [self navigationTransitionViewController:self.navigationTransitionType];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.topView animateTopViewApearence];
    [self presentToturial];
   // [self presetTutorialViewControllerLogic];
    self.topView.disableFakeButtonLayersDrawing = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    [self.speedAccessDataSource removeAllObjects];
    [self.speedAccessCollectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    
    [self.topView setStartApearenceAnimationParameters];
}

#pragma mark - NavigationTransitionViewController

- (void)navigationTransitionViewController:(NavigationTransitionType)navigationType
{
    switch (navigationType) {
//        case NavigationTransitionTypeComplaintASP:{
//            CompliantViewController *compliantViewController = [self.storyboard   instantiateViewControllerWithIdentifier:@"compliantID"];
//            compliantViewController.serviceID = 10;
//            [self.navigationController pushViewController:compliantViewController animated:NO];
//            break;
//        }
//        case NavigationTransitionTypeComplainTRA:{
//            CompliantViewController *compliantViewController = [self.storyboard   instantiateViewControllerWithIdentifier:@"compliantID"];
//            compliantViewController.serviceID = 13;
//            [self.navigationController pushViewController:compliantViewController animated:NO];
//            break;
//        }
        case NavigationTransitionTypeSearch:{
            [self performSegueWithIdentifier:HomeToHomeSearchSegueIdentifier sender:self];
            break;
        }
        default:
            break;
    }
    self.navigationTransitionType = NavigationTransitionTypeDefault;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSUInteger sections = 1;
    if (collectionView == self.menuCollectionView) {
        NSInteger elementsCount = self.otherServiceDataSource.count + self.dynamicServiceDataSorce.count;
        sections = elementsCount / RowCount < MinRowServicesMain ? MinRowServicesMain : (NSInteger)ceilf(elementsCount / RowCount);
    }
    
    return sections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSUInteger itemRowisSection = (NSUInteger)RowCount;
    if (collectionView == self.menuCollectionView) {
        
        NSUInteger elementsCount = self.otherServiceDataSource.count + self.dynamicServiceDataSorce.count;
        NSInteger elementCorrectionNoVisibleCount = elementsCount % (NSUInteger)RowCount;
        elementsCount = elementsCount < MinRowServicesMain * RowCount ? (MinRowServicesMain * RowCount - elementCorrectionNoVisibleCount) : elementsCount;
        
        NSUInteger rowCount = (NSUInteger)RowCount;
        NSUInteger elementsVisibleCount = self.otherServiceDataSource.count + self.dynamicServiceDataSorce.count;
        NSUInteger indexElementsRowCount = (elementsVisibleCount / rowCount) * rowCount;
        if (section * rowCount >= indexElementsRowCount && section * rowCount < elementsVisibleCount ) {
            itemRowisSection = elementsVisibleCount % rowCount;
        }
        
    } else if (collectionView == self.speedAccessCollectionView) {
        itemRowisSection = self.speedAccessDataSource.count;
    }
    return itemRowisSection;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if (collectionView == self.menuCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:MenuCollectionViewCellIdentifier forIndexPath:indexPath];
        if (!cell) {
            cell = [[MenuCollectionViewCell alloc] init];
        }
        [self configureMainCell:(MenuCollectionViewCell *)cell atIndexPath:indexPath];
    } else if (collectionView == self.speedAccessCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:CategoryCollectionViewCellIdentifier forIndexPath:indexPath];
        if (!cell) {
            cell = [[CategoryCollectionViewCell alloc] init];
        }
        ((CategoryCollectionViewCell *)cell).serviceModel = self.speedAccessDataSource[indexPath.row];
    }
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = CellSubmenuHeight;
    CGSize contentSize = self.menuCollectionView.frame.size;
    CGFloat rowCountActual = RowCount;
    
    if (collectionView == self.speedAccessCollectionView) {
        CGFloat heighOFScreen = SCREEN_HEIGHT;
        cellHeight = heighOFScreen * TopViewHeightMultiplierValue;
        contentSize = self.speedAccessCollectionView.frame.size;
    }
    
    CGSize cellSize = CGSizeMake((contentSize.width - (CellSpacing * (rowCountActual + 1))) / rowCountActual, cellHeight);
    return cellSize;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{    
    CGFloat minimumInteritemSpacing = CellSpacing;
    
    if (collectionView == self.menuCollectionView) {
        
        NSUInteger rowCount = (NSUInteger)RowCount;
        NSUInteger elementsVisibleCount = self.otherServiceDataSource.count + self.dynamicServiceDataSorce.count;
        NSUInteger indexElementsRowCount = (elementsVisibleCount / rowCount) * rowCount;
        if (section * RowCount >= indexElementsRowCount && section * RowCount < elementsVisibleCount) {
            CGFloat cellWidth = (self.menuCollectionView.frame.size.width - (CellSpacing * (RowCount + 1))) / RowCount;
            NSInteger rowCountActual = elementsVisibleCount % rowCount;
            
            minimumInteritemSpacing = (self.menuCollectionView.frame.size.width - rowCountActual * cellWidth) / (rowCountActual );
            if (rowCountActual == 2) {
                minimumInteritemSpacing = CellSpacing;
            }
        }
    }
    return minimumInteritemSpacing;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat cellSpacing = CellSpacing;
    
    if (collectionView == self.menuCollectionView) {

        NSUInteger rowCount = (NSUInteger)RowCount;
        NSUInteger elementsVisibleCount = self.otherServiceDataSource.count + self.dynamicServiceDataSorce.count;
        NSUInteger indexElementsRowCount = (elementsVisibleCount / rowCount) * rowCount;
        if (section * RowCount >= indexElementsRowCount && section * RowCount < elementsVisibleCount) {
            CGFloat cellWidth = (self.menuCollectionView.frame.size.width - (CellSpacing * (RowCount + 1))) / RowCount;
            NSInteger rowCountActual = elementsVisibleCount % rowCount;
            
            cellSpacing = (self.menuCollectionView.frame.size.width - rowCountActual * cellWidth) / (rowCountActual * 2);
            if (rowCountActual == 2) {
                cellSpacing = CellSpacing * 2 + cellWidth;
            }
        }
    }
 
    return UIEdgeInsetsMake(0, cellSpacing, 0, cellSpacing);
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    collectionView == self.speedAccessCollectionView ? [self speedAccessCollectionViewCellSelectedAtIndexPath:indexPath] : [self otherServiceCollectionViewCellSelectedAtIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.speedAccessCollectionView) {
        [self animateSpeedAccessCell:cell atIndexPath:indexPath];
    } else if (!collectionView.contentOffset.y) {
        [self animateOtherCell:cell atIndexPath:indexPath];
    }
}

- (void)animationWithStartY:(NSInteger)startY stopY:(NSInteger)stopY duration:(CGFloat)duration andLayer:(CALayer *)layer
{
    CABasicAnimation *topToDownAnimation = [CABasicAnimation animationWithKeyPath:@"position.y"];
    topToDownAnimation.fromValue = @(startY);
    topToDownAnimation.toValue = @(stopY);
    topToDownAnimation.duration = duration;
    [layer addAnimation:topToDownAnimation forKey:nil];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView == self.menuCollectionView) {

        [self animationMaskMainCollectionView];

        [self detectScrollDirectioninScrollView:scrollView];
        CGFloat heighOFScreen = SCREEN_HEIGHT;
        
        CGFloat minimumAllowedY = (heighOFScreen * TopViewHeightMultiplierValue) / 2;
        CGFloat contentOffsetY = scrollView.contentOffset.y;
        
        
        if ( (- self.speedAccessCollectionViewTopSpaceConstraint.constant + contentOffsetY < minimumAllowedY && contentOffsetY >= 0)|| (- self.speedAccessCollectionViewTopSpaceConstraint.constant + contentOffsetY > 0 && contentOffsetY < 0)) {
            self.speedAccessCollectionViewTopSpaceConstraint.constant -= contentOffsetY;
            [self.menuCollectionView setContentOffset:CGPointZero];
        } else {
            self.speedAccessCollectionViewTopSpaceConstraint.constant = contentOffsetY >= 0 ? - minimumAllowedY : 0;
        }
    
        [self.topView stopCABasicAnimations];
        
        CGFloat progress = - self.speedAccessCollectionViewTopSpaceConstraint.constant / minimumAllowedY;
        [self animateTopLogoWithProgress:progress];
        [self animateSpeedAcceesCollectionViewCellWithScaleFactor:progress];
        [self.topView animateBottomWireMovingWithProgress:progress];
        [self.topView animateFakeButtonsLayerMovingWithProgress:progress];
        [self.topView animateOpacityChangesForBottomLayers:progress];
        
        self.lastContentOffset = scrollView.contentOffset.y;
        [self.scrollView setContentOffset:CGPointMake(0, - 0.3 * (self.menuCollectionView.contentOffset.y + self.speedAccessCollectionViewTopSpaceConstraint.constant))];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if (!decelerate) {
        [self animationDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self animationDidScroll:scrollView];
}

#pragma mark - HomeTopBarViewDelegate

- (void)topBarInformationButtonDidPressedInView:(HomeTopBarView *)parentView
{
    [self performSegueWithIdentifier:@"InnovationsSegue" sender:self];
}

- (void)topBarNotificationButtonDidPressedInView:(HomeTopBarView *)parentView
{
    [self performSegueWithIdentifier:HomeToNotificationSegueIdentifier sender:self];
}

- (void)topBarSearchButtonDidPressedInView:(HomeTopBarView *)parentView
{
    [self performSegueWithIdentifier:HomeToHomeSearchSegueIdentifier sender:self];
}

- (void)topBarLogoImageDidTouched:(HomeTopBarView *)parentView
{
    [self.topView setStartApearenceAnimationParameters];

    __weak typeof(self) weakSelf = self;
    if ([NetworkManager sharedManager].isUserLoggined) {
        [self performSegueWithIdentifier:HomeToUserProfileSegueIdentifier sender:self];
    } else {
        UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
        LoginViewController *loginViewController = (LoginViewController *)navController.topViewController;
        loginViewController.shouldAutoCloseAfterLogin = YES;
        loginViewController.didDismissed = ^() {
            [weakSelf.topView animateTopViewApearence];
            [weakSelf updateUserProfileImage];
        };
        [AppHelper presentViewController:navController onController:self.navigationController];
    }
}

#pragma mark - Private

- (void)disableInteractiveGesture
{
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

- (void)prepareDataSource
{
    self.dynamicServiceDataSorce = [[[CoreDataManager sharedManager] dynamicServiceModelArray] mutableCopy];
    self.servicesModelDataSource = [[[CoreDataManager sharedManager] fetchServiceList] mutableCopy];
    
    self.speedAccessDataSource = [self prepareArrayServicesModelWithServicesDictionary:[[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SpeedAccessServices" ofType:@"plist"]]];
    self.otherServiceDataSource = [self prepareArrayServicesModelWithServicesDictionary:[[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"OtherServices" ofType:@"plist"]]];
    
    if (self.dynamicService.language == LanguageTypeArabic) {
        [self reverseDataSource];
    }
}

- (NSMutableArray *)prepareArrayServicesModelWithServicesDictionary:(NSMutableArray *)servicesDictionatry
{
    NSMutableArray *servicesModel = [[NSMutableArray alloc] init];
    for (NSDictionary *serviceDictionary in servicesDictionatry) {
        ServiceModel *serviceModel = [[ServiceModel alloc] initWithDictionary:serviceDictionary];
        for (TRAStaticService *traService in self.servicesModelDataSource) {
            if ([serviceModel.serviceName isEqualToString:traService.serviceName]) {
                serviceModel.serviceEnable = [traService.serviceEnable boolValue];
            }
        }
        [servicesModel addObject:serviceModel];
    }
    return servicesModel;
}

- (void)reverseDataSource
{
    self.speedAccessDataSource = [[self.speedAccessDataSource reversedArray] mutableCopy];
    self.otherServiceDataSource = [self.otherServiceDataSource reversedArrayByElementsInGroup:RowCount];
}

- (void)transformTopView:(CATransform3D)transform
{
    self.topView.layer.transform = transform;
    self.topView.avatarView.layer.transform = transform;
}

#pragma mark - Navigations

- (void)speedAccessCollectionViewCellSelectedAtIndexPath:(NSIndexPath *)indexPath
{
    ServiceModel *selectedServiceDetails = self.speedAccessDataSource[indexPath.row];
    if (selectedServiceDetails.serviceEnable) {
        [self sevriceSwitchPerformSegue:selectedServiceDetails.serviceID];
    }
}

- (void)otherServiceCollectionViewCellSelectedAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row + indexPath.section * RowCount < self.otherServiceDataSource.count) {
        ServiceModel *selectedServiceDetails = self.otherServiceDataSource[indexPath.row];
        if (selectedServiceDetails.serviceEnable) {
            [self sevriceSwitchPerformSegue:selectedServiceDetails.serviceID];
        }
    } else if (self.dynamicServiceDataSorce.count && indexPath.row + indexPath.section * RowCount < self.otherServiceDataSource.count + self.dynamicServiceDataSorce.count) {
        self.selectedDynamicService = self.dynamicServiceDataSorce[indexPath.row + indexPath.section * (NSInteger)RowCount - self.otherServiceDataSource.count];
        
        NSString *segueIdentifier = self.selectedDynamicService.dynamicServiceItems.count ? HomeToDynamicListServicesSegueIdentifier : HomeToDynamicContainerSegueIdentifier;
        
       
        //shayan
        
        //by passing for test shayan
        [self presentLoginOnServices:segueIdentifier];
      
    }
}

-(void)presentLoginOnServices:(NSString *)segueIdentifier{
    
    __weak typeof(self) weakSelf = self;
    if ([NetworkManager sharedManager].isUserLoggined) {
        
        [weakSelf performSegueWithIdentifier:segueIdentifier sender:nil];
    } else {
        UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
        LoginViewController *loginViewController = (LoginViewController *)navController.topViewController;
        loginViewController.shouldAutoCloseAfterLogin = YES;
        loginViewController.didDismissed = ^() {
            
            if ([NetworkManager sharedManager].isUserLoggined) {
                
                
                [weakSelf performSegueWithIdentifier:segueIdentifier sender:nil];
                
            }
            
        };
        [AppHelper presentViewController:navController onController:self.navigationController];
    }
    
}

- (void)sevriceSwitchPerformSegue:(NSInteger)serviceID
{
    if (![NetworkManager sharedManager].networkStatus) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.NoInternetConnection")];
        return;
    }
    
    //shayan coded for login wall on menu
    else if (![[NetworkManager sharedManager] isUserLoggined]){
       
            UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
            LoginViewController *loginViewController = (LoginViewController *)navController.topViewController;
            loginViewController.shouldAutoCloseAfterLogin = YES;
            loginViewController.didDismissed = ^() {
                
                [self.navigationController popViewControllerAnimated:YES];
                
            };
            [AppHelper presentViewController:navController onController:self.navigationController];
  
        return;
    
    }
    switch (serviceID) {
        case 2: {
            [self performSegueWithIdentifier:HomeBarcodeReaderSegueIdentifier sender:@(serviceID)];
            break;
        }
        case 3: {
            [self performSegueWithIdentifier:HomeToSearchBrandNameSegueIdentifier sender:@(serviceID)];
            break;
        }
        case 5:
        case 6: {
            [self performSegueWithIdentifier:HomeToSpamReportSegueidentifier sender:@(serviceID)];
            break;
        }
        case 7: {
            [self performSegueWithIdentifier:HomeCheckDomainSegueIdentifier sender:@(serviceID)];
            break;
        }
        case 8: {
            [self performSegueWithIdentifier:HomeToCoverageSwgueIdentifier sender:@(serviceID)];
            break;
        }
        case 12:{
            [self performSegueWithIdentifier:HomeToComplaintEnquiresSequeIdentifier sender:@(serviceID)];
            break;
        }
        case 13:{
            [self performSegueWithIdentifier:HomeToComplaintTRASequeIdentifier sender:@(serviceID)];
            break;
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[BaseServiceViewController class]]&& [sender isKindOfClass:[NSNumber class]]) {
        [(BaseServiceViewController *)segue.destinationViewController setServiceID:[sender integerValue]];
    }
    
    if ([segue.identifier isEqualToString:HomeToNotificationSegueIdentifier]) {
        [self prepareNotificationViewControllerWithSegue:segue];
    } else if ([segue.identifier isEqualToString:HomeToHomeSearchSegueIdentifier]) {
        [self prepareHomeSearchViewControllerWithSegue:segue];
    } else if ([segue.identifier isEqualToString:HomeToDynamicContainerSegueIdentifier]) {
        [self prepareDynamicContainerServiceViewControllerWithSegue:segue];
    } else if ([segue.identifier isEqualToString:HomeToSpamReportSegueidentifier]) {
        [self prepareSpamReportViewControllerWithSegue:segue senderID:sender];
    } else if ([segue.identifier isEqualToString:HomeToDynamicListServicesSegueIdentifier]) {
        [self prepareDynamicListServicesViewControllerWithSegue:segue];
    }
}

- (void)prepareDynamicListServicesViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    DynamicListServicesViewController *dynamicListServicesViewController = segue.destinationViewController;
    dynamicListServicesViewController.dynamicServiceModel = self.selectedDynamicService;
}

- (void)prepareSpamReportViewControllerWithSegue:(UIStoryboardSegue *)segue senderID:(NSNumber *)selectedID
{
    SpamReportViewController *viewController = segue.destinationViewController;
    NSIndexPath *indexPath = [[self.speedAccessCollectionView indexPathsForSelectedItems] firstObject];
    NSDictionary *selectedServiceDetails = self.speedAccessDataSource[indexPath.row];
    NSUInteger serviceID = selectedID ? [selectedID integerValue] : [[selectedServiceDetails valueForKey:@"serviceID"] integerValue];
    if (serviceID == 5) {
        viewController.selectSpamReport = SpamReportTypeSMS;
    } else if (serviceID == 6) {
        viewController.selectSpamReport = SpamReportTypeWeb;
    }
}

- (void)prepareDynamicContainerServiceViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    DynamicContainerServiceViewController *dynamicContainerServiceViewController = segue.destinationViewController;
    dynamicContainerServiceViewController.dynamicServiceModel = self.selectedDynamicService;
}

- (void)prepareHomeSearchViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    HomeSearchViewController *homeSearchViewController = segue.destinationViewController;
    homeSearchViewController.fakeBackground = [AppHelper snapshotForView:self.tabBarController.view];
    homeSearchViewController.hidesBottomBarWhenPushed = YES;
    homeSearchViewController.searchType = SearchTypeAllApp;

    __weak typeof(self) weakSelf = self;
    homeSearchViewController.didSelectService = ^(TRABaseService *selectedServise){

//        weakSelf.speedAccessDataSource = [[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SpeedAccessServices" ofType:@"plist"]];
        [UIView performWithoutAnimation:^{
            if ([selectedServise isKindOfClass:[TRAStaticService class]]) {
                [weakSelf sevriceSwitchPerformSegue:[((TRAStaticService *)selectedServise).serviceInternalID integerValue]];
            } else if ([selectedServise isKindOfClass:[TRADynamicService class]]) {
                weakSelf.selectedDynamicService = [[DynamicServiceModel alloc] initWithTRADynamicServise:(TRADynamicService *)selectedServise];
                [self performSegueWithIdentifier:HomeToDynamicContainerSegueIdentifier sender:nil];
            }
        }];
    };
}

- (void)prepareNotificationViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    NotificationViewController *notificationViewController = segue.destinationViewController;
    notificationViewController.fakeBackground = [AppHelper snapshotForView:self.tabBarController.view];
    notificationViewController.hidesBottomBarWhenPushed = YES;
}
//shayan coded
-(void)presentToturial{

    NSInteger tutorialShowed = (NSInteger)[[NSUserDefaults standardUserDefaults] integerForKey:KeyIsTutorialShowed];
    TutorialViewController *tutorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialID"];
    if (tutorialShowed == 2) {
        
       
        [AppHelper presentViewController:tutorialViewController onController:self.navigationController];
        
    }
    
    
    
}
- (void)presetTutorialViewControllerLogic
{
    NSInteger tutorialShowed = (NSInteger)[[NSUserDefaults standardUserDefaults] integerForKey:KeyIsTutorialShowed];
    if (tutorialShowed != 1) {
        self.speedAccessCollectionViewTopSpaceConstraint.constant = 0;
        [self.menuCollectionView setContentOffset:CGPointZero];
        
        TutorialViewController *tutorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialID"];
        tutorialViewController.speedAccessCollectionViewImage = [AppHelper snapshotForView:self.speedAccessCollectionView];
        tutorialViewController.tabBarImage = [AppHelper snapshotForView:self.tabBarController.tabBar];
        tutorialViewController.tabBarRect = self.tabBarController.tabBar.frame;
        tutorialViewController.speedAccessCollectionViewRect = self.speedAccessCollectionView.frame;
    
        
        __weak typeof(self) weakSelf = self;
        
        if (tutorialShowed == 2) {
            
            tutorialViewController.didCloseViewController = ^() {
                self.tabBarController.selectedIndex = self.dynamicService.language == LanguageTypeArabic ? 0 : 4;
            };
        } else {
            tutorialViewController.didCloseViewController = ^() {
                [weakSelf.topView animateTopViewApearence];
            };
        }
        [AppHelper presentViewController:tutorialViewController onController:self.navigationController];
    }
}

- (void)prepareFekeSplashViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FakeSplashViewController *fakeSplashViewController = [storyboard instantiateViewControllerWithIdentifier:fakeSplashViewControllerIdentifier];
    fakeSplashViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self.navigationController pushViewController:fakeSplashViewController animated:NO];
}

#pragma mark - TopBar

- (void)prepareTopBar
{
    self.topView.delegate = self;
    self.topView.informationButtonImage = [UIImage imageNamed:@"ic_lamp"];
    self.topView.searchButtonImage = [UIImage imageNamed:@"ic_search"];
    self.topView.notificationButtonImage = [UIImage imageNamed:@"ic_not"];
}

- (void)updateUserProfileImage
{
    UserModel *user = [[KeychainStorage new] loadCustomObjectWithKey:userModelKey];
    if (user.avatarImageBase64.length && [NetworkManager sharedManager].isUserLoggined) {
        NSData *data = [[NSData alloc] initWithBase64EncodedString:user.avatarImageBase64 options:kNilOptions];
        UIImage *image = [UIImage imageWithData:data];
        self.topView.logoImage = image;
    } else {
        self.topView.logoImage = [UIImage imageNamed:DefaultLogoImageName];
    }
}

#pragma mark - SuperclassMethods

- (void)localizeUI
{

}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"background"];
    UIImage *movableImage = [UIImage imageNamed:@"res_polygons"];
    if (self.dynamicService.colorScheme == ApplicationColorBlackAndWhite) {
        movableImage = [[BlackWhiteConverter sharedManager] convertedBlackAndWhiteImage:movableImage];
        
    }
    self.movableImageView.image = movableImage;
    
    [self.topView updateUIColor];
    
}

- (void)setRTLArabicUI
{
    [self transformTopView:TRANFORM_3D_SCALE];
}

- (void)setLTREuropeUI
{
    [self transformTopView:CATransform3DIdentity];
}

#pragma mark - Configurations for Cells

- (void)configureMainCell:(MenuCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.cellPresentationMode = indexPath.row % 2 ? PresentationModeModeBottom : PresentationModeModeTop;

    NSUInteger rowCount = (NSUInteger)RowCount;
    NSUInteger elementsVisibleCount = self.otherServiceDataSource.count + self.dynamicServiceDataSorce.count;
    NSUInteger indexElementsRowCount = (elementsVisibleCount / rowCount) * rowCount;
    NSUInteger rowLastCount = elementsVisibleCount % rowCount;
    if (indexPath.row + indexPath.section * RowCount >= indexElementsRowCount && indexPath.row + indexPath.section * RowCount < elementsVisibleCount ) {
        if (2 == rowLastCount) {
            cell.cellPresentationMode = indexPath.row % 2 ? PresentationModeModeTop : PresentationModeModeBottom;
        }
    }
    
    if (indexPath.row + indexPath.section * RowCount < self.otherServiceDataSource.count) {
        cell.serviceModel = self.otherServiceDataSource[indexPath.row + indexPath.section];
    } else if (indexPath.row + indexPath.section * RowCount >= self.otherServiceDataSource.count && indexPath.row + indexPath.section * RowCount < self.otherServiceDataSource.count + self.dynamicServiceDataSorce.count) {
        [self configureCellDynamicService:cell atIndexPath:indexPath];
    } 

    cell.itemLogoImageView.tintColor = [[UIColor menuItemGrayColor] colorWithAlphaComponent:0.8];

    cell.menuTitleLabel.textColor = [UIColor menuItemGrayColor];
    cell.menuTitleLabel.tag = DeclineTagForFontUpdate;
    cell.widthMenuTitleLabelConstraint.constant = [self widthMenuCollectionViewCellTitleLabel];
}

- (void)configureCellDynamicService:(MenuCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [AppHelper addHexagonBorderForLayer:cell.polygonView.layer color:[UIColor menuItemGrayColor] width:1];
    DynamicServiceModel *dynamicServiceModel = self.dynamicServiceDataSorce[indexPath.row + indexPath.section * (NSInteger)RowCount - self.otherServiceDataSource.count];
    cell.menuTitleLabel.text = [dynamicServiceModel localizableString:dynamicServiceModel.dynamicServiceName];
    UIImage *logo = [UIImage imageNamed:@"ic_domain_logo"];
    [AppHelper downloadToImageView:cell.itemLogoImageView url:dynamicServiceModel.dynamicServiceIcon placeholderImage:logo];
}

- (CGFloat)widthMenuCollectionViewCellTitleLabel
{
    return (self.menuCollectionView.frame.size.width - (CellSpacing * (RowCount + 1))) / RowCount;
}

#pragma mark - Decorations

- (void)addMaskMainCollectionView
{
    UIBezierPath *zigZagPath = [self setupZigZagPath];

    [zigZagPath addLineToPoint:CGPointMake(self.view.frame.size.width, self.view.frame.size.height)];
    [zigZagPath addLineToPoint:CGPointMake(0, self.view.frame.size.height)];
    [zigZagPath closePath];
    
    [self.collectionViewMaskLayer removeFromSuperlayer];

    self.collectionViewMaskLayer = [CAShapeLayer layer];
    self.collectionViewMaskLayer.path = zigZagPath.CGPath;
    self.collectionViewMaskLayer.frame = self.view.bounds;
    self.collectionViewMaskLayer.strokeColor = [UIColor redColor].CGColor;
    self.collectionViewMaskLayer.fillColor = [UIColor blackColor].CGColor;
    
    CGRect frame = self.collectionViewMaskLayer.frame;
    frame.origin = CGPointMake(0, -CGRectGetMaxY(self.speedAccessCollectionView.bounds) + self.menuCollectionView.contentOffset.y );
    self.collectionViewMaskLayer.frame = frame;
    
    self.menuCollectionView.layer.mask = self.collectionViewMaskLayer;
}

- (void)prepareZigZagView
{
    UIView *zigZagView = [[UIView alloc] initWithFrame:self.speedAccessCollectionView.frame];
    zigZagView.backgroundColor = [UIColor clearColor];
    
    UIBezierPath *zigZagPath = [self setupZigZagPath];
    
    CAShapeLayer *zigZagLayer = [CAShapeLayer layer];
    zigZagLayer.path = zigZagPath.CGPath;
    zigZagLayer.strokeColor = [UIColor whiteColor].CGColor;
    zigZagLayer.fillColor = [UIColor clearColor].CGColor;
    [zigZagView.layer addSublayer:zigZagLayer];
    zigZagView.tag = ZigZagViewTag;
    
    self.speedAccessCollectionView.backgroundView = zigZagView;
}

- (UIBezierPath *)setupZigZagPath
{
    CGFloat heighOFScreen = SCREEN_HEIGHT;
    
    CGSize cellSize = CGSizeMake(( self.speedAccessCollectionView.frame.size.width - (CellSpacing * (RowCount + 1))) / RowCount, heighOFScreen * 0.18f);
    CGSize  hexagonSize = CGSizeMake(cellSize.width * 0.6f, cellSize.height * 0.87f);
    CGFloat minimumYPoint = hexagonSize.height * (IS_IPAD ? 0.8f : 0.9f);
    CGFloat maximumYPoint = hexagonSize.height * 1.15f;

    UIBezierPath *zigZagPath = [UIBezierPath bezierPath];
    [zigZagPath moveToPoint:CGPointMake(0, minimumYPoint + CellSpacing)];
    [zigZagPath addLineToPoint:CGPointMake(CellSpacing, minimumYPoint)];
    
    CGFloat shadowOffset = 2;
    
    for (int i = 0; i < RowCount; i++) {
        [zigZagPath addLineToPoint:CGPointMake(((cellSize.width / 2 + CellSpacing - shadowOffset / 2) + (cellSize.width + CellSpacing ) * i), maximumYPoint)];
        if (i != RowCount - 1) {
            [zigZagPath addLineToPoint:CGPointMake((cellSize.width + CellSpacing ) * (i + 1) + shadowOffset,  minimumYPoint)];
        } else {
            [zigZagPath addLineToPoint:CGPointMake((cellSize.width + CellSpacing) * (i + 1),  minimumYPoint)];
        }
    }
    [zigZagPath addLineToPoint:CGPointMake(SCREEN_WIDTH, minimumYPoint + CellSpacing)];
    return zigZagPath;
}

#pragma mark - Animations vs Calculation for Animations

- (void)animationDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.menuCollectionView) {
        self.stopAnimate = YES;
        CGFloat heighOFScreen = SCREEN_HEIGHT;
        CGFloat minimumAllowedY = (heighOFScreen * TopViewHeightMultiplierValue) / 2;
        CGFloat constantValue = self.isScrollintToTop ? minimumAllowedY : 0;
        
        if (- self.speedAccessCollectionViewTopSpaceConstraint.constant == 0 || - self.speedAccessCollectionViewTopSpaceConstraint.constant == minimumAllowedY) {
            self.stopAnimate = NO;
        } else {
            [self.view layoutIfNeeded];
            __weak typeof(self) weakSelf = self;
            CGFloat duration =  0.3 * ABS(self.speedAccessCollectionViewTopSpaceConstraint.constant + constantValue) / minimumAllowedY;
            
            [UIView animateWithDuration:duration
                             animations:^{
                weakSelf.speedAccessCollectionViewTopSpaceConstraint.constant = - constantValue;
                [weakSelf.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                weakSelf.stopAnimate = NO;
            }];
            
            self.topView.duration =  duration;
            [CATransaction begin];
            [CATransaction setDisableActions:YES];
            [self.topView scaleLogo:!self.isScrollintToTop];
            [self speedAcceesCollectionViewCellScale:!self.isScrollintToTop duration:duration];
            [self.topView animateBottomElementsMovingToTop:self.isScrollintToTop];
            [self.topView animateFakeButtonsLayerMovingToTop:self.isScrollintToTop];
            [CATransaction setDisableActions:NO];
            [CATransaction commit];
        }
    }
}

- (void)animationMaskMainCollectionView
{
    CGRect frame = self.collectionViewMaskLayer.frame;
    frame.origin = CGPointMake(0, -CGRectGetMaxY(self.speedAccessCollectionView.bounds) + self.menuCollectionView.contentOffset.y );
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    self.collectionViewMaskLayer.frame = frame;
    [CATransaction setDisableActions:NO];
    [CATransaction commit];
}

- (void)animateTopLogoWithProgress:(CGFloat)progress
{
    CGFloat scalePercent = progress == 1 ? progress : 1 - (1 - LogoScaleMinValue) * progress;
    
    if (scalePercent < 1 && scalePercent) {
        [self.topView animateLogoScaling:scalePercent];
    }
}

- (void)animateSpeedAcceesCollectionViewCellWithScaleFactor:(CGFloat)scaleFactor
{
    CGFloat scalePercent = scaleFactor == 1 ? scaleFactor : 1 - (1 - MaxScaleFactorForSpeedAccessCell) * scaleFactor;
    
    if (scalePercent < 1 && scalePercent) {
        for (CategoryCollectionViewCell *cell in [self.speedAccessCollectionView visibleCells]) {
            CATransform3D transformation = CATransform3DIdentity;
            transformation = CATransform3DScale(transformation, scalePercent, scalePercent, 1);
            cell.layer.transform  = transformation;
        }
    }
}

- (void)speedAcceesCollectionViewCellScale:(BOOL)animate duration:(CGFloat)duration
{
    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell *)[[self.speedAccessCollectionView visibleCells] firstObject];
    CATransform3D startTransform = cell.layer.transform;
    
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    CATransform3D transformation = CATransform3DIdentity;
    transformation = animate ? CATransform3DIdentity : CATransform3DScale(transformation, MaxScaleFactorForSpeedAccessCell, MaxScaleFactorForSpeedAccessCell, 1);
    scaleAnimation.fromValue = [NSValue valueWithCATransform3D:startTransform];
    scaleAnimation.toValue = [NSValue valueWithCATransform3D:transformation];
    scaleAnimation.duration = duration;
    
    for (CategoryCollectionViewCell *cell in [self.speedAccessCollectionView visibleCells]) {
        [cell.layer addAnimation:scaleAnimation forKey:nil];
        cell.layer.transform  = transformation;
    }
}

- (void)detectScrollDirectioninScrollView:(UIScrollView *)scrollView
{
    if (self.lastContentOffset > scrollView.contentOffset.y) {
        self.isScrollintToTop = NO;
    } else if (self.lastContentOffset < scrollView.contentOffset.y) {
        self.isScrollintToTop = YES;
    }
}

- (void)animateSpeedAccessCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CAKeyframeAnimation *moveAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    moveAnim.values = @ [
                         [NSValue valueWithCGPoint:CGPointMake(cell.center.x, 0)],
                         [NSValue valueWithCGPoint:CGPointMake(cell.center.x, 0)],
                         [NSValue valueWithCGPoint:cell.center]
                         ];
    moveAnim.keyTimes = @[@(0), @(0.1 * indexPath.row), @(1)];
    
    CABasicAnimation *opacityAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnim.fromValue = @(0);
    opacityAnim.toValue = @(1);
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[moveAnim, opacityAnim];
    group.duration = 0.15 + 0.05 * indexPath.row;
    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [cell.layer addAnimation:group forKey:nil];
    
    BOOL shouldBeScaled = self.speedAccessCollectionView.frame.origin.y < CGRectGetMaxY(self.topView.frame);
    
    CATransform3D transformation = CATransform3DIdentity;
    transformation = shouldBeScaled ? CATransform3DScale(transformation, MaxScaleFactorForSpeedAccessCell, MaxScaleFactorForSpeedAccessCell, 1) : CATransform3DIdentity;
    cell.layer.transform = transformation;
}

- (void)animateOtherCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CABasicAnimation *opacityAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnim.fromValue = @(0);
    opacityAnim.toValue = @(1);
    opacityAnim.duration = 0.15 + 0.05 * indexPath.row;
    [cell.layer addAnimation:opacityAnim forKey:nil];
    
    CGPoint center = cell.center;
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        [self animationWithStartY:center.y - 5 stopY:center.y duration:0.2 andLayer:cell.layer];
    }];
    [self animationWithStartY:center.y + 50 stopY:center.y - 5 duration:0.2 andLayer:cell.layer];
    [CATransaction commit];
}

@end
