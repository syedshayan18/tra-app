//
//  NewHomeMenuCollectionViewCell.h
//  TRA Smart Services
//
//  Created by Shayan Ali on 13/09/2018.
//  Copyright © 2018 UAE TRA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewHomeMenuCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *menuLabel;
@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;
@end
