//
//  NewHomeViewController.h
//  TRA Smart Services
//
//  Created by Shayan Ali on 13/09/2018.
//  Copyright © 2018 UAE TRA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTopBarView.h"

typedef NS_ENUM(NSUInteger, NavigationTransitionType) {
    NavigationTransitionTypeDefault,
    NavigationTransitionTypeComplaintASP,
    NavigationTransitionTypeComplainTRA,
    NavigationTransitionTypeSearch
};



@interface NewHomeViewController : BaseDynamicUIViewController <UICollectionViewDataSource, UICollectionViewDelegate, HomeTopBarViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong) NSArray *menuImages,*menuLabels,*sectionNames,*enquiryLabels,*IFSALabels,*customsLabel;
@property (assign, nonatomic) NavigationTransitionType navigationTransitionType;

- (void)navigationTransitionViewController:(NavigationTransitionType)navigationType;

@end
