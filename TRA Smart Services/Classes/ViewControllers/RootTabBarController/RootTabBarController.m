//
//  RootTabBarController.m
//  TRA Smart Services
//
//  Created by Admin on 16.06.16.
//  Copyright © 2016 . All rights reserved.
//

#import "RootTabBarController.h"
#import "AnnoucementsViewController.h"
#import "InfoHubViewController.h"
#import "InfoHubNavigationController.h"

static NSString *const InfoHubViewControllerStoryboardId = @"InfoHubViewController";
static NSString *const AnnoucementsViewControllerStoryboardId = @"AnnoucementsViewController";

@implementation RootTabBarController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.delegate = self;
}

#pragma mark - UITabBarControllerDelegate

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if ([viewController isKindOfClass:[InfoHubNavigationController class]]) {
        InfoHubNavigationController *infoHubNavigationController = (InfoHubNavigationController *)viewController;
        if ([NetworkManager sharedManager].isUserLoggined) {
            if (![infoHubNavigationController.viewControllers[0] isKindOfClass:[InfoHubViewController class]]) {
                InfoHubViewController *infoHubViewController = [self.storyboard instantiateViewControllerWithIdentifier:InfoHubViewControllerStoryboardId];
                infoHubNavigationController.viewControllers = @[infoHubViewController];
            }
        } else {
            if (![infoHubNavigationController.viewControllers[0] isKindOfClass:[AnnoucementsViewController class]]) {
                AnnoucementsViewController *annoucementsViewController = [self.storyboard instantiateViewControllerWithIdentifier:AnnoucementsViewControllerStoryboardId];
                infoHubNavigationController.viewControllers = @[annoucementsViewController];
            }
        }
    }
    return YES;
}

#pragma mark - Private

- (void)selectRootViewController
{
    if ([NetworkManager sharedManager].isUserLoggined) {
        if (![self.viewControllers[0] isKindOfClass:[InfoHubViewController class]]) {
            InfoHubViewController *infoHubViewController = [self.storyboard instantiateViewControllerWithIdentifier:InfoHubViewControllerStoryboardId];
            self.viewControllers = @[infoHubViewController];
        }
    } else {
        if (![self.viewControllers[0] isKindOfClass:[AnnoucementsViewController class]]) {
            AnnoucementsViewController *annoucementsViewController = [self.storyboard instantiateViewControllerWithIdentifier:AnnoucementsViewControllerStoryboardId];
            self.viewControllers = @[annoucementsViewController];
        }
    }
}


@end
