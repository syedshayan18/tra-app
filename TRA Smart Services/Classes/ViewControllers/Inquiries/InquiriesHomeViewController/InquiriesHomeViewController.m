
//
//  InquiriesHomeViewController.m
//  TRA Smart Services
//
//  Created by Admin on 02.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "InquiriesHomeViewController.h"
#import "InquiriesHomeCollectionViewCell.h"
#import "InquiriesSelectServicesViewController.h"
#import "LoginViewController.h"
static NSString *const InquiriesHomeToSelectServicesSegueIdentifier = @"InquiriesHomeToSelectServicesSegue";
static NSString *const InquiriesHomeToFAQSegueIdentifier = @"segueFAQViewController";
static CGFloat const HeigthInquiriesHomeCollectionViewCell = 125.;

@interface InquiriesHomeViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *categoriTitlesLabel;
@property (strong, nonatomic) NSArray *inquiriesIconArray;
@property (strong, nonatomic) NSArray *inquiriesTitleArray;

@end

@implementation InquiriesHomeViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareDataSourceCollectionView];
    [self prepareNavigationBar];
}
-(void)presentLoginIfNeeded{
    
    
    if ([NetworkManager sharedManager].isUserLoggined) {
        
    } else {
        UINavigationController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavigationController"];
        __weak typeof(self) weakSelf = self;
        ((LoginViewController *)viewController.topViewController).didCloseViewController = ^() {
            if (![NetworkManager sharedManager].isUserLoggined) {
                weakSelf.tabBarController.selectedIndex = 0;
            }
        };
        ((LoginViewController *)viewController.topViewController).didDismissed = ^() {
            if ([NetworkManager sharedManager].isUserLoggined) {
                
            }
        };
        ((LoginViewController *)viewController.topViewController).shouldAutoCloseAfterLogin = YES;
        [AppHelper presentViewController:viewController onController:self.navigationController];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self presentLoginIfNeeded];
    self.collectionView.userInteractionEnabled = YES;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.inquiriesTitleArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    InquiriesHomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:InquiriesHomeCollectionViewCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell indexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //shayan coded
    if (indexPath.row == 0) {
                [self openConsumerGiude];
            } else {
                [self performSegueWithIdentifier:InquiriesHomeToFAQSegueIdentifier sender:nil];
            }
    
    //original code commented shayan
//    if (indexPath.row < 2) {
//        [self performSegueWithIdentifier:InquiriesHomeToSelectServicesSegueIdentifier sender:nil];
//    } else if (indexPath.row == 2) {
//        [self openConsumerGiude];
//    } else if (indexPath.row == 3) {
//        [self performSegueWithIdentifier:InquiriesHomeToFAQSegueIdentifier sender:nil];
//    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat offset = self.collectionView.frame.size.width / 4. - 60.;
    return UIEdgeInsetsMake(0.0, offset, 0.0, offset);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize screenSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
    CGFloat collectionViewWidth = screenSize.width;
    CGFloat minimumSpacingForCell = 10.;
    CGFloat offset = self.collectionView.frame.size.width / 4. - 60.;
    
    CGFloat cellWidth = collectionViewWidth / 2. - minimumSpacingForCell - offset;
    CGFloat cellHeight = HeigthInquiriesHomeCollectionViewCell;
    
    if (self.inquiriesIconArray.count % 2 && indexPath.row == self.inquiriesIconArray.count - 1) {
        cellWidth = collectionViewWidth - 2 * offset;
    }
    
    CGSize cellSize = CGSizeMake(cellWidth, cellHeight);
    return cellSize;
}

#pragma mark - Navigations

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:InquiriesHomeToSelectServicesSegueIdentifier]) {
        [self prepareInquiriesSelectServicesViewControllerWithSegue:segue];
    }
}

- (void)prepareInquiriesSelectServicesViewControllerWithSegue:(UIStoryboardSegue *)segue
{
    NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
    NSInteger cellRow = [indexPaths[0] row];
    InquiriesSelectServicesViewController *destantionViewController = segue.destinationViewController;
    destantionViewController.iconImage = self.inquiriesIconArray[cellRow];
    destantionViewController.selectType = cellRow ? InquiriesSelectSuggections : InquiriesSelectComplaints;
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"inquiriesHomeViewController.title");
    self.categoriTitlesLabel.text = dynamicLocalizedString(@"inquiriesHomeViewController.categoriTitlesLabel");
    [self prepareDataSourceCollectionView];
    [self.collectionView reloadData];
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
}

- (void)setLTREuropeUI
{
    
}

- (void)setRTLArabicUI
{
    
}

#pragma mark - ConfigureCell

- (void)configureCell:(InquiriesHomeCollectionViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    
    cell.inquriesTitleLabel.text = self.inquiriesTitleArray[indexPath.row];
    cell.inquiriesIconImageView.image = self.inquiriesIconArray[indexPath.row];
    
    
    UIColor *colorTintCell = self.dynamicService.currentApplicationColor;
    
    cell.inquriesTitleLabel.textColor = colorTintCell;
    cell.inquiriesIconImageView.tintColor = colorTintCell;
    [AppHelper addHexagonBorderForLayer:cell.polygonView.layer color:colorTintCell width:1];
    if (self.dynamicService.colorScheme == ApplicationColorBlackAndWhite && indexPath.row == 0) {
        
         cell.inquiriesIconImageView.image = [[BlackWhiteConverter sharedManager] convertedBlackAndWhiteImage:cell.inquiriesIconImageView.image];
        
        
        
    }
}

#pragma mark - Private

- (void)prepareDataSourceCollectionView
{
    //shayan original comment
    //    self.inquiriesTitleArray = @[dynamicLocalizedString(@"inquiriesHomeViewController.segueName.complaints"), dynamicLocalizedString(@"inquiriesHomeViewController.segueName.suggestions"),
    //        dynamicLocalizedString(@"inquiriesHomeViewController.segueName.giude"), dynamicLocalizedString(@"FAQViewController.title")];
    //    self.inquiriesIconArray = @[[UIImage imageNamed:@"ic_inq_edit"], [UIImage imageNamed:@"ic_inq_suggest"], [UIImage imageNamed:@"ic_inq_guide"], [UIImage imageNamed:@"ic_faq"]];
    
    
    self.inquiriesTitleArray = @[
                                 dynamicLocalizedString(@"inquiriesHomeViewController.segueName.giude"), dynamicLocalizedString(@"FAQViewController.title")];
    self.inquiriesIconArray = @[[UIImage imageNamed:@"ic_inq_guide"], [UIImage imageNamed:@"ic_faq"]];
}

- (void)prepareNavigationBar
{
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [AppHelper titleFontForNavigationBar:self.navigationController.navigationBar];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style: UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

- (void)openConsumerGiude
{
    NSString *arPDF = [[NSUserDefaults standardUserDefaults] stringForKey:@"arPDF"];
    NSString *enPDF = [[NSUserDefaults standardUserDefaults] stringForKey:@"enPDF"];
    
    if (arPDF != nil || enPDF != nil) {
        NSString *currentPDF = [DynamicUIService service].language == LanguageTypeArabic ? arPDF : enPDF;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:currentPDF]];
    }
}

@end
