//
//  InquiriesHomeViewController.h
//  TRA Smart Services
//
//  Created by Admin on 02.02.16.
//  Copyright © 2016 . All rights reserved.
//

static NSString *const InquiriesHomeCollectionViewCellIdentifier = @"InquiriesHomeCollectionViewCell";

@interface InquiriesHomeViewController : BaseDynamicUIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@end
