//
//  InquiriesSelectServicesViewController.h
//  TRA Smart Services
//
//  Created by Admin on 02.02.16.
//  Copyright © 2016 . All rights reserved.
//

typedef NS_ENUM(NSUInteger, InquiriesSelectType) {
    InquiriesSelectComplaints,
    InquiriesSelectSuggections
};

@interface InquiriesSelectServicesViewController : BaseDynamicUIViewController <UICollectionViewDataSource, UIBarPositioningDelegate, UICollectionViewDelegateFlowLayout>

@property (assign, nonatomic) InquiriesSelectType selectType;
@property (strong, nonatomic) UIImage *iconImage;

@end
