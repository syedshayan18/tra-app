//
//  InquiriesSelectServicesViewController.m
//  TRA Smart Services
//
//  Created by Admin on 02.02.16.
//  Copyright © 2016 . All rights reserved.
//

static CGFloat const WidthCell = 100.;

#import "InquiriesSelectServicesViewController.h"
#import "InquiriesSelectServiceCollectionViewCell.h"
#import "SpamReportViewController.h"
#import "InquiriesTutorialSelectViewController.h"
#import "TRAStaticService.h"
#import "DynamicContainerServiceViewController.h"
#import "TRADynamicService.h"

@interface InquiriesSelectServicesViewController ()

@property (weak, nonatomic) IBOutlet UIView *polygonView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *containetView;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeGestureRecognazer;
@property (strong, nonatomic) CAShapeLayer *lineLayer;

@property (strong, nonatomic) NSArray <TRADynamicService *> *dataSource;

@property (strong, nonatomic) NSMutableArray *inquiriesSelectServiceDataSource;
@property (strong, nonatomic) NSMutableArray *traServicesEnableList;

@end

@implementation InquiriesSelectServicesViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareDataSource];
    [self prepareTutorilButton];
    if (self.selectType == InquiriesSelectSuggections) {
        self.swipeGestureRecognazer.direction = UISwipeGestureRecognizerDirectionDown;
    } else {
        self.swipeGestureRecognazer.direction = UISwipeGestureRecognizerDirectionUp;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareUI];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style: UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
 
    [self updateLineCellToCenter];
}

#pragma mark - IBAction

- (IBAction)swipeAction:(id)sender
{
    if (self.selectType == InquiriesSelectComplaints) {
        self.selectType = InquiriesSelectSuggections;
        self.swipeGestureRecognazer.direction = UISwipeGestureRecognizerDirectionDown;
    } else {
        self.selectType = InquiriesSelectComplaints;
        self.swipeGestureRecognazer.direction = UISwipeGestureRecognizerDirectionUp;
    }
    [self animationReloadData];
}

- (void)tapTutolialBarButtonItem
{
    InquiriesTutorialSelectViewController *inquiriesTutorialSelectViewController = [self.storyboard instantiateViewControllerWithIdentifier:InquiriesTutorialSelectViewControllerStoryboadID];
    inquiriesTutorialSelectViewController.backgroundImage = [AppHelper snapshotForView:self.tabBarController.view];
    
    NSArray *visibleCells = [self.collectionView visibleCells];
    for (InquiriesSelectServiceCollectionViewCell *cell in visibleCells) {
        cell.titleServiceLabel.textColor = [UIColor whiteColor];
    }
    self.containetView.alpha = 0.5;
    self.collectionView.alpha = 0.;
    inquiriesTutorialSelectViewController.snapshotContainerViewImage = [AppHelper snapshotForView:self.containetView];
    self.containetView.alpha = 1.;
    self.collectionView.alpha = 1.;
    inquiriesTutorialSelectViewController.snapshotCollectionViewImage = [AppHelper snapshotForView:self.collectionView];
    self.polygonView.backgroundColor = [UIColor whiteColor];
    
    [self presentViewController:inquiriesTutorialSelectViewController animated:NO completion:nil];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.inquiriesSelectServiceDataSource.count + self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    InquiriesSelectServiceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:InquiriesSelectServiceCollectionViewCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell indexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (![NetworkManager sharedManager].networkStatus) {
        [AppHelper alertViewWithMessage:dynamicLocalizedString(@"message.NoInternetConnection")];
        return;
    }
    if (indexPath.row < self.inquiriesSelectServiceDataSource.count) {
        NSDictionary *selectedServiceDetails = self.inquiriesSelectServiceDataSource[indexPath.row];
        BOOL disableCell = NO;
        for (TRAStaticService *traService in self.traServicesEnableList) {
            if ([traService.serviceName isEqualToString:[selectedServiceDetails valueForKey:@"serviceName"]]) {
                disableCell = ![traService.serviceEnable boolValue];
            }
        }
        if (!disableCell) {
            [self performNavigationToServiceWithIndex:[[selectedServiceDetails valueForKey:@"serviceID"] integerValue]];
        }
    } else {
        TRADynamicService *traDynamicService = (TRADynamicService *)self.dataSource[indexPath.row - self.inquiriesSelectServiceDataSource.count];
        DynamicServiceModel *dynamicServiceModel = [[DynamicServiceModel alloc] initWithTRADynamicServise:traDynamicService];
        [self presentDynamicService:dynamicServiceModel];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat offset = (self.collectionView.frame.size.width - WidthCell) / 2.;
    return UIEdgeInsetsMake(0.0, offset, 0.0, offset);
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateLineCellToCenter];
}

#pragma mark - Navigations

- (void)presentDynamicService:(DynamicServiceModel *)dynamicServiceModel
{
    UIViewController *selectedDynamicService = [self.storyboard instantiateViewControllerWithIdentifier:DynamicServiceIDstoryboard];
    ((DynamicContainerServiceViewController *)selectedDynamicService).dynamicServiceModel = dynamicServiceModel;
    [self.navigationController pushViewController:selectedDynamicService animated:YES];
}

- (void)performNavigationToServiceWithIndex:(NSUInteger)navigationIndex
{
    NSArray *serviceIdentifiers = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ServiceViewControllersIdentifiers" ofType:@"plist"]];
    
    UIViewController *selectedService = [self.storyboard instantiateViewControllerWithIdentifier:serviceIdentifiers[navigationIndex]];

    if (navigationIndex == 5) {
        ((SpamReportViewController *)selectedService).selectSpamReport = SpamReportTypeSMS;
    } else if (navigationIndex == 6) {
        ((SpamReportViewController *)selectedService).selectSpamReport = SpamReportTypeWeb;
    }
    
    if (selectedService) {
        if ([selectedService isKindOfClass:[BaseServiceViewController class]]) {
            if (navigationIndex != 14) {
                [(BaseServiceViewController *)selectedService setServiceID:navigationIndex];
            }
        }
        [self.navigationController pushViewController:selectedService animated:YES];
    }
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    if (self.selectType == InquiriesSelectComplaints) {
        self.title = dynamicLocalizedString(@"inquiriesSelectServicesViewController.title.complaints");
    } else if (self.selectType == InquiriesSelectSuggections) {
        self.title = dynamicLocalizedString(@"inquiriesSelectServicesViewController.title.suggestions");
    }
    [self.collectionView reloadData];
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
}

- (void)setLTREuropeUI
{
    
}

- (void)setRTLArabicUI
{
    
}

#pragma mark - ConfigureCell

- (void)configureCell:(InquiriesSelectServiceCollectionViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    UIColor *color = self.dynamicService.currentApplicationColor;
    UIColor *textColor = [UIColor inquiriesActiveTextColor];

    if (indexPath.row < self.inquiriesSelectServiceDataSource.count) {
        NSDictionary *selectedServiceDetails = self.inquiriesSelectServiceDataSource[indexPath.row];
        BOOL disableCell = NO;
        for (TRAStaticService *traService in self.traServicesEnableList) {
            if ([traService.serviceName isEqualToString:[selectedServiceDetails valueForKey:@"serviceName"]]) {
                disableCell = ![traService.serviceEnable boolValue];
            }
        }
        if (disableCell) {
            color = [UIColor inquiriesGreyColor];
            textColor = [UIColor inquiriesGreyColor];
        }
        if ([[selectedServiceDetails valueForKey:@"serviceLogo"] length]) {
            cell.logoImageView.image = [UIImage imageNamed:[selectedServiceDetails valueForKey:@"serviceLogo"]];
        }
        cell.categoryID = [[selectedServiceDetails valueForKey:@"serviceID"] integerValue];
        cell.titleServiceLabel.text = dynamicLocalizedString([selectedServiceDetails valueForKey:@"serviceReName"]);
    } else {
        TRADynamicService *traDynamicService = (TRADynamicService *)self.dataSource[indexPath.row - self.inquiriesSelectServiceDataSource.count];
        UIImage *logoPlaceholder = [UIImage imageNamed:@"ic_domain_logo"];
        [AppHelper downloadToImageView:cell.logoImageView url:traDynamicService.serviceIcon placeholderImage:logoPlaceholder];
        NSString *nameService = self.dynamicService.language == LanguageTypeArabic ? traDynamicService.serviceNameAR : traDynamicService.serviceNameEN;
        cell.titleServiceLabel.text = [nameService stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    }
    
    cell.titleServiceLabel.font = self.dynamicService.language == LanguageTypeArabic ? [UIFont droidKufiRegularFontForSize:12] : [UIFont latoRegularWithSize:12];
    cell.titleServiceLabel.textColor = textColor;
    cell.logoImageView.tintColor = color;
    [AppHelper addHexagonBorderForLayer:cell.polygonView.layer color:color width:1];
    [AppHelper addHexagoneOnView:cell.polygonView];
    [self prepareShadowView:cell.containerShadowView];
}

- (void)prepareShadowView:(UIView *)view
{
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
    view.layer.shadowOffset = CGSizeMake(1, 1);
    view.layer.shadowOpacity = 0.2;
    view.layer.masksToBounds = YES;
    view.layer.shadowRadius = 1.5;
}

#pragma mark - Private

- (void)prepareUI
{
    [AppHelper addHexagonBorderForLayer:self.polygonView.layer color:[UIColor inquiriesLineColor] width:1];
    [AppHelper addHexagoneOnView:self.polygonView];

    self.iconImageView.image = self.iconImage;
    self.iconImageView.tintColor = [UIColor inquiriesLineColor];
    self.collectionView.decelerationRate = UIScrollViewDecelerationRateFast;
    self.collectionView.scrollEnabled = NO;
}

- (void)prepareTutorilButton
{
    UIBarButtonItem *tutorialBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_quest"] style:UIBarButtonItemStylePlain target:self action:@selector(tapTutolialBarButtonItem)];
    self.navigationItem.rightBarButtonItem = tutorialBarButtonItem;
}

- (void)prepareDataSource
{
    NSString *fileName = self.selectType ? @"InquiriesSuggectionsServices" : @"InquiriesComplaintsServices";
    self.inquiriesSelectServiceDataSource = [[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"]];
    self.traServicesEnableList = [[[CoreDataManager sharedManager] fetchServiceList] mutableCopy];

    self.dataSource = [[[CoreDataManager sharedManager] fetchDynamicServiceList] mutableCopy];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"serviceInquiriesCategory == %@", self.selectType ?  @"Suggestions" : @"Complaints"];
    self.dataSource = [self.dataSource filteredArrayUsingPredicate:predicate];
}

- (void)updateLineCellToCenter
{
    NSArray *visibleCell = [self.collectionView visibleCells];
    
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    
    for (InquiriesSelectServiceCollectionViewCell *cell in visibleCell) {
        [linePath moveToPoint:self.polygonView.center];
        CGRect frame = [self.containetView convertRect:cell.bounds fromView:cell];
        
        [linePath addLineToPoint:CGPointMake(frame.origin.x + frame.size.width / 2, frame.origin.y + cell.polygonView.center.y)];
    }
    [self.lineLayer removeFromSuperlayer];
    self.lineLayer = [CAShapeLayer layer];
    self.lineLayer.frame = self.view.frame;
    self.lineLayer.path = linePath.CGPath;
    self.lineLayer.fillColor = [UIColor lightGrayBorderColor].CGColor;
    self.lineLayer.strokeColor = [UIColor lightGrayBorderColor].CGColor;
    [self.containetView.layer insertSublayer:self.lineLayer below:self.polygonView.layer];
}

- (void)animationReloadData
{
    __weak typeof(self) weakSelf = self;
    [self.view layoutIfNeeded];
    NSArray *visbleCells = [self.collectionView visibleCells];

    [UIView animateWithDuration:0.15 animations:^{
        weakSelf.polygonView.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5);
        weakSelf.containetView.alpha = 0;
        for (UICollectionView *cell in visbleCells) {
            cell.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5);
            cell.alpha = 0;
        }
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [weakSelf animationReloadDataComplition];
    }];
}

- (void)animationReloadDataComplition
{
    [self prepareDataSource];
    [self localizeUI];
    __weak typeof(self) weakSelf = self;
    [self.collectionView performBatchUpdates:^{
        [weakSelf.collectionView reloadData];
    } completion:^(BOOL finished) {
        NSArray *visbleCells = [weakSelf.collectionView visibleCells];
        for (UITableViewCell *cell in visbleCells) {
            cell.alpha = 0;
            cell.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5);
            
            [UIView beginAnimations:@"scaleCellAnimation" context:NULL];
            [UIView setAnimationDuration:0.2];
            cell.alpha = 1;
            cell.layer.transform = CATransform3DIdentity;
            [UIView commitAnimations];
        }
        
        NSString *polygonImageName = weakSelf.selectType ? @"ic_inq_suggest" : @"ic_inq_edit";
        weakSelf.iconImageView.image = [UIImage imageNamed:polygonImageName];
        [weakSelf.view layoutIfNeeded];
        [UIView animateWithDuration:0.2 animations:^{
            weakSelf.containetView.alpha = 1;
            weakSelf.polygonView.layer.transform = CATransform3DIdentity;
            [weakSelf.view layoutIfNeeded];
        }];
        [weakSelf updateLineCellToCenter];
    }];
    
}

@end
