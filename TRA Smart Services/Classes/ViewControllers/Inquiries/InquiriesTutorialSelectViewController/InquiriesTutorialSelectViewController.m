//
//  InquiriesTutorialSelectViewController.m
//  TRA Smart Services
//
//  Created by Admin on 05.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "InquiriesTutorialSelectViewController.h"
#import "Animation.h"

@interface InquiriesTutorialSelectViewController ()

@property (weak, nonatomic) IBOutlet UILabel *descriptionTextSwipeLabel;
@property (weak, nonatomic) IBOutlet UIView *containerImageSwipeView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundColorView;
@property (weak, nonatomic) IBOutlet UIImageView *snapshotContainerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *snapshotCollectionViewImageView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation InquiriesTutorialSelectViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareUI];
    [self.containerView.layer addAnimation:[Animation fadeAnimFromValue:0.f to:1.0f delegate:nil] forKey:nil];
}

#pragma mark - IBAction

- (IBAction)tapCloseButton:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    self.titleLabel.text = dynamicLocalizedString(@"inquiriesTutorialSelectViewController.titleLabel");
    self.descriptionTextSwipeLabel.text = dynamicLocalizedString(@"inquiriesTutorialSelectViewController.descriptionTextSwipeLabel");
}

- (void)updateColors
{
    self.backgroundColorView.backgroundColor = [self.dynamicService.currentApplicationColor colorWithAlphaComponent:0.95];
}

- (void)setLTREuropeUI
{
    self.descriptionTextSwipeLabel.textAlignment = NSTextAlignmentLeft;
}

- (void)setRTLArabicUI
{
    self.descriptionTextSwipeLabel.textAlignment = NSTextAlignmentRight;
}

#pragma mark - Private

- (void)prepareUI
{
    self.snapshotContainerImageView.image = self.snapshotContainerViewImage;
    self.snapshotCollectionViewImageView.image = self.snapshotCollectionViewImage;
    self.backgroundImageView.image = self.backgroundImage;
    [AppHelper addHexagonBorderForLayer:self.containerImageSwipeView.layer color:[UIColor whiteColor] width:2];
}

@end
