//
//  InquiriesTutorialSelectViewController.h
//  TRA Smart Services
//
//  Created by Admin on 05.02.16.
//  Copyright © 2016 . All rights reserved.
//

static NSString *const InquiriesTutorialSelectViewControllerStoryboadID = @"InquiriesTutorialSelectViewControllerID";

@interface InquiriesTutorialSelectViewController : BaseDynamicUIViewController

@property (strong, nonatomic) UIImage *backgroundImage;
@property (strong, nonatomic) UIImage *snapshotContainerViewImage;
@property (strong, nonatomic) UIImage *snapshotCollectionViewImage;

@end
