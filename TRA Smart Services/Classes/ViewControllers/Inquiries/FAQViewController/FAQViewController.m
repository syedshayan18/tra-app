//
//  FAQViewController.m
//  TRA Smart Services
//
//  Created by Admin on 28.12.15.
//  Copyright © 2015 . All rights reserved.
//

#import "FAQViewController.h"
#import "TRALoaderViewController.h"

@interface FAQViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) TRALoaderViewController *loader;

@end

@implementation FAQViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareHtmlStringWebView];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.loader dismissTRALoader:YES];
}

#pragma mark - Superclass Methods

- (void)localizeUI
{
    self.title = dynamicLocalizedString(@"FAQViewController.title");
}

- (void)updateColors
{
    [super updateBackgroundImageNamed:@"fav_back_orange"];
}

- (void)setLTREuropeUI
{
    
}

- (void)setRTLArabicUI
{
    
}

#pragma mark - Private

- (void)prepareHtmlStringWebView
{
    self.webView.delegate = self;
    self.loader = [TRALoaderViewController presentLoaderOnViewController:self requestName:self.title closeButton:NO];

    NSString *fileName = self.dynamicService.language == LanguageTypeArabic ? @"faqContentAr" : @"faqContentEn";
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:fileName ofType:@"" inDirectory:nil];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:htmlString baseURL:nil];
}

@end
