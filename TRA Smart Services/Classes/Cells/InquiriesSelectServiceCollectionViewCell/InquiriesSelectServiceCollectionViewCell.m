//
//  InquiriesSelectServiceCollectionViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 04.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "InquiriesSelectServiceCollectionViewCell.h"

@implementation InquiriesSelectServiceCollectionViewCell

- (void)prepareForReuse
{
    self.logoImageView.image = nil;
    self.titleServiceLabel.text = @"";
    for (CALayer *layer in self.polygonView.layer.sublayers) {
        if ([layer.name isEqualToString:HexagonBorderLayerName]) {
            [layer removeFromSuperlayer];
        }
    }
}

@end
