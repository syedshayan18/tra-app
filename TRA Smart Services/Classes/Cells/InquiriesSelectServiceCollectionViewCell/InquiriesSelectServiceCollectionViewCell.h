//
//  InquiriesSelectServiceCollectionViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 04.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "ModeAlwaysTemplateImageView.h"

static NSString *const InquiriesSelectServiceCollectionViewCellIdentifier = @"InquiriesSelectServiceCollectionViewCell";

@interface InquiriesSelectServiceCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *containerShadowView;
@property (weak, nonatomic) IBOutlet UILabel *titleServiceLabel;
@property (weak, nonatomic) IBOutlet UIView *polygonView;
@property (weak, nonatomic) IBOutlet ModeAlwaysTemplateImageView *logoImageView;
@property (assign, nonatomic) NSInteger categoryID;

@end
