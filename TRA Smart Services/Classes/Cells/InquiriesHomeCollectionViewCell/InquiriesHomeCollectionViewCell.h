//
//  InquiriesHomeCollectionViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 02.02.16.
//  Copyright © 2016 . All rights reserved.
//

@interface InquiriesHomeCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *inquiriesIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *inquriesTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *polygonView;


@end
