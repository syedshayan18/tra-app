//
//  InquiriesHomeCollectionViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 02.02.16.
//  Copyright © 2016 . All rights reserved.
//

#import "InquiriesHomeCollectionViewCell.h"

@implementation InquiriesHomeCollectionViewCell

- (void)prepareForReuse
{
    self.inquiriesIconImageView.image = nil;
    self.inquriesTitleLabel.text = @"";
    for (CALayer *layer in self.polygonView.layer.sublayers) {
        if ([layer.name isEqualToString:HexagonBorderLayerName]) {
            [layer removeFromSuperlayer];
        }
    }
}

@end
