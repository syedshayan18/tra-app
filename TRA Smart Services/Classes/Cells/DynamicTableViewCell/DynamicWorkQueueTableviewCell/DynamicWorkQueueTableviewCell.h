//
//  DynamicWorkQueueTableviewCell.h
//  TRA Smart Services
//
//  Created by Admin on 16.11.15.
//

#import "DynamicHeaderView.h"
#import "BaseDynamicTableViewCell.h"

static NSString *const DynamicWorkQueueTableviewCellIdentifier = @"DynamicWorkQueueTableviewCell";

@interface DynamicWorkQueueTableviewCell : BaseDynamicTableViewCell <UITableViewDelegate, UITableViewDataSource, DynamicHeaderViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (strong, nonatomic) NSArray *dataSourceWorkQueueTable;

@end
