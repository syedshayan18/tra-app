//
//  DynamicWorkQueueTableviewCell.m
//  TRA Smart Services
//
//  Created by Admin on 16.11.15.
//

#import "DynamicWorkQueueTableviewCell.h"
#import "DynamicWorkItemTableViewCell.h"
#import "DynamicHeaderView.h"
#import "ConfinedTextField.h"

static CGFloat const HeightForToolbars = 44.;

@interface DynamicWorkQueueTableviewCell()

@property (weak, nonatomic) IBOutlet ConfinedTextField *sortTextField;
@property (weak, nonatomic) IBOutlet UITextField *filterTextField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *nameTable;
@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) UITextField *keyboardTextField;

@property (strong, nonatomic) NSMutableArray *sectorFieldOpening;

@end

@implementation DynamicWorkQueueTableviewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DynamicWorkItemTableViewCell" bundle:nil] forCellReuseIdentifier:DynamicWorkItemTableViewCellIdentifier];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self prepareInitialParameters];
    
    [self prepareNotification];
}

- (void)dealloc
{
    [self removeNotifications];
}

#pragma mark - Custom Accessors

- (void)setDynamicInputItem:(DynamicInputItem *)dynamicInputItem
{
    [super setDynamicInputItem:dynamicInputItem];
    
    [self prepareDataSource];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSourceWorkQueueTable.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sectorFieldOpening[section] boolValue] ? 4 : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DynamicWorkItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DynamicWorkItemTableViewCellIdentifier];
    
    cell.dataSource = @[@1, @2, @3, @4];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    DynamicHeaderView *headerCell = [[DynamicHeaderView alloc] init];
    headerCell.titleLabel.text = self.dataSourceWorkQueueTable[section];
    headerCell.delegate = self;
    headerCell.selectedOpenHeader = [self.sectorFieldOpening[section] boolValue];
    headerCell.tag = section;
    return headerCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HeihgtLabel * 4;
}

#pragma mark - DynamicHeaderViewDelegate

- (void)tappedDynamicHeaderView:(DynamicHeaderView *)dynamicHeaderView
{
    self.sectorFieldOpening[dynamicHeaderView.tag] = @(![self.sectorFieldOpening[dynamicHeaderView.tag] boolValue]);
    
    NSMutableArray *indexPathArray = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < 4; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:dynamicHeaderView.tag];
        [indexPathArray addObject:indexPath];
    }
    [self.tableView beginUpdates];

    if ([self.sectorFieldOpening[dynamicHeaderView.tag] boolValue]) {
        [self.tableView insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationNone];
    } else {
        [self.tableView deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationNone];
    }
    [self.tableView endUpdates];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"Choice-%i",(int)row];
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.filterTextField || textField == self.sortTextField) {
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.filterTextField) {
        [self.keyboardTextField becomeFirstResponder];
    } else if (textField == self.sortTextField) {
        
    }
}

#pragma mark - IBAction

- (void)tappedDoneButtonPickerView
{
    if (self.keyboardTextField.text.length) {
        self.filterTextField.text = self.keyboardTextField.text;
        self.keyboardTextField.text = @"";
    }
    
    [self.contentView endEditing:YES];
}

- (void)tappedCancelButtonPickerView
{
    [self.contentView endEditing:YES];
}

#pragma mark - Private

- (void)prepareInitialParameters
{
    self.tableView.tableFooterView = [[UIView alloc] init];

    [self prepareTextField:self.filterTextField image:[UIImage imageNamed: @"ic_filter"]];
    [self prepareTextField:self.sortTextField image:[UIImage imageNamed: @"ic_sort"]];
    self.filterTextField.placeholder = dynamicLocalizedString(@"DynamicWorkQueueTableviewCell.filterTextField.placeholder");
    self.sortTextField.placeholder = dynamicLocalizedString(@"DynamicWorkQueueTableviewCell.sortTextField.placeholder");
    
    [self configurePickerController];
    [self configureKeyboardToolbar:self.filterTextField];
}

- (void)prepareTextField:(UITextField *)textField image:(UIImage *)image
{
    textField.layer.borderWidth = 1.;
    textField.layer.cornerRadius = 2.5;
    textField.layer.borderColor = [DynamicUIService service].currentApplicationColor.CGColor;
    textField.textColor = [DynamicUIService service].currentApplicationColor;
    textField.delegate = self;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 35, 13)];
    imageView.contentMode = UIViewContentModeCenter;
    imageView.image = image;
    imageView.tintColor = [DynamicUIService service].currentApplicationColor;
    textField.rightView = nil;
    textField.leftView = nil;
    if ([DynamicUIService service].language == LanguageTypeArabic) {
        textField.rightViewMode = UITextFieldViewModeAlways;
        textField.rightView = imageView;
        textField.textAlignment = NSTextAlignmentRight;
    } else {
        textField.leftViewMode = UITextFieldViewModeAlways;
        textField.leftView = imageView;
        textField.textAlignment = NSTextAlignmentLeft;
    }
}

- (void)configurePickerController
{
    self.pickerView = [[UIPickerView alloc] init];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    self.sortTextField.inputView = self.pickerView;
    [self configureKeyboardToolbar:self.sortTextField];
}

- (void)configureKeyboardToolbar:(UITextField *)textField
{
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, HeightForToolbars)];
    toolBar.backgroundColor = [UIColor clearColor];
    toolBar.tintColor = [[DynamicUIService service] currentApplicationColor];
    
    UIBarButtonItem *barItemDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(tappedDoneButtonPickerView)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *barItamCancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(tappedCancelButtonPickerView)];
    
    if (textField == self.filterTextField) {
        [self prepareKeyboardTextField:toolBar.frame];
        UIBarButtonItem *fakeTextField = [[UIBarButtonItem alloc] initWithCustomView:self.keyboardTextField];
        
        [toolBar setItems:@[barItamCancel, flexibleSpace, fakeTextField, flexibleSpace, barItemDone]];
        
        [self.filterTextField setInputAccessoryView:toolBar];
    } else {
        [toolBar setItems:@[barItamCancel,flexibleSpace, barItemDone]];
        
        [self.sortTextField setInputAccessoryView:toolBar];
    }
}

- (void)prepareKeyboardTextField:(CGRect)frame
{
    if (!self.keyboardTextField) {
        CGFloat heightForTextField = HeightForToolbars * 0.8;
        self.keyboardTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, heightForTextField / 2, frame.size.width / 2, heightForTextField)];
        self.keyboardTextField.borderStyle = UITextBorderStyleRoundedRect;
        self.keyboardTextField.placeholder = dynamicLocalizedString(@"DynamicWorkQueueTableviewCell.keyboardextField.placeholder");
        self.keyboardTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.keyboardTextField.delegate = self;
    }
}

- (void)prepareDataSource
{
    self.nameTable.text = [self.dynamicInputItem localizableString:self.dynamicInputItem.dynamicInputItemDisplayName];

    self.dataSourceWorkQueueTable = @[ @"test", @"test2", @"test3", @"test4", @"test5"];
    
    self.sectorFieldOpening = [[NSMutableArray alloc] init];
    if (self.dataSourceWorkQueueTable.count == 1) {
        [self.sectorFieldOpening addObject:@YES];
    } else {
        for (NSInteger i =0; i < self.dataSourceWorkQueueTable.count; i++) {
            [self.sectorFieldOpening addObject:@NO];
        }
    }
}

#pragma mark - Keyboard

- (void)prepareNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)removeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)keyboardDidAppear:(NSNotification *)notification
{
    [self.keyboardTextField becomeFirstResponder];
}

@end
