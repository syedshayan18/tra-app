//
//  DynamicTreeViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 19.11.15.
//

#import "DynamicTreeViewTableViewCell.h"
#import "DynamicElementTreeViewTableViewCell.h"
#import "TreeViewDataModel.h"

@interface DynamicTreeViewTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *dynamicLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *listDataSourceFiltered;
@property (strong, nonatomic) TreeViewDataModel *rootTreeViewDataModel;

@end

@implementation DynamicTreeViewTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self baseInit];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
}

#pragma mark - Custom Accessors

- (void)setDynamicInputItem:(DynamicInputItem *)dynamicInputItem
{
    [super setDynamicInputItem:dynamicInputItem];
    
    [self prepareDataSource];
}

#pragma mark - UITableViewDataSource

-  (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listDataSourceFiltered.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DynamicElementTreeViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DynamicElementTreeViewTableViewCellIdentifier];
    
    [self configureCell:cell indexPath:indexPath];

    return  cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    TreeViewDataModel *selectedTreeViewDataModel = self.listDataSourceFiltered[indexPath.row];
    selectedTreeViewDataModel.treeViewOpen = !selectedTreeViewDataModel.treeViewOpen;
    
    self.listDataSourceFiltered = [self.rootTreeViewDataModel arrayListOpenTreeView:self.rootTreeViewDataModel];
    
    NSMutableArray *indexPathArray = [[NSMutableArray alloc] init];
    for (NSInteger i = indexPath.row + 1; i <= indexPath.row + [TreeViewDataModel countOpeningTreeView:selectedTreeViewDataModel]; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPathArray addObject:indexPath];
    }
    [tableView beginUpdates];
    if (selectedTreeViewDataModel.treeViewOpen) {
        [tableView insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationNone];
    }else {
        [tableView deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationNone];
    }
    [tableView endUpdates];
}

#pragma mark - ConfigureCell

- (void)configureCell:(DynamicElementTreeViewTableViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    TreeViewDataModel *indexTreeView = self.listDataSourceFiltered[indexPath.row];
    if (indexTreeView.treeViewArray.count) {
        cell.elementContainerView.backgroundColor = [DynamicUIService service].currentApplicationColor;
    } else {
        cell.elementContainerView.backgroundColor = [UIColor clearColor];
    }
    cell.elementTextLabel.text = indexTreeView.treeViewTitle;
    cell.marginElementContainerConstraint.constant = (indexTreeView.treeViewRang - 1) * 10.;
}

#pragma mark - Privte

- (void)baseInit
{
    [self.tableView registerNib:[UINib nibWithNibName:@"DynamicElementTreeViewTableViewCell" bundle:nil] forCellReuseIdentifier:DynamicElementTreeViewTableViewCellIdentifier];
}

- (void)prepareDataSource
{
    self.dynamicLabel.text = [self.dynamicInputItem localizableString:self.dynamicInputItem.dynamicInputItemDisplayName];
    
 //   self.dynamicInputItem.dynamicInputItemAdditional
    
    NSMutableArray *treeViewDataModelArray = [[NSMutableArray alloc] init];
    for (NSDictionary *item in self.dynamicInputItem.dynamicInputItemDataSource) {
        TreeViewDataModel *treeViewDataModel = [[TreeViewDataModel alloc] initWihtDictionary:item];
        [treeViewDataModelArray addObject:treeViewDataModel];
    }
    self.rootTreeViewDataModel = [[TreeViewDataModel alloc] initWihtString:@"root" acvive:YES array:treeViewDataModelArray];
    
    self.listDataSourceFiltered = [self.rootTreeViewDataModel arrayListOpenTreeView:self.rootTreeViewDataModel];
}

@end
