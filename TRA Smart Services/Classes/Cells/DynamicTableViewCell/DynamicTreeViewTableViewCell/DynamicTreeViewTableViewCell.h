//
//  DynamicTreeViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 19.11.15.
//

#import "BaseDynamicTableViewCell.h"

@interface DynamicTreeViewTableViewCell : BaseDynamicTableViewCell <UITableViewDelegate, UITableViewDataSource>

@end
