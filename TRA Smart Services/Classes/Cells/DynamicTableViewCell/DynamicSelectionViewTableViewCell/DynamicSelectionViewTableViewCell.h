//
//  DynamicSelectionViewTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 18.04.16.
//  Copyright © 2016 . All rights reserved.
//

#import "BaseDynamicTableViewCell.h"
#import "SelectionView.h"
#import "SelectionPopapView.h"

@interface DynamicSelectionViewTableViewCell : BaseDynamicTableViewCell <SelectionViewDelegate, SelectionPopapViewDelegate>


@property (weak, nonatomic) UIViewController *currentViewController;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UIButton *employeeButton;
@property (weak, nonatomic) IBOutlet UIButton *serviceButton;
@property (weak, nonatomic) IBOutlet UIButton *othersButton;
@property  BOOL IsbuttonView;
@end
