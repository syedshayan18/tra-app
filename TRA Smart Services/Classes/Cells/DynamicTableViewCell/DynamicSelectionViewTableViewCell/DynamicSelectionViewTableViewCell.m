//
//  DynamicSelectionViewTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 18.04.16.
//  Copyright © 2016 . All rights reserved.
//

#import "DynamicSelectionViewTableViewCell.h"

@interface DynamicSelectionViewTableViewCell()


@property (weak, nonatomic) IBOutlet UILabel *dynamicLabel;
@property (weak, nonatomic) IBOutlet SelectionView *selectionView;

@property (strong, nonatomic) SelectionPopapView *selectionPopapView;

@end

@implementation DynamicSelectionViewTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.selectionView.dataSource = nil;
    self.dynamicLabel.text = @"";
}
- (void)awakeFromNib{
    
    if (_IsbuttonView) {
        [_buttonsView setHidden:NO];
    }
   
    [super awakeFromNib];
    
}

#pragma mark - Custom Accessors

- (void)setDynamicInputItem:(DynamicInputItem *)dynamicInputItem
{
    [super setDynamicInputItem:dynamicInputItem];
    
    
    self.selectionView.dataSource = dynamicInputItem.pickerDataSource;
    self.dynamicLabel.text = [dynamicInputItem localizableString:dynamicInputItem.dynamicInputItemDisplayName];
    self.selectionView.selectedIndex = dynamicInputItem.clientSelectPicker;
    
    self.selectionView.delegate = self;
    [self.selectionView requiredIndicatorHidden:YES];
}

#pragma mark - SelectionViewDelegate

- (void)tappedSelectionView:(SelectionView *)selectionView
{
    CGRect frame = [self.contentView convertRect:self.selectionView.bounds fromView:self.selectionView];
    
    frame = [self.selectionView convertRect:self.contentView.bounds toView:self.currentViewController.view];
    
    [self.currentViewController.view endEditing:YES];
    
    self.selectionPopapView = [SelectionPopapView addSelectionPopapMenuToViewController:self.currentViewController dataSource:self.selectionView.dataSource positionYStartAnimation:frame.origin.y + frame.size.height / 2];
    self.selectionPopapView.delegate = self;
}

#pragma mark - SelectionPopapViewDelegate

- (void)SelectionPopapView:(SelectionPopapView *)selectionPopapView selectedIndex:(NSInteger)index
{
    self.selectionView.selectedIndex = index;
    self.dynamicInputItem.clientSelectPicker = index;
    if ([self.delegate respondsToSelector:@selector(dynamicCellDidBecomeFirstResponder:)]) {
        [self.delegate dynamicCellDidBecomeFirstResponder:self];
    }
}

#pragma mark - LocolizibleUI

- (void)setRTLArabicUI
{
    self.dynamicLabel.textAlignment = NSTextAlignmentRight;
    [self.selectionView updateLocalozed];
}

- (void)setLTREuropeUI
{
    self.dynamicLabel.textAlignment = NSTextAlignmentLeft;
    [self.selectionView updateLocalozed];
}

@end
