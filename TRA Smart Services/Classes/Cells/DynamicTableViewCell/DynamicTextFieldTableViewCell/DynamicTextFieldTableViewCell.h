//
//  DynamicTextFieldTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 13.10.15.
//

#import "BaseDynamicTableViewCell.h"

@class DynamicTextFieldTableViewCell;

@protocol DynamicTextFieldTableViewCellDelegate <BaseDynamicTableViewCellDelegate>

@optional
- (void)dynamicCellDidTappedAttachmentButton:(DynamicTextFieldTableViewCell *)cell;

@end

@interface DynamicTextFieldTableViewCell : BaseDynamicTableViewCell <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>


@property (strong, nonatomic) NSArray *dataSourcePickerView;
@property (strong, nonatomic) NSDate *selectedDate;
@property (assign, nonatomic) NSInteger selectedElement;
@property (copy, nonatomic) NSString *inputText;

@property (assign, nonatomic) ValidationType validationType;

@property (weak, nonatomic) id <DynamicTextFieldTableViewCellDelegate> delegate;

@end
