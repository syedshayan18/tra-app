//
//  DynamicTextFieldTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 13.10.15.
//

static CGFloat const HeightForToolbars = 44.f;
static NSString *const ImageNameButtonAttachClear = @"btn_attach";
static NSString *const ImageNameButtonAttachData = @"btn_attach_file";

#import "DynamicTextFieldTableViewCell.h"
#import "NSString+Validation.h"

@interface DynamicTextFieldTableViewCell()

@property (weak, nonatomic) IBOutlet BottomBorderTextField *dynamicTextField;
@property (weak, nonatomic) IBOutlet UILabel *dynamicLabel;

@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong, nonatomic) UIButton *attachButton;

@end

@implementation DynamicTextFieldTableViewCell
@synthesize delegate;

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.dynamicTextField.text = @"";
    self.dynamicLabel.text = @"";
    self.pickerView = nil;
    self.datePicker = nil;
    self.attachButton = nil;
    self.dynamicTextField.rightView = nil;
    self.dynamicTextField.leftView = nil;
    [self.dynamicTextField setInputAccessoryView:nil];

    
    self.dataSourcePickerView = nil;
    self.selectedDate = nil;
    self.selectedElement = -1;
}

#pragma mark - CustomAccessors

- (void)setDynamicInputItem:(DynamicInputItem *)dynamicInputItem
{
    [super setDynamicInputItem:dynamicInputItem];
    
    [self switchTypeDynamicTextFieldTableViewCell];

    self.validationType = dynamicInputItem.dynamicInputItemValidationType;
    
    self.dataSourcePickerView = dynamicInputItem.pickerDataSource;
    
    self.dynamicLabel.text = [dynamicInputItem localizableString:dynamicInputItem.dynamicInputItemDisplayName];
    self.dynamicTextField.placeholder = [dynamicInputItem localizableString:dynamicInputItem.dynamicInputItemPlaceholdel];
    self.dynamicTextField.delegate = self;
    
    self.inputText = dynamicInputItem.clientString;
    self.selectedElement = dynamicInputItem.clientSelectPicker;
    self.selectedDate = dynamicInputItem.clientDate;
    
    switch (dynamicInputItem.dynamicInputItemValidationType) {
        case ValidationTypeEmail: {
            self.dynamicTextField.keyboardType = UIKeyboardTypeEmailAddress;
            break;
        }
        case ValidationTypeNumber: {
            self.dynamicTextField.keyboardType = UIKeyboardTypeNumberPad;
            break;
        }
        case ValidationTypeURL: {
            self.dynamicTextField.keyboardType = UIKeyboardTypeURL;
            break;
        }
        default: {
            self.dynamicTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            break;
        }
    }
}

- (void)setInputText:(NSString *)inputText
{
    _inputText = inputText;
    self.dynamicTextField.text = inputText;
    [self indicateValidOfString:inputText];
    
    [AppHelper setStyleForTextField:self.dynamicTextField];
}

#pragma mark - IBActions

- (void)doneButtonTapped
{
    [self.dynamicTextField resignFirstResponder];
}

- (void)actionDatePicker:(id)sender
{
    self.dynamicTextField.text = [AppHelper detailedDateStringFrom:self.datePicker.date];
    self.selectedDate = self.datePicker.date;
}

- (void)actionSelectImage
{
    if ([self.delegate respondsToSelector:@selector(dynamicCellDidTappedAttachmentButton:)]) {
        [self.delegate dynamicCellDidTappedAttachmentButton:self];
    }
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.dynamicInputItem.dynamicInputItemInputType == InputItemTypeSelectFile) {
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.dynamicInputItem.dynamicInputItemInputType == InputItemTypeDatePicker || self.dynamicInputItem.dynamicInputItemInputType == InputItemTypePicker ) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.returnKeyType == UIReturnKeyDefault) {
        [textField endEditing:YES];
        return YES;
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.dynamicInputItem.dynamicInputItemInputType == InputItemTypeDatePicker) {
        [self actionDatePicker:nil];
    } else if (self.dynamicInputItem.dynamicInputItemInputType == InputItemTypePicker) {
        [self pickerView:self.pickerView didSelectRow:0 inComponent:0];
    }
    [self saveDataDynamicInputItem];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self saveDataDynamicInputItem];
    [self indicateValidOfString:textField.text];
    
    [self dynamicCellTextFieldDidBecomeFirstRsponder];

    return YES;
}

- (IBAction)textFieldDidChangeValue:(UITextField *)sender
{
    [self saveDataDynamicInputItem];
    [self indicateValidOfString:sender.text];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField layoutIfNeeded];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.dataSourcePickerView.count;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.dataSourcePickerView[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.dynamicTextField.text = self.dataSourcePickerView[row];
    self.selectedElement = row;
    [self textFieldDidChangeValue:nil];
}

#pragma mark - LocolizibleUI

- (void)setRTLArabicUI
{
    [self setTextAligmentLabelSettingViewController:NSTextAlignmentRight];
}

- (void)setLTREuropeUI
{
    [self setTextAligmentLabelSettingViewController:NSTextAlignmentLeft];
}

#pragma mark - Private

- (void)setTextAligmentLabelSettingViewController:(NSTextAlignment)textAlignment
{
    self.dynamicLabel.textAlignment = textAlignment;
    self.dynamicTextField.textAlignment = textAlignment;
}

- (void)saveDataDynamicInputItem
{
    if (self.dynamicTextField.text.length) {
        self.dynamicInputItem.clientString = self.dynamicTextField.text;
    }
    if (self.selectedDate) {
        self.dynamicInputItem.clientDate = self.selectedDate;
    }
    if (self.selectedElement) {
        self.dynamicInputItem.clientSelectPicker = self.selectedElement;
    }
}

- (void)dynamicCellTextFieldDidBecomeFirstRsponder
{
    if ([self.delegate respondsToSelector:@selector(dynamicCellDidBecomeFirstResponder:)]) {
        [self.delegate dynamicCellDidBecomeFirstResponder:self];
    }
}

- (void)switchTypeDynamicTextFieldTableViewCell
{
    switch (self.dynamicInputItem.dynamicInputItemInputType) {
        case InputItemTypePicker: {
            [self configurePickerController];
            break;
        }
        case InputItemTypeDatePicker: {
            [self configureDataPickerController];
            break;
        }
        case InputItemTypeSelectFile: {
            [self addAttachButtonTextField:self.dynamicTextField];
            break;
        }
        default:
            break;
    }
}

- (void)configurePickerController
{
    self.pickerView = [[UIPickerView alloc] init];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    self.dynamicTextField.inputView = self.pickerView;
    [self configureKeyboardButtonDone];
}

- (void)configureDataPickerController
{
    self.datePicker = [[UIDatePicker alloc]init];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.date = [NSDate date];
    [self.datePicker addTarget:self action:@selector(actionDatePicker:) forControlEvents:UIControlEventValueChanged];
    [self.dynamicTextField setInputView:self.datePicker];
    [self configureKeyboardButtonDone];
}

- (void)configureKeyboardButtonDone
{
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, HeightForToolbars)];
    toolBar.backgroundColor = [UIColor clearColor];
    toolBar.tintColor = [[DynamicUIService service] currentApplicationColor];
    
    UIBarButtonItem *barItemDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTapped)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolBar setItems:@[flexibleSpace, barItemDone]];
    
    [self.dynamicTextField setInputAccessoryView:toolBar];
}

- (void)addAttachButtonTextField:(UITextField *)textField
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        [textField setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
    }

    textField.rightView = nil;
    textField.leftView = nil;
    
    UIImage *buttonAttachImage = self.dynamicInputItem.clientUploadedImageId ? [UIImage imageNamed:ImageNameButtonAttachData] : [UIImage imageNamed:ImageNameButtonAttachClear];
    
    self.attachButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.attachButton setImage:buttonAttachImage forState:UIControlStateNormal];
    [self.attachButton addTarget:self action:@selector(actionSelectImage) forControlEvents:UIControlEventTouchUpInside];
    self.attachButton.backgroundColor = [UIColor clearColor];
    self.attachButton.tintColor = [[DynamicUIService service] currentApplicationColor];
    [self.attachButton setFrame:CGRectMake(0, 0, textField.frame.size.height * 0.75, textField.frame.size.height)];
    
    if ([DynamicUIService service].language == LanguageTypeArabic) {
        textField.leftView = self.attachButton;
        textField.leftViewMode = UITextFieldViewModeAlways;
    } else {
        textField.rightView = self.attachButton;
        textField.rightViewMode = UITextFieldViewModeAlways;
    }
}

#pragma mark - Validation

- (void)indicateValidOfString:(NSString *)string
{
    BOOL isValid = NO;
    switch (self.validationType) {
        case ValidationTypeEmail: {
            isValid = [string isValidEmailUseHardFilter:NO];
            break;
        }
        case ValidationTypeNumber: {
            isValid = [string isValidNumber];
            break;
        }
        case ValidationTypeURL : {
            isValid = [string isValidURL];
            break;
        }
        case ValidationTypeString : {
            isValid = [string isValidString];
            break;
        }
        case ValidationTypeNone: {
            return;
            break;
        }
    }
    UIColor *borderColor = isValid ? [UIColor lightGreenTextColor] : [UIColor redTextColor];
    borderColor = string.length ? borderColor : [DynamicUIService service].currentApplicationColor;
    if ([DynamicUIService service].colorScheme != ApplicationColorBlackAndWhite) {
        self.dynamicTextField.bottomBorderColor = borderColor;
        self.dynamicTextField.tintColor = borderColor;
    }
}

@end