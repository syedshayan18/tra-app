//
//  DynamicLabelTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 13.10.15.
//

#import "DynamicLabelTableViewCell.h"

@interface DynamicLabelTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *dynamicLabel;

@end

@implementation DynamicLabelTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.dynamicLabel.text = @"";
    self.descriptionLabel.text = @"";
}

#pragma mark - Custom Accessors

- (void)setDynamicInputItem:(DynamicInputItem *)dynamicInputItem
{
    [super setDynamicInputItem:dynamicInputItem];
    
    //self.dynamicLabel.text = [dynamicInputItem localizableString:dynamicInputItem.dynamicInputItemDisplayName];
    self.descriptionLabel.text = [dynamicInputItem localizableString:dynamicInputItem.dynamicInputItemDisplayName];
}

#pragma mark - LocolizibleUI

- (void)setRTLArabicUI
{
    [self setTextAligmentLabelSettingViewController:NSTextAlignmentRight];
}

- (void)setLTREuropeUI
{
    [self setTextAligmentLabelSettingViewController:NSTextAlignmentLeft];
}

#pragma mark - Private

- (void)setTextAligmentLabelSettingViewController:(NSTextAlignment)textAlignment
{
    self.dynamicLabel.textAlignment = textAlignment;
    self.descriptionLabel.textAlignment = textAlignment;
}

@end
