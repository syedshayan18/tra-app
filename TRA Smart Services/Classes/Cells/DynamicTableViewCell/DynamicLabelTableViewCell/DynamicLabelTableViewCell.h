//
//  DynamicLabelTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 13.10.15.
//

#import "BaseDynamicTableViewCell.h"

@interface DynamicLabelTableViewCell : BaseDynamicTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
