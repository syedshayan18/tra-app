//
//  DynamicButtonTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 13.10.15.
//

@class DynamicButtonTableViewCell;

@protocol DynamicButtonTableViewCellDelegate <NSObject>

@optional
- (void)dynamicButtonDidTappedInCell:(DynamicButtonTableViewCell *)cell;

@end

@interface DynamicButtonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *dynamicButton;

@property (weak, nonatomic) id <DynamicButtonTableViewCellDelegate> delegate;

@end
