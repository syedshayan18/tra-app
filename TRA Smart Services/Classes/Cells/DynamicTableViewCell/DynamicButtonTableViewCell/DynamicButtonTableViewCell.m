//
//  DynamicButtonTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 13.10.15.
//

#import "DynamicButtonTableViewCell.h"

@implementation DynamicButtonTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self minimumScaleFactorTitleButton:self.dynamicButton];
    [self prepareButton:self.dynamicButton];
}

- (void)prepareForReuse
{
    [self.dynamicButton setTitle:@"" forState:UIControlStateNormal];
    self.dynamicButton.backgroundColor = [UIColor clearColor];
}

#pragma mark - IBAction

- (IBAction)dynamicButtonTapped:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(dynamicButtonDidTappedInCell:)]) {
        [self.delegate dynamicButtonDidTappedInCell:self];
    }
}

#pragma mark - Private

- (void)minimumScaleFactorTitleButton:(UIButton *)button
{
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    button.titleLabel.minimumScaleFactor = 0.7f;
}

- (void)prepareButton:(UIButton *)button
{
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [DynamicUIService service].currentApplicationColor;
    button.layer.shadowColor = [UIColor blackColor].CGColor;
    button.layer.shadowOpacity = 0.5;
    button.layer.shadowRadius = 1.;
    button.layer.shadowOffset = CGSizeMake(1.5, 1.5);
}

@end