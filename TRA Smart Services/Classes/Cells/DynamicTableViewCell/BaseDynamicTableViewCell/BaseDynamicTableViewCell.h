//
//  BaseDynamicTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 18.04.16.
//  Copyright © 2016 . All rights reserved.
//

#import "DynamicInputItem.h"

@class BaseDynamicTableViewCell;

@protocol BaseDynamicTableViewCellDelegate <NSObject>

@optional

-(void)dynamicCellDidBecomeFirstResponder:(BaseDynamicTableViewCell *)cell;

@end

@protocol LocalizationUI <NSObject>

@optional

-(void)setRTLArabicUI;
-(void)setLTREuropeUI;

@end

@interface BaseDynamicTableViewCell : UITableViewCell <LocalizationUI>

@property (strong, nonatomic) DynamicInputItem *dynamicInputItem;

@property (weak, nonatomic) id <BaseDynamicTableViewCellDelegate> delegate;

@end
