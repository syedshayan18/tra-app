//
//  BaseDynamicTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 18.04.16.
//  Copyright © 2016 . All rights reserved.
//

#import "BaseDynamicTableViewCell.h"

static NSString *const RequiredIndicatorString = @"*";

@interface BaseDynamicTableViewCell()

@property (strong, nonatomic) IBOutlet UILabel *requiredIndicatorLabel;

@end

@implementation BaseDynamicTableViewCell

#pragma mark - LifeCycle 

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self updateLocalizationUI];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.requiredIndicatorLabel.hidden = YES;
    [self updateLocalizationUI];
}

#pragma mark - Accesser

- (void)setDynamicInputItem:(DynamicInputItem *)dynamicInputItem
{
    _dynamicInputItem = dynamicInputItem;
    
    [self indicateRequiredField:dynamicInputItem.dynamicInputItemRequred];
}

#pragma mark - Private

- (void)updateLocalizationUI
{
    if ([DynamicUIService service].language == LanguageTypeArabic) {
        [self setRTLArabicUI];
    } else {
        [self setLTREuropeUI];
    }
}

- (void)indicateRequiredField:(BOOL)required
{
    [self prepareRequiredIndicatorLabel];
    self.requiredIndicatorLabel.hidden = !required;
    if (!required) {
        return;
    }
    if ([DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite) {
        self.requiredIndicatorLabel.textColor = [UIColor blackColor];
    } else {
        self.requiredIndicatorLabel.textColor = [UIColor redColor];
    }
}

- (void)prepareRequiredIndicatorLabel
{
    if (!self.requiredIndicatorLabel) {
        self.requiredIndicatorLabel = [UILabel new];
        self.requiredIndicatorLabel.text = RequiredIndicatorString;
        self.requiredIndicatorLabel.textAlignment = NSTextAlignmentCenter;
        self.requiredIndicatorLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.requiredIndicatorLabel.hidden = YES;
        [self.contentView addSubview:self.requiredIndicatorLabel];
        [self addConstraintRequiredIndicatorLabel];
    }
}

- (void)addConstraintRequiredIndicatorLabel
{
    [self.requiredIndicatorLabel addConstraints:@[[NSLayoutConstraint constraintWithItem:self.requiredIndicatorLabel attribute:NSLayoutAttributeHeight
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                                                              multiplier:1.0 constant:14.0],
                                                  [NSLayoutConstraint constraintWithItem:self.requiredIndicatorLabel attribute:NSLayoutAttributeWidth
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                                                              multiplier:1.0 constant:14.0]
     ]];
    
    NSLayoutAttribute localizationAttribute = [DynamicUIService service].language == LanguageTypeArabic ? NSLayoutAttributeLeft : NSLayoutAttributeRight;
    [self.contentView addConstraints:@[[NSLayoutConstraint constraintWithItem:self.contentView
                                                                    attribute:NSLayoutAttributeTop
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.requiredIndicatorLabel attribute:NSLayoutAttributeTop
                                                                   multiplier:1.0 constant:-4.],
                                       [NSLayoutConstraint constraintWithItem:self.contentView
                                                                    attribute:localizationAttribute
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.requiredIndicatorLabel attribute:localizationAttribute
                                                                   multiplier:1.0 constant:([DynamicUIService service].language == LanguageTypeArabic ? -1. : 1.) * 16.]
    ]];
}

@end
