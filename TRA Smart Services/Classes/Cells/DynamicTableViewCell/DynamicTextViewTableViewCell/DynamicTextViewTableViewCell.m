//
//  DynamicTextViewTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 13.10.15.
//

#import "DynamicTextViewTableViewCell.h"
#import "PlaceholderTextView.h"

static CGFloat const HeightForToolbars = 44.f;

@interface DynamicTextViewTableViewCell()

@property (weak, nonatomic) IBOutlet PlaceholderTextView *dynamicTextView;
@property (weak, nonatomic) IBOutlet UILabel *dynamicLabel;

@property (copy, nonatomic) NSString *inputText;
@property (assign, nonatomic) ValidationType validationType;

@end

@implementation DynamicTextViewTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self configureKeyboardButtonDone];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.dynamicTextView.text = @"";
    self.dynamicLabel.text = @"";
}

#pragma mark - Custom Accessors

- (void)setDynamicInputItem:(DynamicInputItem *)dynamicInputItem
{
    [super setDynamicInputItem:dynamicInputItem];

    self.validationType = dynamicInputItem.dynamicInputItemValidationType;
    self.dynamicLabel.text = [dynamicInputItem localizableString:dynamicInputItem.dynamicInputItemDisplayName];
    self.dynamicTextView.placeholder = [dynamicInputItem localizableString:dynamicInputItem.dynamicInputItemPlaceholdel];
    self.dynamicTextView.delegate = self;
    self.inputText = dynamicInputItem.clientString;
}

- (void)setInputText:(NSString *)inputText
{
    _inputText = inputText;
    self.dynamicTextView.text = inputText;
    [self indicateValidOfString:inputText];
    [AppHelper setStyleForTextView:self.dynamicTextView];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(PlaceholderTextView *)textView
{
    [self indicateValidOfString:textView.text];
}

- (void)textViewDidEndEditing:(PlaceholderTextView *)textView
{
    self.dynamicInputItem.clientString = self.dynamicTextView.text;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(dynamicCellDidBecomeFirstResponder:)]) {
        [self.delegate dynamicCellDidBecomeFirstResponder:self];
    }
}

#pragma mark - IBAction

- (void)doneButtonTapped
{
    [self.dynamicTextView resignFirstResponder];
}

#pragma mark - LocolizibleUI

- (void)setRTLArabicUI
{
    [self setTextAligmentLabelSettingViewController:NSTextAlignmentRight];
}

- (void)setLTREuropeUI
{
    [self setTextAligmentLabelSettingViewController:NSTextAlignmentLeft];
}

#pragma mark - Private

- (void)setTextAligmentLabelSettingViewController:(NSTextAlignment)textAlignment
{
    self.dynamicLabel.textAlignment = textAlignment;
    self.dynamicTextView.textAlignment = textAlignment;
}

#pragma mark - Validation

- (void)indicateValidOfString:(NSString *)string
{
    BOOL isValid = NO;
    switch (self.validationType) {
        case ValidationTypeEmail: {
            isValid = [string isValidEmailUseHardFilter:NO];
            break;
        }
        case ValidationTypeNumber: {
            isValid = [string isValidNumber];
            break;
        }
        case ValidationTypeURL : {
            isValid = [string isValidURL];
            break;
        }
        case ValidationTypeString : {
            isValid = [string isValidString];
            break;
        }
        case ValidationTypeNone: {
            return;
            break;
        }
    }
    UIColor *borderColor = isValid ? [UIColor lightGreenTextColor] : [UIColor redTextColor];
    borderColor = string.length ? borderColor : [DynamicUIService service].currentApplicationColor;
    if ([DynamicUIService service].colorScheme != ApplicationColorBlackAndWhite) {
        self.dynamicTextView.bottomBorderColor = borderColor;
        self.dynamicTextView.tintColor = borderColor;
    }
}

- (void)configureKeyboardButtonDone
{
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, HeightForToolbars)];
    toolBar.backgroundColor = [UIColor clearColor];
    toolBar.tintColor = [[DynamicUIService service] currentApplicationColor];
    
    UIBarButtonItem *barItemCustomDone = [[UIBarButtonItem alloc] initWithTitle:dynamicLocalizedString(@"uiElement.keyboardButtom.title") style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonTapped)];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolBar setItems:@[flexibleSpace, barItemCustomDone]];
    
    [self.dynamicTextView setInputAccessoryView:toolBar];
}

@end