//
//  DynamicTextViewTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 13.10.15.
//

#import "BaseDynamicTableViewCell.h"

@interface DynamicTextViewTableViewCell : BaseDynamicTableViewCell <UITextViewDelegate>

@end
