//
//  DynamicSwitchTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 18.04.16.
//  Copyright © 2016 . All rights reserved.
//

#import "DynamicSwitchTableViewCell.h"

static CGFloat const OptionScaleSwitch = 1.;

@interface DynamicSwitchTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UISwitch *dynamicSwitchView;
@property (weak, nonatomic) IBOutlet UILabel *dynamicLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

@implementation DynamicSwitchTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self prepareUISwitch:self.dynamicSwitchView];
    [self addGestureRecognizer];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.dynamicLabel.text = @"";
    [self.dynamicSwitchView setOn:NO];
}

#pragma mark - Custom Accessors

- (void)setDynamicInputItem:(DynamicInputItem *)dynamicInputItem
{
    [super setDynamicInputItem:dynamicInputItem];
    
    self.dynamicSwitchView.onTintColor = [DynamicUIService service].currentApplicationColor;
    self.separatorView.backgroundColor = [DynamicUIService service].currentApplicationColor;
    
    self.dynamicLabel.text = [dynamicInputItem localizableString:dynamicInputItem.dynamicInputItemDisplayName];
    [self.dynamicSwitchView setOn:dynamicInputItem.clientSelectPicker == 1 ? YES : NO];
}

#pragma mark - IBAction

- (void)tapGesture:(UITapGestureRecognizer *)gesture
{
    [self.dynamicSwitchView setOn:!self.dynamicSwitchView.isOn animated:YES];
    self.dynamicInputItem.clientSelectPicker = self.dynamicSwitchView.isOn ? 1 : 2;
    
    if ([self.delegate respondsToSelector:@selector(dynamicCellDidBecomeFirstResponder:)]) {
        [self.delegate dynamicCellDidBecomeFirstResponder:self];
    }
}

#pragma mark - Gestures

- (void)addGestureRecognizer
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.containerView addGestureRecognizer:tapGesture];
}

#pragma mark - LocolizibleUI

- (void)setRTLArabicUI
{
    self.dynamicLabel.textAlignment = NSTextAlignmentRight;
    self.dynamicSwitchView.layer.transform = CATransform3DMakeScale(- OptionScaleSwitch, OptionScaleSwitch, 1);
    [self transformUILayer:TRANFORM_3D_SCALE];
}

- (void)setLTREuropeUI
{
    self.dynamicLabel.textAlignment = NSTextAlignmentLeft;
    self.dynamicSwitchView.layer.transform = CATransform3DMakeScale(OptionScaleSwitch, OptionScaleSwitch, 1);
    [self transformUILayer:CATransform3DIdentity];
}

#pragma mark - Private

- (void)transformUILayer:(CATransform3D)animCATransform3D
{
    self.containerView.layer.transform = animCATransform3D;
    self.dynamicLabel.layer.transform = animCATransform3D;
}

- (void)prepareUISwitch:(UISwitch *) prepareSwitch
{
    prepareSwitch.backgroundColor = [UIColor grayBorderTextFieldTextColor];
    prepareSwitch.layer.cornerRadius = prepareSwitch.bounds.size.height / 2;
    prepareSwitch.tintColor = [UIColor grayBorderTextFieldTextColor];
}

@end
