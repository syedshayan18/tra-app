//
//  DynamicWorkItemTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 16.11.15.
//

static NSString *const DynamicWorkItemTableViewCellIdentifier = @"DynamicWorkItemTableViewCell";
static CGFloat const HeihgtLabel = 35.;

@interface DynamicWorkItemTableViewCell : UITableViewCell

@property (strong, nonatomic) NSArray *dataSource;

@end
