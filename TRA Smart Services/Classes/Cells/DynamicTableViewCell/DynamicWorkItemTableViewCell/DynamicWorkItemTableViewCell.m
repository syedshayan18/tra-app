//
//  DynamicWorkItemTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 16.11.15.
//

#import "DynamicWorkItemTableViewCell.h"

static CGFloat const OffsetLabel = 16.;
static CGFloat const WidthLabelTitle = 124.;

@interface DynamicWorkItemTableViewCell()

@end

@implementation DynamicWorkItemTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

- (void)prepareForReuse
{
    NSArray *viewsToRemove = self.subviews;
    for (UIView *subView in viewsToRemove) {
        if ([subView isKindOfClass:[UILabel class]]) {
            [subView removeFromSuperview];
        }
    }
}

- (void)setDataSource:(NSArray *)dataSource
{
    _dataSource = dataSource;
    
    [self setupDynamicLabel];
}

#pragma mark - Private

- (void)setupDynamicLabel
{
    for (NSInteger i = 0; i < self.dataSource.count; i++) {

        CGRect frame = CGRectMake(OffsetLabel, i * HeihgtLabel, WidthLabelTitle, HeihgtLabel);
        UILabel *dynamicLabelTitle = [[UILabel alloc] initWithFrame:frame];
        dynamicLabelTitle.backgroundColor = [UIColor clearColor];
        dynamicLabelTitle.textColor = [UIColor lightGrayBorderColor];
        dynamicLabelTitle.textAlignment = NSTextAlignmentLeft;
        dynamicLabelTitle.font = [DynamicUIService service].language == LanguageTypeArabic ? [UIFont droidKufiRegularFontForSize:14] : [UIFont latoRegularWithSize:14];
        dynamicLabelTitle.text = @"title";
        dynamicLabelTitle.numberOfLines = 1;
        dynamicLabelTitle.adjustsFontSizeToFitWidth = YES;
        [self addSubview:dynamicLabelTitle];
        
        frame = CGRectMake(WidthLabelTitle + OffsetLabel, i * HeihgtLabel, SCREEN_WIDTH - WidthLabelTitle - 2 * OffsetLabel, HeihgtLabel);
        UILabel *dynamicLabelText = [[UILabel alloc] initWithFrame:frame];
        dynamicLabelText.backgroundColor = [UIColor clearColor];
        dynamicLabelText.textColor = [UIColor blackColor];
        dynamicLabelText.textAlignment = NSTextAlignmentLeft;
        dynamicLabelText.font = [DynamicUIService service].language == LanguageTypeArabic ? [UIFont droidKufiRegularFontForSize:14] : [UIFont latoRegularWithSize:14];
        dynamicLabelText.text = @"text text text";
        dynamicLabelText.numberOfLines = 1;
        dynamicLabelText.adjustsFontSizeToFitWidth = YES;
        [self addSubview:dynamicLabelText];
    }
}

@end
