//
//  DynamicElementTreeViewTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 19.11.15.
//

static NSString *const DynamicElementTreeViewTableViewCellIdentifier = @"DynamicElementTreeViewTableViewCell";

@interface DynamicElementTreeViewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *indicatorActiveImageView;
@property (weak, nonatomic) IBOutlet UILabel *elementTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *indicatorOpenImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginElementContainerConstraint;
@property (weak, nonatomic) IBOutlet UIView *elementContainerView;

@end
