//
//  DynamicElementTreeViewTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 19.11.15.
//

#import "DynamicElementTreeViewTableViewCell.h"

@implementation DynamicElementTreeViewTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.indicatorOpenImageView.image = nil;
    self.indicatorActiveImageView.image = nil;
    self.elementTextLabel.text = @"";
    self.elementContainerView.backgroundColor = [UIColor clearColor];
}

@end
