//
//  FavouriteTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 17.08.15.
//

#import "ModeAlwaysTemplateImageView.h"

@class AddToFavouriteTableViewCell;

static NSString *const AddToFavouriteEuroCellIdentifier = @"euroAddToFavCell";
static NSString *const AddToFavouriteArabicCellIdentifier = @"arabicAddToFavCell";

@protocol AddToFavouriteTableViewCellDelegate <NSObject>

@optional
- (void)addRemoveFavoriteService:(NSIndexPath *)indexPath;

@end

@interface AddToFavouriteTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *favourieDescriptionLabel;
@property (weak, nonatomic) IBOutlet ModeAlwaysTemplateImageView *favouriteServiceLogoImageView;

@property (strong, nonatomic) NSIndexPath *indexPath;

@property (weak, nonatomic) id <AddToFavouriteTableViewCellDelegate> delegate;

- (void)setServiceFavourite:(BOOL)isFavourite;

@end