//
//  CategoryCollectionViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 30.07.15.
//

#import "CategoryCollectionViewCell.h"

@implementation CategoryCollectionViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self resetParameters];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self resetParameters];
}

#pragma mark - Custom Accessor

- (void)setServiceModel:(ServiceModel *)serviceModel
{
    _serviceModel = serviceModel;
    
    UIColor *color = self.serviceModel.serviceEnable ? [DynamicUIService service].currentApplicationColor : [[UIColor menuItemGrayColor] colorWithAlphaComponent:0.2];
    [self setTintColorForLabel:color];
    
    if (self.serviceModel.serviceLogo.length) {
        UIImage *serviceLogo = [UIImage imageNamed:self.serviceModel.serviceLogo];
        serviceLogo = [serviceLogo imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        if ([DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite) {
            serviceLogo = [[BlackWhiteConverter sharedManager] convertedBlackAndWhiteImage:serviceLogo];
        }
        self.categoryLogoImageView.image = serviceLogo;
        self.categoryLogoImageView.tintColor = color;
    }
    self.categoryTitleLabel.text = dynamicLocalizedString(self.serviceModel.serviceName);
    self.categoryTitleLabel.tag = DeclineTagForFontUpdate;
    self.categoryID = self.serviceModel.serviceID;
}

#pragma mark - Public

- (void)setTintColorForLabel:(UIColor *)color
{
    self.categoryTitleLabel.textColor = color;
    self.polygonView.viewStrokeColor = color;
    self.polygonView.viewFillColor = [UIColor whiteColor];
}

#pragma mark - Private

- (void)resetParameters
{
    self.categoryLogoImageView.image = nil;
    self.categoryTitleLabel.text = @"";
    [self.polygonView removeAllDrawings];
    self.categoryTitleLabel.textColor = [UIColor whiteColor];
}

@end
