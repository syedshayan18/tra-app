//
//  TutorialScreen3CollectionViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 22.01.16.
//  Copyright © 2016 . All rights reserved.
//

#import "TutorialProfileCollectionViewCell.h"

static NSString *const TutorialTabBarCollectionViewCellIdentifier = @"TutorialTabBarCollectionViewCell";

@interface TutorialTabBarCollectionViewCell : TutorialProfileCollectionViewCell

@property (strong, nonatomic) UIImage *tabBarImage;
@property (assign, nonatomic) CGRect tabBarRect;

@end
