//
//  TutorialScreen3CollectionViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 22.01.16.
//  Copyright © 2016 . All rights reserved.
//

#import "TutorialTabBarCollectionViewCell.h"

static CGFloat const SizeFont = 14.f;

@interface TutorialTabBarCollectionViewCell()

@property (strong, nonatomic) UIImageView *tabBarImageView;
@property (strong, nonatomic) CAShapeLayer *lineLayer;
@property (strong, nonatomic) CAShapeLayer *borderTextLayer;
@property (strong, nonatomic) CATextLayer *textTitleLayer;
@property (strong, nonatomic) CATextLayer *textDescriptionLayer;

@property (assign, nonatomic) CGFloat minimumYPoint;

@end

@implementation TutorialTabBarCollectionViewCell

#pragma mark - Accessor

- (void)prepareCell
{
    self.tabBarImageView = [[UIImageView alloc] initWithImage:self.tabBarImage];
    self.tabBarImageView.frame = self.tabBarRect;
    [self.contentView addSubview:self.tabBarImageView];
    [self prepareLineLayer];
    [self prepareTextLayerScreen];
}

#pragma mark - Private

- (void)prepareLineLayer
{
    [self.lineLayer removeFromSuperlayer];
    self.lineLayer = [CAShapeLayer layer];
    self.lineLayer.frame = self.bounds;
    self.lineLayer.path = [self prepareLinePath].CGPath;
    self.lineLayer.fillColor = [UIColor whiteColor].CGColor;
    self.lineLayer.strokeColor = [UIColor whiteColor].CGColor;
    [self.contentView.layer addSublayer:self.lineLayer];
}

- (UIBezierPath *)prepareLinePath
{
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    self.minimumYPoint = self.tabBarRect.origin.y - 5.;
    
    [linePath moveToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2 + 1,  self.minimumYPoint)];
    [linePath addArcWithCenter:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2,  self.minimumYPoint) radius:2. startAngle:0 endAngle:2 * M_PI clockwise:YES];
    [linePath moveToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2, self.minimumYPoint)];
    [linePath addLineToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2, self.minimumYPoint - 95)];
    
    [linePath moveToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2, self.minimumYPoint - 135)];
    [linePath addLineToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2, self.minimumYPoint - 180)];
    
    [linePath moveToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2 + 1,  self.minimumYPoint - 180)];
    [linePath addArcWithCenter:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2,  self.minimumYPoint - 180) radius:2. startAngle:0 endAngle:2 * M_PI clockwise:YES];
    
    return linePath;
}

- (void)prepareTextLayerScreen
{
    UIFont *font = [DynamicUIService service].language == LanguageTypeArabic ? [UIFont droidKufiBoldFontForSize:SizeFont] : [UIFont latoBoldWithSize:SizeFont];

    NSString *textTitle = dynamicLocalizedString(@"tutorialViewController.cellTabBar.textTitle");
    
    CGSize sizeOfText = [textTitle sizeWithAttributes:@{NSFontAttributeName : font}];
    CGRect frameText = CGRectMake((((CGFloat)SCREEN_WIDTH) - sizeOfText.width - 30.) / 2., self.minimumYPoint - 130., sizeOfText.width + 30., 30.);
    
    [self prepareTextBorder:frameText];
    
    CGRect textTitleFrame = CGRectMake(frameText.origin.x, frameText.origin.y + (30. - sizeOfText.height) / 2., frameText.size.width, frameText.size.height);
    [self prepareTextLayer:self.textTitleLayer string:textTitle frame:textTitleFrame aligmentMode:kCAAlignmentCenter];
    
    NSString *textDecription = dynamicLocalizedString(@"tutorialViewController.cellTabBar.textDescription");
    CGRect descriptionTextFrame = [textDecription boundingRectWithSize:CGSizeMake(((CGFloat)SCREEN_WIDTH) - 30, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    
    CGFloat textDeltaHeight = [DynamicUIService service].language == LanguageTypeArabic ? 1.14 : 1;
    CGRect textFrameDescription = CGRectMake((((CGFloat)SCREEN_WIDTH) - descriptionTextFrame.size.width) / 2, self.minimumYPoint - 185 - descriptionTextFrame.size.height * textDeltaHeight, descriptionTextFrame.size.width, descriptionTextFrame.size.height * textDeltaHeight);
    
    [self prepareTextLayer:self.textDescriptionLayer string:textDecription frame:textFrameDescription aligmentMode:[DynamicUIService service].language == LanguageTypeArabic ?  kCAAlignmentRight : kCAAlignmentLeft];
}

@end
