//
//  TutorialScreen1CollectionViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 22.01.16.
//  Copyright © 2016 . All rights reserved.
//

static NSString *const TutorialProfileCollectionViewCellIdentifier = @"TutorialProfileCollectionViewCell";

@interface TutorialProfileCollectionViewCell : UICollectionViewCell

@property (assign, nonatomic) CGRect rectLogoImage;

- (void)prepareCell;

- (void)prepareTextLayer:(CATextLayer *)textLayer string:(NSString *)string frame:(CGRect)frame aligmentMode:(NSString *)aligment;
- (void)prepareTextBorder:(CGRect)frame;
- (CGRect)frameToBounds:(CGRect)frame;
- (UIBezierPath *)prepareBorderTextPathForRect:(CGRect)rect;

@end
