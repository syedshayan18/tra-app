//
//  TutorialScreen1CollectionViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 22.01.16.
//  Copyright © 2016 . All rights reserved.
//

#import "TutorialProfileCollectionViewCell.h"

static CGFloat const LeftOffset = 10.f;
static CGFloat const ElementsInRowCount = 8.f;
static CGFloat const CornerWidthForAvatar = 3.f;
static CGFloat const IsIPadAddElementsInRowCount = 2.f;
static CGFloat const SizeFont = 14.f;

@interface TutorialProfileCollectionViewCell()

@property (strong, nonatomic) UIImage *logoImage;

@property (strong, nonatomic) CAShapeLayer *maskLayer;
@property (strong, nonatomic) CAShapeLayer *borderLogoLayer;
@property (strong, nonatomic) CAShapeLayer *borderTextLayer;
@property (strong, nonatomic) CAShapeLayer *lineLayer;

@property (strong, nonatomic) CATextLayer *textTitleLayer;
@property (strong, nonatomic) CATextLayer *textDescriptionLayer;

@property (strong, nonatomic) CALayer *avatarImageLayer;

@end

@implementation TutorialProfileCollectionViewCell

#pragma mark - Public

- (void)prepareCell
{
    [self prepareLayers];
}

- (CGRect)frameToBounds:(CGRect)frame
{
    return CGRectMake(0, 0, frame.size.width, frame.size.height);
}

- (UIBezierPath *)prepareBorderTextPathForRect:(CGRect)rect
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointMake(rect.origin.x, rect.origin.y)];
    [path addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y)];
    [path addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height)];
    [path addLineToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height)];
    [path closePath];
    
    return path;
}

- (void)prepareTextLayer:(CATextLayer *)textLayer string:(NSString *)string frame:(CGRect)frame aligmentMode:(NSString *)aligment
{
    [textLayer removeFromSuperlayer];
    textLayer = [CATextLayer layer];
    textLayer.wrapped = YES;
    textLayer.frame = frame;
    textLayer.string = string;
    textLayer.fontSize = SizeFont;
    textLayer.font = (__bridge CFTypeRef _Nullable)([DynamicUIService service].language == LanguageTypeArabic ? [UIFont droidKufiBoldFontForSize:SizeFont] : [UIFont latoBoldWithSize:SizeFont]);
    textLayer.foregroundColor = [UIColor whiteColor].CGColor;
    textLayer.alignmentMode = aligment;
    textLayer.contentsScale = [UIScreen mainScreen].scale * 2;
    [self.contentView.layer addSublayer:textLayer];
}

- (void)prepareTextBorder:(CGRect)frame
{
    [self.borderTextLayer removeFromSuperlayer];
    self.borderTextLayer = [CAShapeLayer layer];
    self.borderTextLayer.fillColor = [UIColor clearColor].CGColor;
    self.borderTextLayer.strokeColor = [UIColor whiteColor].CGColor;
    self.borderTextLayer.lineWidth = 1;
    self.borderTextLayer.frame = frame;
    self.borderTextLayer.path = [self prepareBorderTextPathForRect:[self frameToBounds:frame]].CGPath;
    [self.contentView.layer addSublayer:self.borderTextLayer];
}

#pragma mark - Private

- (void)prepareLayers
{
    self.logoImage = [UIImage imageNamed:DefaultLogoImageName];
    self.rectLogoImage = [self prepareLogoViewCGRect];
    
    [self prepareLogoImage];
    [self prepareLineLayer];
    [self prepareTextLayerScreen];
    
//    if ([DynamicUIService service].language == LanguageTypeArabic) {
//        [self transformUILayer:TRANFORM_3D_SCALE];
//    } else {
//        [self transformUILayer:CATransform3DIdentity];
//    }
}

//- (void)transformUILayer:(CATransform3D)animCATransform3D
//{
//    self.contentView.layer.transform = animCATransform3D;
//    self.textTitleLayer.transform = animCATransform3D;
//    self.textDescriptionLayer.transform = animCATransform3D;
//}

- (CGRect)prepareLogoViewCGRect
{
    CGFloat heightLogoView = (((CGFloat)SCREEN_HEIGHT) - 49. - 20.) * 0.17 * 0.745454;
    CGFloat widthLogoView = heightLogoView * 35. / 41.;
    CGRect fremeAvatarView = CGRectMake(15, (((CGFloat)SCREEN_HEIGHT) - 49. - 20.) / 27.7222, widthLogoView, heightLogoView);
    
    CGFloat hexagonSizeWidth = ((CGFloat)SCREEN_WIDTH) / (ElementsInRowCount +  (IS_IPAD ? IsIPadAddElementsInRowCount : 0));
    CGFloat originX = hexagonSizeWidth + (hexagonSizeWidth / 2 - LeftOffset) - fremeAvatarView.size.width / 2;
    if ([DynamicUIService service].language == LanguageTypeArabic) {
        originX = ((CGFloat)SCREEN_WIDTH) - originX - widthLogoView;
    }

    fremeAvatarView = CGRectMake(originX, fremeAvatarView.origin.y, fremeAvatarView.size.width, fremeAvatarView.size.height);
    
    return fremeAvatarView;
}

- (UIBezierPath *)prepareHexagonPathForRect:(CGRect)hexagonRect
{
    UIBezierPath *hexagonPath = [UIBezierPath bezierPath];
    
    [hexagonPath moveToPoint:CGPointMake(CGRectGetMidX(hexagonRect), CGRectGetMinY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMaxX(hexagonRect), CGRectGetHeight(hexagonRect) * 0.25 + CGRectGetMinY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMaxX(hexagonRect), CGRectGetHeight(hexagonRect) * 0.75 + CGRectGetMinY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMidX(hexagonRect), CGRectGetMaxY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMinX(hexagonRect), CGRectGetHeight(hexagonRect) * 0.75 + CGRectGetMinY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMinX(hexagonRect), CGRectGetHeight(hexagonRect) * 0.25 + CGRectGetMinY(hexagonRect))];
    [hexagonPath addLineToPoint:CGPointMake(CGRectGetMidX(hexagonRect), CGRectGetMinY(hexagonRect))];
    
    return hexagonPath;
}

- (void)prepareLogoImage
{
    [self.avatarImageLayer removeFromSuperlayer];
    
    self.avatarImageLayer = [self layerWithLogoImage:self.logoImage inRect:self.rectLogoImage forMainLogo:YES];
    if ([DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite) {
        self.avatarImageLayer.backgroundColor = [UIColor blackColor].CGColor;
    } else {
        self.avatarImageLayer.backgroundColor = [UIColor itemGradientTopColor].CGColor;
    }
    
    [self addHexagoneMaskForLayer:self.avatarImageLayer];
    
    self.borderLogoLayer = [CAShapeLayer layer];
    self.borderLogoLayer.fillColor = [UIColor clearColor].CGColor;
    self.borderLogoLayer.strokeColor = [UIColor whiteColor].CGColor;
    self.borderLogoLayer.lineWidth = CornerWidthForAvatar;
    self.borderLogoLayer.frame = [self frameToBounds:self.rectLogoImage];
    self.borderLogoLayer.path = [self prepareHexagonPathForRect:[self frameToBounds:self.rectLogoImage]].CGPath;
    [self.avatarImageLayer addSublayer:self.borderLogoLayer];
    self.contentView.layer.masksToBounds = YES;
    [self.contentView.layer addSublayer:self.avatarImageLayer];
}

- (CALayer *)layerWithLogoImage:(UIImage *)image inRect:(CGRect)rect forMainLogo:(BOOL)mainLogo
{
    CALayer *layer= [CALayer layer];
    layer.frame = rect;
    layer.backgroundColor = [DynamicUIService service].currentApplicationColor.CGColor;
    
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = [self frameToBounds:rect];
    imageLayer.backgroundColor = [UIColor clearColor].CGColor;
    imageLayer.contents =(__bridge id __nullable)(image).CGImage;
    imageLayer.contentsGravity = kCAGravityResizeAspect;
    
    [layer addSublayer:imageLayer];
    layer.contentsGravity = kCAGravityResizeAspect;
    
    return layer;
}

- (void)addHexagoneMaskForLayer:(CALayer *)layer
{
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = layer.bounds;
    maskLayer.path = [self prepareHexagonPathForRect:layer.bounds].CGPath;
    layer.mask = maskLayer;
}

- (void)prepareLineLayer
{
    [self.lineLayer removeFromSuperlayer];
    self.lineLayer = [CAShapeLayer layer];
    self.lineLayer.frame = self.bounds;
    self.lineLayer.path = [self prepareLinePath].CGPath;
    self.lineLayer.fillColor = [UIColor whiteColor].CGColor;
    self.lineLayer.strokeColor = [UIColor whiteColor].CGColor;
    [self.contentView.layer addSublayer:self.lineLayer];
}

- (UIBezierPath *)prepareLinePath
{
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    
    CGPoint moveLine = CGPointMake(self.rectLogoImage.origin.x + self.rectLogoImage.size.width / 2., self.rectLogoImage.origin.y + self.rectLogoImage.size.height + 5);
    [linePath moveToPoint:CGPointMake(moveLine.x, moveLine.y + 20.)];
    [linePath addLineToPoint:moveLine];
    [linePath moveToPoint:CGPointMake(moveLine.x + 1, moveLine.y)];
    [linePath addArcWithCenter:moveLine radius:2. startAngle:0 endAngle:2 * M_PI clockwise:YES];
    
    [linePath moveToPoint:CGPointMake(moveLine.x, moveLine.y + 60.)];
    [linePath addLineToPoint:CGPointMake(moveLine.x, moveLine.y + 105.)];
    [linePath moveToPoint:CGPointMake(moveLine.x + 1, moveLine.y + 105)];
    [linePath addArcWithCenter:CGPointMake(moveLine.x, moveLine.y + 105) radius:2. startAngle:0 endAngle:2 * M_PI clockwise:YES];
    
    return linePath;
}

- (void)prepareTextLayerScreen
{
    UIFont *font = [DynamicUIService service].language == LanguageTypeArabic ? [UIFont droidKufiBoldFontForSize:SizeFont] : [UIFont latoBoldWithSize:SizeFont];

    NSString *textTitle = dynamicLocalizedString(@"tutorialViewController.cellProfile.textTitle");
    CGSize sizeOfText = [textTitle sizeWithAttributes:@{NSFontAttributeName : font}];
    CGRect frameTextTitleBorder = CGRectMake(self.rectLogoImage.origin.x, self.rectLogoImage.origin.y + self.rectLogoImage.size.height + 30., sizeOfText.width + 30., 30.);
    
    if ([DynamicUIService service].language == LanguageTypeArabic) {
        CGFloat frameTextTitleBorderX = self.rectLogoImage.origin.x + self.rectLogoImage.size.width - frameTextTitleBorder.size.width;
        frameTextTitleBorder = CGRectMake(frameTextTitleBorderX, frameTextTitleBorder.origin.y, frameTextTitleBorder.size.width, frameTextTitleBorder.size.height);
    }
    
    [self prepareTextBorder:frameTextTitleBorder];

    CGRect textTitleFrame = CGRectMake(frameTextTitleBorder.origin.x, frameTextTitleBorder.origin.y + (30. - sizeOfText.height) / 2., frameTextTitleBorder.size.width, frameTextTitleBorder.size.height);
    [self prepareTextLayer:self.textTitleLayer  string:textTitle frame:textTitleFrame aligmentMode:kCAAlignmentCenter];
    
    NSString *textDecription = dynamicLocalizedString(@"tutorialViewController.cellProfile.textDescription");
    CGRect descriptionTextBounds = [textDecription boundingRectWithSize:CGSizeMake(((CGFloat)SCREEN_WIDTH) - 30, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];

    CGRect textFrameDescription = CGRectMake(frameTextTitleBorder.origin.x, frameTextTitleBorder.origin.y + 80 + (SizeFont - 1.) / 2., descriptionTextBounds.size.width, descriptionTextBounds.size.height * 1.14);
    
    if ([DynamicUIService service].language == LanguageTypeArabic) {
        CGFloat textFrameDescriptionX = ((CGFloat)SCREEN_WIDTH - textFrameDescription.size.width) / 2.;
        textFrameDescription = CGRectMake(textFrameDescriptionX, textFrameDescription.origin.y, textFrameDescription.size.width, textFrameDescription.size.height);
    }
    
    [self prepareTextLayer:self.textDescriptionLayer string:textDecription frame:textFrameDescription aligmentMode:[DynamicUIService service].language == LanguageTypeArabic ?  kCAAlignmentRight : kCAAlignmentLeft];
}

@end
