//
//  TutorialScreen2CollectionViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 22.01.16.
//  Copyright © 2016 . All rights reserved.
//

#import "TutorialSpeedAccessCollectionViewCell.h"

static CGFloat const CellSpacing = 5.f;
static CGFloat const RowCount = 4.f;
static CGFloat const SizeFont = 14.f;

@interface TutorialSpeedAccessCollectionViewCell()

@property (strong, nonatomic) UIImageView *speedAccessImageView;
@property (strong, nonatomic) CAShapeLayer *collectionViewMaskLayer;
@property (strong, nonatomic) CAShapeLayer *lineLayer;
@property (strong, nonatomic) CAShapeLayer *borderTextLayer;
@property (strong, nonatomic) CATextLayer *textTitleLayer;
@property (strong, nonatomic) CATextLayer *textDescriptionLayer;

@property (assign, nonatomic) CGFloat minimumYPoint;

@end

@implementation TutorialSpeedAccessCollectionViewCell

#pragma mark - Accessor

- (void)prepareCell
{
    [self.speedAccessImageView removeFromSuperview];
    self.speedAccessImageView = [[UIImageView alloc] initWithImage:self.speedAccessCollectionViewImage];
    self.speedAccessImageView.frame = self.speedAccessCollectionViewRect ;
    [self.contentView addSubview:self.speedAccessImageView];
    [self addMaskMainCollectionView];
    
    [self prepareLineLayer];
    [self prepareTextLayerScreen];
}

#pragma mark - Private

- (void)addMaskMainCollectionView
{
    UIBezierPath *zigZagPath = [self setupZigZagPath];
    
    [zigZagPath addLineToPoint:CGPointMake(self.speedAccessImageView.frame.size.width, 0)];
    [zigZagPath addLineToPoint:CGPointMake(0, 0)];
    [zigZagPath closePath];
    
    [self.collectionViewMaskLayer removeFromSuperlayer];
    
    self.collectionViewMaskLayer = [CAShapeLayer layer];
    self.collectionViewMaskLayer.path = zigZagPath.CGPath;
    self.collectionViewMaskLayer.frame = self.speedAccessImageView.bounds;
    self.collectionViewMaskLayer.strokeColor = [UIColor redColor].CGColor;
    self.collectionViewMaskLayer.fillColor = [UIColor blackColor].CGColor;
    
    CGRect frame = self.collectionViewMaskLayer.frame;
    frame.origin = CGPointMake(0, frame.origin.y - 6);
    self.collectionViewMaskLayer.frame = frame;
    
    self.speedAccessImageView.layer.mask = self.collectionViewMaskLayer;
}

- (UIBezierPath *)setupZigZagPath
{
    CGSize cellSize = CGSizeMake(( self.speedAccessImageView.frame.size.width - (CellSpacing * (RowCount + 1))) / RowCount, ((CGFloat)SCREEN_HEIGHT) * 0.18f);
    CGSize  hexagonSize = CGSizeMake(cellSize.width * 0.6f, cellSize.height * 0.87f);
    CGFloat minimumYPoint = hexagonSize.height * (IS_IPAD ? 0.8f : 0.9f);
    CGFloat maximumYPoint = hexagonSize.height * 1.15f;
    
    UIBezierPath *zigZagPath = [UIBezierPath bezierPath];
    [zigZagPath moveToPoint:CGPointMake(0, minimumYPoint + CellSpacing)];
    [zigZagPath addLineToPoint:CGPointMake(CellSpacing, minimumYPoint)];
    
    CGFloat shadowOffset = 2;
    
    for (int i = 0; i < RowCount; i++) {
        [zigZagPath addLineToPoint:CGPointMake(((cellSize.width / 2 + CellSpacing - shadowOffset / 2) + (cellSize.width + CellSpacing ) * i), maximumYPoint)];
        if (i != RowCount - 1) {
            [zigZagPath addLineToPoint:CGPointMake((cellSize.width + CellSpacing ) * (i + 1) + shadowOffset,  minimumYPoint)];
        } else {
            [zigZagPath addLineToPoint:CGPointMake((cellSize.width + CellSpacing) * (i + 1),  minimumYPoint)];
        }
    }
    [zigZagPath addLineToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH), minimumYPoint + CellSpacing)];
    return zigZagPath;
}

- (void)prepareLineLayer
{
    [self.lineLayer removeFromSuperlayer];
    self.lineLayer = [CAShapeLayer layer];
    self.lineLayer.frame = self.bounds;
    self.lineLayer.path = [self prepareLinePath].CGPath;
    self.lineLayer.fillColor = [UIColor whiteColor].CGColor;
    self.lineLayer.strokeColor = [UIColor whiteColor].CGColor;
    [self.contentView.layer addSublayer:self.lineLayer];
}

- (UIBezierPath *)prepareLinePath
{
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    
    CGSize cellSize = CGSizeMake(( self.speedAccessImageView.frame.size.width - (CellSpacing * (RowCount + 1))) / RowCount, ((CGFloat)SCREEN_HEIGHT) * 0.18f);
    self.minimumYPoint =  self.speedAccessImageView.frame.origin.y + self.speedAccessImageView.frame.size.height - 15.;

    for (int i = 0; i < RowCount; i++) {
        CGFloat pointX = cellSize.width / 2 + CellSpacing  + (cellSize.width + CellSpacing ) * i;
        
        [linePath moveToPoint:CGPointMake(pointX,  self.minimumYPoint)];
        [linePath addLineToPoint:CGPointMake(pointX, self.minimumYPoint + 20)];
        [linePath moveToPoint:CGPointMake(pointX + 1,  self.minimumYPoint)];
        [linePath addArcWithCenter:CGPointMake(pointX,  self.minimumYPoint) radius:2. startAngle:0 endAngle:2 * M_PI clockwise:YES];
    }
    [linePath moveToPoint:CGPointMake(cellSize.width / 2 + CellSpacing, self.minimumYPoint + 20)];
    [linePath addLineToPoint:CGPointMake(cellSize.width / 2 + CellSpacing + (cellSize.width + CellSpacing ) * 3 , self.minimumYPoint + 20)];
    
    [linePath moveToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2, self.minimumYPoint + 20)];
    [linePath addLineToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2, self.minimumYPoint + 40)];
    
    [linePath moveToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2, self.minimumYPoint + 80)];
    [linePath addLineToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2, self.minimumYPoint + 125)];
    
    [linePath moveToPoint:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2 + 1,  self.minimumYPoint + 125)];
    [linePath addArcWithCenter:CGPointMake(((CGFloat)SCREEN_WIDTH) / 2,  self.minimumYPoint + 125) radius:2. startAngle:0 endAngle:2 * M_PI clockwise:YES];
    
    return linePath;
}

- (void)prepareTextLayerScreen
{
    UIFont *font = [DynamicUIService service].language == LanguageTypeArabic ? [UIFont droidKufiBoldFontForSize:SizeFont] : [UIFont latoBoldWithSize:SizeFont];

    NSString *textTitle = dynamicLocalizedString(@"tutorialViewController.cellSpeedAccess.textTitle");
    
    CGSize sizeOfText = [textTitle sizeWithAttributes:@{NSFontAttributeName : font}];
    CGRect frameText = CGRectMake((((CGFloat)SCREEN_WIDTH) - sizeOfText.width - 30.) / 2., self.minimumYPoint + 45., sizeOfText.width + 30., 30.);
    
    [self prepareTextBorder:frameText];

    CGRect textTitleFrame = CGRectMake(frameText.origin.x, frameText.origin.y + (30. - sizeOfText.height) / 2., frameText.size.width, frameText.size.height);
    [self prepareTextLayer:self.textTitleLayer string:textTitle frame:textTitleFrame aligmentMode:kCAAlignmentCenter];
    
    NSString *textDecription = dynamicLocalizedString(@"tutorialViewController.cellSpeedAccess.textDescription");
    CGRect descriptionTextFrame = [textDecription boundingRectWithSize:CGSizeMake(((CGFloat)SCREEN_WIDTH) - 30, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    
    CGFloat textDeltaHeight = [DynamicUIService service].language == LanguageTypeArabic ? 1.14 : 1;
    CGRect textFrameDescription = CGRectMake((((CGFloat)SCREEN_WIDTH) - descriptionTextFrame.size.width) / 2, frameText.origin.y + 80 + (SizeFont - 1.) / 2., descriptionTextFrame.size.width, descriptionTextFrame.size.height * textDeltaHeight);
    
    [self prepareTextLayer:self.textDescriptionLayer string:textDecription frame:textFrameDescription aligmentMode:[DynamicUIService service].language == LanguageTypeArabic ?  kCAAlignmentRight : kCAAlignmentLeft];
}

@end
