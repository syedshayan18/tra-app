//
//  TutorialScreen2CollectionViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 22.01.16.
//  Copyright © 2016 . All rights reserved.
//

#import "TutorialProfileCollectionViewCell.h"

static NSString *const TutorialSpeedAccessCollectionViewCellIdentifier = @"TutorialSpeedAccessCollectionViewCell";

@interface TutorialSpeedAccessCollectionViewCell : TutorialProfileCollectionViewCell

@property (strong, nonatomic) UIImage *speedAccessCollectionViewImage;
@property (assign, nonatomic) CGRect speedAccessCollectionViewRect;

@end
