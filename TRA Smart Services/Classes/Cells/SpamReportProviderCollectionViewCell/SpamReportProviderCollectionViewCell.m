//
//  SpamReportProviderCollectionViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 17.06.16.
//  Copyright © 2016 . All rights reserved.
//

#import "SpamReportProviderCollectionViewCell.h"
#import "BlackImageView.h"

@interface SpamReportProviderCollectionViewCell ()

@property (weak, nonatomic) IBOutlet BlackImageView *iconProviderImageView;
@property (weak, nonatomic) IBOutlet UIView *HexagonView;

@property (strong, nonatomic) CAShapeLayer *hexagonBorderLayer;

@end

@implementation SpamReportProviderCollectionViewCell

- (void)setSelected:(BOOL)selected
{
    UIColor *selectedColor = selected ? [DynamicUIService service].currentApplicationColor : [UIColor lightGrayBorderColor];
    self.hexagonBorderLayer.strokeColor = selectedColor.CGColor;
    
    self.iconProviderImageView.image = selected ? self.iconProvider : [self.iconProvider imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)setIconProvider:(UIImage *)iconProvider
{
    _iconProvider = iconProvider;
    
    [AppHelper addHexagoneOnView:self.HexagonView];
    [self.hexagonBorderLayer removeFromSuperlayer];
    self.hexagonBorderLayer = [AppHelper layerAddHexagonBorderForLayer:self.HexagonView.layer color:[UIColor lightGrayBorderColor] width:3.0f];
    
    self.iconProviderImageView.image = [iconProvider imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.iconProviderImageView.tintColor = [[UIColor menuItemGrayColor] colorWithAlphaComponent:0.8];
}

@end
