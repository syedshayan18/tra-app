//
//  SpamReportProviderCollectionViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 17.06.16.
//  Copyright © 2016 . All rights reserved.
//

static NSString *const SpamReportProviderCollectionViewCellIdentifier = @"SpamReportProviderCollectionViewCell";

@interface SpamReportProviderCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) UIImage *iconProvider;

@end
