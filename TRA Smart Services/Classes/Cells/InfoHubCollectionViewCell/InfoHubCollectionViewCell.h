//
//  InfoHubCollectionViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 18.08.15.
//

#import "BlackImageView.h"

static NSString *const InfoHubCollectionViewCellEuropeIdentifier = @"InfoHubCollectionViewCellEuropeUI";
static NSString *const InfoHubCollectionViewCellArabicIdentifier = @"InfoHubCollectionViewCellArabicUI";

@interface InfoHubCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UILabel *announcementPreviewDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *announcementPreviewDescriptionLabel;
@property (strong, nonatomic) IBOutlet BlackImageView *announcementPreviewIconImageView;

@end
