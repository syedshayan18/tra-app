//
//  InfoHubCollectionViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 18.08.15.
//

#import "InfoHubCollectionViewCell.h"

@implementation InfoHubCollectionViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [AppHelper addHexagoneOnView:self.announcementPreviewIconImageView];
    [AppHelper addHexagonBorderForLayer:self.announcementPreviewIconImageView.layer color:[UIColor lightGrayBorderColor] width:2];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.announcementPreviewIconImageView.image = nil;
    self.announcementPreviewDateLabel.text = @"";
    self.announcementPreviewDescriptionLabel.text = @"";
}

@end
