//
//  InfoHubTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 18.08.15.
//

#import "InfoHubTableViewCell.h"

@implementation InfoHubTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [AppHelper addHexagoneOnView:self.infoHubHexagonView];
    [AppHelper addHexagonBorderForLayer:self.infoHubHexagonView.layer color:[UIColor lightGrayBorderColor] width:2];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.infoHubTransactionImageView.image = nil;
    self.infoHubTransactionDescriptionLabel.text = @"";
    self.infoHubTransactionDateLabel.text = @"";
    self.infoHubTransactionTitleLabel.text = @"";
}

@end
