//
//  MenuCollectionViewCell.m
//  testPentagonCells
//
//  Created by Admin on 30.07.15.
//

#import "MenuCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface MenuCollectionViewCell()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *polygonViewTopSpaceConstraint;

@end

@implementation MenuCollectionViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self updateViewMode:self.cellPresentationMode];
    self.itemLogoImageView.image = nil;
    self.itemLogoImageView.tag = 0;
    self.menuTitleLabel.text = @"";
    self.contentView.alpha = 1.;
    
    for(CAShapeLayer *layer in [self.polygonView.layer.sublayers copy]) {
        if ([layer.name isEqualToString:HexagonBorderLayerName]) {
            [layer removeFromSuperlayer];
        }
    }
}

#pragma mark - Custom Accessors

- (void)setServiceModel:(ServiceModel *)serviceModel
{
    _serviceModel = serviceModel;
    
    self.contentView.alpha = self.serviceModel.serviceEnable ? 1 : 0.2;
    
    if (self.serviceModel.serviceLogo.length) {
        [AppHelper addHexagonBorderForLayer:self.polygonView.layer color:[UIColor menuItemGrayColor] width:1];
        UIImage *serviceLogo = [UIImage imageNamed:self.serviceModel.serviceLogo];
        self.itemLogoImageView.image = serviceLogo;
    }
    self.categoryID = self.serviceModel.serviceID;
    self.menuTitleLabel.text = dynamicLocalizedString(self.serviceModel.serviceName);
}

- (void)setCellPresentationMode:(PresentationMode)cellPresentationMode
{
    _cellPresentationMode = cellPresentationMode;
    [self updateViewMode:cellPresentationMode];
}

#pragma mark - Private

- (void)updateViewMode:(PresentationMode)cellPresentationMode
{
    self.polygonViewTopSpaceConstraint.constant = cellPresentationMode ? self.frame.size.height * 0.2 : 2;
}

@end