//
//  MenuCollectionViewCell.h
//  testPentagonCells
//
//  Created by Admin on 30.07.15.
//

#import "HexagonView.h"
#import "ServiceModel.h"
#import "ModeAlwaysTemplateImageView.h"

typedef NS_ENUM(NSUInteger, PresentationMode) {
    PresentationModeUndefined = 0,
    PresentationModeModeTop = 0,
    PresentationModeModeBottom,
};

static NSString *const MenuCollectionViewCellIdentifier = @"menuCell";

@interface MenuCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *polygonView;
@property (weak, nonatomic) IBOutlet ModeAlwaysTemplateImageView *itemLogoImageView;
@property (weak, nonatomic) IBOutlet UILabel *menuTitleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthMenuTitleLabelConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) ServiceModel *serviceModel;

@property (assign, nonatomic) PresentationMode cellPresentationMode;
@property (assign, nonatomic) NSUInteger categoryID;

@end
