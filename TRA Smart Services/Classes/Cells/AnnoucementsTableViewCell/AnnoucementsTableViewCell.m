//
//  AnnoucementsTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 18.08.15.
//

#import "AnnoucementsTableViewCell.h"

@implementation AnnoucementsTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [AppHelper addHexagoneOnView:self.annoucementsImageView];
    [AppHelper addHexagonBorderForLayer:self.annoucementsImageView.layer color:[UIColor lightGrayBorderColor] width:2];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.annoucementsImageView.image = nil;
    self.annoucementsDateLabel.text = @"";
    self.annoucementsDescriptionLabel.text = @"";
}

@end