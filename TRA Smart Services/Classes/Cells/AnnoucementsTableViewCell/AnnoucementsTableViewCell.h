//
//  AnnoucementsTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 18.08.15.
//

#import "BlackImageView.h"

static NSString *const AnnoucementsTableViewCellEuropeIdentifier = @"annoucementsCellEuropeUIIdentifier";
static NSString *const AnnoucementsTableViewCellArabicIdentifier = @"annoucementsCellArabicUIIdentifier";

@interface AnnoucementsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UILabel *annoucementsDescriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *annoucementsDateLabel;
@property (strong, nonatomic) IBOutlet BlackImageView *annoucementsImageView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *marginAnnouncementContainerConstraint;

@end
