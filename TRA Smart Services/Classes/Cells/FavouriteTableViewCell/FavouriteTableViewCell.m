//
//  FavouriteTableViewCell.m
//  TRA Smart Services
//
//  Created by Admin on 17.08.15.
//

#import "FavouriteTableViewCell.h"

@interface FavouriteTableViewCell()

@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UIButton *serviceInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *ratingButton;
@property (weak, nonatomic) IBOutlet UIView *hexagonBackgroundView;

@end

@implementation FavouriteTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self localizeButtons];
    self.backgroundColor = [UIColor clearColor];
    [self addGestureRecognizer];
    [self.favouriteServiceLogoImageView setContentMode:UIViewContentModeScaleAspectFit];
    self.favourieDescriptionLabel.tag = DeclineTagForFontUpdate;
    [AppHelper addHexagoneOnView:self.hexagonBackgroundView];
    [AppHelper addHexagonBorderForLayer:self.hexagonBackgroundView.layer color:[UIColor menuItemGrayColor] width:3.0f];
    _favouriteServiceLogoImageView.tintColor = [UIColor menuItemGrayColor];
    
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self localizeButtons];
    self.backgroundColor = [UIColor clearColor];
    
    self.favourieDescriptionLabel.text = @"";
    self.favouriteServiceLogoImageView.image = nil;
    self.favouriteServiceLogoImageView.tag = 0;
    self.contentView.alpha = 1.;
}

#pragma mark - Custom Accessors

- (void)setTypeCell:(TypeCellFavorite)typeCell
{
    _typeCell = typeCell;
    
   // shayan commmented
   self.removeButton.hidden = typeCell == TypeCellRating;
    
    UIColor *color = [[DynamicUIService service] currentApplicationColor];

    [self prepareButton:self.serviceInfoButton color:color];
    [self prepareButton:self.ratingButton color:color];
    [self prepareButton:self.removeButton color:[UIColor lightGrayBorderColor]];
}

#pragma mark - IBActions

- (IBAction)serviceInfoButtonTapped:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(favouriteServiceInfoButtonDidPressedInCell:)]) {
        [self.delegate favouriteServiceInfoButtonDidPressedInCell:self];
    }
}

- (IBAction)ratingButtonTapped:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(favouriteServiceInfoRatingDidPressedInCell:)]) {
        [self.delegate favouriteServiceInfoRatingDidPressedInCell:self];
    }
}

- (void)markRemoveButtonSelected:(BOOL)selected
{
    UIColor *selectionColor = selected ? [UIColor redColor] : [[DynamicUIService service] currentApplicationColor];
    UIImage *actRemoveImage = [UIImage imageNamed:@"ic_remove_act"];
    if ([DynamicUIService service].colorScheme == ApplicationColorBlackAndWhite) {
        selectionColor = [[[DynamicUIService service] currentApplicationColor] colorWithAlphaComponent: selected ? 1 : 0.8f];
        actRemoveImage = [UIImage imageNamed:@"ic_remove_dctv"];
    }
    [self.removeButton setImage:selected ? actRemoveImage : [UIImage imageNamed:@"ic_remove_dctv"] forState:UIControlStateNormal];
    
    [self prepareButton:self.removeButton color:selectionColor];
}

#pragma mark - Private

- (void)prepareButton:(UIButton *)button color:(UIColor *)color
{
    button.tintColor = color;
    [button setTitleColor:color forState:UIControlStateNormal];
    
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    button.titleLabel.minimumScaleFactor = 0.7f;
    
    if ([DynamicUIService service].language == LanguageTypeArabic) {
        button.titleEdgeInsets = UIEdgeInsetsMake(0, - button.imageView.frame.size.width, 0, button.imageView.frame.size.width);
        button.imageEdgeInsets = UIEdgeInsetsMake(0, button.titleLabel.frame.size.width + 5, 0, - button.titleLabel.frame.size.width - 5);
    }
}

- (void)localizeButtons
{
    [self.serviceInfoButton setTitle:dynamicLocalizedString(@"favouriteCell.infoButton.title") forState:UIControlStateNormal];
    [self.ratingButton setTitle:dynamicLocalizedString(@"favouriteCell.ratingButton.title") forState:UIControlStateNormal];
    [self.removeButton setTitle:dynamicLocalizedString(@"favouriteCell.deleteButton.title") forState:UIControlStateNormal];
}

#pragma mark - Gestures

- (void)addGestureRecognizer
{
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTapGesture:)];
    longPressGesture.minimumPressDuration = 0.1;
    [self.removeButton addGestureRecognizer:longPressGesture];
}

- (void)longTapGesture:(UILongPressGestureRecognizer *)gesture
{
    if (self.typeCell == TypeCellFavorites && self.delegate && [self.delegate respondsToSelector:@selector(favouriteServiceDeleteButtonDidReceiveGestureRecognizerInCell:gesture:)]) {
        [self.delegate favouriteServiceDeleteButtonDidReceiveGestureRecognizerInCell:self gesture:gesture];
    }
}

@end
