//
//  FavouriteTableViewCell.h
//  TRA Smart Services
//
//  Created by Admin on 17.08.15.
//

#import "ModeAlwaysTemplateImageView.h"

typedef NS_ENUM(NSUInteger, TypeCellFavorite) {
    TypeCellFavorites,
    TypeCellRating,
};

@class FavouriteTableViewCell;

static NSString *const FavouriteEuroCellIdentifier = @"euroFavCell";
static NSString *const FavouriteArabicCellIdentifier = @"arabicFavCell";

@protocol FavouriteTableViewCellDelegate <NSObject>

@optional
- (void)favouriteServiceInfoButtonDidPressedInCell:(FavouriteTableViewCell *)cell;
- (void)favouriteServiceDeleteButtonDidReceiveGestureRecognizerInCell:(FavouriteTableViewCell *)cell gesture:(UILongPressGestureRecognizer *)gesture;
- (void)favouriteServiceInfoRatingDidPressedInCell:(FavouriteTableViewCell *)cell;

@end

@interface FavouriteTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *favourieDescriptionLabel;
@property (weak, nonatomic) IBOutlet ModeAlwaysTemplateImageView *favouriteServiceLogoImageView;

@property (assign, nonatomic) TypeCellFavorite typeCell;

@property (weak, nonatomic) id <FavouriteTableViewCellDelegate> delegate;

- (void)markRemoveButtonSelected:(BOOL)selected;

@end