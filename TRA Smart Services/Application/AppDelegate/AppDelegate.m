//
//  AppDelegate.m
//  TRA Smart Services
//
//  Created by Admin on 13.07.15.
//

#import "AppDelegate.h"
#import "KeychainStorage.h"
#import "SettingViewController.h"
#import "ReverseSecure.h"
#import "HomeViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


static NSString *const ItemString1 = @"compliantViewController.title.compliantAboutServiceProvider";
static NSString *const ItemString2 = @"compliantViewController.title.comtlaintTRA";
static NSString *const ItemString3 = @"homeSearchViewController.homeSearchTextField.placeholder";

@interface AppDelegate ()

@property (assign, nonatomic) BOOL backgroundMode;

@end

@implementation AppDelegate

#pragma mark - LifeCycle

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
//        [self createDynamicShortcutItems];
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Fabric with:@[[Crashlytics class]]];

    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
        [[UINavigationBar appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        [[UITabBar appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        
        if ([launchOptions objectForKey:UIApplicationLaunchOptionsShortcutItemKey]) {
            self.backgroundMode = NO;
        } else {
            self.backgroundMode = YES;
        }
    }
    
    [[NetworkManager sharedManager] startMonitoringNetwork];
    if ([ReverseSecure isJailBroken]) {
        [[[UIAlertView alloc] initWithTitle:@"JailbrakeDevice" message:dynamicLocalizedString(@"message.JailbrakeDevice") delegate:nil cancelButtonTitle:nil otherButtonTitles: nil] show];
    } else {
        [DynamicUIService service];
        [BlackWhiteConverter sharedManager];
        [[NetworkManager sharedManager] startMonitoringNetwork];
        
        [self scaleUIIfNeeded];
        
        [AppHelper prepareTabBarItems];
        [AppHelper prepareTabBarGradient];
    }
    return YES;
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(nonnull UIApplicationShortcutItem *)shortcutItem completionHandler:(nonnull void (^)(BOOL))completionHandler
{
    [self selectApplicationShortcutItem:shortcutItem];
}

#pragma mark - Private

- (void)selectApplicationShortcutItem:(UIApplicationShortcutItem *)shortcutItem
{
    if ([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.%@",[[NSBundle mainBundle] bundleIdentifier],ItemString1]]) {
        [self navigationToHomeController:NavigationTransitionTypeComplaintASP];
    }
    if ([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.%@",[[NSBundle mainBundle] bundleIdentifier],ItemString2]]) {
        [self navigationToHomeController:NavigationTransitionTypeComplainTRA];
    }
    if ([shortcutItem.type isEqualToString:[NSString stringWithFormat:@"%@.%@",[[NSBundle mainBundle] bundleIdentifier],ItemString3]]) {
        [self navigationToHomeController:NavigationTransitionTypeSearch];
    }
}

- (void)navigationToHomeController:(NavigationTransitionType)navigationTransitionType
{
    [self dissmissViewControllerToTabBarController];
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    tabBarController.selectedIndex = ([DynamicUIService service].language == LanguageTypeArabic) ? 4 : 0;
    UINavigationController *navController = tabBarController.selectedViewController;

    if (self.backgroundMode) {
        [navController popToRootViewControllerAnimated:NO];
        if ([navController.visibleViewController isKindOfClass:[HomeViewController class]]) {
            [((HomeViewController *)navController.visibleViewController)  navigationTransitionViewController:navigationTransitionType];
        }
    } else {
        for (UIViewController *viewController in navController.viewControllers) {
            if ([viewController isKindOfClass:[HomeViewController class]]) {
                ((HomeViewController *)viewController).navigationTransitionType = navigationTransitionType;
            }
        }
    }
    self.backgroundMode = YES;
}

- (void)dissmissViewControllerToTabBarController
{
    if ([self.window.rootViewController presentedViewController] && ![[self.window.rootViewController presentedViewController] isKindOfClass:[UITabBarController class]]) {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
        [self dissmissViewControllerToTabBarController];
    }
}

- (void)createDynamicShortcutItems
{
    UIApplicationShortcutItem *item1 = [[UIApplicationShortcutItem alloc] initWithType:[NSString stringWithFormat:@"%@.%@",[[NSBundle mainBundle] bundleIdentifier],ItemString1] localizedTitle:dynamicLocalizedString(ItemString1) localizedSubtitle:nil icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"ic_edit"] userInfo:nil];
    UIApplicationShortcutItem *item2 = [[UIApplicationShortcutItem alloc] initWithType:[NSString stringWithFormat:@"%@.%@",[[NSBundle mainBundle] bundleIdentifier],ItemString2] localizedTitle:dynamicLocalizedString(ItemString2) localizedSubtitle:nil icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"ic_chat"] userInfo:nil];
    UIApplicationShortcutItem *item3 = [[UIApplicationShortcutItem alloc] initWithType:[NSString stringWithFormat:@"%@.%@",[[NSBundle mainBundle] bundleIdentifier],ItemString3] localizedTitle:dynamicLocalizedString(ItemString3) localizedSubtitle:nil icon:[UIApplicationShortcutIcon iconWithTemplateImageName:@"ic_search"] userInfo:nil];
   
    [UIApplication sharedApplication].shortcutItems = @[item1, item2, item3];
}

- (void)scaleUIIfNeeded
{
    if (IS_IPAD) {
        [[UITableViewCell appearance] setBackgroundColor:[UIColor clearColor]];
        CGFloat xscale = [UIScreen mainScreen].bounds.size.width / INITIAL_IPAD_WIDTH;
        CGFloat yscale = [UIScreen mainScreen].bounds.size.height / INITIAL_IPAD_HEIGTH;
        
        self.window = [[UIWindow alloc] initWithFrame:CGRectMake(0, 0, INITIAL_IPAD_WIDTH, INITIAL_IPAD_HEIGTH)];
        self.window.transform = CGAffineTransformMakeScale([[NSNumber numberWithFloat:xscale] floatValue], [[NSNumber numberWithFloat:yscale] floatValue]);
        self.window.center = CGPointMake(CGRectGetMidX([UIScreen mainScreen].bounds), CGRectGetMidY([UIScreen mainScreen].bounds));
        [self.window makeKeyAndVisible];
        
        UITabBarController *rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateInitialViewController];
        
        UITabBarController *tabBar = [[UITabBarController alloc] init];
        self.window.rootViewController = tabBar;
        
        [AppHelper presentViewController:rootViewController onController:tabBar];
    }
}

@end
