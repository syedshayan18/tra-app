//
//  TRADynamicService+CoreDataProperties.h
//  TRA Smart Services
//
//  Created by Admin on 23.06.16.
//  Copyright © 2016 . All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TRADynamicService.h"

NS_ASSUME_NONNULL_BEGIN

@interface TRADynamicService (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *serviceIconImage;
@property (nullable, nonatomic, retain) NSString *serviceID;
@property (nullable, nonatomic, retain) NSString *serviceInquiriesCategory;
@property (nullable, nonatomic, retain) NSString *serviceNameAR;
@property (nullable, nonatomic, retain) NSString *serviceNameEN;
@property (nullable, nonatomic, retain) NSNumber *serviceNeedAuth;
@property (nullable, nonatomic, retain) NSSet<TRADynamicService *> *relationshipChild;
@property (nullable, nonatomic, retain) TRADynamicService *relationshipFahter;

@end

@interface TRADynamicService (CoreDataGeneratedAccessors)

- (void)addRelationshipChildObject:(TRADynamicService *)value;
- (void)removeRelationshipChildObject:(TRADynamicService *)value;
- (void)addRelationshipChild:(NSSet<TRADynamicService *> *)values;
- (void)removeRelationshipChild:(NSSet<TRADynamicService *> *)values;

@end

NS_ASSUME_NONNULL_END
