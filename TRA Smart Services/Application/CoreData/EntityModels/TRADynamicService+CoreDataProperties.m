//
//  TRADynamicService+CoreDataProperties.m
//  TRA Smart Services
//
//  Created by Admin on 23.06.16.
//  Copyright © 2016 . All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TRADynamicService+CoreDataProperties.h"

@implementation TRADynamicService (CoreDataProperties)

@dynamic serviceIconImage;
@dynamic serviceID;
@dynamic serviceInquiriesCategory;
@dynamic serviceNameAR;
@dynamic serviceNameEN;
@dynamic serviceNeedAuth;
@dynamic relationshipChild;
@dynamic relationshipFahter;

@end
