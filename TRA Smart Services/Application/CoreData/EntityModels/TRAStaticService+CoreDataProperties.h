//
//  TRAStaticService+CoreDataProperties.h
//  TRA Smart Services
//
//  Created by Admin on 28.04.16.
//  Copyright © 2016 . All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TRAStaticService.h"

NS_ASSUME_NONNULL_BEGIN

@interface TRAStaticService (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *serviceDescription;
@property (nullable, nonatomic, retain) NSString *serviceIconName;
@property (nullable, nonatomic, retain) NSNumber *serviceInternalID;
@property (nullable, nonatomic, retain) NSNumber *serviceType;
@property (nullable, nonatomic, retain) NSString *serviceName;
@property (nullable, nonatomic, retain) NSNumber *serviceEnable;

@end

NS_ASSUME_NONNULL_END
