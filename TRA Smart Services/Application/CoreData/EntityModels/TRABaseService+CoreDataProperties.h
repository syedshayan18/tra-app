//
//  TRABaseService+CoreDataProperties.h
//  TRA Smart Services
//
//  Created by Admin on 28.04.16.
//  Copyright © 2016 . All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TRABaseService.h"

NS_ASSUME_NONNULL_BEGIN

@interface TRABaseService (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *serviceOrder;
@property (nullable, nonatomic, retain) NSNumber *serviceFavoriteOrder;
@property (nullable, nonatomic, retain) NSString *serviceIcon;
@property (nullable, nonatomic, retain) NSNumber *serviceIsFavorite;

@end

NS_ASSUME_NONNULL_END
