//
//  TRABaseService+CoreDataProperties.m
//  TRA Smart Services
//
//  Created by Admin on 28.04.16.
//  Copyright © 2016 . All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TRABaseService+CoreDataProperties.h"

@implementation TRABaseService (CoreDataProperties)

@dynamic serviceOrder;
@dynamic serviceFavoriteOrder;
@dynamic serviceIcon;
@dynamic serviceIsFavorite;

@end
