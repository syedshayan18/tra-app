//
//  TRAStaticService+CoreDataProperties.m
//  TRA Smart Services
//
//  Created by Admin on 28.04.16.
//  Copyright © 2016 . All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TRAStaticService+CoreDataProperties.h"

@implementation TRAStaticService (CoreDataProperties)

@dynamic serviceDescription;
@dynamic serviceIconName;
@dynamic serviceInternalID;
@dynamic serviceType;
@dynamic serviceName;
@dynamic serviceEnable;

@end
