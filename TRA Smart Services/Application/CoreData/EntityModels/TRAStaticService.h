//
//  TRAStaticService.h
//  TRA Smart Services
//
//  Created by Admin on 28.04.16.
//  Copyright © 2016 . All rights reserved.
//

#import "TRABaseService.h"

NS_ASSUME_NONNULL_BEGIN

@interface TRAStaticService : TRABaseService

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "TRAStaticService+CoreDataProperties.h"
